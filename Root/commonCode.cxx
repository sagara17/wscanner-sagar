#include "commonCode.h"
#include "InterpolatedGraph.h"
#include "log.h"

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>


using namespace std;
 
std::vector<TString> tokenizeStr(TString str, TString key)
{
    TObjArray *Varparts = str.Tokenize(key);
    vector<TString> varNameVec;
    if(Varparts->GetEntriesFast()) {
        TIter iString(Varparts);
        TObjString* os=0;
        while ((os=(TObjString*)iString())) {
            varNameVec.push_back(os->GetString());
        }
    }
    delete Varparts;
    return varNameVec;
}

std::map<TString, TString> readMetadata(TTree* tree)
{
    map<TString,TString> metaData;

    TObjArray* listLf = tree->GetListOfLeaves();

    std::map<std::string, TString*> m_stringMetaDataVar;

    for (Int_t i = 0; i < listLf->GetEntries(); i++)
    {
        TLeaf* lf = (TLeaf*) listLf->At(i);

        TString typeName   = lf->GetTypeName();
        TString titleName  = lf->GetTitle();

        if(typeName.Contains("TString"))
        {
            m_stringMetaDataVar[lf->GetName()] = 0;
        }
    }   

    for (auto it = m_stringMetaDataVar.begin(); it != m_stringMetaDataVar.end(); ++it)
    {
        TString varName = it->first;
        tree->SetBranchAddress(varName, &it->second);
    }

    tree->GetEntry(0);

    // Copy over the map
    for (auto it = m_stringMetaDataVar.begin(); it != m_stringMetaDataVar.end(); ++it)
    {
        TString varName = it->first;
        metaData[it->first] = *it->second;
    }

    TString dataType = "";
    if(metaData["doData"].Contains("true")) dataType = "data";
    else dataType = "asimov";

    std::map<TString, TString> infoMap;

    infoMap["scanType"] = metaData["scanType"];
    infoMap["dataType"] = dataType;
    infoMap["label"] = "";
    dataType.ToLower();
    
    if(dataType.Contains("asimov")) infoMap["label"] += "Exp";
    else                            infoMap["label"] += "Obs";
    
    metaData["scanType"].ToLower();
    if(metaData["scanType"].Contains("statonly"))   infoMap["label"] += "-noSys";
    else if(metaData["scanType"].Contains("modtheory"))   infoMap["label"] += "-noTh";
    else                                            infoMap["label"] += "-Sys";

    // cout<<metaData["scanType"]<<" "<<infoMap["label"]<<endl;

    infoMap["dimension"]        = metaData["dimension"];
    infoMap["workspaceType"]    = metaData["workspaceType"];
    infoMap["labelName"]        = metaData["labelName"];

    if(infoMap["dimension"].Contains("1D") or infoMap["dimension"].EqualTo("1"))
    {
        std::vector<TString> poiName = tokenizeStr(metaData["poiScan"], ":");
        infoMap["poi"] = poiName[0];
    }
    else if(infoMap["dimension"].Contains("2D") or infoMap["dimension"].EqualTo("2"))
    {
        std::vector<TString> poiName = tokenizeStr(metaData["poiScan"], ",");
        if(poiName.size() != 2)
        {
            cout<<"number of POI specified don't match what is required for 2D"<<endl;
            cout<<metaData["poiScan"]<<endl;
        }

        auto poi1Info = tokenizeStr(poiName[0], ":");
        auto poi2Info = tokenizeStr(poiName[1], ":");
        infoMap["poi_1"] = poi1Info[0];
        infoMap["poi_2"] = poi2Info[0];

        double deltaPOI1 = (poi1Info.at(3).Atof() - poi1Info.at(2).Atof())/poi1Info.at(1).Atof();
        double deltaPOI2 = (poi2Info.at(3).Atof() - poi2Info.at(2).Atof())/poi2Info.at(1).Atof();


        infoMap["poi_1_delta"] = std::to_string(deltaPOI1);
        infoMap["poi_2_delta"] = std::to_string(deltaPOI2);
    }
    else if(infoMap["dimension"].EqualTo("correlationHist"))
    {
        // Do nothing for now
    }
    else
    {
        cout<<"Input dimension not recognized"<<endl;
        cout<<"Input option: "<<infoMap["dimension"]<<endl;
        throw "Input dimension not recognized";
    }


    infoMap["fileName"]        = metaData.at("fileName");
    infoMap["workspaceName"]   = metaData.at("workspaceName");
    infoMap["modelConfigName"] = metaData.at("modelConfigName");


    return infoMap;
}


TString getSymbol(std::map<TString, TString> metaData, TString poiName)
{
    TString fancyName = "#sigma#upoint#it{B}";
    // cout<<metaData["workspaceType"]<<" "<<poiName<<endl;
    if(metaData["workspaceType"].EqualTo("btagging")) fancyName = "#varepsilon";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eL")) fancyName = "#varepsilon_{L}}";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eR")) fancyName = "#varepsilon_{R}";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("kZZ")) fancyName = "#kappa_{ZZ}";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eEl")) fancyName = "(#varepsilon_{Ze_{L}} = #varepsilon_{Ze_{R}})";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eMu")) fancyName = "(#varepsilon_{Z#mu_{L}} = #varepsilon_{Z#mu_{R}})";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eElR")) fancyName = "(#varepsilon_{Ze_{L}} = -#varepsilon_{Ze_{R}})";
    if(metaData["workspaceType"].EqualTo("H4lPOs") && poiName.EqualTo("eMuR")) fancyName = "(#varepsilon_{Z#mu_{L}} = -#varepsilon_{Z#mu_{R}})";

    if(metaData["workspaceType"].EqualTo("H4lKappa") && poiName.EqualTo("CV")) fancyName = "#kappa_{V}";
    if(metaData["workspaceType"].EqualTo("H4lKappa") && poiName.EqualTo("CF")) fancyName = "#kappa_{F}";

    if(metaData["workspaceType"].EqualTo("differential") && poiName.Contains("MuqqZZ")) fancyName = "#mu";
    if(metaData["workspaceType"].EqualTo("differential") && poiName.Contains("MuqqZZ")) fancyName = "#mu";


    if(metaData["workspaceType"].EqualTo("EFT")) fancyName = "";
    if(poiName.Contains("r_")) fancyName = "N";

    return fancyName;
}

TString getXAxisSymbol(std::map<TString, TString> metaData, TString poiName)
{
    TString xLabel = poiName;

    if(metaData["workspaceType"].EqualTo("mu")) xLabel = "#mu_{" + getFancyXname(poiName) + "}";
    else if(metaData["workspaceType"].EqualTo("mu")) xLabel = "#mu_{" + getFancyXname(poiName) + "}";
    else if(metaData["workspaceType"].EqualTo("XS") && poiName.Contains("mu"))
    { 
        if(getFancyXname(poiName).Length() > 0) xLabel = "("+ getSymbol(metaData, poiName) +"/#sigma#upoint#it{B}_{SM})_{" + getFancyXname(poiName) + "}";
        else xLabel =  getSymbol(metaData, poiName) +"/(#sigma#upoint#it{B})_{SM}";
    }

    else if(metaData["workspaceType"].EqualTo("XS") && poiName.Contains("r")) xLabel = "("+ getSymbol(metaData, poiName) +"/N_{SM})_{" + getFancyXname(poiName) + "}";
    


    else if(metaData["workspaceType"].EqualTo("differential")) xLabel = getSymbol(metaData, poiName) +"_{" + getFancyXname(poiName) + "}";
    else if(poiName.EqualTo("cHG"))   xLabel  = "c_{HG}";
    else if(poiName.EqualTo("cHW"))   xLabel  = "c_{HW}";
    else if(poiName.EqualTo("cHB"))   xLabel  = "c_{HB}";
    else if(poiName.EqualTo("cHWB"))   xLabel  = "c_{HWB}";
    else if(poiName.EqualTo("cuH"))   xLabel  = "c_{uH}";
    else if(poiName.EqualTo("cHGtil"))   xLabel  = "c_{H#tilde{G}}";
    else if(poiName.EqualTo("cHWtil"))   xLabel  = "c_{H#tilde{W}}";
    else if(poiName.EqualTo("cHBtil"))   xLabel  = "c_{H#tilde{B}}";
    else if(poiName.EqualTo("cHWtilB"))   xLabel  = "c_{H#tilde{W}B}";
    else if(poiName.EqualTo("cuHtil"))   xLabel  = "c_{#tilde{u}H}";
    else if(metaData["workspaceType"].EqualTo("H4lPOs"))
    {
        xLabel = getSymbol(metaData, poiName);
    }




    cout<<"poiName: "<<poiName<<" xLabel: "<<xLabel<<" workspaceType: "<<metaData["workspaceType"]<<endl;




    return xLabel;
}

TString getFancyXname(TString name)
{


    TString fancyName = "";
    if(name.EqualTo("mu_gg2H")      || name.EqualTo("XS_gg2H"))  fancyName = "gg2H";
    else if(name.EqualTo("mu_ggF")   || name.EqualTo("XS_ggF"))   fancyName = "ggF";
    else if(name.EqualTo("mu_VBF")   || name.EqualTo("XS_VBF"))   fancyName = "VBF";
    else if(name.EqualTo("mu_VH")    || name.EqualTo("XS_VH"))    fancyName = "#it{VH}";
    else if(name.EqualTo("mu_ttH")   || name.EqualTo("XS_ttH"))   fancyName = "#it{ttH}";

    else if(name.EqualTo("mu_qqH2qq"))   fancyName = "qq2Hqq";
    else if(name.EqualTo("mu_qq2Hll"))   fancyName = "#it{VH}-Lep";



    else if(name.EqualTo("mu_gg2H_0J")                   || name.EqualTo("XS_gg2H_0J"))                  fancyName = "gg2H-0#it{j}";
    else if(name.EqualTo("mu_gg2H_0J_PTH_0_10"))          fancyName = "gg2H-0#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_gg2H_0J_PTH_GT10"))          fancyName = "gg2H-0#it{j}-#it{p}_{T}^{#it{H}}-High";
    else if(name.EqualTo("mu_gg2H_1J_PTH_0_60")          || name.EqualTo("XS_gg2H_1J_PTH_0_60"))         fancyName = "gg2H-1#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_gg2H_1J_PTH_60_120")        || name.EqualTo("XS_gg2H_1J_PTH_60_120"))       fancyName = "gg2H-1#it{j}-#it{p}_{T}^{#it{H}}-Med";
    else if(name.EqualTo("mu_gg2H_1J_PTH_GT120")         || name.EqualTo("XS_gg2H_1J_PTH_GT120") || name.EqualTo("mu_gg2H_1J_PTH_120_200") )        fancyName = "gg2H-1#it{j}-#it{p}_{T}^{#it{H}}-High";
    else if(name.EqualTo("mu_gg2H_GE2J")                 || name.EqualTo("XS_gg2H_GE2J"))                fancyName = "gg2H-2#it{j}";
    else if(name.EqualTo("mu_gg2H_PTH_GT200")                 || name.EqualTo("XS_gg2H_PTH_GT200"))                fancyName = "gg2H-#it{p}_{T}^{#it{H}}-High";


    else if(name.EqualTo("mu_ggF_0J") )                  fancyName = "ggF-0#it{j}";
    else if(name.EqualTo("mu_ggF_0J_PTH_0_10"))          fancyName = "ggF-0#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_ggF_0J_PTH_GT10"))          fancyName = "ggF-0#it{j}-#it{p}_{T}^{#it{H}}-High";
    else if(name.EqualTo("mu_ggF_1J_PTH_0_60"))          fancyName = "ggF-1#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_ggF_1J_PTH_60_120"))        fancyName = "ggF-1#it{j}-#it{p}_{T}^{#it{H}}-Med";
    else if(name.EqualTo("mu_ggF_1J_PTH_GT120") ||name.EqualTo("mu_ggF_1J_PTH_120_200") )        fancyName = "ggF-1#it{j}-#it{p}_{T}^{#it{H}}-High";
    else if(name.EqualTo("mu_ggF_GE2J") )                fancyName = "ggF-2#it{j}";
    else if(name.EqualTo("mu_ggF_PTH_GT200"))            fancyName = "ggF-#it{p}_{T}^{#it{H}}-High";

    else if(name.EqualTo("mu_ttH_all")                   || name.EqualTo("XS_ttH_all"))                  fancyName = "#it{ttH}";
    else if(name.EqualTo("mu_VBF_qq2qq_PTJET1_GT200")    || name.EqualTo("XS_VBF_qq2qq_PTJET1_GT200"))   fancyName = "VBF-#it{p}_{T}^{#it{j}}-High";
    else if(name.EqualTo("mu_VBF_qq2qq_PTJET1_LE200")    || name.EqualTo("XS_VBF_qq2qq_PTJET1_LE200"))   fancyName = "VBF-#it{p}_{T}^{#it{j}}-Low";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_LE200_nJLE1"))   fancyName = "VBF-01#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_LE200_nJGE2"))   fancyName = "VBF-2#it{j}-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_GT200"))   fancyName = "VBF-#it{p}_{T}^{#it{H}}-High";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_LE200"))   fancyName = "VBF-#it{p}_{T}^{#it{H}}-Low";
    else if(name.EqualTo("mu_VH_Had")                    || name.EqualTo("XS_VH_Had"))                   fancyName = "#it{VH}-Had";
    else if(name.EqualTo("mu_VH_Lep")                    || name.EqualTo("XS_VH_Lep"))                   fancyName = "#it{VH}-Lep";

    else if(name.EqualTo("mu_qqH2qq_VHHad"))                fancyName = "qq2Hqq-#it{VH}";
    else if(name.EqualTo("mu_qq2Hqq_mJJ_GT350_PTH_GT200"))  fancyName = "qq2Hqq-BSM";
    else if(name.EqualTo("mu_qqH2qq_rest"))                 fancyName = "qq2Hqq-VBF";


    else if(name.EqualTo("r_ttV"))            fancyName = "#it{ttV}";
    else if(name.EqualTo("r_ZZ"))            fancyName = "#it{ZZ}";
    
    else if(name.EqualTo("r_ZZ_0jet"))            fancyName = "#it{ZZ-0j}";
    else if(name.EqualTo("r_ZZ_1jet"))            fancyName = "#it{ZZ-1j}";
    else if(name.EqualTo("r_ZZ_2jet"))            fancyName = "#it{ZZ-2j}";


    else if(name.EqualTo("mu_0jet_b1_recoCat"))                   fancyName = "0j BDT Bin 0-4";
    else if(name.EqualTo("mu_0jet_b2_recoCat"))                   fancyName = "0j BDT Bin 5-9";
    else if(name.EqualTo("mu_0jet_b3_recoCat"))                   fancyName = "0j BDT Bin 10-14";
    else if(name.EqualTo("mu_1jet_Pt4l_0_60_b1_recoCat"))         fancyName = "1j p_{T}^{4l} Low BDT Bin 0-4";
    else if(name.EqualTo("mu_1jet_Pt4l_0_60_b2_recoCat"))         fancyName = "1j p_{T}^{4l} Low BDT Bin 5-9";
    else if(name.EqualTo("mu_1jet_Pt4l_60_120_b1_recoCat"))       fancyName = "1j p_{T}^{4l} Med BDT Bin 0-4";
    else if(name.EqualTo("mu_1jet_Pt4l_60_120_b2_recoCat"))       fancyName = "1j p_{T}^{4l} Med BDT Bin 5-9";
    else if(name.EqualTo("mu_1jet_Pt4l_GE120_b1_recoCat"))        fancyName = "1j p_{T}^{4l} High";
    else if(name.EqualTo("mu_2jet_VBF_ptJ1GE200_b1_recoCat"))     fancyName = "VBF-enriched p_{T}^{j} High";
    else if(name.EqualTo("mu_2jet_VBF_ptJ1LE200_b1_recoCat"))     fancyName = "VBF-enriched p_{T}^{j} Low BDT Bin 0-4";
    else if(name.EqualTo("mu_2jet_VBF_ptJ1LE200_b2_recoCat"))     fancyName = "VBF-enriched p_{T}^{j} Low BDT Bin 5-9";
    else if(name.EqualTo("mu_2jet_VH_Had_b1_recoCat"))            fancyName = "VH-Had enriched BDT Bin 0-4";
    else if(name.EqualTo("mu_2jet_VH_Had_b2_recoCat"))            fancyName = "VH-Had enriched BDT Bin 5-9";
    else if(name.EqualTo("mu_ttH_b1_recoCat"))                    fancyName = "ttH-enriched";
    else if(name.EqualTo("mu_VHLep_b1_recoCat"))                  fancyName = "VH-Lep enriched";
    else if(name.EqualTo("mu_0jet_c1_recoCat"))                   fancyName = "0-jet";
    else if(name.EqualTo("mu_1jet_Pt4l_0_60_c1_recoCat"))         fancyName = "1-jet, p_{T}^{4l} < 60 GeV";
    else if(name.EqualTo("mu_1jet_Pt4l_60_120_c1_recoCat"))       fancyName = "1-jet, 60 #leq p_{T}^{4l} < 120 GeV";
    else if(name.EqualTo("mu_2jet_VBF_ptJ1LE200_c1_recoCat"))     fancyName = "VBF-enriched, p_{T}^{j1} < 200 GeV";
    else if(name.EqualTo("mu_2jet_VH_Had_c1_recoCat"))            fancyName = "VH-enriched";
    else if(name.EqualTo("kAgg"))            return "#kappa_{Agg}";
    else if(name.EqualTo("kAvv"))            return "#kappa_{Avv}";
    else if(name.EqualTo("mu_onShell"))      return "#mu_{On-Shell}";
    else if(name.EqualTo("gamma"))           return "#Gamma/#Gamma_{SM}";
    else if(name.EqualTo("epsilon"))         return "";
    else if(name.EqualTo("efficiency"))      return "";
    else if(name.EqualTo("eL"))      return "";
    else if(name.EqualTo("eR"))      return "";
    else if(name.EqualTo("kZZ"))      return "";
    else if(name.EqualTo("eEl"))      return "";
    else if(name.EqualTo("eMu"))      return "";
    else if(name.EqualTo("mu"))      return "";
    else if(name.EqualTo("CV"))      return "";
    else if(name.EqualTo("CF"))      return "";
    else if(name.EqualTo("cHG"))            return "c_{HG}";
    else if(name.EqualTo("cHGtil"))         return "c_{H#tilde{G}}";
    else if(name.EqualTo("cuH"))            return "c_{uH}";
    else if(name.EqualTo("cuHtil"))         return "c_{#tilde{u}H}";
    else if(name.EqualTo("cHW"))            return "c_{HW}";
    else if(name.EqualTo("cHWtil"))         return "c_{H#tilde{W}}";
    else if(name.EqualTo("cHB"))            return "c_{HB}";
    else if(name.EqualTo("cHBtil"))         return "c_{H#tilde{B}}";
    else if(name.EqualTo("cHWB"))           return "c_{HWB}";
    else if(name.EqualTo("cHWtilB"))        return "c_{H#tilde{W}B}";

    else if(name.EqualTo("r_ggF_yy"))     fancyName = "ggF_{yy}";
    else if(name.EqualTo("r_VBF_yy"))     fancyName = "VBF_{yy}";
    else if(name.EqualTo("r_VH_yy"))      fancyName = "#it{VH}_{yy}";
    else if(name.EqualTo("r_ttH_yy"))     fancyName = "#it{ttH}_{yy}";
    else if(name.EqualTo("r_ggF_ZZ"))     fancyName = "ggF_{WW}";
    else if(name.EqualTo("r_VBF_ZZ"))     fancyName = "VBF_{ZZ}";
    else if(name.EqualTo("r_VH_ZZ"))      fancyName = "#it{VH}_{ZZ}";
    else if(name.EqualTo("r_ggF_WW"))     fancyName = "ggF_{WW}";
    else if(name.EqualTo("r_VBF_WW"))     fancyName = "VBF_{WW}";
    else if(name.EqualTo("r_ggF_tautau")) fancyName = "ggF_{#tau#tau}";
    else if(name.EqualTo("r_VBF_tautau")) fancyName = "VBF_{#tau#tau}";
    else if(name.EqualTo("r_ttH_tautau")) fancyName = "#it{ttH}_{#tau#tau}";
    else if(name.EqualTo("r_VBF_bb"))     fancyName = "VBF_{bb}";
    else if(name.EqualTo("r_VH_bb"))      fancyName = "#it{VH}_{bb}";
    else if(name.EqualTo("r_ttH_bb"))     fancyName = "#it{ttH}_{bb}";
    else if(name.EqualTo("r_ttH_VV"))     fancyName = "#it{ttH}_{VV}";

    else if(name.EqualTo("r_ggF"))   fancyName = "ggF";
    else if(name.EqualTo("r_VBF"))   fancyName = "VBF";
    else if(name.EqualTo("r_VH") )   fancyName = "#it{VH}";
    else if(name.EqualTo("r_WH") )   fancyName = "#it{WH}";
    else if(name.EqualTo("r_ZH") )   fancyName = "#it{ZH}";
    else if(name.EqualTo("r_ttH"))   fancyName = "#it{ttH}";


    else if(name.Contains("sigma") && name.Contains("bin") && name.Contains("incl"))
    {
        fancyName = name;
        fancyName = fancyName.ReplaceAll("sigma", "");
        fancyName = fancyName.ReplaceAll("bin", "");
        fancyName = fancyName.ReplaceAll("incl", "");
        fancyName = fancyName.ReplaceAll("_", "");

        fancyName = "bin " + fancyName;
    }

    else if(name.Contains("MuqqZZ"))
    {
        fancyName = name;
        fancyName = fancyName.ReplaceAll("MuqqZZ", "");
        fancyName = fancyName.ReplaceAll("_", "");

        fancyName = "ZZ bin " + fancyName;
    }

    else  fancyName = name;
    

    return fancyName;
}


TString getFancyLegname(TString name)
{
    return name.ReplaceAll("noSys", "Stat. only").ReplaceAll("noTh", "No Theory");
}

vector<TString> getVariableList(TTree *tree)
{
    TObjArray* listLf = tree->GetListOfLeaves();

    vector<TString> varList;
    for (Int_t i = 0; i < listLf->GetEntries(); i++)
    {
        TLeaf* lf = (TLeaf*) listLf->At(i);

        TString typeName   = lf->GetTypeName();
        std::string brName = lf->GetTitle();
        TString titleName  = lf->GetTitle();

        if(titleName.Contains("alpha"))
        {
            varList.push_back(titleName);
        }
    }
    return varList;
}

std::vector<std::map<TString, TString>> getLabels(std::vector<TString> fileNameList)
{

    std::vector<std::map<TString, TString>> myLabels;
    for(auto file:fileNameList)
    {
        TFile *fileName = TFile::Open(file, "r");
        TTree *tree = (TTree*)fileName->Get("metaData");
        // // read the metaData

        std::map<TString, TString> infoMap = readMetadata(tree);
        myLabels.push_back(infoMap);

        fileName->Close();
    }
    return myLabels;
}
map<double,double> read1DVar(TString varName, TTree* tree)
{
    map<double,double> info;

    double var;
    double poi;
    double isUnconditional;
    double fitType;

    tree->SetBranchAddress("poi", &poi);         
    tree->SetBranchAddress(varName, &var);
    tree->SetBranchAddress("isUnconditional",   &isUnconditional);
    tree->SetBranchAddress("fitType",   &fitType);

    double min = 0;
    if(varName.EqualTo("nll"))
    {
        min = tree->GetMinimum("nll");
    }

    double sumVarConditional = 0;
    int    countVarConditional = 0;
    double averVarConditional = 0;

    // Calculate the average of the unconditional NLL
    if(varName.EqualTo("nll"))
    {
        bool hasUnparmUnconditional = (tree->GetEntries("fitType == 2")>0 ? 1:0);

        for(int i = 0 ; i < tree->GetEntries(); i++)
        {
            tree->GetEntry(i);
            if(hasUnparmUnconditional)
            {
                if(fitType > 1.5 && fitType < 2.5)
                {
                    countVarConditional++;
                    sumVarConditional  += var;
                }
            }
            else
            {
                if(fitType > 0.5 && fitType < 1.5)
                {
                    countVarConditional++;
                    sumVarConditional  += var;
                }
            }
        }
        averVarConditional = sumVarConditional/countVarConditional;
    }
    min = averVarConditional;


    for(int i = 0; i < tree->GetEntries(); i++)
    {
        tree->GetEntry(i);
        //cout<<"reading 1D var poi: "<<poi<<" "<<varName<<" "<<var<<" "<<(var-min)<<endl;


        if(!(fitType > -0.5 && fitType < 1.5)) continue;
        
        if(std::isnan(var)) continue;
        if(varName.EqualTo("nll") && (var-min) > 10) continue;
        info[poi] = var - min;
    }

    return info;
}

TGraph* get1DGraph(map<double,double>& varInfo)
{
    vector<double> xVal;
    vector<double> yVal;

    for (auto it = varInfo.begin(); it != varInfo.end(); ++it)
    {
        xVal.push_back(it->first);       
        yVal.push_back(it->second);       
    }

    TGraph *g = new TGraph(xVal.size(),&xVal[0],&yVal[0]);

    return g;
}

void fixOffset(map<double,double>& varInfo)
{
    double minVal = 99999;

    for (auto it = varInfo.begin(); it != varInfo.end(); ++it)
    {
        if(it->second < minVal) minVal = it->second;
    }

    // do the actual subtraction
    for (auto it = varInfo.begin(); it != varInfo.end(); ++it)
    {
        it->second -=  minVal;
    }
}

std::vector<double> printFitInfo(map<double,double>& NLLVar, double& outMinValX, double& outLeftErr, double& outRightErr, bool doBetterError)
{
    double minValX = 99999;
    double minValY = std::numeric_limits<double>::infinity();

    for (auto it = NLLVar.begin(); it != NLLVar.end(); ++it)
    {
        if(it->second < minValY)
        {
            minValX = it->first;
            minValY = it->second;
        }
    }

    outMinValX = minValX;


    // Error band
    vector<double> xValLeft;
    vector<double> yValLeft;
    
    vector<double> xValRight;
    vector<double> yValRight;

    for (auto it = NLLVar.begin(); it != NLLVar.end(); ++it)
    {
        if(it->first < minValX)
        {
            xValLeft.push_back(it->first);
            yValLeft.push_back(it->second);
        }
        else if(it->first > minValX)
        {
            xValRight.push_back(it->first);
            yValRight.push_back(it->second);
        }
    }

    TGraph* gLeft  = NULL; 
    if(xValLeft.size() > 0) gLeft = new TGraph(xValLeft.size(),&yValLeft[0],&xValLeft[0]);
    TGraph* gRight = NULL;
    if(xValRight.size() > 0) gRight = new TGraph(xValRight.size(),&yValRight[0],&xValRight[0]);

    double leftErr = 0;
    if(gLeft) leftErr = gLeft->Eval(1) - minValX;
    double rightErr = 0;
    if(gLeft)  rightErr = gRight->Eval(1) - minValX;
    
    // if(opts["doUpperLim"].Contains("true"))
    // {
    //     leftErr = gLeft->Eval(4) - minValX;
    //     rightErr = gRight->Eval(4) - minValX;
    // }

    outLeftErr = leftErr;
    outRightErr = rightErr;
    
    // cout<<"outMinValX: "<<outMinValX<<endl;
    // cout<<"outLeftErr: "<<outLeftErr<<endl;
    // cout<<"outRightErr: "<<outRightErr<<endl;

    cout<<"outMinValX: "<<outMinValX<<endl;
    cout<<"68CL: "<<(gLeft->Eval(1) - minValX)<<" , "<<(gRight->Eval(1) - minValX)<<endl;
    cout<<"95CL: "<<(gLeft->Eval(4) - minValX)<<" , "<<(gRight->Eval(4) - minValX)<<endl;

    double Low68CL = (gLeft->Eval(1) - minValX);
    double High68CL = (gRight->Eval(1) - minValX);
    double Low95CL = (gLeft->Eval(4) - minValX);
    double High95CL = (gRight->Eval(4) - minValX);

    vector<double> NLLMinValues = {outMinValX,Low68CL,High68CL,Low95CL,High95CL};

    if(doBetterError)
    {
        // Get better graph
        TGraph* g = get1DGraph(NLLVar);
        // outMinValX = findmin (g, outMinValX + 2*outLeftErr, outMinValX + 2*outRightErr, 3, 0);

        // cout<<"outMinValX: "<<outMinValX<<" outLeftErr: "<<outLeftErr<<" outRightErr: "<<outRightErr<<endl;

        outLeftErr = findy (g, 1, outMinValX + 1.1*outLeftErr, outMinValX, true, 3) - outMinValX;
        outRightErr = findy (g, 1, outMinValX, outMinValX + 1.1*outRightErr, true, 3)  - outMinValX;

        delete g;
    }
    // cout<<"outMinValX: "<<outMinValX<<endl;
    // cout<<"outLeftErr: "<<outLeftErr<<endl;
    // cout<<"outRightErr: "<<outRightErr<<endl;
    return NLLMinValues;
}


PlottingInfo parsePlottingInfo(std::vector<TString> fileNameList)
{

    LOG(logINFO)<<"Parsing plotting style info from the fileName list - if plots look weird grep for this line and modify accordingly";
    bool hasStat = false;
    bool hasSys = false;


    std::set<TString> uniqueFolder;
    for (const auto& fileName: fileNameList)
    {
        if(fileName.Contains("statOnly")) hasStat = true;
        if(fileName.Contains("allSys")) hasSys = true;

        vector<TString> filePartList = tokenizeStr(fileName,"/");

        if(filePartList.size() > 4) uniqueFolder.insert(filePartList.at(filePartList.size() - 4));
    }

    PlottingInfo plotInfo;
    plotInfo.hasStatSys = hasStat &&  hasSys;
    plotInfo.hasMultipleFolder = uniqueFolder.size() > 1;

    return plotInfo;
}
bool doesExist(const std::string& name) 
{
    ifstream f(name.c_str());
    return f.good();
}

vector<TString> splitString(TString string, TString delimiter)
{
    TObjArray *Varparts = string.Tokenize(delimiter);
    vector<TString> varNameVec;
    if(Varparts->GetEntriesFast()) {
        TIter iString(Varparts);
        TObjString* os=0;
        while ((os=(TObjString*)iString())) {
            varNameVec.push_back(os->GetString());
        }
    }
    delete Varparts;
    return varNameVec;
}

TString joinList(vector<TString> arr, TString delimiter)
{
    if (arr.empty()) return "";

    string str;
    for (auto i : arr)
        str += i + delimiter;
    str = str.substr(0, str.size() - delimiter.Length());
    return TString(str);
}



void process_mem_usage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
}


double plotToy(TH1D* toyHist, double nomNLLVal, TString subDirectoryName, TString histName)
{
    TCanvas *c = new TCanvas("", "", 0, 0, 600, 600);
    toyHist->GetXaxis()->SetTitle("#Delta nll");

    toyHist->Scale(1/toyHist->Integral());
    double perExc = toyHist->Integral(toyHist->FindFixBin(nomNLLVal),toyHist->FindFixBin(1000));
    if(std::isnan(perExc)) perExc = -999;
    cout << "Perc: " << perExc << " nomNLLVal: " << nomNLLVal << " integral" << toyHist->Integral()<< endl;

    TLine *line = new TLine(nomNLLVal, 0, nomNLLVal, toyHist->GetMaximum());
    line->SetLineColor(kRed);
    line->SetLineWidth(2);
    toyHist->SetLineWidth(2);
    toyHist->SetMaximum(toyHist->GetMaximum()*1.3);
    toyHist->Draw("hist");
    line->Draw();

    double leftDist = 0.19;

    // ATLASLabel(leftDist, 0.875, "Internal", 1);
    TString channelStr = "H #rightarrow ZZ* #rightarrow 4l";
    TLatex* tMain = new TLatex (leftDist, 0.81, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.76, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    double rightDist = 0.6;
    double topDist = 0.86;
    TString numToys = Form("nToys: %.2f", toyHist->GetEntries());
    TLatex* numToysInfo = new TLatex (rightDist, topDist, numToys);
    numToysInfo->SetNDC();
    numToysInfo->SetTextSize(0.0325);
    numToysInfo->SetTextFont(42);
    numToysInfo->Draw();

    TString exclStr = Form("Perc. Excluded: %.3f", perExc);
    TLatex* exclInfo = new TLatex (rightDist, topDist - 0.04, exclStr);
    exclInfo->SetNDC();
    exclInfo->SetTextSize(0.0325);
    exclInfo->SetTextFont(42);
    exclInfo->Draw();

    TString nllStr = Form("nll Val: %.3f", nomNLLVal);
    TLatex* nllInfo = new TLatex (rightDist, topDist - 0.08, nllStr);
    nllInfo->SetNDC();
    nllInfo->SetTextSize(0.0325);
    nllInfo->SetTextFont(42);
    nllInfo->Draw();


    c->SaveAs(subDirectoryName + histName + ".pdf");
    delete c;

    return perExc;
}

