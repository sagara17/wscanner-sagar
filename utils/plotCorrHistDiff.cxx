#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <stdlib.h>
#include <limits>
#include <math.h> 
#include <algorithm>
// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"
#include "TGraphAsymmErrors.h"

using namespace std;

// Global vars
map<TString,TString> opts;
map<TString,TString> metaData;

enum MatrixType { covMat, corrMat };    

// // Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
TString getBaseName(TString pathName);

void setPrettyStuff();
void plotMatrix(TH2D* inHist, std::map<TString, TString> labelsList, MatrixType matrixType, TString fileName);
vector<TString> getPOIList(std::map<TString, TString> labelsList);
vector<TString> getClearPOIList(std::map<TString, TString> labelsList);

RooWorkspace* m_ws;
void fillWS(TString fileName);
int main(int argc, char** argv)
{
    setPrettyStuff();

    if (!cmdline(argc,argv,opts)) return 0;
    m_ws = NULL;

    // Split the results
    vector<TString> fileNameList = tokenizeStr(opts["fileName"],",");
    std::vector<std::map<TString, TString>> labelsList = getLabels(fileNameList);

    if(fileNameList.size() > 1)
    {
        cout<<"More than 1 file provided to plotCorrHist... there is no support for this"<<endl;
        exit(1);
    }

    for(size_t i = 0; i < fileNameList.size(); i++)
    {
        cout<<"---------------------------"<<endl;
        cout<<fileNameList[i]<<endl;
        for(const auto& cMap: labelsList[i]) cout<<cMap.first<<" "<<cMap.second<<endl;
        if(i==0) fillWS(fileNameList[i]);
    }

    // Extract the correlation and covariance matrix
    TFile* inFile = TFile::Open(fileNameList[0]);
    TH2D* covMatrix     = (TH2D*) inFile->Get("covarianceHist");
    TH2D* corrMatric    = (TH2D*) inFile->Get("correlation_matrix");

    plotMatrix(covMatrix, labelsList[0], covMat, fileNameList[0]);
    plotMatrix(corrMatric, labelsList[0], corrMat, fileNameList[0]);
}

void plotMatrix(TH2D* inHist, std::map<TString, TString> labelsList, MatrixType matrixType, TString fileName)
{
    // First get a subset of the inHist with just the POI
    vector<TString> poiList = getPOIList(labelsList);
    vector<TString> poiClearList = getClearPOIList(labelsList);
    vector<int> xIndex, yIndex;


    // Get the index for the size
    for(const auto& poiName: poiList)
    {
        bool isFound = false;
        for(int i = 0; i < inHist->GetXaxis()->GetNbins();i++)
        {
            TString binLabel = inHist->GetXaxis()->GetBinLabel(i+1);    
            if(binLabel.EqualTo(poiName)) {xIndex.push_back(i+1); isFound = true;}

        }
        for(int i = 0; i < inHist->GetYaxis()->GetNbins();i++)
        {
            TString binLabel = inHist->GetYaxis()->GetBinLabel(i+1);    
            if(binLabel.EqualTo(poiName)) {yIndex.push_back(i+1); isFound = true;}
            // cout << binLabel << endl;
        }
        if(!isFound) {cout<<"Can't find: "<<poiName<<endl;}
    }
    // for(int i = 0; i < xIndex.size(); i++)
    // {
    //     cout << xIndex.at(i) << "\t" << poiList.at(i) << endl;
    // }

    if(xIndex.size() != poiList.size() || yIndex.size() != poiList.size())
    {
        cout<<"POI index size doesn't match the label size"<<endl;
        exit(1);
    }

    // Set the bin content
    TH2D* cHist = new TH2D("cHist", "cHist", xIndex.size(), 0, xIndex.size(), yIndex.size(), 0, yIndex.size());
    for(size_t i = 0 ; i < poiList.size(); i++)
    {
        for(size_t j = 0 ; j < poiList.size()-i; j++)
        {
            if(poiClearList.size() > 0 && (i != yIndex.size() - j - 1))
            {
                if(find(poiClearList.begin(), poiClearList.end(), poiList[yIndex.size() - j - 1]) != poiClearList.end())
                {
                    continue;
                }
            }

            cHist->SetBinContent(i+1, j+1, inHist->GetBinContent(xIndex[i], yIndex[yIndex.size() - j - 1]));
        }        
    }

    // Set the bin labels
   // for(size_t i = 0 ; i < poiList.size(); i++)
   // {
   //     cHist->GetXaxis()->SetBinLabel(i+1, getFancyXname(inHist->GetXaxis()->GetBinLabel(xIndex[i])));
   //     cHist->GetYaxis()->SetBinLabel(i+1, getFancyXname(inHist->GetYaxis()->GetBinLabel(yIndex[yIndex.size() - i - 1])));     
   // }

    // Define the color
    const int Number = 5;
    Double_t Green[Number]  = {0.31, 0.67, 1.00, 0.69, 0.29};
    Double_t Blue[Number]   = {0.60, 0.85, 1.00, 0.72, 0.14};
    Double_t Red[Number]    = {0.13, 0.45, 1.00, 0.96, 0.74};
    Double_t Length[Number] = {0.00, 0.10, 0.50, 0.90, 1.00};//16

    if(matrixType == covMat)
    {
        double max = cHist->GetMaximum();
        double min = cHist->GetMinimum();
        Length[1] =  fabs(min)/(max-min)*0.5;
        Length[2] = fabs(min)/(max-min);
        Length[3] = fabs(min)/(max-min)*1.50;
    }
    else
    {
        cHist->SetMaximum(1);
        cHist->SetMinimum(-1);
    }

    Int_t nb = 1000;//20;
    TColor::CreateGradientColorTable(Number, Length, Red, Green, Blue, nb);

    // Format the hist
    cHist->LabelsOption("v");
    cHist->SetMarkerSize(0.90);
    cHist->GetXaxis()->SetLabelSize(0.026);
    cHist->GetYaxis()->SetLabelSize(0.026);
    cHist->GetXaxis()->SetTitleSize(0.03);
    cHist->GetYaxis()->SetTitleSize(0.03);
    cHist->GetXaxis()->SetTitleOffset(1.7);
    cHist->GetYaxis()->SetTitleOffset(1.7);
    cHist->GetXaxis()->SetTitle("#it{p}_{T}^{4#it{l}} [GeV]");
    cHist->GetYaxis()->SetTitle("#it{p}_{T}^{4#it{l}} [GeV]");
    //cHist->GetXaxis()->SetTitle("#it{N}_{jets}");
    //cHist->GetYaxis()->SetTitle("#it{N}_{jets}");
    gStyle->SetPaintTextFormat("2.2f");  
    cHist->SetContour(1000);

    std::vector<TString> njet_label;
    njet_label.push_back("#it{N}_{jets}=0");
    njet_label.push_back("#it{N}_{jets}=1");
    njet_label.push_back("#it{N}_{jets}=2");
    njet_label.push_back("#it{N}_{jets}>=3");
    njet_label.push_back("ZZ (#it{N}_{jets}=0)");
    njet_label.push_back("ZZ (#it{N}_{jets}=1)");
    njet_label.push_back("ZZ (#it{N}_{jets}>=2)");
    
    std::vector<TString> pt_label;
    pt_label.push_back("0-10");
    pt_label.push_back("10-20");
    pt_label.push_back("20-30");
    pt_label.push_back("30-45");
    pt_label.push_back("45-60");
    pt_label.push_back("60-80");
    pt_label.push_back("80-120");
    pt_label.push_back("120-200");
    pt_label.push_back("200-350");
    pt_label.push_back("350-1000");
    pt_label.push_back("ZZ (0-10)");
    pt_label.push_back("ZZ (10-20)");
    pt_label.push_back("ZZ (20-30)");
    pt_label.push_back("ZZ (30-60)");
    pt_label.push_back("ZZ (60-1000)");

    for (int i=1;i<cHist->GetNbinsX()+1;i++){
	   cHist->GetXaxis()->SetBinLabel(i,njet_label.at(i-1));
        cHist->GetYaxis()->SetBinLabel(i,njet_label.at(cHist->GetNbinsX() - i)); 
    }


    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 600, 600);
    c1->SetRightMargin(0.15);
    // if(poiList.size() > 8)
    // {
    //     c1->SetLeftMargin(0.225);
    //     c1->SetBottomMargin(0.225);
    // }
    // else
    // {

    // }
    cHist->GetZaxis()->SetLabelSize(0.03);
    cHist->Draw("colz");
    cHist->Draw("sameText");

    double leftDist = 0.44;
    double offset = 0.0;
    TString dataType = labelsList["dataType"];
    dataType.ToLower();
    if(dataType.Contains("asimov"))
    {
        ATLASLabel(leftDist, 0.875, "Simulation", 1);
        offset = 0.02;
        TLatex p; 
        p.SetNDC();
        p.SetTextFont(42);
        p.SetTextColor(1);
        p.DrawLatex(leftDist,0.835,"Internal");
    }
    else ATLASLabel(leftDist, 0.875, "Internal", 1);

    TLatex* tMain = new TLatex (leftDist, 0.815 - offset, "H #rightarrow ZZ* #rightarrow 4l");
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.78 - offset, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    // Draw the hatches
    for(const auto& poiSkip:poiClearList)
    {
        vector<Float_t> binCenter  ;
        vector<Float_t> binContent ;
        vector<Float_t> upErr      ;
        vector<Float_t> downErr    ;
        vector<Float_t> xupErr     ;
        vector<Float_t> xdownErr   ;

        int yIndex = -1;
        for(size_t i = 0; i < poiList.size(); i++)
        {
            if(poiList[i].EqualTo(poiSkip)) yIndex = poiList.size() - i -1;
        }

        for(size_t i = 0; i < poiList.size(); i++)
        {
            if(i >= poiList.size()  - 1 - yIndex) continue;
            binCenter.push_back(i+0.5);
            binContent.push_back(yIndex + 0.5);
            upErr.push_back(0.5);
            downErr.push_back(0.5);
            xupErr.push_back(0.5);
            xdownErr.push_back(0.5);
        }     

        TGraphAsymmErrors* hatch = new TGraphAsymmErrors(binCenter.size(),&binCenter[0],&binContent[0],&xupErr[0],&xdownErr[0],&upErr[0],&downErr[0]);
        hatch->SetMarkerStyle(0);
        hatch->SetLineColor(0);
        hatch->SetFillColor(1);
        hatch->SetFillStyle(3454);
        hatch->Draw("2same");   
    }


    TString saveName = opts["plotDir"] + "/" + getBaseName(fileName) + "/";
    system("mkdir -vp " + saveName);

    if(!dataType.Contains("asimov")) saveName += "Obs_";
    else                             saveName += "Exp_";

    if(labelsList["scanType"].Contains("statonly")) saveName += "noSys_";
    else                                            saveName += "Sys_";

    if(matrixType == corrMat) saveName += "corrMatrix.eps"; 
    if(matrixType == covMat) saveName += "covMatrix.eps";
    c1->SaveAs(saveName);

    delete cHist;
    delete c1;
}

vector<TString> getPOIList(std::map<TString, TString> labelsList)
{
    vector<TString> POIList;

    // Some default orders
    vector<TString> SXTS_incl   = {"mu", "r_ZZ_0jet", "r_ZZ_1jet", "r_ZZ_2jet", "r_ttV"};
    vector<TString> SXTS_S0     = {"mu_gg2H", "mu_VBF", "mu_VH", "mu_ttH", "r_ZZ_0jet", "r_ZZ_1jet", "r_ZZ_2jet", "r_ttV"};
    vector<TString> SXTS_RS1p1  = {"mu_gg2H_0J_PTH_0_10", "mu_gg2H_0J_PTH_GT10", "mu_gg2H_1J_PTH_0_60", "mu_gg2H_1J_PTH_60_120", "mu_gg2H_1J_PTH_120_200", "mu_gg2H_GE2J", "mu_gg2H_PTH_GT200", "mu_VBF_qq2qq_PTH_LE200", "mu_VBF_qq2qq_PTH_GT200", "mu_VH_Had", "mu_VH_Lep", "mu_ttH", "r_ZZ_0jet", "r_ZZ_1jet", "r_ZZ_2jet", "r_ttV"};
    vector<TString> diffList    = {"sigma_bin0_incl", "sigma_bin1_incl", "sigma_bin2_incl", "sigma_bin3_incl","sigma_bin4_incl", "sigma_bin5_incl", "sigma_bin6_incl", "sigma_bin7_incl", "sigma_bin8_incl", "sigma_bin9_incl", "sigma_bin10_incl", "sigma_bin11_incl", "sigma_bin12_incl", "sigma_bin13_incl", "sigma", "sigma_2l2l", "sigma_4l", "sigma_4mu", "sigma_4e", "sigma_2mu2e", "sigma_2e2mu", "sigma_sum", "MuqqZZ0", "MuqqZZ1", "MuqqZZ2", "MuqqZZ3", "MuqqZZ4", "MuqqZZ5"};


    if(opts["impactPOIs"].Contains("mu_gg2H_0J_PTH_0_10"))
    {
        // Yay, we are looking at Stage 1p1
        POIList = SXTS_RS1p1;
    }
    else if(opts["impactPOIs"].Contains("mu_gg2H"))
    {
        // Yay, we are looking at Stage 0
        POIList = SXTS_S0;        
    }
    else if(opts["impactPOIs"].Contains("mu") && opts["impactPOIs"].Contains("r_ttV"))
    {
        // Yay, we are looking at incl mu
        POIList = SXTS_incl;        
    }
    else if(opts["impactPOIs"].Contains("sigma_bin"))
    {
        // Yay, we are looking at Diff
        POIList = diffList;        
    }
    else if(opts["impactPOIs"].Contains("sigma"))
    {
        // Yay, we are looking at Diff
        POIList = diffList;        
    }

    // Now clean up
    if(POIList.size() > 0)
    {
        // clearip the list
        auto tempList = POIList;
        POIList.clear();
        for(const auto& poiName: tempList)
        {
            if(poiName.EqualTo("sigma"))
            {
                if(opts["impactPOIs"].EqualTo(poiName)) {POIList.push_back(poiName);}
                continue;
            }

            if(opts["impactPOIs"].Contains(poiName) && m_ws->var(poiName)) POIList.push_back(poiName);
            if(m_ws && m_ws->var(poiName) && poiName.Contains("MuqqZZ")) POIList.push_back(poiName);
        }
    }
    else
    {
        // Try to decripher the list from here
        POIList = tokenizeStr(opts["impactPOIs"],",");
    }


//POIList = {





// "efficiency",
// "nbkgUntagged",
// "nbkgTagged",
// "nsig",
// "alpha_mTop",
// "alpha_NP_bkgTaggedShape",
// "alpha_NP_bkgpreTaggedShape",
// "alpha_NP_LARGERJET_Strong_JET_Comb_Baseline_All",
// "alpha_NP_LARGERJET_Strong_JET_Comb_Modelling_All",
// "alpha_NP_LARGERJET_Strong_JET_Comb_TotalStat_All",
// "alpha_NP_LARGERJET_Strong_JET_Comb_Tracking_All",
// "alpha_NP_LARGERJET_Strong_JET_MassRes_Top",

// nsig
// nbkgUntagged
// nbkgTagged
// efficiency
// alpha_mTop
// alpha_NP_bkgpreTaggedShape
// alpha_NP_bkgTaggedShape
// alpha_NP_LARGERJET_Strong_JET_MassRes_Top
// alpha_NP_LARGERJET_Strong_JET_Comb_Tracking_All
// alpha_NP_LARGERJET_Strong_JET_Comb_TotalStat_All
// alpha_NP_LARGERJET_Strong_JET_Comb_Modelling_All
// alpha_NP_LARGERJET_Strong_JET_Comb_Baseline_All
//};
    return POIList;
}

vector<TString> getClearPOIList(std::map<TString, TString> labelsList)
{
    vector<TString> POIList;
    // TString dataType = labelsList["dataType"];
    // dataType.ToLower();
    // if(!dataType.Contains("asimov")) POIList = {"mu_ttH"};
    return POIList;
}

bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["fileName"]    = "asimovScan.root";
    opts["baseName"]    = "Empty";
    opts["plotDir"]     = "Plots";
    opts["impactPOIs"]  = "";

    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") {
            cout<<"--fileName       : path to ttree containing the information"<<endl;
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    //if(opts["fileName"].Contains("ttH")) opts["doUpperLim"]  = "true";
    return true;
}

TString getBaseName(TString pathName)
{
    TString baseName = "";
    if(opts["baseName"].EqualTo("Empty"))
    { 
        std::vector<TString> folderName = tokenizeStr(pathName, "/");
        return folderName[1];
    }
    else
    {
        return opts["baseName"];
    }
    // cout << "size"<< folderName.size() << endl;
    // for(const auto& part: folderName) cout << "Prinint out paths "<< part << endl;

    
}


void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1756; // color index
    vector<TColor*> TColors;
    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;
    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;          
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++; 
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    //new TColor(ci, 0.6350,    0.0780,    0.1840); //62
    //ci++;
    //new TColor(ci, 142.0/255 , 0.0/255 , 62.0/255);
    //ci++;
    //new TColor(ci, 96.0/255 , 78.0/255 , 0.0/255);
    //ci++;
    //new TColor(ci, 92.0/255 , 174.0/255 , 0.0/255);
    //ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}


void fillWS(TString fileName)
{

    if(doesExist(fileName.Data()))
    {
        auto label = getLabels(vector<TString>{fileName}).at(0);
        
        if(doesExist(label["fileName"].Data()))
        {
            TFile* wsFile = TFile::Open(label["fileName"]);
            m_ws = (RooWorkspace*) wsFile->Get(label["workspaceName"]);

            cout<<"Found the ws"<<endl;
            return;
        }
    }
    
}




