#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"
#include "InterpolatedGraph.h"
#include "TGraphAsymmErrors.h"
#include <TSystem.h>

using namespace std;

// Global vars
map<TString,TString> opts;
map<TString,TString> metaData;

struct paramFitInfo 
{
    bool isLinear;
    double slope;
    double intercept;
    double slopeErr;
    double interceptErr;

    double constVal;
    double constErr;
};

struct pointInfo 
{
    double cenVal;
    double upErr;
    double dwnErr;

};

struct humanRequest
{

    TString paramName;
    TString paramType;
};


// // Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
TString getBaseName(TString pathName);
void setPrettyStuff();
TString getDecayChannel(TString inputChannel);
TString getBDTbin(TString bdtBin);
TString getRenamedPOI(TString orgPOIName);
// double getMin(std::map<double,double> NLL);
pointInfo getPoint(TString fileName);
paramFitInfo plotParam(TString bin, TString chan, TString param, std::vector<pointInfo> yValInfo);
void fixBottomLabel(TH1F* hist, double VerticalCanvasSplit, TH1F* frame);
TString getFancy(TString name);



std::vector<TString> massVecS   = {"123", "124","125", "125p5", "126", "127"};
std::vector<double>  massVecD   = {123, 124, 125, 125.5, 126, 127};

std::map<TString, std::map<TString, paramFitInfo>> textMap;
std::map<TString, std::vector<TString>> humanOverideMap;


// ../build/getDCBParams_Iter1 --baseName root-files/Jl30_Analytic_DCBWS_XXX_YYY_AllP_Nominal_allMP

int main(int argc, char** argv)
{
    // humanOverideMap["4e_BDT_bin4"] = std::vector<TString> {"etaLo_const"};
    // humanOverideMap["4e_BDT_bin1"] = std::vector<TString> {"etaLo_const"};
    // humanOverideMap["2mu2e_BDT_bin1"] = std::vector<TString> {"etaLo_const"};

    setPrettyStuff();

    if (!cmdline(argc,argv,opts)) return 0;

    if(opts["baseName"].Length() == 0)
    {
    	cout << "Folder baseName needs to be provided" << endl;
    	exit(1);
    }

    std::vector<TString> bdtList 	= tokenizeStr(opts["bdtBin"], ",");
    std::vector<TString> chanList 	= tokenizeStr(opts["channel"], ",");
    std::vector<TString> paramList 	= tokenizeStr(opts["poiList"], ",");



    for(const auto bdt: bdtList)
    {
    	for(const auto chan:chanList)
    	{
            std::map<TString, paramFitInfo> paramMap;

    		for(const auto param: paramList)
    		{
                std::vector<pointInfo> minosResults;

    			for(const auto massS: massVecS)
    			{

	    			TString shortBin = bdt;
	    			shortBin.ReplaceAll("BDT_", "").ReplaceAll("in","");
                    TString baseNameCopy = opts["baseName"];

	    			TString rootFileName = baseNameCopy.ReplaceAll("XXX", chan).ReplaceAll("YYY", shortBin) + "/allSys/asimov/sJob/DCB_" + param + "_ggHVBFSig" + massS + ".root";
	    			
                    // cout << bdt << "\t" << chan << "\t" << param << "\t" << massS << endl;
                    cout << "FileName:: " << rootFileName << endl;

	    			minosResults.push_back(getPoint(rootFileName));

	    		}

                paramMap[param] = plotParam(bdt, chan, param, minosResults);

    		}

            textMap[chan + "_" + bdt] = paramMap;
    	}
    }


    // if a file output path is provided write the fit results to it
    if(opts["iter2File"].Length() > 0)
    {
        ofstream myfile;
        myfile.open (opts["iter2File"]);
        
        for(const auto category:textMap)
        {
            myfile << "[" + category.first << "] \n";
           
            for(const auto paramInfo:category.second)
            {
                myfile << "name:" << paramInfo.first << ",";

                if(paramInfo.second.isLinear) 
                {
                    myfile << "type:simFitLinear,";
                    myfile << "slope:" << paramInfo.second.slope << ",";
                    myfile << "intercept:" << paramInfo.second.intercept << "\n";
                    // myfile << "slopeErr:" << paramInfo.second.slopeErr << ",";
                    // myfile << "interceptErr:" << paramInfo.second.interceptErr << "\n";

                }
                else
                {
                    myfile << "type:simFitConstant,";
                    myfile << "val:" << paramInfo.second.constVal << ",";
                    myfile << "lwrLim:" << paramInfo.second.constVal - 2*(paramInfo.second.constErr)/5 << ",";
                    myfile << "uprLim:" << paramInfo.second.constVal + 2*(paramInfo.second.constErr)/5 << "\n";

                }



            } 

            myfile << "\n\n\n";


        }


        // [2mu2e_BDT_bin3]
        // name:mean,type:simFitLinear,slope:0.965594414847,intercept:-0.329581803173
        // name:sigma,type:simFitConstant,val:2.1908563704,lwrLim:1,uprLim:3.2
        // name:alphaLo,type:simFitConstant,val:1.25050418515,lwrLim:0.2,uprLim:3
        // name:alphaHi,type:simFitConstant,val:1.6336116548,lwrLim:0.2,uprLim:5
        // name:etaLo,type:simFitConstant,val:3.89934121666,lwrLim:0.1,uprLim:7.0
        // name:etaHi,type:simFitConstant,val:12.7595296072,lwrLim:1,uprLim:200



        myfile.close();

    }



}

paramFitInfo plotParam(TString bin, TString chan, TString param, std::vector<pointInfo> yValInfo)
{


    paramFitInfo fitInfo;

    std::vector<double> yVal;
    std::vector<double> yUp;
    std::vector<double> yDwn;
    std::vector<double> zero;

    for(const auto point:yValInfo)
    {
        yVal.push_back(point.cenVal);
        yUp.push_back(fabs(point.upErr));
        yDwn.push_back(fabs(point.dwnErr));
        zero.push_back(0);

    }

    int maxElementIndex = std::max_element(yVal.begin(),yVal.end()) - yVal.begin();
    double maxElement = *std::max_element(yVal.begin(), yVal.end());

    int minElementIndex = std::min_element(yVal.begin(),yVal.end()) - yVal.begin();
    double minElement = *std::min_element(yVal.begin(), yVal.end());


    TH1F* frame = new TH1F("frame", "frame", massVecD.size() + 2 , massVecD[0] - 1, massVecD[massVecD.size() -1] + 1);
    frame->SetMaximum( (maxElement + yUp.at(maxElementIndex))*1.1 );
    frame->SetMinimum( (minElement - yDwn.at(minElementIndex))*0.9 );


    // cout << "umm.." << maxElement + yUp.at(maxElementIndex) << "\t" << minElement - yDwn.at(minElementIndex) << endl;

    frame->GetXaxis()->SetTitle(getFancy(param) );
    frame->GetYaxis()->SetTitle("Fit Value");

    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 600, 600);
    TString cname = "ratio";
    double VerticalCanvasSplit = 0.25;
    TPad* p1 = new TPad("p1_"+cname,cname,0.0,VerticalCanvasSplit,1.0,1.0);
    TPad* p2 = new TPad("p2_"+cname,cname,0.0,0.0,1.0,VerticalCanvasSplit);

    p1->SetBottomMargin(0.01);
    p1->SetBottomMargin(0.00);
    p1->SetRightMargin(0.1);

    p2->SetTopMargin(0.05);
    p2->SetTopMargin(0.00);
    p2->SetBottomMargin(0.4);
    p2->SetRightMargin(0.1);

    c1->cd();
    p1->Draw();
    p2->Draw();

    p1->cd();
    frame->Draw();

    TString baseNameCopy = opts["baseName"];
    TString binCopy = bin;


    TString plotDir = opts["plotDir"] + "/" + baseNameCopy.ReplaceAll("root-files/", "").ReplaceAll("_XXX_YYY_", "_")  + "/" + chan + "/" + bin + "/";
    cout << "PLOTDIR " << plotDir << endl;


    system("mkdir -vp " + plotDir);

    TString fileName = param + ".eps";

    TGraphAsymmErrors* g = new TGraphAsymmErrors(yVal.size(), &massVecD[0], &yVal[0], &zero[0], &zero[0], &yUp[0], &yDwn[0]);
    g->SetMarkerStyle(20);

    TString formula = "[0]+[1]*x";
    if(param.EqualTo("mean"))
    {
        cout << "is mean" << endl;
        formula = "[0] + 125. + [1]*(x - 125.)";
    }

    TLegend *elLeg =  new TLegend (0.52, 0.725, 0.85, 0.875);
    elLeg->SetFillColor(0);
    elLeg->SetBorderSize(0);
    elLeg->SetTextFont(42);
    elLeg->SetTextSize(0.0275);

    TF1* linearFit = new TF1("linearFit", formula, frame->GetXaxis()->GetXmin (), frame->GetXaxis()->GetXmax () );


    linearFit->SetLineColor(kAzure + 7);
    linearFit->SetLineWidth(2);
    g->Fit("linearFit");
    g->Draw("P same");
    linearFit->Draw("L same");
    elLeg->AddEntry(g, "Individual Fits", "PE");
    elLeg->AddEntry(linearFit, "F(mass) Linear", "L");

    float par0_err = fabs(linearFit->GetParError(0));
    float par1_err = fabs(linearFit->GetParError(1));

    // if its not the mean or the error on the slope is less than 100%
    // write the parameter in its linear form
    // if(param.EqualTo("mean") ||  fabs(linearFit->GetParError(1)/linearFit->GetParameter(1)) < 1.0 ) fitInfo.isLinear = true;
    if(param.EqualTo("mean")  ) fitInfo.isLinear = true;

    else fitInfo.isLinear = false;

    // chance for human overide to make true or false
    if(humanOverideMap.find(chan + "_" + bin) != humanOverideMap.end())
    {
        cout << "Override found for " << chan + "_" + bin << endl;

        for(auto overrideParamInfo: humanOverideMap[chan + "_" + bin])
        {
            if(overrideParamInfo.Contains(param))
            {
                if(overrideParamInfo.Contains("linear") ) fitInfo.isLinear = true;
                else if(overrideParamInfo.Contains("const") ) fitInfo.isLinear = false;
                else
                {
                    cout << "Override does not contain linear or const, fix" << endl;
                    exit(1);
                }

            }
        }

    }
    if(fitInfo.isLinear)
    {   
        fitInfo.slope = linearFit->GetParameter(1);
        fitInfo.intercept = linearFit->GetParameter(0);
    }
    else
    {
        TF1* constFit = new TF1("constFit", "[0]", frame->GetXaxis()->GetXmin (), frame->GetXaxis()->GetXmax () );
        constFit->SetLineColor(kGreen + 3);
        constFit->SetLineWidth(2);
        g->Fit("constFit");
        constFit->Draw("L same");

        fitInfo.constVal = constFit->GetParameter(0);
        fitInfo.constErr = constFit->GetParError(0);

        elLeg->AddEntry(constFit, "F(mass) Constant", "L");

    }


    TF1* linearFitUp = new TF1("linearFitUp", formula, frame->GetXaxis()->GetXmin (), frame->GetXaxis()->GetXmax () );
    linearFitUp->SetParameter(0, linearFit->GetParameter(0) + par0_err );
    linearFitUp->SetParameter(1, linearFit->GetParameter(1) + par1_err);
    linearFitUp->SetLineColor(14);
    linearFitUp->SetLineStyle(kDashed);
    linearFitUp->SetLineWidth(2);
    linearFitUp->Draw("L same");

    TF1* linearFitDown = new TF1("linearFitDown", formula, frame->GetXaxis()->GetXmin (), frame->GetXaxis()->GetXmax () );
    linearFitDown->SetParameter(0, linearFit->GetParameter(0) - par0_err );
    linearFitDown->SetParameter(1, linearFit->GetParameter(1) - par1_err);
    linearFitDown->SetLineColor(14);
    linearFitDown->SetLineStyle(kDashed);
    linearFitDown->SetLineWidth(2);
    linearFitDown->Draw("L same");

    // cout << linearFit->GetParameter(0)  << "\t" <<par0_err << "\t" << linearFit->GetParameter(0) + par0_err << endl;

    // cout << linearFit->GetParameter(1)  << "\t" <<par1_err << "\t" << linearFit->GetParameter(1) + par1_err << endl;


    // elLeg->AddEntry(linearFitUp, "F(mass) Uncertainty", "L");

 

    double leftDist = 0.19;

    ATLASLabel(leftDist, 0.875, "Internal", 1);

    TString channelStr = "H #rightarrow ZZ* #rightarrow " + chan;

    TLatex* tMain = new TLatex (leftDist, 0.81, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw(); 

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.76, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    TString otherInfoString = "Discriminant Bin " + binCopy.ReplaceAll("BDT_bin", "") ;
    TLatex* otherInfo = new TLatex (leftDist, 0.72, otherInfoString);
    otherInfo->SetNDC();
    otherInfo->SetTextSize(0.0325);
    otherInfo->SetTextFont(42);
    otherInfo->Draw();

    elLeg->Draw();

    p2->cd();

    // // Ratio!
    TH1F* frameSys = new TH1F("frameSys", "frameSys",  massVecD.size() + 2 , massVecD[0] - 1, massVecD[massVecD.size() -1] + 1);
    frameSys->GetXaxis()->SetTitle(getFancy(param) );

    int lineColour = kGray + 3;
    TLine* line = new TLine(frame->GetXaxis()->GetXmin (), 1, frame->GetXaxis()->GetXmax(), 1);
    line->SetLineColor(lineColour);
    line->SetLineStyle(2);
    line->SetLineWidth(2);
    line->Draw();

    std::vector<double> resY;



    for(int i=0; i < g->GetN() ; ++i) 
    {
        double x,y;
        double yFit;

        g->GetPoint(i, x, y);
        yFit = linearFit->Eval(x);

        resY.push_back(yFit - y);
    }

    TGraphAsymmErrors* gRes = new TGraphAsymmErrors(resY.size(), &massVecD[0], &resY[0], &zero[0], &zero[0], &yUp[0], &yDwn[0]);
    gRes->SetMarkerStyle(20);
    
    double minY = TMath::MinElement(gRes->GetN(),gRes->GetY()) - 2*yDwn.at(maxElementIndex);
    double maxY = TMath::MaxElement(gRes->GetN(),gRes->GetY()) + 2*yUp.at(minElementIndex);

    frameSys->SetLabelOffset(0.015,"X");
    frameSys->SetLabelOffset(0.015,"Y");
    frameSys->GetYaxis()->SetTitle("Fit - Data");


    // int firstKey = 0;
    
    // frameSys->GetXaxis()->SetTitle(axisName);

    frameSys->SetMinimum(minY);
    frameSys->SetMaximum(maxY);
    fixBottomLabel(frameSys, VerticalCanvasSplit, frame);

    frameSys->Draw();
    gRes->Draw("pe same");



    c1->SaveAs(plotDir + fileName);

    // TGraphAsymmErrors (Int_t n, const Double_t *x, const Double_t *y, const Double_t *exl=0, const Double_t *exh=0, const Double_t *eyl=0, const Double_t *eyh=0)


    return fitInfo;
}


TString getFancy(TString name)
{

    if(name.EqualTo("mean") ) return "Mean [GeV]";
    if(name.EqualTo("sigma") ) return "#sigma [GeV]";
    if(name.EqualTo("alphaLo") ) return "#alpha_{Lo} [GeV]";
    if(name.EqualTo("alphaHi") ) return "#alpha_{Hi} [GeV]";
    if(name.EqualTo("etaLo") ) return "n_{Lo} [GeV]";
    if(name.EqualTo("etaHi") ) return "n_{Hi} [GeV]";

    else return name;
}

void fixBottomLabel(TH1F* hist, double VerticalCanvasSplit, TH1F* frame)
{
    double labelscalefact = 1.0 / (1.4 * VerticalCanvasSplit);

    double scale = 0.65;
    hist->GetXaxis()->SetTitleSize(1.1*scale* frame->GetYaxis()->GetTitleSize()/VerticalCanvasSplit);
    hist->GetXaxis()->SetLabelSize(scale*frame->GetYaxis()->GetLabelSize()/VerticalCanvasSplit);

    hist->GetYaxis()->SetTitleSize(0.85*labelscalefact * hist->GetYaxis()->GetTitleSize());          
    hist->GetYaxis()->SetLabelSize(scale * frame->GetYaxis()->GetLabelSize()/VerticalCanvasSplit);
    hist->GetYaxis()->SetTitleOffset(1.2/labelscalefact * hist->GetYaxis()->GetTitleOffset());


    hist->GetYaxis()->SetNdivisions(305);

}


pointInfo getPoint(TString fileName)
{

	pointInfo cInfo;

	TFile* file = TFile::Open(fileName);
	TTree* tree = (TTree*)file->Get("results");

    double VarVal	   = -9999;
    double VarErrorLow = -9999;
    double VarErrorHi  = -9999;

    tree->SetBranchAddress("VarVal", &VarVal);
    tree->SetBranchAddress("VarErrorLow", &VarErrorLow);
    tree->SetBranchAddress("VarErrorHi", &VarErrorHi);

    for(int i = 0; i < tree->GetEntries(); i++)
    {
    	tree->GetEntry(i);
    	if(i > 1) 
    	{
    		cout << "Where is there more than 2 entries for this minos fit file? " << tree->GetEntries() << endl;
    	}

    }



 	cInfo.cenVal    = VarVal;
    cInfo.upErr     = VarErrorHi;
 	cInfo.dwnErr    = VarErrorLow;

    // cout << cInfo.cenVal  << "\t" << cInfo.upErr  << "\t" << cInfo.upErr  << endl;

	return cInfo;

}



TString getDecayChannel(TString inputChannel)
{
    if(inputChannel.EqualTo("4mu") ) return "4#mu Channel";
    else if(inputChannel.EqualTo("4e") ) return "4e Channel";
    else if(inputChannel.EqualTo("2mu2e") ) return "2#mu2e Channel";
    else if(inputChannel.EqualTo("2e2mu") ) return "2e2#mu Channel";

    return inputChannel;
}

TString getBDTbin(TString bdtBin)
{
    if(bdtBin.EqualTo("BDT_bin1") ) return "BDT Bin 1 [-1.0, -0.5]";
    else if(bdtBin.EqualTo("BDT_bin2") ) return "BDT Bin 2 [-0.5, 0.0]";
    else if(bdtBin.EqualTo("BDT_bin3") ) return "BDT Bin 3 [0.0, 0.5]";
    else if(bdtBin.EqualTo("BDT_bin4") ) return "BDT Bin 4 [0.5, 1.0]";
    return bdtBin;
}


bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();

    // defaults
    opts["baseName"]        = "";
    opts["channel"]         = "4mu,4e,2mu2e,2e2mu";
    opts["bdtBin"]          = "BDT_bin4,BDT_bin3,BDT_bin2,BDT_bin1";
    opts["iter2File"]       = "";
    opts["poiList"]         = "mean,sigma,alphaLo,alphaHi,etaLo,etaHi";
    // opts["poiList"]         = "mean";
    opts["plotDir"]         = "Plots";
    opts["noErr"]           = "false";
    opts["systematic"]      = "Nominal";
    opts["outputFolder"]    = "fillMe";
    opts["seperatePois"]    = "false";
    opts["scanName"]        = "DCB_mean_slope";
    opts["isNormSys"]       = "false";



    for (int i=1;i<argc;i++) {

        string opt=argv[i];

        if (opt=="--help") {
            cout<<"--fileName       : path to ttree containing the information"<<endl;
            // cout<<"--label          : Label for the legend for the files"<<endl;
            // cout<<"--outFile        : Name for the output file"<<endl;
            // cout<<"--doXS           : do XS poi"<<endl;
            // cout<<"--doNs           : do Ns poi"<<endl;
            // cout<<"--doUpperLim     : true or false"<<endl;
            // cout<<"--plotNP     : true or false"<<endl;
            return false;
        }

        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }

        opts[opt]=nxtopt;
        i++;
    }
    //if(opts["fileName"].Contains("ttH")) opts["doUpperLim"]  = "true";
    return true;
}

TString getBaseName(TString pathName)
{
    TString baseName = "";
    if(opts["baseName"].EqualTo("Empty"))
    {
        std::vector<TString> folderName = tokenizeStr(pathName, "/");
        return folderName[1];
    }
    else
    {
        return opts["baseName"];
    }
}


void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;
    new TColor(ci, 62/255.,    153/255.,    247/255.); //54
    ci++;

    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;

    new TColor(ci, 254/255., 139/255., 113/255.); //55
    ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++;
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}

