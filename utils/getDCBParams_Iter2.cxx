#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"
#include "InterpolatedGraph.h"
#include "TGraphAsymmErrors.h"
#include <TSystem.h>

using namespace std;

// Global vars
map<TString,TString> opts;
map<TString,TString> metaData;



struct pointInfo 
{
    double cenVal;
    double upErr;
    double dwnErr;

};




// // Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
TString getBaseName(TString pathName);
void setPrettyStuff();
TString getDecayChannel(TString inputChannel);
TString getBDTbin(TString bdtBin);
TString getRenamedPOI(TString orgPOIName);
// double getMin(std::map<double,double> NLL);
pointInfo getPoint(TString fileName, TString param);
void fixBottomLabel(TH1F* hist, double VerticalCanvasSplit, TH1F* frame);
TString getFancy(TString name);



std::vector<TString> massVecS   = {"123", "124", "125p5", "126", "127"};
std::vector<double>  massVecD   = {123, 124, 125.5, 126, 127};

std::map<TString,  std::map<TString, pointInfo>> textMap;


// ../build/getDCBParams_Iter2 --baseName root-files/J27_Analytic_DCBWS_XXX_YYY_AllP_Nominal_allMP_Sim

int main(int argc, char** argv)
{
    setPrettyStuff();

    if (!cmdline(argc,argv,opts)) return 0;

    if(opts["baseName"].Length() == 0)
    {
    	cout << "Folder baseName needs to be provided" << endl;
    	exit(1);
    }

    std::vector<TString> bdtList 	= tokenizeStr(opts["bdtBin"], ",");
    std::vector<TString> chanList 	= tokenizeStr(opts["channel"], ",");
    std::vector<TString> paramList 	= tokenizeStr(opts["poiList"], ",");

    for(const auto bdt: bdtList)
    {
    	for(const auto chan:chanList)
    	{
            std::map<TString, pointInfo> pointInfo;

    		for(const auto param: paramList)
    		{
                // std::vector<pointInfo> minosResults;

    			TString shortBin = bdt;
    			shortBin.ReplaceAll("BDT_", "").ReplaceAll("in","");
                TString baseNameCopy = opts["baseName"];

    			TString rootFileName = baseNameCopy.ReplaceAll("XXX", chan).ReplaceAll("YYY", shortBin) + "/allSys/asimov/sJob/";
                if(param.EqualTo("sigmaSF"))
                {
                    rootFileName += param + ".root";

                }
                else
                {
                    rootFileName += "DCB_" + param + ".root";
                }

                
    			

                cout << "FileName:: " << rootFileName << endl;

                pointInfo[param] = getPoint(rootFileName, param);                

    		}

            textMap[chan + "_" + bdt] = pointInfo;
    	}
    }

    if(opts["configFolder"].Length() > 0)
    {
        cout << "Writing configs to " << opts["configFolder"] << endl;

        std::vector<TString> fileLines;

        string line;
        ifstream myfile (opts["baseConfig"]);
        if (myfile.is_open())
        {
            while ( getline (myfile,line) )
            {
              fileLines.push_back(line);
            }
            myfile.close();
        }


        system("mkdir -vp " + opts["configFolder"]);

        for(const auto category:textMap)
        {

            TString fileName = "DCBConfig_" + category.first + "_" + opts["iter"] + ".txt";

            ofstream myfile;
            myfile.open (opts["configFolder"] + "/" + fileName);

            myfile << "[POI] \n";
            myfile << "name:*, float:true,lwrLim:-500,uprLim:500  \n"; 

            for(const auto paramInfo:category.second)
            {
                if(opts["iter"].EqualTo("2p1")) 
                {
                    if(paramInfo.first.Contains("mean") ||  paramInfo.first.Contains("sigma") ) continue;
                    myfile << "name:DCB_" << paramInfo.first << ",float:false, centralVal:" << paramInfo.second.cenVal << " \n";
                }
                if(opts["iter"].EqualTo("2p2")) 
                {
                    myfile << "name:DCB_" << paramInfo.first << ",float:true, centralVal:" << paramInfo.second.cenVal << " \n";
                }
            }

            for(auto originalLines: fileLines) myfile << originalLines <<"\n";


            myfile.close();
        }


    }


    // if a file output path is provided write the fit results to it
    if(opts["DCBFolder"].Length() > 0)
    {
        cout << "Writing DCB Params to " << opts["DCBFolder"] << endl;
        system("mkdir -vp " + opts["DCBFolder"]);

        TString baseFolder = opts["DCBFolder"];
        baseFolder.ReplaceAll("Nominal", "/");

        ofstream myfile;
        myfile.open (baseFolder + "/info.txt");
        myfile << "Made with " << opts["baseName"] << "\n";
        myfile.close();


        for(const auto category:textMap)
        {

            TString fileName = "parameters_" + category.first + ".txt";

            ofstream myfile;
            myfile.open (opts["DCBFolder"] + "/" + fileName);

            for(const auto paramInfo:category.second)
            {
                myfile << paramInfo.first << " " << paramInfo.second.cenVal << " 0.0 \n";
            }

            myfile.close();
        }
    }
}



TString getFancy(TString name)
{

    if(name.EqualTo("mean") ) return "Mean [GeV]";
    if(name.EqualTo("sigma") ) return "#sigma [GeV]";
    if(name.EqualTo("alphaLo") ) return "#alpha_{Lo} [GeV]";
    if(name.EqualTo("alphaHi") ) return "#alpha_{Hi} [GeV]";
    if(name.EqualTo("etaLo") ) return "n_{Lo} [GeV]";
    if(name.EqualTo("etaHi") ) return "n_{Hi} [GeV]";

    else return name;
}

void fixBottomLabel(TH1F* hist, double VerticalCanvasSplit, TH1F* frame)
{
    double labelscalefact = 1.0 / (1.4 * VerticalCanvasSplit);

    double scale = 0.65;
    hist->GetXaxis()->SetTitleSize(1.1*scale* frame->GetYaxis()->GetTitleSize()/VerticalCanvasSplit);
    hist->GetXaxis()->SetLabelSize(scale*frame->GetYaxis()->GetLabelSize()/VerticalCanvasSplit);

    hist->GetYaxis()->SetTitleSize(0.85*labelscalefact * hist->GetYaxis()->GetTitleSize());          
    hist->GetYaxis()->SetLabelSize(scale * frame->GetYaxis()->GetLabelSize()/VerticalCanvasSplit);
    hist->GetYaxis()->SetTitleOffset(1.2/labelscalefact * hist->GetYaxis()->GetTitleOffset());


    hist->GetYaxis()->SetNdivisions(305);

}


pointInfo getPoint(TString fileName, TString param)
{

	pointInfo cInfo;
    double VarVal      = -9999;
    double VarErrorLow = -9999;
    double VarErrorHi  = -9999;
    double otherVal    = -9999;

    cInfo.cenVal    = VarVal;
    cInfo.upErr     = VarErrorHi;
    cInfo.dwnErr    = VarErrorLow;

    if (gSystem->AccessPathName(fileName) && opts["iter"].EqualTo("2p2") ) 
    {

        std::vector<TString> vectorFile = tokenizeStr(fileName, "/");

        TString newFile = "";
        for(int i = 0; i < vectorFile.size() - 1; i++) newFile += vectorFile.at(i) + "/";
        newFile += "DCB_mean_slope.root";

        fileName = newFile;

    }

	TFile* file = TFile::Open(fileName);

    if (!file->GetListOfKeys()->Contains("results")) 
    {
        cout << "Corrupted File!!! " << fileName << endl;
        return cInfo; 
    }


	TTree* tree = (TTree*)file->Get("results");


    tree->SetBranchAddress("VarVal", &VarVal);
    tree->SetBranchAddress("VarErrorLow", &VarErrorLow);
    tree->SetBranchAddress("VarErrorHi", &VarErrorHi);
    if(param.EqualTo("sigmaSF"))
    {
        tree->SetBranchAddress(param, &otherVal);
    }
    else
    {
        tree->SetBranchAddress("DCB_" + param, &otherVal);
    }

    for(int i = 0; i < tree->GetEntries(); i++)
    {
    	tree->GetEntry(i);
    	if(i > 1) 
    	{
    		cout << "Where is there more than 2 entries for this minos fit file? " << tree->GetEntries() << endl;
    	}

    }



 	cInfo.cenVal    = VarVal;
    cInfo.upErr     = VarErrorHi;
 	cInfo.dwnErr    = VarErrorLow;

    if(opts["iter"].EqualTo("2p2")) cInfo.cenVal    = otherVal;

    // cout << cInfo.cenVal  << "\t" << cInfo.upErr  << "\t" << cInfo.upErr  << endl;

	return cInfo;

}



TString getDecayChannel(TString inputChannel)
{
    if(inputChannel.EqualTo("4mu") ) return "4#mu Channel";
    else if(inputChannel.EqualTo("4e") ) return "4e Channel";
    else if(inputChannel.EqualTo("2mu2e") ) return "2#mu2e Channel";
    else if(inputChannel.EqualTo("2e2mu") ) return "2e2#mu Channel";

    return inputChannel;
}

TString getBDTbin(TString bdtBin)
{
    if(bdtBin.EqualTo("BDT_bin1") ) return "BDT Bin 1 [-1.0, -0.5]";
    else if(bdtBin.EqualTo("BDT_bin2") ) return "BDT Bin 2 [-0.5, 0.0]";
    else if(bdtBin.EqualTo("BDT_bin3") ) return "BDT Bin 3 [0.0, 0.5]";
    else if(bdtBin.EqualTo("BDT_bin4") ) return "BDT Bin 4 [0.5, 1.0]";
    return bdtBin;
}


bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();

    // defaults
    opts["baseName"]        = "";
    opts["channel"]         = "4mu,4e,2mu2e,2e2mu";
    opts["bdtBin"]          = "BDT_bin4,BDT_bin3,BDT_bin2,BDT_bin1";
    opts["iter2File"]       = "";
    opts["poiList"]         = "mean_slope,mean_intercept,sigma,alphaLo,alphaHi,etaLo,etaHi";
    // opts["poiList"]         = "mean";
    opts["plotDir"]         = "Plots";
    opts["DCBFolder"]       = "";
    opts["noErr"]           = "false";
    opts["systematic"]      = "Nominal";
    opts["outputFolder"]    = "fillMe";
    opts["seperatePois"]    = "false";
    opts["scanName"]        = "DCB_mean_slope";
    opts["isNormSys"]       = "false";
    opts["baseConfig"]      = "../source/wscanner/data/H4lMass/DCBBaseConfig.txt";
    opts["configFolder"]    = "";
    opts["iter"]            = "2p1";



    for (int i=1;i<argc;i++) {

        string opt=argv[i];

        if (opt=="--help") {
            cout<<"--fileName       : path to ttree containing the information"<<endl;
            // cout<<"--label          : Label for the legend for the files"<<endl;
            // cout<<"--outFile        : Name for the output file"<<endl;
            // cout<<"--doXS           : do XS poi"<<endl;
            // cout<<"--doNs           : do Ns poi"<<endl;
            // cout<<"--doUpperLim     : true or false"<<endl;
            // cout<<"--plotNP     : true or false"<<endl;
            return false;
        }

        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }

        opts[opt]=nxtopt;
        i++;
    }
    //if(opts["fileName"].Contains("ttH")) opts["doUpperLim"]  = "true";
    return true;
}

TString getBaseName(TString pathName)
{
    TString baseName = "";
    if(opts["baseName"].EqualTo("Empty"))
    {
        std::vector<TString> folderName = tokenizeStr(pathName, "/");
        return folderName[1];
    }
    else
    {
        return opts["baseName"];
    }
}


void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;
    new TColor(ci, 62/255.,    153/255.,    247/255.); //54
    ci++;

    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;

    new TColor(ci, 254/255., 139/255., 113/255.); //55
    ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++;
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}

