#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TString.h>
#include <TRegexp.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"

// RooFit includes
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooArgSet.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/AsymptoticCalculator.h"
#include "RooMinimizer.h"
#include "RooNLLVar.h"
#include "TStopwatch.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include "RooNumIntConfig.h"
#include "RooMinuit.h"
#include "RooRealSumPdf.h"

using namespace std;

// Global vars
map<TString,TString> opts;

struct ScanInfo
{
	double bestFitVal;
	double lwrErr;
	double uprErr;

	TString POIName;
	TString scanType;
	TString dataType;
};

struct FormatOut
{
	TString cVal;
	TString lwrErr;
	TString uprErr;

	TString cAveVal;
	TString aveErr;
};


// Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
vector<ScanInfo> getPOIInfo(TString dataLabel, TString scanLabel, TString dataType, TString scanType);
vector<ScanInfo> dounCertDecom(vector<ScanInfo>& firstList, vector<ScanInfo>& secondList);
void printTable(map<TString, vector<ScanInfo>>& scanMap, vector<TString> colList);
void printTable(map<TString, vector<ScanInfo>>& scanMap, vector<vector<TString>> colList);
TString formatLine(ScanInfo scanInfo);
TString formatLine(vector<ScanInfo> scanInfoList);
std::map<TString, TString> commonMetaData;
pair<double, double> getThSys(TString poiName);

RooWorkspace* m_ws;
void fillWS(TString dataLabel, TString scanLabel, TString dataType, TString scanType);
FormatOut pdfRound(ScanInfo sInfo, double lwrErrOver = 0, double uprErrOver = 0,  int addExtraDecimalPlace = 0);
void printCouplingTable(map<TString, vector<ScanInfo>>& scanMap, vector<TString> colList);

vector<TString> varToSum(TString poiName);
vector<TString> varToMult(TString poiName);
TString getFancySTXSname(TString name);

int main(int argc, char** argv)
{
    if (!cmdline(argc,argv,opts)) return 0;

    m_ws = NULL;
    fillWS(opts["asimovLabel"], opts["sysAllLabel"], "asimov", "sysAll");

    map<TString, vector<ScanInfo>> scanInfoMap;

    scanInfoMap["data_StatOnly"] = getPOIInfo(opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly");
    scanInfoMap["data_SysAll"]   = getPOIInfo(opts["dataLabel"], opts["sysAllLabel"], 	"data", "sysAll");
    scanInfoMap["data_SysOnly"]	 = dounCertDecom(scanInfoMap["data_SysAll"], scanInfoMap["data_StatOnly"]);


    scanInfoMap["asimov_StatOnly"] = getPOIInfo(opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly");
    scanInfoMap["asimov_SysAll"]   = getPOIInfo(opts["asimovLabel"], opts["sysAllLabel"], 	"asimov", "sysAll");
    scanInfoMap["asimov_SysOnly"]  = dounCertDecom(scanInfoMap["asimov_SysAll"], scanInfoMap["asimov_StatOnly"]);




    cout<<"POI - asimov sys - asimov stat - data sys - data stat"<<endl;
    printTable(scanInfoMap, vector<TString>{"asimov_StatOnly", "asimov_SysAll", "data_StatOnly", "data_SysAll"});

    cout<<" ---------------------------------- "<<endl;
    cout<<"POI - asimov stat sys - data stat sys"<<endl;
    printTable(scanInfoMap, vector<vector<TString>>{vector<TString>{"asimov_StatOnly", "asimov_SysOnly"}, vector<TString>{"data_StatOnly", "data_SysOnly"}});



    if(!opts["doTh"].Contains("false"))
    {
	    scanInfoMap["data_modTh"]   = getPOIInfo(opts["dataLabel"], opts["modThLabel"], 	"data", "modTheory");
    	scanInfoMap["data_ThOnly"]	= dounCertDecom(scanInfoMap["data_SysAll"], scanInfoMap["data_modTh"]);
    	scanInfoMap["data_ExpOnly"]	= dounCertDecom(scanInfoMap["data_modTh"], scanInfoMap["data_StatOnly"]);
    	
	    scanInfoMap["asimov_modTh"]   = getPOIInfo(opts["asimovLabel"], opts["modThLabel"], 	"asimov", "modTheory");
    	scanInfoMap["asimov_ThOnly"]	= dounCertDecom(scanInfoMap["asimov_SysAll"], scanInfoMap["asimov_modTh"]);
    	scanInfoMap["asimov_ExpOnly"]	= dounCertDecom(scanInfoMap["asimov_modTh"], scanInfoMap["asimov_StatOnly"]);


	    cout<<" ---------------------------------- "<<endl;
	    cout<<"POI - asimov stat ExpSys THSys - data ExpSys THSys"<<endl;
	    printTable(scanInfoMap, vector<vector<TString>>{vector<TString>{"asimov_StatOnly", "asimov_ExpOnly", "asimov_ThOnly"}, vector<TString>{"data_StatOnly", "data_ExpOnly", "data_ThOnly"}});
    }


    if(!opts["doCoupling"].Contains("false"))
    {

	    cout<<" ---------------------------------- "<<endl;
	    cout<<"Coupling Table"<<endl;

	    scanInfoMap["data_modTh"]   = getPOIInfo(opts["dataLabel"], opts["modThLabel"], 	"data", "modTheory");
		scanInfoMap["data_ThOnly"]	= dounCertDecom(scanInfoMap["data_SysAll"], scanInfoMap["data_modTh"]);
		scanInfoMap["data_ExpOnly"]	= dounCertDecom(scanInfoMap["data_modTh"], scanInfoMap["data_StatOnly"]);

		if(opts["poiList"].Contains("mu_ggF_") || opts["poiList"].Contains("mu_gg2H_"))
		{
		    printCouplingTable(scanInfoMap, vector<TString>{"data_StatOnly", "data_SysOnly"});
		}
		else
		{
		    printCouplingTable(scanInfoMap, vector<TString>{"data_StatOnly", "data_ExpOnly", "data_ThOnly"});
		}
	}

}


void printCouplingTable(map<TString, vector<ScanInfo>>& scanMap, vector<TString> colList)
{
	for(size_t i = 0; i < scanMap[colList.at(0)].size(); i++)
	{
		TString label = scanMap[colList.at(0)].at(i).POIName;

		TString xName = getFancySTXSname(label);
		label = xName;
		
        cout<<"$ "<<setw(30)<<left<<label<<"$ ";


		if(scanMap[colList.at(0)].at(i).bestFitVal < -100)
		{
        	cout<<" & "<<"$"<<setw(28)<<left<<"-"<<"$";
		}
		else
		{

			vector<ScanInfo> sListXS;
			for(const auto& cList:colList) sListXS.push_back(scanMap[cList].at(i));

			// Get the XS

			// Multiple by the XS or N data
	        auto varList = varToSum(scanMap[colList.at(0)].at(i).POIName);

	        double sumTotal = 0;
	        double sumTotalUp = 0;
	        double sumTotalDown = 0;
	        for(const auto& varName: varList)
	        { 
	        	auto thSys = getThSys(varName);

	        	if(!m_ws->var(varName))
	        	{
	        		cout<<"Can't find "<<varName<<endl;
	        		continue;
	        	}
	        	sumTotal 		+= m_ws->var(varName)->getVal();
	            sumTotalUp      += m_ws->var(varName)->getVal() * fabs(thSys.first);
	            sumTotalDown    += m_ws->var(varName)->getVal() * fabs(thSys.second);

	        }

	        for(auto& sInfo: sListXS)
	        {
	        	sInfo.bestFitVal *= sumTotal;
	        	sInfo.lwrErr 	 *= sumTotal;
	        	sInfo.uprErr 	 *= sumTotal;
	        }


	        ScanInfo thInfo;
	        thInfo.bestFitVal = sumTotal;
	        thInfo.lwrErr = sumTotalDown;
	        thInfo.uprErr = sumTotalUp;


        	cout<<" & "<<"$ "<<setw(30)<<left<<formatLine(thInfo)<<"$";
        	cout<<" & "<<"$ "<<setw(80)<<left<<formatLine(sListXS)<<"$";

			vector<ScanInfo> sList;
			for(const auto& cList:colList) sList.push_back(scanMap[cList].at(i));
        	cout<<" & "<<"$ "<<setw(70)<<left<<formatLine(sList)<<"$";




   	 	}
		
		cout<<"\\\\"<<endl;
	}
}

void printTable(map<TString, vector<ScanInfo>>& scanMap, vector<vector<TString>> colList)
{
	for(size_t i = 0; i < scanMap[colList.at(0).at(0)].size(); i++)
	{
		TString label = scanMap[colList.at(0).at(0)].at(i).POIName;

		TString symbol = getSymbol(commonMetaData, label);
		TString xName = getFancyXname(label);
		label = symbol + "_{" + xName + "}";
		if(!opts["doXS"].Contains("true") && !symbol.Contains("mu"))  label += "/" + symbol + "_{SM}";
		label.ReplaceAll("#it", "\\textit");
		label.ReplaceAll("#", "\\");

        cout<<"$ "<<setw(60)<<left<<label<<"$ ";

		for(size_t j = 0; j < colList.size(); j++)
		{
			if(scanMap[colList.at(j).at(0)].at(i).bestFitVal < -100)
			{
            	cout<<" & "<<"$"<<setw(28)<<left<<"-"<<"$";
			}
			else
			{
				vector<ScanInfo> sList;
				for(const auto& cList:colList.at(j)) sList.push_back(scanMap[cList].at(i));
            	cout<<" & "<<"$"<<setw(50)<<left<<formatLine(sList)<<"$";
       	 	}
		}
		cout<<"\\\\"<<endl;
	}
}


void printTable(map<TString, vector<ScanInfo>>& scanMap, vector<TString> colList)
{
	for(size_t i = 0; i < scanMap[colList.at(0)].size(); i++)
	{
		TString label = scanMap[colList.at(0)].at(i).POIName;

		TString symbol = getSymbol(commonMetaData, label);
		TString xName = getFancyXname(label);
		label = symbol + "_{" + xName + "}";
		if(!opts["doXS"].Contains("true") && !symbol.Contains("mu")) label += "/" + symbol + "_{SM}";
		label.ReplaceAll("#it", "\\textit");
		label.ReplaceAll("#", "\\");
		
        cout<<"$ "<<setw(60)<<left<<label<<"$ ";

		for(size_t j = 0; j < colList.size(); j++)
		{
			if(scanMap[colList.at(j)].at(i).bestFitVal < -100)
			{
            	cout<<" & "<<"$"<<setw(28)<<left<<"-"<<"$";
			}
			else
			{
            	cout<<" & "<<"$"<<setw(28)<<left<<formatLine(scanMap[colList.at(j)].at(i))<<"$";
       	 	}
		}
		cout<<"\\\\"<<endl;
	}
}

TString formatLine(ScanInfo scanInfo)
{
	TString info;
	info.Form("%.2f^{+%.2f}_{%.2f}", scanInfo.bestFitVal, scanInfo.uprErr , scanInfo.lwrErr);

	if(scanInfo.bestFitVal > 10) info.Form("%.1f^{+%.1f}_{%.1f}", scanInfo.bestFitVal, scanInfo.uprErr , scanInfo.lwrErr);
	if(scanInfo.bestFitVal > 100) info.Form("%.0f^{+%.0f}_{%.0f}", scanInfo.bestFitVal, scanInfo.uprErr , scanInfo.lwrErr);
	if(scanInfo.bestFitVal < 0.1) info.Form("%.3f^{+%.3f}_{%.3f}", scanInfo.bestFitVal, scanInfo.uprErr , scanInfo.lwrErr);


	auto outInfo = pdfRound(scanInfo);
	// sym
	bool sym = false;
	if(fabs(fabs(scanInfo.lwrErr) - fabs(scanInfo.uprErr))/((fabs(scanInfo.lwrErr) + fabs(scanInfo.uprErr))*0.5) < 0.2) sym = true;


	if(!sym) info.Form("%s^{+%s}_{%s}", outInfo.cVal.Data(), outInfo.uprErr.Data(), outInfo.lwrErr.Data());
	else  info.Form("%s \\pm %s", outInfo.cAveVal.Data(), outInfo.aveErr.Data());

	return info;
}

TString formatLine(vector<ScanInfo> sList)
{
	TString info;

	double lwrErr = 999999;
	double uprErr = 999999;

	for(size_t i = 0; i < sList.size(); i++)
	{
		if(fabs(sList[i].uprErr) < uprErr) uprErr = sList[i].uprErr;
		if(fabs(sList[i].lwrErr) < lwrErr) lwrErr = sList[i].lwrErr;
	}

	// To the stat error, which is always the first one
	uprErr = sList[0].uprErr;
	lwrErr = sList[0].lwrErr;


	for(size_t i = 0; i < sList.size(); i++)
	{
		TString tinfo;
		if(i == 0) 	tinfo.Form("%.2f^{+%.2f}_{%.2f}", sList[i].bestFitVal, sList[i].uprErr , sList[i].lwrErr);
		else 		tinfo.Form("$ $^{+%.2f}_{%.2f}", sList[i].uprErr , sList[i].lwrErr);

		if(sList[i].bestFitVal > 10)  
		{
			if(i == 0) 	tinfo.Form("%.1f^{+%.1f}_{%.1f}", sList[i].bestFitVal, sList[i].uprErr , sList[i].lwrErr);
			else 		tinfo.Form("$ $^{+%.1f}_{%.1f}", sList[i].uprErr , sList[i].lwrErr);
		}
		if(sList[i].bestFitVal > 100) 
		{
			if(i == 0) 	tinfo.Form("%.0f^{+%.0f}_{%.0f}", sList[i].bestFitVal, sList[i].uprErr , sList[i].lwrErr);
			else 		tinfo.Form("$ $^{+%.0f}_{%.0f}", sList[i].uprErr , sList[i].lwrErr);
		}
		if(sList[i].bestFitVal < 0.1) 
		{
			if(i == 0) 	tinfo.Form("%.3f^{+%.3f}_{%.3f}", sList[i].bestFitVal, sList[i].uprErr , sList[i].lwrErr);
			else 		tinfo.Form("$ $^{+%.3f}_{%.3f}", sList[i].uprErr , sList[i].lwrErr);
		}

		auto outInfo = pdfRound(sList[i], lwrErr, uprErr);

		// sym
		bool sym = false;

		double cLErr = fabs(sList[i].lwrErr);
		double cUErr = fabs(sList[i].uprErr);

		// double cLErr = fabs(outInfo.lwrErr.Atof());
		// double cUErr = fabs(outInfo.uprErr.Atof());

		if(fabs(cLErr - cUErr)/((cLErr + cUErr)*0.5) < 0.20) sym = true;

		if(!sym)
		{
			if(i == 0) tinfo.Form("%s^{+%s}_{%s}", outInfo.cVal.Data(), outInfo.uprErr.Data(), outInfo.lwrErr.Data());
			else       tinfo.Form("$ $^{+%s}_{%s}", outInfo.uprErr.Data(), outInfo.lwrErr.Data());
		}
		else
		{
			if(i == 0) tinfo.Form("%s \\pm %s", outInfo.cAveVal.Data(), outInfo.aveErr.Data());
			else       tinfo.Form(" \\pm %s", outInfo.aveErr.Data());
		} 
		info += tinfo;
		
	}
	return info;
}

vector<ScanInfo> getPOIInfo(TString dataLabel, TString scanLabel, TString dataType, TString scanType)
{
	TString folderName = opts["folder"] + "/" + scanLabel + "/" + dataLabel + "/";

	vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(opts["poiList"], ",");

    // read all the info
    for(const auto& poiName: poiList)
    {
    	TString fileName = folderName + "/" + poiName + ".root";
        ScanInfo sInfo;
        sInfo.POIName  = poiName;
        sInfo.dataType = dataType;
        sInfo.scanType = scanType;
    		// cout<<"checking : "<<fileName<<endl;

    	if(doesExist(fileName.Data()))
    	{
    		// cout<<"File exist: "<<fileName<<endl;
	    	auto label = getLabels(vector<TString>{fileName}).at(0);
	    	commonMetaData = label;
			
			TChain *chain = new TChain("results");
	        chain->Add(fileName);

	        // Read the NLL into a map
	        map<double,double> NLL = read1DVar("nll", chain);
	        fixOffset(NLL);
	        printFitInfo(NLL, sInfo.bestFitVal, sInfo.lwrErr, sInfo.uprErr);

		     double sumTotal = 0;

	        if(opts["doXS"].Contains("true"))
	        {
		        // Multiple by the XS or N data
		        auto varList = varToSum(poiName);

		        for(const auto& varName: varList)
		        { 
		        	if(!m_ws->var(varName))
		        	{
		        		cout<<"Can't find "<<varName<<endl;
		        		continue;
		        	}
		        	sumTotal += m_ws->var(varName)->getVal();
		        }


		        // Multiple by the XS or N data
		        varList = varToMult(poiName);
		        for(const auto& varName: varList)
		        { 
		        	// Special Case
		        	if(varName.EqualTo("1000"))
		        	{
		        		sumTotal *= 1000;
		        		continue;
		        	}
		        	if(!m_ws->var(varName))
		        	{
		        		cout<<"Can't find "<<varName<<endl;
		        		continue;
		        	}
		        	sumTotal *= m_ws->var(varName)->getVal();
		        }
	    	}
	    	else sumTotal = 1;

	       	sInfo.bestFitVal *= sumTotal;
	       	sInfo.lwrErr *= sumTotal;
	       	sInfo.uprErr *= sumTotal;

	       	if(!opts["doPercentErr"].Contains("false"))
	       	{
		       	sInfo.lwrErr /= sInfo.bestFitVal;
		       	sInfo.uprErr /= sInfo.bestFitVal;	  
		      	sInfo.lwrErr *= 100;
		       	sInfo.uprErr *= 100;

	       	}
		}
		else
		{
			sInfo.bestFitVal = -999;
			sInfo.lwrErr = -999;
			sInfo.uprErr = -999;
		}
        sInfoList.push_back(sInfo);
    }
    return sInfoList;
}

vector<ScanInfo> dounCertDecom(vector<ScanInfo>& firstList, vector<ScanInfo>& secondList)
{
	vector<ScanInfo> sInfoList;

	for(size_t i = 0; i < firstList.size(); i++)
	{
		ScanInfo sInfo = firstList[i];

		sInfo.lwrErr = sqrt(pow(firstList[i].lwrErr, 2)  - pow(secondList[i].lwrErr, 2))  *  firstList[i].lwrErr/fabs(firstList[i].lwrErr);
		sInfo.uprErr = sqrt(pow(firstList[i].uprErr, 2)  - pow(secondList[i].uprErr, 2))  *  firstList[i].uprErr/fabs(firstList[i].uprErr);

		sInfoList.push_back(sInfo);
	}
    
    return sInfoList;
}


bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["folder"]      	= "root-files/STXS_XS_S0_v19_NN_withSys";
    opts["asimovLabel"]    	= "asimov";
    opts["dataLabel"]    	= "data";

    opts["statOnlyLabel"]   = "statOnly";
    opts["sysAllLabel"]    	= "allSys";
    opts["modThLabel"]    	= "modTheory";

    opts["poiList"]  		= "mu_ggF,mu_VBF,mu_VH,mu_ttH";


    opts["doXS"]  			= "false";
    opts["doTh"]  			= "false";
    opts["doPercentErr"]    = "false";
    opts["doCoupling"]      = "false";


    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") 
        {
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    return true;
}


void fillWS(TString dataLabel, TString scanLabel, TString dataType, TString scanType)
{
	// Grab metadata from one file
	TString folderName = opts["folder"] + "/" + scanLabel + "/" + dataLabel + "/";

	vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(opts["poiList"], ",");

    // read all the info
    for(const auto& poiName: poiList)
    {
    	TString fileName = folderName + "/" + poiName + ".root";
    	if(doesExist(fileName.Data()))
    	{
	    	auto label = getLabels(vector<TString>{fileName}).at(0);
			
	    	if(doesExist(label["fileName"].Data()))
	    	{
	    		TFile* wsFile = TFile::Open(label["fileName"]);
	    		m_ws = (RooWorkspace*) wsFile->Get(label["workspaceName"]);

	    		cout<<"Found the ws"<<endl;
	    		return;
	    	}
		}
    }
}

vector<TString> varToSum(TString poiName)
{
	map<TString, vector<TString>> sumList;
	sumList["mu_ggF"] 	= {"XS_ggF_ggF", "XS_bbH_ggF"};
	sumList["mu_VBF"] 	= {"XS_VBF_VBF"};
	sumList["mu_VH"] 	= {"XS_VH_VH"};
	sumList["mu_ttH"] 	= {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};

	sumList["mu_ggF_0J_PTH_0_10"] 		= {"XS_ggF_ggF_0J_PTH_0_10", "XS_bbH_ggF_0J_PTH_0_10"};
	sumList["mu_ggF_0J_PTH_GT10"] 		= {"XS_ggF_ggF_0J_PTH_GT10", "XS_bbH_ggF_0J_PTH_GT10"};
	sumList["mu_ggF_1J_PTH_0_60"] 		= {"XS_ggF_ggF_1J_PTH_0_60", "XS_bbH_ggF_1J_PTH_0_60"};
	sumList["mu_ggF_1J_PTH_120_200"] 	= {"XS_ggF_ggF_1J_PTH_120_200", "XS_bbH_ggF_1J_PTH_120_200"};
	sumList["mu_ggF_1J_PTH_60_120"] 	= {"XS_ggF_ggF_1J_PTH_60_120", "XS_bbH_ggF_1J_PTH_60_120"};
	sumList["mu_ggF_GE2J"] 			    = {"XS_ggF_ggF_GE2J", "XS_bbH_ggF_GE2J"};
	sumList["mu_ggF_PTH_GT200"] 		= {"XS_ggF_ggF_PTH_GT200", "XS_bbH_ggF_PTH_GT200"};
	sumList["mu_VBF_qq2qq_PTH_GT200"] 	= {"XS_VBF_VBF_qq2qq_PTH_GT200"};
	sumList["mu_VBF_qq2qq_PTH_LE200"] 	= {"XS_VBF_VBF_qq2qq_PTH_LE200"};
	sumList["mu_VH_Had"] 				= {"XS_VH_VH_Had"};
	sumList["mu_VH_Lep"] 				= {"XS_VH_VH_Lep"};
	sumList["mu_ttH"] 					= {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};


	sumList["mu_gg2H_0J_PTH_0_10"] 		= {"XS_gg2H_gg2H_0J_PTH_0_10", 		"XS_bb2H_gg2H_0J_PTH_0_10"};
	sumList["mu_gg2H_0J_PTH_GT10"] 		= {"XS_gg2H_gg2H_0J_PTH_GT10", 		"XS_bb2H_gg2H_0J_PTH_GT10"};
	sumList["mu_gg2H_1J_PTH_0_60"] 		= {"XS_gg2H_gg2H_1J_PTH_0_60", 		"XS_bb2H_gg2H_1J_PTH_0_60"};
	sumList["mu_gg2H_1J_PTH_120_200"] 	= {"XS_gg2H_gg2H_1J_PTH_120_200", 	"XS_bb2H_gg2H_1J_PTH_120_200"};
	sumList["mu_gg2H_1J_PTH_60_120"] 	= {"XS_gg2H_gg2H_1J_PTH_60_120", 	"XS_bb2H_gg2H_1J_PTH_60_120"};
	sumList["mu_gg2H_GE2J"] 			= {"XS_gg2H_gg2H_GE2J", 			"XS_bb2H_gg2H_GE2J"};
	sumList["mu_gg2H_PTH_GT200"] 		= {"XS_gg2H_gg2H_PTH_GT200", 		"XS_bb2H_gg2H_PTH_GT200"};
	sumList["mu_qq2Hqq_mJJ_GT350_PTH_GT200"]	= {"XS_qq2Hqq_qq2Hqq_mJJ_GT350_PTH_GT200"};
	sumList["mu_qqH2qq_VHHad"] 					= {"XS_qq2Hqq_qqH2qq_VHHad"};
	sumList["mu_qqH2qq_rest"] 					= {"XS_qq2Hqq_qqH2qq_rest"};
	sumList["mu_qq2Hll"] 					= {"XS_qq2Hll_qq2Hll"};





	sumList["mu"] 	= {"XS_ggF_ggF", "XS_bbH_ggF", "XS_VBF_VBF", "XS_VH_VH", "XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};



	sumList["r_ZZ_0jet"] 	= {"N_qqZZ_all_0jet_Pt4l_0_10_4l_13TeV","N_qqZZ_all_0jet_Pt4l_10_100_4l_13TeV","N_qqZZ_all_0jet_Pt4l_GE100_4l_13TeV","N_qqZZ_all_SB_0jet_4l_13TeV","N_qqZZ_all_SB_VHLep_4l_13TeV","N_qqZZ_all_VHLep_4l_13TeV",
	"N_ggZZ_all_0jet_Pt4l_0_10_4l_13TeV","N_ggZZ_all_0jet_Pt4l_10_100_4l_13TeV","N_ggZZ_all_SB_0jet_4l_13TeV","N_ggZZ_all_SB_VHLep_4l_13TeV","N_ggZZ_all_VHLep_4l_13TeV",
	"N_EWZZjj_all_0jet_Pt4l_0_10_4l_13TeV","N_EWZZjj_all_0jet_Pt4l_10_100_4l_13TeV","N_EWZZjj_all_SB_0jet_4l_13TeV","N_EWZZjj_all_SB_VHLep_4l_13TeV","N_EWZZjj_all_VHLep_4l_13TeV",};
	sumList["r_ZZ_1jet"] 	= {"N_qqZZ_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_qqZZ_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_qqZZ_all_1jet_Pt4l_120_200_4l_13TeV","N_qqZZ_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_qqZZ_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_qqZZ_all_1jet_Pt4l_GE200_4l_13TeV","N_qqZZ_all_SB_1jet_4l_13TeV",
	"N_ggZZ_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_ggZZ_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_ggZZ_all_1jet_Pt4l_120_200_4l_13TeV","N_ggZZ_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_ggZZ_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_ggZZ_all_1jet_Pt4l_GE200_4l_13TeV","N_ggZZ_all_SB_1jet_4l_13TeV",
	"N_EWZZjj_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_EWZZjj_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_EWZZjj_all_1jet_Pt4l_120_200_4l_13TeV","N_EWZZjj_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_EWZZjj_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_EWZZjj_all_1jet_Pt4l_GE200_4l_13TeV","N_EWZZjj_all_SB_1jet_4l_13TeV",};
	sumList["r_ZZ_2jet"] 	= {"N_qqZZ_all_2jet_4l_13TeV_b1","N_qqZZ_all_2jet_4l_13TeV_b2","N_qqZZ_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_qqZZ_all_SB_2jet_4l_13TeV","N_ggZZ_all_2jet_4l_13TeV_b1",
	"N_ggZZ_all_2jet_4l_13TeV_b2","N_ggZZ_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_ggZZ_all_SB_2jet_4l_13TeV",
	"N_EWZZjj_all_2jet_4l_13TeV_b1","N_EWZZjj_all_2jet_4l_13TeV_b2","N_EWZZjj_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_EWZZjj_all_SB_2jet_4l_13TeV",};
	sumList["r_ttV"] 	= {"N_ttV_all_SB_ttH_4l_13TeV", "N_ttV_all_ttH_hadronic_4l_13TeV_b1", "N_ttV_all_ttH_hadronic_4l_13TeV_b2", "N_ttV_all_ttH_leptonic_4l_13TeV"};


	return sumList.at(poiName);
}

vector<TString> varToMult(TString poiName)
{
	map<TString, vector<TString>> sumList;
	sumList["mu_ggF"] 	= {};
	sumList["mu_VBF"] 	= {};
	sumList["mu_VH"] 	= {};
	sumList["mu_ttH"] 	= {};

	sumList["mu"] 		= {};


	sumList["mu_ggF_0J_PTH_0_10"] 	= {};
	sumList["mu_ggF_0J_PTH_GT10"] 	= {};
	sumList["mu_ggF_1J_PTH_0_60"] 	= {};
	sumList["mu_ggF_1J_PTH_120_200"] 	= {};
	sumList["mu_ggF_1J_PTH_60_120"] 	= {};
	sumList["mu_ggF_GE2J"] 			= {};
	sumList["mu_ggF_PTH_GT200"] 		= {};
	sumList["mu_VBF_qq2qq_PTH_GT200"] 	= {};
	sumList["mu_VBF_qq2qq_PTH_LE200"] 	= {};
	sumList["mu_VH_Had"] 				= {};
	sumList["mu_VH_Lep"] 				= {};
	sumList["mu_ttH"] 						= {};


	sumList["mu_gg2H_0J_PTH_0_10"] 	= {};
	sumList["mu_gg2H_0J_PTH_GT10"] 	= {};
	sumList["mu_gg2H_1J_PTH_0_60"] 	= {};
	sumList["mu_gg2H_1J_PTH_120_200"] 	= {};
	sumList["mu_gg2H_1J_PTH_60_120"] 	= {};
	sumList["mu_gg2H_GE2J"] 			= {};
	sumList["mu_gg2H_PTH_GT200"] 		= {};
	sumList["mu_qq2Hqq_mJJ_GT350_PTH_GT200"] 	= {};
	sumList["mu_qqH2qq_VHHad"] 	= {};
	sumList["mu_qqH2qq_rest"] 				= {};
	sumList["mu_qq2Hll"] 				= {};
	sumList["mu_ttH"] 						= {};


	sumList["r_ZZ_0jet"] 	= {"Lumi_H4l"};
	sumList["r_ZZ_1jet"] 	= {"Lumi_H4l"};
	sumList["r_ZZ_2jet"] 	= {"Lumi_H4l"};
	sumList["r_ttV"] 		= {"Lumi_H4l"};

	return sumList.at(poiName);
}


FormatOut pdfRound(ScanInfo sInfo, double lwrErrOver, double uprErrOver,  int addExtraDecimalPlace)
{
	// Lets symtrize the Sys
	double val = sInfo.bestFitVal;
	double err = (fabs(sInfo.lwrErr) + fabs(sInfo.uprErr))/2;


	double symErrorForRounding = err;

	double smallErrForRounding = fabs(sInfo.lwrErr);
	if(fabs(sInfo.uprErr) < smallErrForRounding) smallErrForRounding = fabs(sInfo.uprErr);


	if(lwrErrOver != 0)
	{
		symErrorForRounding = (fabs(lwrErrOver) + fabs(uprErrOver))/2;

		smallErrForRounding = fabs(lwrErrOver);
		if(fabs(uprErrOver) < smallErrForRounding) smallErrForRounding = fabs(uprErrOver);
	}

	double smallErr = fabs(sInfo.lwrErr);
	if(fabs(sInfo.uprErr) < smallErr) smallErr = fabs(sInfo.uprErr);
	if(fabs(lwrErrOver) < smallErr && lwrErrOver != 0) smallErr = fabs(lwrErrOver);
	if(fabs(uprErrOver) < smallErr && uprErrOver != 0) smallErr = fabs(uprErrOver);

	auto threeDigits = [](float x) 
	{ 
		TString toParse;
		toParse.Form("%.2e", x);
		TRegexp re("e.*");
		toParse(re) = "";
		toParse = toParse.ReplaceAll(".","").ReplaceAll("+","").ReplaceAll("-","");
		return toParse.Atoi();
	};

	auto nSignificantDigits = [](int threeDigits) 
	{ 
		if(threeDigits < 101) return 2;
		if(threeDigits < 356) return 2;
		if(threeDigits < 950) return 1;
		return 2;
	};


	auto frexp10 = [](float x) 
	{ 
		TString toParse;
		toParse.Form("%e", x);
		TRegexp upre("e.*");
		TRegexp dWre(".*e");
		TString upStr = toParse;
		TString dwStr = toParse;

		upStr(upre) = "";
		dwStr(dWre) = "";
		return vector<double> {upStr.Atof(), dwStr.Atof()};
	};


	auto nDigitsValue = [](double expVal, double expErr, double nDigitsErr) 
	{ 
		return expVal-expErr+nDigitsErr;
	};


	auto formatValue = [](float value, int exponent, int nDigits, int extraRound) 
	{ 
		auto roundAt = nDigits - 1 - exponent - extraRound;
		auto nDec = 0;
		if(exponent < nDigits) nDec = roundAt;
		if(nDec < 0) nDec = 0;

		TString roundExp = "\%." + TString::Itoa(nDec, 10) + "f";
		TString outFormat = "";
		outFormat.Form(roundExp, value);
		return outFormat;
	};


	// For symterized error
	auto tD_ave = threeDigits(symErrorForRounding);

	auto nD_ave = nSignificantDigits(tD_ave) + addExtraDecimalPlace;

    auto expVal_ave = frexp10(val)[1];
    auto expErr_ave = frexp10(symErrorForRounding)[1];

    double extraRound_ave = 0;
    if(tD_ave >= 950) extraRound_ave = 1;


    FormatOut formatOut;

    formatOut.cAveVal = formatValue(val, expVal_ave, nDigitsValue(expVal_ave, expErr_ave, nD_ave), extraRound_ave);
    formatOut.aveErr  = formatValue(err, expErr_ave, nD_ave, extraRound_ave);


    double checkVal = formatOut.cAveVal.Atof();

    // if this is zero, add extra digit
    if(checkVal == 0)
    {
    	return pdfRound(sInfo, lwrErrOver, uprErrOver,  addExtraDecimalPlace+1);
    }

    // For upper and lower error
	auto tD = threeDigits(smallErrForRounding);
	auto nD = nSignificantDigits(tD) + addExtraDecimalPlace;

	// cout<<"smallErr: "<<smallErr<<" tD: "<<tD<<" nD: "<<nD<<endl;

    auto expVal = frexp10(val)[1];
    auto expErr = frexp10(smallErrForRounding)[1];

    double extraRound = 0;
    if(tD >= 950) extraRound = 1;


    formatOut.cVal 	  = formatValue(val, expVal, nDigitsValue(expVal, expErr, nD), extraRound);
    formatOut.lwrErr  = formatValue(sInfo.lwrErr, expErr, nD, extraRound);
    formatOut.uprErr  = formatValue(sInfo.uprErr, expErr, nD, extraRound);

	return formatOut;
}

TString getFancySTXSname(TString name)
{


    TString fancyName = "";		
    if(name.EqualTo("mu_ggF")  || name.EqualTo("XS_ggF"))  		fancyName = "\\STXSggF";
    else if(name.EqualTo("mu_VBF")   || name.EqualTo("XS_VBF"))   	fancyName = "\\STXSVBF";
    else if(name.EqualTo("mu_VH")    || name.EqualTo("XS_VH"))    	fancyName = "\\STXSVH";
    else if(name.EqualTo("mu_ttH")   || name.EqualTo("XS_ttH"))   	fancyName = "\\STXSttH";


    else if(name.EqualTo("mu_ggF_0J_PTH_0_10"))          fancyName = "\\STXSggFZeroJL";
    else if(name.EqualTo("mu_ggF_0J_PTH_GT10"))          fancyName = "\\STXSggFZeroJH";
    else if(name.EqualTo("mu_ggF_1J_PTH_0_60")          || name.EqualTo("XS_ggF_1J_PTH_0_60"))         fancyName = "\\STXSggFOneJL";
    else if(name.EqualTo("mu_ggF_1J_PTH_60_120")        || name.EqualTo("XS_ggF_1J_PTH_60_120"))       fancyName = "\\STXSggFOneJM";
    else if(name.EqualTo("mu_ggF_1J_PTH_GT120")         || name.EqualTo("XS_ggF_1J_PTH_GT120") || name.EqualTo("mu_ggF_1J_PTH_120_200") )        fancyName = "\\STXSggFOneJH";
    else if(name.EqualTo("mu_ggF_GE2J")                 || name.EqualTo("XS_ggF_GE2J"))                fancyName = "\\STXSggFTwoJ";
    else if(name.EqualTo("mu_ggF_PTH_GT200")                 || name.EqualTo("XS_ggF_PTH_GT200"))                fancyName = "\\STXSggFHigh";

    else if(name.EqualTo("mu_ttH_all")                   || name.EqualTo("XS_ttH_all"))                  fancyName = "\\STXSttH";
    else if(name.EqualTo("mu_VBF_qq2qq_PTJET1_GT200")    || name.EqualTo("XS_VBF_qq2qq_PTJET1_GT200"))   fancyName = "\\STXSVBFHigh";
    else if(name.EqualTo("mu_VBF_qq2qq_PTJET1_LE200")    || name.EqualTo("XS_VBF_qq2qq_PTJET1_LE200"))   fancyName = "\\STXSVBFLow";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_GT200"))   fancyName = "\\STXSVBFHigh";
    else if(name.EqualTo("mu_VBF_qq2qq_PTH_LE200"))   fancyName = "\\STXSVBFLow";
    else if(name.EqualTo("mu_VH_Had")                    || name.EqualTo("XS_VH_Had"))                   fancyName = "\\STXSVHHad";
    else if(name.EqualTo("mu_VH_Lep")                    || name.EqualTo("XS_VH_Lep"))                   fancyName = "\\STXSVHLep";

    else if(name.EqualTo("mu_gg2H_0J_PTH_0_10"))          	fancyName = "\\STXSggToHZeroJL";
    else if(name.EqualTo("mu_gg2H_0J_PTH_GT10"))          	fancyName = "\\STXSggToHZeroJH";
    else if(name.EqualTo("mu_gg2H_1J_PTH_0_60"))          	fancyName = "\\STXSggToHOneJL";
    else if(name.EqualTo("mu_gg2H_1J_PTH_60_120"))          fancyName = "\\STXSggToHOneJM";
    else if(name.EqualTo("mu_gg2H_1J_PTH_120_200"))         fancyName = "\\STXSggToHOneJH";
    else if(name.EqualTo("mu_gg2H_GE2J"))          			fancyName = "\\STXSggToHTwoJ";
    else if(name.EqualTo("mu_gg2H_PTH_GT200"))         		fancyName = "\\STXSggToHHigh";
    else if(name.EqualTo("mu_qqH2qq_rest"))          		fancyName = "\\STXSqqtoHqqRest";
    else if(name.EqualTo("mu_qqH2qq_VHHad"))          		fancyName = "\\STXSqqtoHqqVHLike";
    else if(name.EqualTo("mu_qq2Hqq_mJJ_GT350_PTH_GT200"))  fancyName = "\\STXSqqtoHqqBSM";
    else if(name.EqualTo("mu_qq2Hll"))         	 			fancyName = "\\STXSVHLep";

    return fancyName;
}

pair<double, double> getThSys(TString poiName)
{
    map<TString, pair<double, double>> thVars;

    // cout<<fileName<<endl;

    TString baseFileName = poiName + ".txt";
    baseFileName.ReplaceAll("XS_", "");

    TString S0FileName = "sysFiles/S0/" + baseFileName;
    TString RS1p1FileName = "sysFiles/RS1p1/" + baseFileName;
    TString RS1p1qqFFileName = "sysFiles/RS1p1qqH/" + baseFileName;
    ifstream myfile (S0FileName);
    if(!myfile.is_open() )    myfile.open(RS1p1FileName);
    if(!myfile.is_open() )    myfile.open(RS1p1qqFFileName);

    string line;    
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {

            if(line.length() == 0) continue;

            // actual info
            auto splitList = splitString(line, " ");
            TString sysName = splitList[0];
            double lwrSys = TString(splitList[1]).Atof();
            double uprSys = TString(splitList[2]).Atof();

            if(std::isnan(lwrSys) || std::isnan(uprSys)) continue;
            double ave = (fabs(lwrSys) + fabs(uprSys))/2;
            // if(ave>0.01)cout<<sysName<<" "<<lwrSys<<" "<<uprSys<<endl;

            pair<double, double> currVar{lwrSys, uprSys};
            thVars[sysName] = currVar;
        }
        myfile.close();
    }

    double upSqr = 0;
    double downSqr = 0;

    for(const auto& currTh: thVars)
    {
        upSqr += pow(currTh.second.first,2);
        downSqr += pow(currTh.second.second,2);
    }
    // cout<<"upSqr "<<upSqr<<" downSqr: "<<downSqr<<endl;
    
    pair<double, double> outVal = pair<double, double>{sqrt(upSqr)/100, sqrt(downSqr)/100};
    
    return outVal;
}
