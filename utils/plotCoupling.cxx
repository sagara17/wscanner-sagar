#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TRegexp.h>
#include <TColor.h>
#include <TLegend.h>
#include <TGaxis.h>
#include <TString.h>
#include <stdio.h>
#include <TGraphAsymmErrors.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"

using namespace std;

// Global vars
map<TString,TString> opts;

struct ScanInfo
{
	double bestFitVal;
	double lwrErr;
	double uprErr;

	TString POIName;
	TString scanType;
	TString dataType;
};


struct PlotStruct
{
    TGraphAsymmErrors* gr;
    TString POIName;
};

// Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
vector<ScanInfo> getPOIInfo(TString folderName, TString POIList, TString dataLabel, TString scanLabel, TString dataType, TString scanType);
std::map<TString, TString> commonMetaData;
void setPrettyStuff();

void getMaxMin(vector<PlotStruct> gList, double& minXF, double& maxXF);
void formatGraph(PlotStruct plotGraph);

vector<PlotStruct> getGraph(std::vector<TString> uniquePOIList, vector<ScanInfo> scanInfo, double xOffset = 0, double yOffSet = 0);

RooWorkspace* m_ws;
void fillWS(TString basefolderName, TString POIList, TString dataLabel, TString scanLabel);
PlotStruct getThPrediction(vector<PlotStruct> sysGraph, bool normalized, TString folderName);
vector<PlotStruct> getLegendEntry(TString type, double shiftedminXF, double shiftedmaxXF);

double getThPrediction(TString POIName);
vector<TString> varToSum(TString poiName);
pair<double, double> getThSys(TString fileName);
vector<TLatex*> POIInfo(double maxXF, double minXF, vector<PlotStruct> obsList, PlotStruct thList);
vector<TLatex*> POIInfoZZ(double maxXF, double minXF, vector<PlotStruct> obsList);
TString pdfRound(double val, double lwrErr, double uprErr, int addExtraDigit = 0);

map<TString, TString> pValList;


TString type ;

int main(int argc, char** argv)
{
    if (!cmdline(argc,argv,opts)) return 0;
    setPrettyStuff();

    map<TString, map<TString, vector<ScanInfo>>> scanInfoMap;

    type = opts["type"];

    vector<TString> labelList;
    // Read the information
    if(type.EqualTo("S0"))     labelList    = {"inc", "S0"};
    if(type.EqualTo("RS1p1"))  labelList    = {"RS1p1"};
    if(type.EqualTo("S0qqH"))     labelList = {"inc", "S0qqH"};
    if(type.EqualTo("RS1p1qqH"))  labelList = {"RS1p1qqH"};

    pValList["S0"]          = "91%";
    pValList["RS1p1"]       = "73%";
    pValList["RS1p1qqH"]    = "77%";


    for(const auto& label: labelList)
    {
        scanInfoMap[label]["asimov_StatOnly"] = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + label], opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly");
        scanInfoMap[label]["asimov_SysAll"]   = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + label], opts["asimovLabel"], opts["sysAllLabel"],   "asimov", "sysAll");

        scanInfoMap[label]["data_StatOnly"] = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + label], opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly");
        scanInfoMap[label]["data_SysAll"]   = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + label], opts["dataLabel"], opts["sysAllLabel"],   "data", "sysAll");
    
        scanInfoMap[label]["ZZ_asimov_StatOnly"] = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + "ZZ"], opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly");
        scanInfoMap[label]["ZZ_asimov_SysAll"]   = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + "ZZ"], opts["asimovLabel"], opts["sysAllLabel"],   "asimov", "sysAll");

        scanInfoMap[label]["ZZ_data_StatOnly"] = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + "ZZ"], opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly");
        scanInfoMap[label]["ZZ_data_SysAll"]   = getPOIInfo(opts[TString("folderList_") + label], opts[TString("poiList_") + "ZZ"], opts["dataLabel"], opts["sysAllLabel"],   "data", "sysAll");
    }


    // Lets get the graph
    map<TString, vector<PlotStruct>> sysGraphList;
    map<TString, vector<PlotStruct>> statGraphList;

    map<TString, vector<PlotStruct>> ZZ_sysGraphList;
    map<TString, vector<PlotStruct>> ZZ_statGraphList;

    for(const auto& label: labelList)
    {
        int yOffSet = 0;
        if(label.Contains("S0")) yOffSet++;
        if(label.Contains("RS1p1")) yOffSet++;
        auto poiList = tokenizeStr(opts.at(TString("poiList_") + label), ",");
        std::reverse(poiList.begin(),poiList.end());
        cout << "The poiList contains: " << endl;
        for(auto poi:poiList) cout << poi << endl;
        cout << "\n\n";

        TString datasetlabel = opts["dataset"];


        statGraphList[label]    = getGraph(poiList, scanInfoMap[label][datasetlabel + "_StatOnly"], 0, yOffSet);
        sysGraphList[label]     = getGraph(poiList, scanInfoMap[label][datasetlabel + "_SysAll"], 0, yOffSet);


        yOffSet = 0;
        poiList = tokenizeStr(opts.at(TString("poiList_") + "ZZ"), ",");
        std::reverse(poiList.begin(),poiList.end());
        
        ZZ_statGraphList[label]    = getGraph(poiList, scanInfoMap[label]["ZZ_" + datasetlabel + "_StatOnly"], 0, yOffSet);
        ZZ_sysGraphList[label]     = getGraph(poiList, scanInfoMap[label]["ZZ_" + datasetlabel + "_SysAll"], 0, yOffSet);

    }


    // These are the vectors that will be plot in the end
    vector<PlotStruct> sysGraph;
    vector<PlotStruct> statGraph;
    vector<PlotStruct> ZZ_sysGraph;
    vector<PlotStruct> ZZ_statGraph;


    if(type.Contains("S0") || type.Contains("S0qqH"))
    {
        // if you want mu in
        sysGraph = sysGraphList.at("inc");
        sysGraph.insert( sysGraph.end(), sysGraphList.at(type).begin(), sysGraphList.at(type).end() );
        statGraph = statGraphList["inc"];
        statGraph.insert( statGraph.end(), statGraphList.at(type).begin(), statGraphList.at(type).end() );

        // sysGraph = sysGraphList.at(type);
        // statGraph = statGraphList.at(type);

    }
    else
    {
        sysGraph = sysGraphList.at(type);
        statGraph = statGraphList.at(type);
    }
    ZZ_sysGraph  = ZZ_sysGraphList.at(type);
    ZZ_statGraph = ZZ_statGraphList.at(type);


    fillWS(opts[TString("folderList_") + type], opts[TString("poiList_") + type], opts["dataLabel"], opts["statOnlyLabel"]);
    if(!m_ws) fillWS(opts[TString("folderList_") + type], opts[TString("poiList_") + type], opts["asimovLabel"], opts["statOnlyLabel"]);
    // Get the frame

	// Start plotting the graph
    double minXF = 10000000000000;
    double maxXF = -1000000000000;
    getMaxMin(sysGraph, minXF, maxXF);


    // Get the TH graph
    auto thPrediction   = getThPrediction(sysGraph, true,  "sysFiles/"+type+"/");
    auto thXSPrediction = getThPrediction(sysGraph, false, "sysFiles/"+type+"/");
    cout << "got pred" << endl;

    int numPOI = sysGraph.size();

    double shiftedminXF = minXF - (maxXF - minXF) * 0.10;
    double shiftedmaxXF = maxXF + (maxXF - minXF) * 0.60;

    // double labelStat    = maxXF + (maxXF - minXF) * 0.325;
    // double labelSys     = maxXF + (maxXF - minXF) * 0.05;

    int offSet = 4;
    if(numPOI > 8) offSet += 3 ;
    if(type.Contains("RS1p1")) offSet += 1 ;
    TH2D* axisHist = new TH2D("axis","axis", 1, shiftedminXF, shiftedmaxXF, numPOI+offSet  , 0, numPOI+offSet);   
    // axisHist->GetYaxis()->SetTitle("POI");
    axisHist->GetXaxis()->SetTitle("#sigma#upoint#it{B}/(#sigma#upoint#it{B})_{SM}");
    axisHist->GetXaxis()->SetTitleSize(0.8*axisHist->GetXaxis()->GetTitleSize());
    if(numPOI > 8) axisHist->GetYaxis()->SetLabelSize(0.6*axisHist->GetYaxis()->GetLabelSize());
    axisHist->GetXaxis()->SetTickSize(0);
    axisHist->GetYaxis()->SetTickSize(0);



    int axisOffset = 0;
    if(type.Contains("RS1p1")) axisOffset += 1 ;

    TGaxis* axis_theta = new TGaxis(axisHist->GetXaxis()->GetXmin(), axisHist->GetYaxis()->GetXmin()+axisOffset, axisHist->GetXaxis()->GetXmin(), numPOI+axisOffset, axisHist->GetYaxis()->GetXmin()+axisOffset, numPOI+axisOffset, 510);
    axis_theta->ImportAxisAttributes(axisHist->GetXaxis());
    axis_theta->SetName("axis_theta");
    axis_theta->SetTitleOffset(1.1);
    axis_theta->SetLineColor(kBlack);
    axis_theta->SetLabelColor(kBlack);
    axis_theta->SetTitleColor(kBlack);
    axis_theta->SetLabelSize(0);
    axis_theta->SetTitleSize(0);
    axis_theta->SetNdivisions(1 * (numPOI));

    TGaxis* axis_thetaLeft = new TGaxis(axisHist->GetXaxis()->GetXmax(), numPOI+axisOffset , axisHist->GetXaxis()->GetXmax(), axisHist->GetYaxis()->GetXmin()+axisOffset, axisHist->GetYaxis()->GetXmin()+axisOffset, numPOI+axisOffset, 510,"-");
    // axis_thetaLeft->ImportAxisAttributes(axisHist->GetXaxis());
    axis_thetaLeft->SetName("axis_thetaLeft");
    axis_thetaLeft->SetTitleOffset(1.1);
    axis_thetaLeft->SetLineColor(kBlack);
    axis_thetaLeft->SetLabelColor(kBlack);
    axis_thetaLeft->SetTitleColor(kBlack);
    axis_thetaLeft->SetLabelSize(0);
    axis_thetaLeft->SetOption("-");
    axis_thetaLeft->SetTitleSize(0);
    axis_thetaLeft->SetNdivisions(1 * (numPOI));


    TGaxis* axis_bottom = new TGaxis(axisHist->GetXaxis()->GetXmin(), axisHist->GetYaxis()->GetXmin(), axisHist->GetXaxis()->GetXmax(), axisHist->GetYaxis()->GetXmin(), axisHist->GetXaxis()->GetXmin(), axisHist->GetXaxis()->GetXmax(), 510, "+S");
    axis_bottom->SetName("axis_bottom");
    axis_bottom->SetLineColor(kBlack);
    axis_bottom->SetLabelColor(kBlack);
    axis_bottom->SetTitleColor(kBlack);
    axis_bottom->SetLabelSize(0);
    axis_bottom->SetTitleSize(0);
    axis_bottom->SetTickLength(0.015);
    axis_bottom->SetTickSize(0.015);
    // gStyle->SetTickLength(0.01,"x");
    // axis_bottom->SetNdivisions(1 * (numPOI));




    int counter = 0;
    if(type.Contains("RS1p1")) counter += 1 ;

    for(const auto& g: sysGraph)
    {
    	axisHist->GetYaxis()->SetBinLabel(counter + 1, getFancyXname(g.POIName));
    	counter++;
    }
	axisHist->GetXaxis()->LabelsOption("v");

    // Fancy title stuff
    double leftDist = 0.19;
    if(numPOI > 8) leftDist = 0.19;


    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 700, 600);
    if(numPOI > 8) c1->SetLeftMargin(0.16);
    if(numPOI > 8) c1->SetBottomMargin(0.125);

    double lLwr = 0.64;
    if(type.Contains("RS1p1")) lLwr = 0.66;
    TLegend *elLeg =  new TLegend (leftDist, lLwr, leftDist+0.45, lLwr + 0.075);
    elLeg->SetFillColor(0);
    elLeg->SetBorderSize(0);
    elLeg->SetTextFont(42);
    elLeg->SetTextSize(0.025);

    vector<PlotStruct> legEntry =  getLegendEntry(type, shiftedminXF, shiftedmaxXF);
    elLeg->AddEntry(legEntry[0].gr, "Observed: Stat+Sys", "lp");
    elLeg->AddEntry(thPrediction.gr, "SM Prediction", "f");
    elLeg->AddEntry(legEntry[0].gr, "Observed: Stat-Only", "p");
    elLeg->SetNColumns(2);


    // draw the canvas
    axisHist->Draw();
    axis_theta->Draw();
    axis_thetaLeft->Draw();
    axis_bottom->Draw();



    thPrediction.gr->Draw("2");

    TLine l;
    l.SetLineWidth(1.0);
    l.SetLineColor(kGray + 3);
    l.SetLineStyle(2);
    l.DrawLine( 1. , axisOffset,  1. , numPOI+axisOffset);

    TString saveName = "";
    for(const auto& grl: sysGraph) grl.gr->Draw("ZP");
	gStyle->SetEndErrorSize(5.0);
    for(const auto& grl: statGraph) grl.gr->Draw("[]");

    ATLASLabel(leftDist, 0.875, "Internal", 1);

    TString channelStr = "H #rightarrow ZZ* #rightarrow 4l";
    TLatex* tMain = new TLatex (leftDist, 0.825, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.785, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    TString stxsString = "Stage 0 - |y_{H}| < 2.5";
    if(type.Contains("RS1p1qqFine")) stxsString = "STXS Reduced Stage 1.1 - |y_{H}| < 2.5";
    else if(type.Contains("RS1p1")) stxsString = "Reduced Stage 1.1 - |y_{H}| < 2.5";
    TLatex* stxsInfo = new TLatex (leftDist, 0.750, stxsString);
    stxsInfo->SetNDC();
    stxsInfo->SetTextSize(0.03);
    stxsInfo->SetTextFont(42);
    stxsInfo->Draw();


    auto latexList = POIInfo(shiftedmaxXF, shiftedminXF, sysGraph, thXSPrediction);


	gPad->RedrawAxis();


    {
        //ZZ
        TPad* p2 = 0;
        p2 = new TPad("p2","p2", 0.56, 0.71, 0.96, 0.94);
        if(numPOI > 8) p2 = new TPad("p2","p2", 0.56, 0.71, 0.96, 0.94);
        p2->SetTopMargin(0.);
        p2->SetLeftMargin(0.12);
        p2->SetBottomMargin(0.25);

        c1->cd();
        p2->Draw("same");   
        p2->cd();

        // Start plotting the graph
        double ZZ_minXF = 10000000000000;
        double ZZ_maxXF = -1000000000000;
        getMaxMin(ZZ_sysGraph, ZZ_minXF, ZZ_maxXF);

        int ZZ_numPOI = ZZ_sysGraph.size();

        // double ZZ_shiftedminXF = ZZ_minXF - (ZZ_maxXF - ZZ_minXF) * 0.10;
        // double ZZ_shiftedmaxXF = ZZ_maxXF + (ZZ_maxXF - ZZ_minXF) * 0.60;

        double ZZ_shiftedminXF = ZZ_minXF - (ZZ_maxXF - ZZ_minXF) * 0.05;
        double ZZ_shiftedmaxXF = ZZ_maxXF + (ZZ_maxXF - ZZ_minXF) * 0.10;

        // double ZZ_labelStat    = ZZ_maxXF + (ZZ_maxXF - ZZ_minXF) * 0.325;
        // double ZZ_labelSys     = ZZ_maxXF + (ZZ_maxXF - ZZ_minXF) * 0.05;

        int ZZ_offSet = 0;

        TH2D* ZZ_axisHist = new TH2D("axisZZ","axisZZ", 1, ZZ_shiftedminXF, ZZ_shiftedmaxXF, ZZ_numPOI + ZZ_offSet  , 0, ZZ_numPOI + ZZ_offSet);   
        // axisHist->GetYaxis()->SetTitle("POI");
        ZZ_axisHist->GetXaxis()->SetTitle("N/N_{SM}");
        ZZ_axisHist->GetXaxis()->SetTitleSize(1.5 * ZZ_axisHist->GetXaxis()->GetTitleSize());
        ZZ_axisHist->GetXaxis()->SetLabelSize(1.8 * ZZ_axisHist->GetXaxis()->GetLabelSize());
        ZZ_axisHist->GetYaxis()->SetLabelSize(2.5 * ZZ_axisHist->GetYaxis()->GetLabelSize());
        ZZ_axisHist->GetXaxis()->SetTickSize(0);



        TGaxis* ZZ_axis_bottom = new TGaxis(ZZ_axisHist->GetXaxis()->GetXmin(), ZZ_axisHist->GetYaxis()->GetXmin(), ZZ_axisHist->GetXaxis()->GetXmax(), ZZ_axisHist->GetYaxis()->GetXmin(), ZZ_axisHist->GetXaxis()->GetXmin(), ZZ_axisHist->GetXaxis()->GetXmax(), 510,"+S");
        ZZ_axis_bottom->SetName("axis_bottom");
        ZZ_axis_bottom->SetLineColor(kBlack);
        ZZ_axis_bottom->SetLabelColor(kBlack);
        ZZ_axis_bottom->SetTitleColor(kBlack);
        ZZ_axis_bottom->SetLabelSize(0);
        ZZ_axis_bottom->SetTitleSize(0);
        ZZ_axis_bottom->SetTickLength(0.05);
        ZZ_axis_bottom->SetTickSize(0.05);



        int counter = 0;
        for(const auto& g: ZZ_sysGraph)
        {
            ZZ_axisHist->GetYaxis()->SetBinLabel(counter + 1, getFancyXname(g.POIName));
            counter++;
        }
        ZZ_axisHist->GetXaxis()->LabelsOption("v");

        ZZ_axisHist->Draw();

        TLine l;
        l.SetLineWidth(1.0);
        l.SetLineColor(kGray + 3);
        l.SetLineStyle(2);
        l.DrawLine( 1. , 0.,  1. , ZZ_axisHist->GetYaxis()->GetXmax());

        for(const auto& grl: ZZ_sysGraph) grl.gr->Draw("ZP");
        ZZ_axis_bottom->Draw();
        gStyle->SetEndErrorSize(5.0);
        // for(const auto& grl: ZZ_statGraph) grl.gr->Draw("[]");


        //auto ZZ_latexList = POIInfoZZ(ZZ_shiftedmaxXF, ZZ_shiftedminXF, ZZ_sysGraph);
        //for(const auto& l: ZZ_latexList) l->Draw();


    }

    c1->cd();
    // for(auto& grl: sysGraph) elLeg->AddEntry(grl.gr, labelMap[gr.first], "PE");
    elLeg->Draw();
    // legEntry[0].gr->Draw("psame");
    legEntry[0].gr->Draw("[]same");

    for(const auto& l: latexList) l->Draw();

    gPad->RedrawAxis();


   
    c1->SaveAs(type + "_summary.eps");
}


vector<PlotStruct> getGraph(std::vector<TString> uniquePOIList, vector<ScanInfo> scanInfo, double xOffset, double yOffSet)
{
	double counter = 0.5;

    vector<PlotStruct> plotStructList;
	for(const auto& poiName: uniquePOIList)
	{
		for(const auto& poiInfo: scanInfo)
		{

            vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;

			if(!poiInfo.POIName.EqualTo(poiName)) continue;
			y.push_back(counter + yOffSet);
			yErrUpr.push_back(0);
			yErrLwr.push_back(0);

			x.push_back(poiInfo.bestFitVal + xOffset);
			xErrUpr.push_back(fabs(poiInfo.uprErr) + xOffset);
			xErrLwr.push_back(fabs(poiInfo.lwrErr) + xOffset);

            auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);

            PlotStruct plotStruct;
            plotStruct.gr = gr;
            plotStruct.POIName = poiName;
            formatGraph(plotStruct);
            plotStructList.push_back(plotStruct);
		}
		counter+=1;
	}

	return plotStructList;
}


vector<ScanInfo> getPOIInfo(TString basefolderName, TString POIList, TString dataLabel, TString scanLabel, TString dataType, TString scanType)
{
	TString folderName = basefolderName + "/" + scanLabel + "/" + dataLabel + "/";

	vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(POIList, ",");
    std::reverse(poiList.begin(),poiList.end());

    // read all the info
    for(const auto& poiName: poiList)
    {
    	TString fileName = folderName + "/" + poiName + ".root";
        ScanInfo sInfo;
        sInfo.POIName  = poiName;
        sInfo.dataType = dataType;
        sInfo.scanType = scanType;
        // cout<<"Checking: "<<fileName<<endl;
    	if(doesExist(fileName.Data()))
    	{
                    // cout<<"found: "<<fileName<<endl;

	    	auto label = getLabels(vector<TString>{fileName}).at(0);
	    	commonMetaData = label;
			
			TChain *chain = new TChain("results");
	        chain->Add(fileName);

	        // Read the NLL into a map
	        map<double,double> NLL = read1DVar("nll", chain);
	        fixOffset(NLL);
	        printFitInfo(NLL, sInfo.bestFitVal, sInfo.lwrErr, sInfo.uprErr);

            // if(poiName.Contains("r_ttV"))
            // {
            //     sInfo.bestFitVal /= 2.0;
            //     sInfo.lwrErr /= 2.0;
            //     sInfo.uprErr /= 2.0;        
            // }
		}
		else
		{
			sInfo.bestFitVal = -999;
			sInfo.lwrErr = -999;
			sInfo.uprErr = -999;
		}
        sInfoList.push_back(sInfo);
    }
    return sInfoList;
}

void getMaxMin(vector<PlotStruct> gList, double& minXF, double& maxXF)
{
    for(const auto& grL: gList)
    {
        auto g = grL.gr;
        for(int i = 0; i < g->GetN(); i++)
        {
            double maxX = g->GetX()[i] + g->GetErrorXhigh(i);
            double minX = g->GetX()[i] - g->GetErrorXlow(i);
            if(minX < minXF) minXF = minX;
            if(maxX > maxXF) maxXF = maxX;
        }
    }
}

bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["folderList_inc"]  = "root-files/WS_combined_sigmaXS_STXS_S0_v23_V7_model";

    opts["folderList_S0"]   = "root-files/WS_combined_XS_STXS_S0_v23_V7_model";
    opts["folderList_RS1p1"]= "root-files/WS_combined_XS_STXS_RS1p1_v23_V7_model";

    opts["folderList_S0qqH"]   = "root-files/WS_combined_XS_STXS_S0qqH_v23_V7_model";
    opts["folderList_RS1p1qqH"]= "root-files/WS_combined_XS_STXS_RS1p1qqH_v23_V7_model";

    opts["asimovLabel"]    	= "asimov";
    opts["dataLabel"]    	= "data";

    opts["statOnlyLabel"]   = "statOnly";
    opts["sysAllLabel"]    	= "allSys";

    opts["poiList_inc"]     = "mu";

    opts["poiList_S0"]      = "mu_ggF,mu_VBF,mu_VH,mu_ttH";
    opts["poiList_RS1p1"]   = "mu_ggF_0J_PTH_0_10,mu_ggF_0J_PTH_GT10,mu_ggF_1J_PTH_0_60,mu_ggF_1J_PTH_60_120,mu_ggF_1J_PTH_120_200,mu_ggF_GE2J,mu_ggF_PTH_GT200,mu_VBF_qq2qq_PTH_LE200,mu_VBF_qq2qq_PTH_GT200,mu_VH_Had,mu_VH_Lep,mu_ttH";

    opts["poiList_S0qqH"]      = "mu_gg2H,mu_qqH2qq,mu_qq2Hll,mu_ttH";
    opts["poiList_RS1p1qqH"]   = "mu_gg2H_0J_PTH_0_10,mu_gg2H_0J_PTH_GT10,mu_gg2H_1J_PTH_0_60,mu_gg2H_1J_PTH_60_120,mu_gg2H_1J_PTH_120_200,mu_gg2H_GE2J,mu_gg2H_PTH_GT200,mu_qqH2qq_VHHad,mu_qqH2qq_rest,mu_qq2Hqq_mJJ_GT350_PTH_GT200,mu_qq2Hll,mu_ttH";

    opts["poiList_ZZ"]      = "r_ZZ_0jet,r_ZZ_1jet,r_ZZ_2jet,r_ttV";


    opts["dataset"]      = "asimov";


    opts["type"]      = "S0";

    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") 
        {
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    return true;
}

void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;

    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;
    
    new TColor(ci, 0/255.,    66/255.,    125/255.); //54
    ci++;

    new TColor(ci, 254/255., 139/255., 113/255.); //55
    ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;          
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++; 
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    //new TColor(ci, 0.6350,    0.0780,    0.1840); //62
    //ci++;
    //new TColor(ci, 142.0/255 , 0.0/255 , 62.0/255);
    //ci++;
    //new TColor(ci, 96.0/255 , 78.0/255 , 0.0/255);
    //ci++;
    //new TColor(ci, 92.0/255 , 174.0/255 , 0.0/255);
    //ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}

void formatGraph(PlotStruct plotGraph)
{
    int ggFC_light   = TColor::GetColor("#67B2FF");
    int VBFC_light   = TColor::GetColor("#99E148");
    int VHC_light    = TColor::GetColor("#EB9B35");
    int ttHC_light   = TColor::GetColor("#236AEB");
    int ZZC_light    = TColor::GetColor("#BA0A07");
    int ttVC_light   = TColor::GetColor("#D1CE0F");

    int ggFC_dark   = TColor::GetColor("#3E6B99");
    int VBFC_dark   = TColor::GetColor("#6DA133");
    int VHC_dark    = TColor::GetColor("#B87929");
    int ttHC_dark   = TColor::GetColor("#1F5BCC");
    int ZZC_dark    = TColor::GetColor("#A30907");
    int ttVC_dark   = TColor::GetColor("#A8A623");

    int lightColour = kGray+2;
    int darkColour = kBlack;

    if(plotGraph.POIName.Contains("gg2H"))  { lightColour = ggFC_light; darkColour = ggFC_dark; }
    if(plotGraph.POIName.Contains("ggF"))   { lightColour = ggFC_light; darkColour = ggFC_dark; }
    if(plotGraph.POIName.Contains("VBF"))   { lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(plotGraph.POIName.Contains("VH"))    { lightColour = VHC_light;  darkColour = VHC_dark; }
    if(plotGraph.POIName.Contains("qq2Hqq")){ lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(plotGraph.POIName.Contains("qqH2qq")){ lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(plotGraph.POIName.Contains("qq2Hll")){ lightColour = VHC_light;  darkColour = VHC_dark; }
    if(plotGraph.POIName.Contains("ttH"))   { lightColour = ttHC_light; darkColour = ttHC_dark; }
    if(plotGraph.POIName.Contains("r_ZZ"))  { lightColour = ZZC_light;  darkColour = ZZC_dark; }
    if(plotGraph.POIName.Contains("r_ttV")) { lightColour = ttVC_light; darkColour = ttVC_dark; }


    plotGraph.gr->SetFillColor(lightColour);
    plotGraph.gr->SetLineColor(lightColour);
    plotGraph.gr->SetLineWidth(2);
    plotGraph.gr->SetMarkerColor(darkColour);        
    plotGraph.gr->SetMarkerStyle(21);
    plotGraph.gr->SetMarkerSize(1);

    if(plotGraph.POIName.Contains("r_"))
    {
        plotGraph.gr->SetMarkerSize(0.75);
    } 
}

void fillWS(TString basefolderName, TString POIList, TString dataLabel, TString scanLabel)
{
    // Grab metadata from one file
    TString folderName = basefolderName + "/" + scanLabel + "/" + dataLabel + "/";

    vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(POIList, ",");
    std::reverse(poiList.begin(),poiList.end());
    // read all the info
    for(const auto& poiName: poiList)
    {
        TString fileName = folderName + "/" + poiName + ".root";
        cout<<"Looking for ws: "<<fileName<<endl;
        if(doesExist(fileName.Data()))
        {
            auto label = getLabels(vector<TString>{fileName}).at(0);
                    cout<<"Looking for ws: "<<fileName<<" "<<label["fileName"]<<endl;

            if(doesExist(label["fileName"].Data()))
            {
                TFile* wsFile = TFile::Open(label["fileName"]);
                m_ws = (RooWorkspace*) wsFile->Get(label["workspaceName"]);

                cout<<"Found the ws"<<endl;
                return;
            }
        }
    }
    cout<<"Can't find ws"<<endl;
}

double getThPrediction(TString POIName)
{
    double sumTotal = 0;

    // Multiple by the XS or N data
    auto varList = varToSum(POIName);

    for(const auto& varName: varList)
    { 

        TString fileName = varName;
        fileName.ReplaceAll("XS_", "");

        if(!m_ws->var(varName))
        {
            cout<<"Can't find "<<varName<<endl;
            continue;
        }
        sumTotal        += m_ws->var(varName)->getVal();
    }

    return sumTotal;
}

vector<PlotStruct> getLegendEntry(TString type, double shiftedminXF, double shiftedmaxXF)
{
    vector<PlotStruct> plotStructList;


    vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;

    if(type.Contains("S0"))
    {
        y.push_back(5.685);
        yErrUpr.push_back(0);
        yErrLwr.push_back(0);

        // x.push_back(0.5975);

        x.push_back(shiftedminXF + 0.0735* (shiftedmaxXF - shiftedminXF));
        xErrUpr.push_back(0.085);
        xErrLwr.push_back(0.085);
    }
    if(type.Contains("RS1p1"))
    {
        y.push_back(12.75 + 0.675);
        yErrUpr.push_back(0);
        yErrLwr.push_back(0);

        // x.push_back(-0.970);
        x.push_back(shiftedminXF + 0.0735* (shiftedmaxXF - shiftedminXF));
        xErrUpr.push_back(0.17);
        xErrLwr.push_back(0.17);
    }
    auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);

    PlotStruct plotStruct;
    plotStruct.gr = gr;
    plotStruct.POIName = "dummy";
    formatGraph(plotStruct);
    plotStructList.push_back(plotStruct);

    return plotStructList;
}

PlotStruct getThPrediction(vector<PlotStruct> sysGraph, bool normalized, TString folderName)
{
    vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;
    for(const auto& graph: sysGraph)
    {

        double sumTotal = 0;
        double sumTotalUp = 0;
        double sumTotalDown = 0;
        cout <<  graph.POIName << endl;

        // Multiple by the XS or N data
        auto varList = varToSum(graph.POIName);
        // cout<<"--------------------------------"<<endl;
        // for(auto var:varList) cout << var << endl;

        for(const auto& varName: varList)
        { 


            TString fileName = varName;
            fileName.ReplaceAll("XS_", "");
            auto thSys = getThSys(folderName + "/" + fileName + ".txt");
            // cout<<"thSYS: "<<thSys.first<<" "<<thSys.second<<endl;

            if(!m_ws->var(varName))
            {
                cout<<"Can't find "<<varName<<endl;
                continue;
            }
            sumTotal        += m_ws->var(varName)->getVal();
            sumTotalUp      += m_ws->var(varName)->getVal() * (1 + thSys.first);
            sumTotalDown    += m_ws->var(varName)->getVal() * (1 + thSys.second);
        }
        // cout<<graph.POIName<<" "<<sumTotal<<" "<<sumTotalUp<<" "<<sumTotalDown<<endl;

        sumTotalUp = fabs(sumTotalUp - sumTotal);
        sumTotalDown = fabs(sumTotalDown - sumTotal);

        // cout<<graph.POIName<<" "<<sumTotal<<" "<<sumTotalUp<<" "<<sumTotalDown<<endl;

        if(normalized) 
        {
            sumTotalUp      /= sumTotal;
            sumTotalDown    /= sumTotal;
            sumTotal        /= sumTotal;

        }

        x.push_back(sumTotal);
        xErrUpr.push_back(sumTotalUp);
        xErrLwr.push_back(sumTotalDown);

        double yVal = graph.gr->GetY()[0];
        y.push_back(yVal);
        yErrUpr.push_back(0.5);
        yErrLwr.push_back(0.5);


    }


    auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);


    int colour = TColor::GetColor("#E8E8E8");

    gr->SetFillColor(colour);        
    gr->SetLineColor(colour);               
    gr->SetMarkerColor(colour);
    gr->SetMarkerSize(0.8);        
    gr->SetMarkerStyle(34);        

    PlotStruct plotStruct;
    plotStruct.gr = gr;
    plotStruct.POIName = "Th";

    return    plotStruct;
}

vector<TString> varToSum(TString poiName)
{
    map<TString, vector<TString>> sumList;
    // // CONF Style
    // sumList["mu_gg2H"]  = {"XS_gg2H_gg2H", "XS_bbH_gg2H"};
    // sumList["mu_VBF"]   = {"XS_VBF_VBF"};
    // sumList["mu_VH"]    = {"XS_VH_VH"};
    // sumList["mu_ttH"]   = {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};

    // sumList["mu_gg2H_0J_PTH_0_10"]      = {"XS_gg2H_gg2H_0J_PTH_0_10", "XS_bbH_gg2H_0J_PTH_0_10"};
    // sumList["mu_gg2H_0J_PTH_GT10"]      = {"XS_gg2H_gg2H_0J_PTH_GT10", "XS_bbH_gg2H_0J_PTH_GT10"};
    // sumList["mu_gg2H_1J_PTH_0_60"]      = {"XS_gg2H_gg2H_1J_PTH_0_60", "XS_bbH_gg2H_1J_PTH_0_60"};
    // sumList["mu_gg2H_1J_PTH_120_200"]   = {"XS_gg2H_gg2H_1J_PTH_120_200", "XS_bbH_gg2H_1J_PTH_120_200"};
    // sumList["mu_gg2H_1J_PTH_60_120"]    = {"XS_gg2H_gg2H_1J_PTH_60_120", "XS_bbH_gg2H_1J_PTH_60_120"};
    // sumList["mu_gg2H_GE2J"]             = {"XS_gg2H_gg2H_GE2J", "XS_bbH_gg2H_GE2J"};
    // sumList["mu_gg2H_PTH_GT200"]        = {"XS_gg2H_gg2H_PTH_GT200", "XS_bbH_gg2H_PTH_GT200"};
    // sumList["mu_VBF_qq2qq_PTH_GT200"]   = {"XS_VBF_VBF_qq2qq_PTH_GT200"};
    // sumList["mu_VBF_qq2qq_PTH_LE200"]   = {"XS_VBF_VBF_qq2qq_PTH_LE200"};
    // sumList["mu_VH_Had"]                = {"XS_VH_VH_Had"};
    // sumList["mu_VH_Lep"]                = {"XS_VH_VH_Lep"};
    // sumList["mu_ttH"]                   = {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};

    // sumList["mu"]   = {"XS_ggF_ggF", "XS_bbH_ggF", "XS_VBF_VBF", "XS_VH_VH", "XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};
    sumList["mu"]   = {"XS_ggF_ggF", "XS_bbH_ggF", "XS_VBF_VBF", "XS_VH_VH", "XS_tHqb_tHqb", "XS_ttH_ttH"};

    //// Paper (conf style) S0, RS1p1
    if(type.EqualTo("S0"))
    {
        sumList["mu_ggF"]  = {"XS_ggF_ggF", "XS_bbH_ggF"};
        sumList["mu_VBF"]   = {"XS_VBF_VBF"};
        sumList["mu_VH"]    = {"XS_VH_VH"};
        // sumList["mu_ttH"]   = {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};
        sumList["mu_ttH"]   = {"XS_tHqb_tHqb", "XS_ttH_ttH"};
    }
    else if(type.EqualTo("RS1p1"))
    {
        sumList["mu_ggF_0J_PTH_0_10"]      = {"XS_ggF_ggF_0J_PTH_0_10", "XS_bbH_ggF_0J_PTH_0_10"};
        sumList["mu_ggF_0J_PTH_GT10"]      = {"XS_ggF_ggF_0J_PTH_GT10", "XS_bbH_ggF_0J_PTH_GT10"};
        sumList["mu_ggF_1J_PTH_0_60"]      = {"XS_ggF_ggF_1J_PTH_0_60", "XS_bbH_ggF_1J_PTH_0_60"};
        sumList["mu_ggF_1J_PTH_120_200"]   = {"XS_ggF_ggF_1J_PTH_120_200", "XS_bbH_ggF_1J_PTH_120_200"};
        sumList["mu_ggF_1J_PTH_60_120"]    = {"XS_ggF_ggF_1J_PTH_60_120", "XS_bbH_ggF_1J_PTH_60_120"};
        sumList["mu_ggF_GE2J"]             = {"XS_ggF_ggF_GE2J", "XS_bbH_ggF_GE2J"};
        sumList["mu_ggF_PTH_GT200"]        = {"XS_ggF_ggF_PTH_GT200", "XS_bbH_ggF_PTH_GT200"};
        sumList["mu_VBF_qq2qq_PTH_GT200"]   = {"XS_VBF_VBF_qq2qq_PTH_GT200"};
        sumList["mu_VBF_qq2qq_PTH_LE200"]   = {"XS_VBF_VBF_qq2qq_PTH_LE200"};
        sumList["mu_VH_Had"]                = {"XS_VH_VH_Had"};
        sumList["mu_VH_Lep"]                = {"XS_VH_VH_Lep"};
        // sumList["mu_ttH"]                   = {"XS_tHW_tHW", "XS_tHqb_tHqb", "XS_ttH_ttH"};
        sumList["mu_ttH"]                   = {"XS_tHqb_tHqb", "XS_ttH_ttH"};
    }

    //// Paper (new STXS way ) S0qqFine, RS1p1qqFine
    else if(type.EqualTo("S0qqH"))
    {
        sumList["mu_gg2H"]      = {"XS_gg2H_gg2H", "XS_bb2H_gg2H"};
        sumList["mu_qqH2qq"]    = {"XS_qq2Hqq_qqH2qq"};
        sumList["mu_qq2Hll"]    = {"XS_qq2Hll_qq2Hll"};
        sumList["mu_ttH"]       = {"XS_tHqb_tHqb", "XS_ttH_ttH"};
    }
    else if(type.EqualTo("RS1p1qqH"))
    {
        sumList["mu_gg2H_0J_PTH_0_10"]      = {"XS_gg2H_gg2H_0J_PTH_0_10", "XS_bb2H_gg2H_0J_PTH_0_10"};
        sumList["mu_gg2H_0J_PTH_GT10"]      = {"XS_gg2H_gg2H_0J_PTH_GT10", "XS_bb2H_gg2H_0J_PTH_GT10"};
        sumList["mu_gg2H_1J_PTH_0_60"]      = {"XS_gg2H_gg2H_1J_PTH_0_60", "XS_bb2H_gg2H_1J_PTH_0_60"};
        sumList["mu_gg2H_1J_PTH_120_200"]   = {"XS_gg2H_gg2H_1J_PTH_120_200", "XS_bb2H_gg2H_1J_PTH_120_200"};
        sumList["mu_gg2H_1J_PTH_60_120"]    = {"XS_gg2H_gg2H_1J_PTH_60_120", "XS_bb2H_gg2H_1J_PTH_60_120"};
        sumList["mu_gg2H_GE2J"]             = {"XS_gg2H_gg2H_GE2J", "XS_bb2H_gg2H_GE2J"};
        sumList["mu_gg2H_PTH_GT200"]        = {"XS_gg2H_gg2H_PTH_GT200", "XS_bb2H_gg2H_PTH_GT200"};
        sumList["mu_qqH2qq_VHHad"]          = {"XS_qq2Hqq_qqH2qq_VHHad"};
        sumList["mu_qqH2qq_rest"]           = {"XS_qq2Hqq_qqH2qq_rest"};
        sumList["mu_qq2Hll"]                = {"XS_qq2Hll_qq2Hll"};
        sumList["mu_qq2Hqq_mJJ_GT350_PTH_GT200"]  = {"XS_qq2Hqq_qq2Hqq_mJJ_GT350_PTH_GT200"};
        sumList["mu_ttH"]                   = {"XS_tHqb_tHqb", "XS_ttH_ttH"};
    }

    sumList["r_ZZ_0jet"]    = {"N_qqZZ_all_0jet_Pt4l_0_10_4l_13TeV","N_qqZZ_all_0jet_Pt4l_10_100_4l_13TeV","N_qqZZ_all_0jet_Pt4l_GE100_4l_13TeV","N_qqZZ_all_SB_0jet_4l_13TeV","N_qqZZ_all_SB_VHLep_4l_13TeV","N_qqZZ_all_VHLep_4l_13TeV",
    "N_ggZZ_all_0jet_Pt4l_0_10_4l_13TeV","N_ggZZ_all_0jet_Pt4l_10_100_4l_13TeV","N_ggZZ_all_SB_0jet_4l_13TeV","N_ggZZ_all_SB_VHLep_4l_13TeV","N_ggZZ_all_VHLep_4l_13TeV",
    "N_EWZZjj_all_0jet_Pt4l_0_10_4l_13TeV","N_EWZZjj_all_0jet_Pt4l_10_100_4l_13TeV","N_EWZZjj_all_SB_0jet_4l_13TeV","N_EWZZjj_all_SB_VHLep_4l_13TeV","N_EWZZjj_all_VHLep_4l_13TeV",};
    sumList["r_ZZ_1jet"]    = {"N_qqZZ_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_qqZZ_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_qqZZ_all_1jet_Pt4l_120_200_4l_13TeV","N_qqZZ_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_qqZZ_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_qqZZ_all_1jet_Pt4l_GE200_4l_13TeV","N_qqZZ_all_SB_1jet_4l_13TeV",
    "N_ggZZ_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_ggZZ_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_ggZZ_all_1jet_Pt4l_120_200_4l_13TeV","N_ggZZ_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_ggZZ_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_ggZZ_all_1jet_Pt4l_GE200_4l_13TeV","N_ggZZ_all_SB_1jet_4l_13TeV",
    "N_EWZZjj_all_1jet_Pt4l_0_60_4l_13TeV_b1","N_EWZZjj_all_1jet_Pt4l_0_60_4l_13TeV_b2","N_EWZZjj_all_1jet_Pt4l_120_200_4l_13TeV","N_EWZZjj_all_1jet_Pt4l_60_120_4l_13TeV_b1","N_EWZZjj_all_1jet_Pt4l_60_120_4l_13TeV_b2","N_EWZZjj_all_1jet_Pt4l_GE200_4l_13TeV","N_EWZZjj_all_SB_1jet_4l_13TeV",};
    sumList["r_ZZ_2jet"]    = {"N_qqZZ_all_2jet_4l_13TeV_b1","N_qqZZ_all_2jet_4l_13TeV_b2","N_qqZZ_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_qqZZ_all_SB_2jet_4l_13TeV","N_ggZZ_all_2jet_4l_13TeV_b1",
    "N_ggZZ_all_2jet_4l_13TeV_b2","N_ggZZ_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_ggZZ_all_SB_2jet_4l_13TeV",
    "N_EWZZjj_all_2jet_4l_13TeV_b1","N_EWZZjj_all_2jet_4l_13TeV_b2","N_EWZZjj_all_2jet_VBF_ptJ1GE200_4l_13TeV","N_EWZZjj_all_SB_2jet_4l_13TeV",};
    sumList["r_ttV"]    = {"N_ttV_all_SB_ttH_4l_13TeV", "N_ttV_all_ttH_hadronic_4l_13TeV_b1", "N_ttV_all_ttH_hadronic_4l_13TeV_b2", "N_ttV_all_ttH_leptonic_4l_13TeV"};


    return sumList.at(poiName);
}

pair<double, double> getThSys(TString fileName)
{
    map<TString, pair<double, double>> thVars;

    // cout<<fileName<<endl;

    TString currFileName = fileName;
    string line;    
    ifstream myfile (currFileName);
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {

            if(line.length() == 0) continue;

            // actual info
            auto splitList = splitString(line, " ");
            TString sysName = splitList[0];
            double lwrSys = TString(splitList[1]).Atof();
            double uprSys = TString(splitList[2]).Atof();

            if(std::isnan(lwrSys) || std::isnan(uprSys)) continue;
            double ave = (fabs(lwrSys) + fabs(uprSys))/2;
            // if(ave>0.01)cout<<sysName<<" "<<lwrSys<<" "<<uprSys<<endl;

            pair<double, double> currVar{lwrSys, uprSys};
            thVars[sysName] = currVar;
        }
        myfile.close();
    }

    double upSqr = 0;
    double downSqr = 0;

    for(const auto& currTh: thVars)
    {
        upSqr += pow(currTh.second.first,2);
        downSqr += pow(currTh.second.second,2);
    }
    // cout<<"upSqr "<<upSqr<<" downSqr: "<<downSqr<<endl;
    
    pair<double, double> outVal = pair<double, double>{sqrt(upSqr)/100, sqrt(downSqr)/100};
    
    return outVal;
}

vector<TLatex*> POIInfo(double maxXF, double minXF, vector<PlotStruct> obsList, PlotStruct thList)
{

    vector<TLatex*> latexList;

    double xPOSSM = minXF + 0.85*(maxXF - minXF);
    double xPOObs = minXF + 0.70*(maxXF - minXF);
    // xPOObs = xPOSSM;

    double yTitleOffset = 0.5;
    double yTitle  = obsList.size() + yTitleOffset;

    if(type.Contains("RS1p1")) yTitle++;


    TLatex * topLabel = new TLatex(xPOObs, yTitle, "#sigma#upoint#it{B} [fb]");
    topLabel->SetTextAlign(12);
    topLabel->SetTextSize(0.025);
    topLabel->SetTextColor(kGray+3);
    topLabel->SetTextFont(42);
    topLabel->Draw();
    latexList.push_back(topLabel);

    topLabel = new TLatex(xPOSSM, yTitle - 0.05, "(#sigma#upoint#it{B})_{SM} [fb]");
    topLabel->SetTextAlign(12);
    topLabel->SetTextSize(0.025);
    topLabel->SetTextColor(kGray+3);
    topLabel->SetTextFont(42);
    topLabel->Draw();
    latexList.push_back(topLabel);



    double xPOSPVale = minXF + 0.385*(maxXF - minXF);
    float pValOffSet = -0.01;
    if(type.EqualTo("S0")) pValOffSet = +0.2;

    topLabel = new TLatex(xPOSPVale, yTitle + pValOffSet, "#it{p}-value = " + pValList[type]);
    topLabel->SetTextAlign(12);
    topLabel->SetTextSize(0.025);
    topLabel->SetTextColor(kBlack);
    topLabel->SetTextFont(42);
    topLabel->Draw();
    latexList.push_back(topLabel);



    double yInfoOffset = 0.02;

    for(const auto plotStruct: obsList)
    {
        double XS = getThPrediction(plotStruct.POIName);

        double yVal   = plotStruct.gr->GetY()[0];
        double cVal   = plotStruct.gr->GetX()[0] * XS * 1000;
        double lwrErr = plotStruct.gr->GetErrorXhigh(0) * XS * 1000;
        double uprErr = plotStruct.gr->GetErrorXlow(0) * XS * 1000;

        topLabel = new TLatex(xPOObs, yVal+yInfoOffset, pdfRound(cVal, lwrErr, uprErr));
        topLabel->SetTextAlign(12);
        topLabel->SetTextSize(0.025);
        topLabel->SetTextColor(kGray+3);
        topLabel->SetTextFont(42);
        topLabel->Draw();
        latexList.push_back(topLabel);
    }


    for(int i = 0; i < thList.gr->GetN(); i++)
    {
        double yVal   = thList.gr->GetY()[i];
        double cVal   = thList.gr->GetX()[i] * 1000;
        double lwrErr = thList.gr->GetErrorXhigh(i) * 1000;
        double uprErr = thList.gr->GetErrorXlow(i) * 1000;

        topLabel = new TLatex(xPOSSM, yVal+yInfoOffset, pdfRound(cVal, lwrErr, uprErr));
        topLabel->SetTextAlign(12);
        topLabel->SetTextSize(0.025);
        topLabel->SetTextColor(kGray+3);
        topLabel->SetTextFont(42);
        topLabel->Draw();
        latexList.push_back(topLabel);
    }
    


    return latexList;
}

vector<TLatex*> POIInfoZZ(double maxXF, double minXF, vector<PlotStruct> obsList)
{

    vector<TLatex*> latexList;

    double xPOSSM = minXF + 0.85*(maxXF - minXF);
    double xPOObs = minXF + 0.75*(maxXF - minXF);
    // xPOObs = xPOSSM;

    double yTitleOffset = 0.5;
    double yTitle  = obsList.size() + yTitleOffset;

    TLatex * topLabel = new TLatex(xPOObs, yTitle, "N/N_{SM}");
    topLabel->SetTextAlign(12);
    topLabel->SetTextSize(0.075);
    topLabel->SetTextColor(kGray+3);
    topLabel->SetTextFont(42);
    topLabel->Draw();
    latexList.push_back(topLabel);

    // topLabel = new TLatex(xPOSSM, yTitle - 0.05, "N_{SM}");
    // topLabel->SetTextAlign(12);
    // topLabel->SetTextSize(0.025);
    // topLabel->SetTextColor(kGray+3);
    // topLabel->SetTextFont(42);
    // topLabel->Draw();
    // latexList.push_back(topLabel);



    for(const auto plotStruct: obsList)
    {

        double yVal   = plotStruct.gr->GetY()[0];
        double cVal   = plotStruct.gr->GetX()[0];
        double lwrErr = plotStruct.gr->GetErrorXhigh(0);
        double uprErr = plotStruct.gr->GetErrorXlow(0);

        topLabel = new TLatex(xPOObs, yVal, pdfRound(cVal, lwrErr, uprErr));
        topLabel->SetTextAlign(12);
        topLabel->SetTextSize(0.075);
        topLabel->SetTextColor(kGray+3);
        topLabel->SetTextFont(42);
        topLabel->Draw();
        latexList.push_back(topLabel);
    }

    return latexList;
}

TString pdfRound(double value, double lwrErr, double uprErr, int addExtraDigit)
{
    // Lets symtrize the Sys
    double val = value;
    double err = (fabs(lwrErr) + fabs(uprErr))/2;
    double smallErr = fabs(lwrErr);
    if(fabs(uprErr) < smallErr) smallErr = fabs(uprErr);

    double diffErr = fabs((fabs(lwrErr) - fabs(uprErr)))/err;

    bool sym = false;
    if(diffErr < 0.20) sym = true;


    auto threeDigits = [](float x) 
    { 
        TString toParse;
        toParse.Form("%.2e", x);
        TRegexp re("e.*");
        toParse(re) = "";
        toParse = toParse.ReplaceAll(".","").ReplaceAll("+","").ReplaceAll("-","");
        return toParse.Atoi();
    };

    auto nSignificantDigits = [](int threeDigits) 
    { 
        if(threeDigits < 101) return 2;
        if(threeDigits < 356) return 2;
        if(threeDigits < 950) return 1;
        return 2;
    };


    auto frexp10 = [](float x) 
    { 
        TString toParse;
        toParse.Form("%e", x);
        TRegexp upre("e.*");
        TRegexp dWre(".*e");
        TString upStr = toParse;
        TString dwStr = toParse;

        upStr(upre) = "";
        dwStr(dWre) = "";
        return vector<double> {upStr.Atof(), dwStr.Atof()};
    };


    auto nDigitsValue = [](double expVal, double expErr, double nDigitsErr) 
    { 
        return expVal-expErr+nDigitsErr;
    };


    auto formatValue = [](float value, int exponent, int nDigits, int extraRound) 
    { 
        auto roundAt = nDigits - 1 - exponent - extraRound;
        auto nDec = 0;
        if(exponent < nDigits) nDec = roundAt;
        if(nDec < 0) nDec = 0;

        TString roundExp = "\%." + TString::Itoa(nDec, 10) + "f";
        TString outFormat = "";
        outFormat.Form(roundExp, value);
        return outFormat;
    };


    // For upper and lower error
    auto tD = threeDigits(err);
    auto nD = nSignificantDigits(tD) + addExtraDigit;

    // cout<<"smallErr: "<<smallErr<<" tD: "<<tD<<" nD: "<<nD<<endl;

    auto expVal = frexp10(val)[1];
    auto expErr = frexp10(err)[1];

    double extraRound = 0;
    if(tD >= 950) extraRound = 1;


    auto cVal     = formatValue(val, expVal, nDigitsValue(expVal, expErr, nD), extraRound);

    if(cVal.Atof() == 0)
    {
        return pdfRound(value, lwrErr, uprErr, addExtraDigit+1);
    }

    auto clwrErr  = formatValue(lwrErr, expErr, nD, extraRound);
    auto cuprErr  = formatValue(uprErr, expErr, nD, extraRound);
    auto cErr     = formatValue(err, expErr, nD, extraRound);


    TString formatErr = "";
    formatErr.Form("%s^{ #plus %s}_{ #minus %s}", cVal.Data(), clwrErr.Data(), cuprErr.Data());


    if(sym) formatErr.Form("%s #pm %s", cVal.Data(), cErr.Data());

    return formatErr;

}
