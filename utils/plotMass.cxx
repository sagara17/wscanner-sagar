#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TRegexp.h>
#include <TColor.h>
#include <TLegend.h>
#include <TGaxis.h>
#include <TString.h>
#include <stdio.h>
#include <TGraphAsymmErrors.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"

using namespace std;

// Global vars
map<TString,TString> opts;

struct ScanInfo
{
	double bestFitVal;
	double lwrErr;
	double uprErr;

	TString POIName;
	TString scanType;
	TString dataType;
};


struct PlotStruct
{
    TGraphAsymmErrors* gr;
    TString POIName;
};

// Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
std::map<TString, TString> commonMetaData;
void setPrettyStuff();
void getMaxMin(TGraphAsymmErrors* g, double& minXF, double& maxXF);
void formatGraph(TGraphAsymmErrors* gr, TString extraQualifier);
TGraphAsymmErrors* getGraph(vector<TString> labelList, map<TString, map<TString, ScanInfo>> scanInfoMap, TString scanType);
vector<PlotStruct> getLegendEntry(TString type, double shiftedminXF, double shiftedmaxXF);
TString pdfRound(double val, double lwrErr, double uprErr);
ScanInfo read1DScan(TString basefolderName, TString poiName, TString dataLabel, TString scanLabel, TString label);
TGraphAsymmErrors* quadSub(TGraphAsymmErrors* totalGraph, TGraphAsymmErrors* statGraph);
TString getFancyLabel(TString labelName);
vector<TLatex*> textInfo(double maxXF, double minXF, TGraphAsymmErrors* sysGraph, TGraphAsymmErrors* statGraph);

double factor = 1.10;
TString type ;

int main(int argc, char** argv)
{
    if (!cmdline(argc,argv,opts)) return 0;
    setPrettyStuff();

    map<TString, map<TString, ScanInfo>> scanInfoMap;

    vector<TString> labelList;
    // Read the information
    if(opts["type"].EqualTo("channels"))    labelList    = {"All_All", "4e_All", "2mu2e_All", "2e2mu_All", "4mu_All"};
    if(opts["type"].EqualTo("4mu"))    labelList    = {"4mu_All", "4mu_BDT_bin4", "4mu_BDT_bin3", "4mu_BDT_bin2", "4mu_BDT_bin1"};
    else if(opts["type"].EqualTo("16Cat"))  labelList    = {"All_All", "4mu_BDT_bin4", "4mu_BDT_bin3", "4mu_BDT_bin2", "4mu_BDT_bin1", "4e_BDT_bin4", "4e_BDT_bin3", "4e_BDT_bin2", "4e_BDT_bin1", "2mu2e_BDT_bin4", "2mu2e_BDT_bin3", "2mu2e_BDT_bin2", "2mu2e_BDT_bin1", "2e2mu_BDT_bin4", "2e2mu_BDT_bin3", "2e2mu_BDT_bin2", "2e2mu_BDT_bin1"};
    else if(opts["type"].EqualTo("11Cat"))  labelList    = {"All_All", "4mu_BDT_bin4", "4mu_BDT_bin3", "4mu_BDT_bin2", "4e_BDT_bin4", "4e_BDT_bin3", "2mu2e_BDT_bin4", "2mu2e_BDT_bin3", "2mu2e_BDT_bin2", "2e2mu_BDT_bin4", "2e2mu_BDT_bin3", "2e2mu_BDT_bin2"};
    // else if(opts["type"].EqualTo("11Cat"))  labelList    = {"All_All", "4mu_BDT_bin4", "4mu_BDT_bin3", "4mu_BDT_bin2", "4e_BDT_bin4", "4e_BDT_bin3", "2mu2e_BDT_bin4", "2mu2e_BDT_bin3", "2e2mu_BDT_bin4", "2e2mu_BDT_bin3", "2e2mu_BDT_bin2"};

    TString sysName = "allSys";
    if(opts["doSys"].EqualTo("false")) sysName = "statOnly";

    for(const auto& label: labelList)
    {
        // read the scan info into a map
        if(opts["scanType"].EqualTo("1D"))
        {
            scanInfoMap[label]["statOnly"]  = read1DScan(opts["baseName"], opts["poiName"], opts["dataset"], "statOnly", label);

            scanInfoMap[label]["allSys"]  = read1DScan(opts["baseName"], opts["poiName"], opts["dataset"], sysName, label);
        }
    }

    TGraphAsymmErrors* statGraph  = getGraph(labelList, scanInfoMap, "statOnly");
    TGraphAsymmErrors* totalGraph   = getGraph(labelList, scanInfoMap, sysName);

    TGraphAsymmErrors* sysGraph = quadSub(totalGraph, statGraph);


    // Start plotting the graph
    double minXF = 10000000000000;
    double maxXF = -1000000000000;
    getMaxMin(statGraph, minXF, maxXF);

    int numPOI = labelList.size();


    double shiftedminXF = minXF - (maxXF - minXF) * 0.10;
    double shiftedmaxXF = maxXF + (maxXF - minXF) * 0.80;

    // double labelStat    = maxXF + (maxXF - minXF) * 0.325;
    // double labelSys     = maxXF + (maxXF - minXF) * 0.05;

    int offSet = 2;
    if(opts["type"].EqualTo("16Cat") ) offSet = 7;
    else if(opts["type"].EqualTo("11Cat")) offSet = 5;


    // cout << "OFFSET IS " << offSet << " " << type << endl;
    TH2D* axisHist = new TH2D("axis","axis", 1, shiftedminXF, shiftedmaxXF, numPOI+offSet  , 0, numPOI+offSet);   
    // axisHist->GetYaxis()->SetTitle("POI");
    axisHist->SetNdivisions(505);
    axisHist->GetXaxis()->SetTitle("#it{m}_{H} [GeV]");
    axisHist->GetXaxis()->SetTitleSize(0.8*axisHist->GetXaxis()->GetTitleSize());
    if(numPOI > 8) axisHist->GetYaxis()->SetLabelSize(0.6*axisHist->GetYaxis()->GetLabelSize()*factor);
    axisHist->GetXaxis()->SetTickSize(0);
    axisHist->GetYaxis()->SetTickSize(0);



    int axisOffset = 0 ;

    TGaxis* axis_theta = new TGaxis(axisHist->GetXaxis()->GetXmin(), 
                                    axisHist->GetYaxis()->GetXmin()+axisOffset, 
                                    axisHist->GetXaxis()->GetXmin(), 
                                    numPOI+axisOffset, 
                                    axisHist->GetYaxis()->GetXmin()+axisOffset, 
                                    numPOI+axisOffset, 510);
    axis_theta->ImportAxisAttributes(axisHist->GetXaxis());
    axis_theta->SetName("axis_theta");
    axis_theta->SetTitleOffset(1.1);
    axis_theta->SetLineColor(kBlack);
    axis_theta->SetLabelColor(kBlack);
    axis_theta->SetTitleColor(kBlack);
    axis_theta->SetLabelSize(0);
    axis_theta->SetTitleSize(0);
    axis_theta->SetNdivisions(1 * (numPOI));

    TGaxis* axis_thetaLeft = new TGaxis(axisHist->GetXaxis()->GetXmax(), 
                                    axisHist->GetYaxis()->GetXmin()+axisOffset, 
                                    axisHist->GetXaxis()->GetXmax(), 
                                    numPOI+axisOffset, 
                                    axisHist->GetYaxis()->GetXmin()+axisOffset, 
                                    numPOI+axisOffset, 510);
    // axis_thetaLeft->ImportAxisAttributes(axisHist->GetXaxis());
    axis_thetaLeft->SetName("axis_thetaLeft");
    axis_thetaLeft->SetTitleOffset(1.1);
    axis_thetaLeft->SetLineColor(kBlack);
    axis_thetaLeft->SetLabelColor(kBlack);
    axis_thetaLeft->SetTitleColor(kBlack);
    axis_thetaLeft->SetLabelSize(0);
    axis_thetaLeft->SetOption("+");
    axis_thetaLeft->SetTitleSize(0);
    axis_thetaLeft->SetNdivisions(1 * (numPOI));


    TGaxis* axis_bottom = new TGaxis(axisHist->GetXaxis()->GetXmin(), axisHist->GetYaxis()->GetXmin(), axisHist->GetXaxis()->GetXmax(), axisHist->GetYaxis()->GetXmin(), axisHist->GetXaxis()->GetXmin(), axisHist->GetXaxis()->GetXmax(), 510, "+S");
    axis_bottom->SetName("axis_bottom");
    axis_bottom->SetLineColor(kBlack);
    axis_bottom->SetLabelColor(kBlack);
    axis_bottom->SetTitleColor(kBlack);
    axis_bottom->SetLabelSize(0);
    axis_bottom->SetTitleSize(0);
    axis_bottom->SetTickLength(0.015);
    axis_bottom->SetTickSize(0.015);
    // gStyle->SetTickLength(0.01,"x");
    // axis_bottom->SetNdivisions(1 * (numPOI));

    int counter = 0;

    // for(const auto& g: sysGraph)
    // {
    //     axisHist->GetYaxis()->SetBinLabel(counter + 1, getFancyXname(g.POIName));
    //     counter++;
    // }
    for(int i = 0; i < labelList.size(); i++)
    {
        axisHist->GetYaxis()->SetBinLabel(i + 1, getFancyLabel(labelList.at(i)) );
    }


    axisHist->GetXaxis()->LabelsOption("v");

    // Fancy title stuff
    double leftDist = 0.19;
    double rightDist = 0.6 ;


    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 700, 600);
    if(numPOI > 8) c1->SetLeftMargin(0.16);
    if(numPOI > 8) c1->SetBottomMargin(0.125);

    double lLwr = 0.8;
    TLegend *elLeg =  new TLegend (rightDist, lLwr, rightDist+0.35, lLwr + 0.1);
    elLeg->SetFillStyle(0);
    elLeg->SetBorderSize(0);
    elLeg->SetTextFont(42);
    elLeg->SetTextSize(0.025*factor);

    vector<PlotStruct> legEntry =  getLegendEntry(type, shiftedminXF, shiftedmaxXF);
    
    if(opts["doSys"].EqualTo("false"))  elLeg->AddEntry(totalGraph, "Observed: Stat", "lp");
    if(opts["doSys"].EqualTo("true")) 
    {
        elLeg->AddEntry(totalGraph, "Observed: Stat+Sys", "lp");
        elLeg->AddEntry(sysGraph, "Observed: Sys-Only", "p");
    }
    // elLeg->SetNColumns(2);


    // draw the canvas
    axisHist->Draw();
    axis_theta->Draw();
    axis_thetaLeft->Draw();
    axis_bottom->Draw();




    // TLine l;
    // l.SetLineWidth(1.0);
    // l.SetLineColor(kGray + 3);
    // l.SetLineStyle(2);
    // l.DrawLine( 124.922 , axisOffset,  124.922 , numPOI+axisOffset);


    // TLine hortL;
    // hortL.SetLineWidth(2.0);
    // hortL.SetLineColor(kGray + 3);
    // hortL.SetLineStyle(2);
    // hortL.DrawLine( axisHist->GetXaxis()->GetXmin() , 1,  axisHist->GetXaxis()->GetXmax() , 1);
    // hortL.Draw();

    totalGraph->Draw("ZP");


    // ////////// for error band
    auto errBand = new TGraphAsymmErrors(1, &vector<double>{124.}[0] , &vector<double>{2.5}[0], &vector<double>{0.19281}[0] ,&vector<double>{0.206921}[0], &vector<double>{2.5}[0], &vector<double>{2.5}[0] );
    errBand->SetFillColor(18);
    errBand->SetMarkerSize(0);
    // errBand->Draw("2");
    gPad->RedrawAxis();

    TLine l;
    l.SetLineWidth(1.0);
    l.SetLineColor(kGray + 3);
    l.SetLineStyle(2);
    // l.DrawLine( 124.922 , axisOffset,  124.922 , numPOI+axisOffset);
    l.DrawLine( 125 , axisOffset,  125 , numPOI+axisOffset);


    TLine hortL;
    hortL.SetLineWidth(2.0);
    hortL.SetLineColor(kGray + 3);
    hortL.SetLineStyle(2);
    hortL.DrawLine( axisHist->GetXaxis()->GetXmin() , 1,  axisHist->GetXaxis()->GetXmax() , 1);
    hortL.Draw();
    totalGraph->Draw("ZP");
    gPad->RedrawAxis();
    // ///////////////// for error band



    gStyle->SetEndErrorSize(5.0);
    // sysGraph->Draw("E5,same");
    sysGraph->Draw("[]");

    // TString saveName = "";
    // for(const auto& grl: sysGraph) grl.gr->Draw("ZP");
    // gStyle->SetEndErrorSize(5.0);
    // for(const auto& grl: statGraph) grl.gr->Draw("[]");

    ATLASLabel(leftDist, 0.875, "Internal", 1);

    TString channelStr = "H #rightarrow ZZ* #rightarrow 4l";
    TLatex* tMain = new TLatex (leftDist, 0.825, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.785, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    // TString stxsString = "Stage 0 - |y_{H}| < 2.5";
    // if(type.Contains("RS1p1qqFine")) stxsString = "STXS Reduced Stage 1.1 - |y_{H}| < 2.5";
    // else if(type.Contains("RS1p1")) stxsString = "Reduced Stage 1.1 - |y_{H}| < 2.5";
    // TLatex* stxsInfo = new TLatex (leftDist, 0.750, stxsString);
    // stxsInfo->SetNDC();
    // stxsInfo->SetTextSize(0.03);
    // stxsInfo->SetTextFont(42);
    // stxsInfo->Draw();


    auto latexList = textInfo(shiftedmaxXF, shiftedminXF, sysGraph, statGraph);

    for(const auto& l: latexList) l->Draw();

    gPad->RedrawAxis();

    c1->cd();
    // for(auto& grl: sysGraph) elLeg->AddEntry(grl.gr, labelMap[gr.first], "PE");
    elLeg->Draw();
    // legEntry[0].gr->Draw(" psame");
    if(opts["doSys"].EqualTo("true")) legEntry[0].gr->Draw("[]same");
    c1->cd();


    axis_bottom->Draw();
    axis_theta->Draw();
    axis_thetaLeft->Draw();
    axis_bottom->Draw();


    gPad->RedrawAxis();

    TString baseName = opts["type"];

    if(opts["baseName"].Contains("ggH")) baseName += "_ggH";
    else if(opts["baseName"].Contains("AllSig")) baseName += "_AllSig";
    else if(opts["baseName"].Contains("noRedBkg")) baseName += "_noRedBkg";

    TString baseNameCopy = opts["baseName"];
    baseNameCopy.ReplaceAll("XXX", "Summary").ReplaceAll("root-files/", "/") ;

    system("mkdir -vp " + opts["plotDir"] + "/" + baseNameCopy + "/");

    c1->SaveAs(opts["plotDir"] + "/" + baseNameCopy + "/" + baseName+ "_"+ opts["dataset"] +"_"+ opts["label"] + "_summary.pdf");






}

TString getFancyLabel(TString labelName)
{

    if(opts["type"].EqualTo("channels"))
    {
        cout << "here" << labelName << endl;
        if(labelName.EqualTo("All_All")) return "Combined";
        labelName.ReplaceAll("_All", "");
        labelName.ReplaceAll("mu", "#mu");

        return labelName;
    }

    else
    {
        return labelName;
    }

}


vector<TLatex*> textInfo(double maxXF, double minXF, TGraphAsymmErrors* sysGraph, TGraphAsymmErrors* statGraph)
{

    vector<TLatex*> latexList;

    double xPOSSM = minXF + 0.85*(maxXF - minXF);
    double xPOObs = minXF + 0.70*(maxXF - minXF);

    double shiftX = 0.15;

    if(opts["symmetrizeErr"].EqualTo("true")) shiftX = 0.25;

    // xPOObs = xPOSSM;

    double yTitleOffset = 0.25;
    double yTitle  = 5 + yTitleOffset;
    if(opts["type"].EqualTo("16Cat") ) yTitle += 13;
    if( opts["type"].EqualTo("11Cat")) yTitle += 8;

    // if(latexList.size() > 10) 
    // shiftX = 0.35;
    shiftX = 0.15;

    TLatex * topLabel = new TLatex(xPOObs - shiftX, yTitle, "#it{m}_{H} [GeV] ");
    topLabel->SetTextAlign(12);
    topLabel->SetTextSize(0.025*factor);
    topLabel->SetTextColor(kGray+3);
    topLabel->SetTextFont(42);
    topLabel->Draw();
    latexList.push_back(topLabel);

    double yInfoOffset = 0.02;


    for(int i = 0; i < sysGraph->GetN(); i++)
    {
        double yVal   = sysGraph->GetY()[i];
        double cVal   = sysGraph->GetX()[i];

        double uprErrStat = statGraph->GetErrorXhigh(i);
        double lwrErrStat = statGraph->GetErrorXlow(i);

        double  uprErrSys = sysGraph->GetErrorXhigh(i);
        double  lwrErrSys = sysGraph->GetErrorXlow(i);

        TString valueStr = "";
        valueStr = Form("%.3f ^{+%.3f}_{-%.3f} (Stat.) ^{+%.3f}_{-%.3f} (Sys.)",cVal, uprErrStat, lwrErrStat, uprErrSys, lwrErrSys);
        if(opts["doSys"].EqualTo("false")) valueStr = Form("%.3f ^{+%.3f}_{-%.3f} (Stat.)",cVal, uprErrStat, lwrErrStat );

        if(opts["symmetrizeErr"].EqualTo("true"))
        {
            valueStr = Form("%.3f #pm %.3f (Stat.) #pm %.3f (sys.)",cVal, (uprErrStat + lwrErrStat)/2, (uprErrSys+lwrErrSys)/2);
            if(opts["doSys"].EqualTo("false")) valueStr = Form("%.3f #pm  %.3f (Stat.)",cVal, (uprErrStat + lwrErrStat)/2);
        } 


        topLabel = new TLatex(xPOObs - shiftX, yVal+yInfoOffset, valueStr);
        topLabel->SetTextAlign(12);
        topLabel->SetTextSize(0.025*factor);
        topLabel->SetTextColor(kGray+3);
        topLabel->SetTextFont(42);
        topLabel->Draw();
        latexList.push_back(topLabel);
    }
    


    return latexList;
}


TGraphAsymmErrors* getGraph(vector<TString> labelList, map<TString, map<TString, ScanInfo>> scanInfoMap, TString scanType)
{

    vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;
    double counter = 0.5;
    double yOffSet = 0;
    double xOffset = 0;

    double scale = 1;
    // if(labelList.size() > 10) scale = 25;


    for(auto label: labelList)
    {
        y.push_back(counter + yOffSet);
        yErrUpr.push_back(0);
        yErrLwr.push_back(0);

        ScanInfo currScanInfor = scanInfoMap[label][scanType];

        x.push_back(currScanInfor.bestFitVal + xOffset);
        xErrUpr.push_back((fabs(currScanInfor.uprErr) + xOffset)/scale);
        xErrLwr.push_back((fabs(currScanInfor.lwrErr) + xOffset)/scale);

        counter++;

    }
        
    auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);
    formatGraph(gr, "None");

	return gr;
}


TGraphAsymmErrors* quadSub(TGraphAsymmErrors* totalGraph, TGraphAsymmErrors* statGraph)
{

    vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;
    double xOffset = 0;

    for(int i = 0; i < totalGraph->GetN(); i++)
    {

        y.push_back(totalGraph->GetY()[i]);
        // yErrUpr.push_back(0.2);
        // yErrLwr.push_back(0.2);

        yErrUpr.push_back(0);
        yErrLwr.push_back(0);

        double quadSubUp    = pow( pow(totalGraph->GetErrorXhigh(i),2) - pow(statGraph->GetErrorXhigh(i),2) ,0.5);        
        double quadSubLow   = pow( pow(totalGraph->GetErrorXlow(i),2) - pow(statGraph->GetErrorXlow(i),2) ,0.5);        

        x.push_back(totalGraph->GetX()[i] + xOffset);
        xErrUpr.push_back(quadSubUp);
        xErrLwr.push_back(quadSubLow);

        cout << totalGraph->GetY()[i] << "\t" << totalGraph->GetX()[i] << "\t" << totalGraph->GetErrorXhigh(i) << "\t" << statGraph->GetErrorXhigh(i) << endl;



    }

    auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);
    formatGraph(gr, "isSys");

    return gr;
}


ScanInfo read1DScan(TString basefolderName, TString poiName, TString dataLabel, TString scanLabel, TString label)
{


    ScanInfo sInfo;
    sInfo.POIName  = poiName;
    sInfo.dataType = dataLabel;
    sInfo.scanType = scanLabel;

    TString folderName = basefolderName + "/" + scanLabel + "/" + dataLabel + "/" + poiName + ".root";

    //TString folderName = basefolderName + "/" + scanLabel + "/" + dataLabel + "/sJob/mH/mH_sJob0.root";
    folderName.ReplaceAll("XXX", label);

    cout << "folderName: " << folderName << endl;

    auto poiList = tokenizeStr(poiName, ",");
    std::reverse(poiList.begin(),poiList.end());

    if(doesExist(folderName.Data()))
    {
        
        TChain *chain = new TChain("results");
        chain->Add(folderName);

        // Read the NLL into a map
        map<double,double> NLL = read1DVar("nll", chain);
        fixOffset(NLL);
        printFitInfo(NLL, sInfo.bestFitVal, sInfo.lwrErr, sInfo.uprErr);
    }
    else
    {
        sInfo.bestFitVal = -999;
        sInfo.lwrErr = -999;
        sInfo.uprErr = -999;
    }
    

    return sInfo;
}



void getMaxMin(TGraphAsymmErrors* g, double& minXF, double& maxXF)
{

        for(int i = 0; i < g->GetN(); i++)
        {
            double maxX = g->GetX()[i] + g->GetErrorXhigh(i);
            double minX = g->GetX()[i] - g->GetErrorXlow(i);
            if(minX < minXF) minXF = minX;
            if(maxX > maxXF) maxXF = maxX;
        }
    
}

bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
    
    opts["type"]      = "channels";
    opts["scanType"]        = "1D";
    opts["baseName"]        = "root-files/J4_Analytic_perBDTBin_constrained_XXX_PerBDTBin_fullMC_workspace";
    opts["asimovLabel"]    	= "asimov";
    opts["dataLabel"]    	= "data";
    opts["statOnlyLabel"]   = "statOnly";
    opts["sysAllLabel"]    	= "allSys";
    opts["poiName"]         = "mH";
    opts["dataset"]         = "asimov";
    opts["symmetrizeErr"]   = "false";
    opts["doSys"]           = "false";
    opts["label"]           = "v1";
    opts["plotDir"]         = "Plots";


    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") 
        {
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    return true;
}

void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;

    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;
    
    new TColor(ci, 0/255.,    66/255.,    125/255.); //54
    ci++;

    new TColor(ci, 254/255., 139/255., 113/255.); //55
    ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;          
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++; 
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    //new TColor(ci, 0.6350,    0.0780,    0.1840); //62
    //ci++;
    //new TColor(ci, 142.0/255 , 0.0/255 , 62.0/255);
    //ci++;
    //new TColor(ci, 96.0/255 , 78.0/255 , 0.0/255);
    //ci++;
    //new TColor(ci, 92.0/255 , 174.0/255 , 0.0/255);
    //ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}

void formatGraph(TGraphAsymmErrors* gr, TString extraQualifier)
{

    TString typeName = opts["type"];

    int ggFC_light   = TColor::GetColor("#67B2FF");
    int VBFC_light   = TColor::GetColor("#99E148");
    int VHC_light    = TColor::GetColor("#EB9B35");
    int ttHC_light   = TColor::GetColor("#236AEB");
    int ZZC_light    = TColor::GetColor("#BA0A07");
    int ttVC_light   = TColor::GetColor("#D1CE0F");

    int ggFC_dark   = TColor::GetColor("#3E6B99");
    int VBFC_dark   = TColor::GetColor("#6DA133");
    int VHC_dark    = TColor::GetColor("#B87929");
    int ttHC_dark   = TColor::GetColor("#1F5BCC");
    int ZZC_dark    = TColor::GetColor("#A30907");
    int ttVC_dark   = TColor::GetColor("#A8A623");

    int lightColour = kGray+2;
    int darkColour = kBlack;

    if(typeName.Contains("gg2H"))  { lightColour = ggFC_light; darkColour = ggFC_dark; }
    if(typeName.Contains("ggF"))   { lightColour = ggFC_light; darkColour = ggFC_dark; }
    if(typeName.Contains("VBF"))   { lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(typeName.Contains("VH"))    { lightColour = VHC_light;  darkColour = VHC_dark; }
    if(typeName.Contains("qq2Hqq")){ lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(typeName.Contains("qqH2qq")){ lightColour = VBFC_light; darkColour = VBFC_dark; }
    if(typeName.Contains("qq2Hll")){ lightColour = VHC_light;  darkColour = VHC_dark; }
    if(typeName.Contains("ttH"))   { lightColour = ttHC_light; darkColour = ttHC_dark; }
    if(typeName.Contains("r_ZZ"))  { lightColour = ZZC_light;  darkColour = ZZC_dark; }
    if(typeName.Contains("r_ttV")) { lightColour = ttVC_light; darkColour = ttVC_dark; }

    if(typeName.Contains("channels"))   { lightColour = ggFC_dark; darkColour = ggFC_dark; }
    if(typeName.Contains("4mu"))   { lightColour = VBFC_dark; darkColour = VBFC_light; }
    if(typeName.Contains("16Cat") || typeName.Contains("11Cat"))   { lightColour = kMagenta+1; darkColour = kMagenta+2; }

    gr->SetFillColor(lightColour);
    gr->SetLineColor(lightColour);
    gr->SetLineWidth(2);
    gr->SetMarkerColor(darkColour);        
    gr->SetMarkerStyle(21);
    gr->SetMarkerSize(1);

    // if(!extraQualifier.EqualTo("None"))
    // {
    //     gr->SetLineColor(kBlack);
    //     gr->SetFillColor(kWhite);
    //     gr->SetFillStyle(0);
    //     gr->SetMarkerSize(0.);
    //     gr->SetLineWidth(1);
    
      
    // }




}



vector<PlotStruct> getLegendEntry(TString type, double shiftedminXF, double shiftedmaxXF)
{
    vector<PlotStruct> plotStructList;


    vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;



    if(type.EqualTo("16Cat") || opts["type"].EqualTo("11Cat") ) y.push_back(18 + 0.895);
    else  y.push_back(5 + 0.895);
    yErrUpr.push_back(0);
    yErrLwr.push_back(0);

    // x.push_back(-0.970);
    // x.push_back(shiftedminXF + 0.6137* (shiftedmaxXF - shiftedminXF));
        x.push_back(shiftedminXF + 0.6128* (shiftedmaxXF - shiftedminXF));

    xErrUpr.push_back(0.075);
    xErrLwr.push_back(0.075);
    
    auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);

    PlotStruct plotStruct;
    plotStruct.gr = gr;
    plotStruct.POIName = "dummy";
    formatGraph(plotStruct.gr, "Sys");
    plotStructList.push_back(plotStruct);

    return plotStructList;
}





TString pdfRound(double value, double lwrErr, double uprErr)
{
    // Lets symtrize the Sys
    double val = value;
    double err = (fabs(lwrErr) + fabs(uprErr))/2;
    double smallErr = fabs(lwrErr);
    if(fabs(uprErr) < smallErr) smallErr = fabs(uprErr);

    double diffErr = fabs((fabs(lwrErr) - fabs(uprErr)))/err;

    bool sym = false;
    if(diffErr < 0.20) sym = true;


    auto threeDigits = [](float x) 
    { 
        TString toParse;
        toParse.Form("%.2e", x);
        TRegexp re("e.*");
        toParse(re) = "";
        toParse = toParse.ReplaceAll(".","").ReplaceAll("+","").ReplaceAll("-","");
        return toParse.Atoi();
    };

    auto nSignificantDigits = [](int threeDigits) 
    { 
        if(threeDigits < 101) return 2;
        if(threeDigits < 356) return 2;
        if(threeDigits < 950) return 1 + 1;
        return 2;
    };


    auto frexp10 = [](float x) 
    { 
        TString toParse;
        toParse.Form("%e", x);
        TRegexp upre("e.*");
        TRegexp dWre(".*e");
        TString upStr = toParse;
        TString dwStr = toParse;

        upStr(upre) = "";
        dwStr(dWre) = "";
        return vector<double> {upStr.Atof(), dwStr.Atof()};
    };


    auto nDigitsValue = [](double expVal, double expErr, double nDigitsErr) 
    { 
        return expVal-expErr+nDigitsErr;
    };


    auto formatValue = [](float value, int exponent, int nDigits, int extraRound) 
    { 
        auto roundAt = nDigits - 1 - exponent - extraRound;
        auto nDec = 0;
        if(exponent < nDigits) nDec = roundAt;
        if(nDec < 0) nDec = 0;

        TString roundExp = "\%." + TString::Itoa(nDec, 10) + "f";
        TString outFormat = "";
        outFormat.Form(roundExp, value);
        return outFormat;
    };


    // For upper and lower error
    auto tD = threeDigits(err);
    auto nD = nSignificantDigits(tD);

    // cout<<"smallErr: "<<smallErr<<" tD: "<<tD<<" nD: "<<nD<<endl;

    auto expVal = frexp10(val)[1];
    auto expErr = frexp10(err)[1];

    double extraRound = 0;
    if(tD >= 950) extraRound = 1;


    auto cVal     = formatValue(val, expVal, nDigitsValue(expVal, expErr, nD), extraRound);
    auto clwrErr  = formatValue(lwrErr, expErr, nD, extraRound);
    auto cuprErr  = formatValue(uprErr, expErr, nD, extraRound);
    auto cErr     = formatValue(err, expErr, nD, extraRound);


    TString formatErr = "";
    formatErr.Form("%s^{ #plus %s}_{ #minus %s}", cVal.Data(), clwrErr.Data(), cuprErr.Data());


    if(sym) formatErr.Form("%s #pm %s", cVal.Data(), cErr.Data());

    return formatErr;

}

