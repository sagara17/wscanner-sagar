#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TString.h>
#include <stdio.h>
#include <TGraphAsymmErrors.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"

using namespace std;

// Global vars
map<TString,TString> opts;

struct ScanInfo
{
	double bestFitVal;
	double lwrErr;
	double uprErr;

	TString POIName;
	TString scanType;
	TString dataType;
};

// Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
vector<ScanInfo> getPOIInfo(TString folderName, TString dataLabel, TString scanLabel, TString dataType, TString scanType);
std::map<TString, TString> commonMetaData;
void setPrettyStuff();


TGraphAsymmErrors* getGraph(std::vector<TString> uniquePOIList, vector<ScanInfo> scanInfo, double offset = 0);

int main(int argc, char** argv)
{
    if (!cmdline(argc,argv,opts)) return 0;
    setPrettyStuff();

	auto folderList = tokenizeStr(opts["folderList"], ",");
	auto labelList  = tokenizeStr(opts["labelList"], ",");
 
	map<TString, map<TString, vector<ScanInfo>>> scanInfoMap;
	map<TString, TString> labelMap;

	int fList = 0;
	for(const auto& folderName: folderList)
	{
		labelMap[folderName] = labelList[fList];
		fList++;
		scanInfoMap[folderName]["asimov_StatOnly"] = getPOIInfo(folderName, opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly");
    	scanInfoMap[folderName]["asimov_SysAll"]   = getPOIInfo(folderName, opts["asimovLabel"], opts["sysAllLabel"], 	"asimov", "sysAll");

        scanInfoMap[folderName]["data_StatOnly"] = getPOIInfo(folderName, opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly");
        scanInfoMap[folderName]["data_SysAll"]   = getPOIInfo(folderName, opts["dataLabel"], opts["sysAllLabel"],   "data", "sysAll");

	}

    auto poiList = tokenizeStr(opts["poiList"], ",");
    std::reverse(poiList.begin(),poiList.end());

	std::set<TString> uniquePOIList;
	std::set<TString> uniqueFolderList;


	for(const auto& scanInfo: scanInfoMap)
	{
		uniqueFolderList.insert(scanInfo.first);
		for(const auto& scanType: scanInfo.second)
		{
			for(const auto& poiInfo: scanType.second)
			{
				uniquePOIList.insert(poiInfo.POIName);
			}
		}
	}
	map<TString, TGraphAsymmErrors*> sysGraph;
	map<TString, TGraphAsymmErrors*> statGraph;

    map<TString, TGraphAsymmErrors*> datasysGraph;
    map<TString, TGraphAsymmErrors*> datastatGraph;


    double gOffset = 0.2;
	for(const auto& folderName: uniqueFolderList)
	{
		auto gr = getGraph(poiList, scanInfoMap[folderName]["asimov_StatOnly"], - gOffset);
		statGraph[folderName] = gr;

		gr = getGraph(poiList, scanInfoMap[folderName]["asimov_SysAll"], - gOffset);
		sysGraph[folderName] = gr;

        gr = getGraph(poiList, scanInfoMap[folderName]["data_StatOnly"], + gOffset);
        datastatGraph[folderName] = gr;

        gr = getGraph(poiList, scanInfoMap[folderName]["data_SysAll"], + gOffset);
        datasysGraph[folderName] = gr;

	}


	// Start plotting the graph
    double minXF = 10000000000000;
    double maxXF = -1000000000000;

    for(const auto& gr: sysGraph)
    {
    	auto g = gr.second;
    	for(int i = 0; i < g->GetN(); i++)
    	{
    		double maxX = g->GetX()[i] + g->GetErrorXhigh(i);
    		double minX = g->GetX()[i] - g->GetErrorXlow(i);
	        if(minX < minXF) minXF = minX;
	        if(maxX > maxXF) maxXF = maxX;
    	}
    }
    for(const auto& gr: datasysGraph)
    {
        auto g = gr.second;
        for(int i = 0; i < g->GetN(); i++)
        {
            double maxX = g->GetX()[i] + g->GetErrorXhigh(i);
            double minX = g->GetX()[i] - g->GetErrorXlow(i);
            if(minX < minXF) minXF = minX;
            if(maxX > maxXF) maxXF = maxX;
        }
    }

    double shiftedminXF = minXF - (maxXF - minXF) * 0.10;
    double shiftedmaxXF = maxXF + (maxXF - minXF) * 0.60;

    double labelStat    = maxXF + (maxXF - minXF) * 0.325;
    double labelSys     = maxXF + (maxXF - minXF) * 0.05;

    int offSet = 5;
    if(poiList.size() > 8) offSet ++;
    TH2D* axisHist = new TH2D("axis","axis", 1, shiftedminXF, shiftedmaxXF, poiList.size()+offSet + 1, -1, poiList.size()+offSet);   

    // axisHist->SetMaximum(poiList.size()+5);
    // axisHist->SetMinimum(-1);

    axisHist->GetYaxis()->SetTitle("POI");
    axisHist->GetXaxis()->SetTitle("#sigma/#sigma_{SM}");
    axisHist->GetXaxis()->SetTitleSize(0.8*axisHist->GetXaxis()->GetTitleSize());
    if(poiList.size() > 8) axisHist->GetYaxis()->SetLabelSize(0.8*axisHist->GetYaxis()->GetLabelSize());
    // axisHist->GetXaxis()->SetTitleOffset(1.25*axisHist->GetXaxis()->GetTitleOffset());
    // axisHist->GetXaxis()->SetLabelOffset(2.5*axisHist->GetXaxis()->GetLabelOffset());

    int counter = 0;
    for(const auto& poiName: poiList)
    {
    	axisHist->GetYaxis()->SetBinLabel(counter + 2, getFancyXname(poiName));
    	// axisHist->SetBinContent(counter + 1, -99999);
    	counter++;
    }
	// axisHist->GetXaxis()->LabelsOption("v");

    // format the plots
    int numDraw = 1754;
    for(const auto& gr :statGraph)
    {
    	statGraph[gr.first]->SetMarkerSize(0);
    	statGraph[gr.first]->SetLineWidth(2);
    	statGraph[gr.first]->SetLineColor(numDraw+1);
    	statGraph[gr.first]->SetMarkerColor(numDraw+1);
  
    	sysGraph[gr.first]->SetMarkerSize(0.8);
    	sysGraph[gr.first]->SetLineWidth(2);
    	sysGraph[gr.first]->SetLineColor(numDraw+1);
    	sysGraph[gr.first]->SetMarkerColor(numDraw+1);
    	numDraw++;

        datastatGraph[gr.first]->SetMarkerSize(0);
        datastatGraph[gr.first]->SetLineWidth(2);
        datastatGraph[gr.first]->SetLineColor(numDraw+1);
        datastatGraph[gr.first]->SetMarkerColor(numDraw+1);
  
        datasysGraph[gr.first]->SetMarkerSize(0.8);
        datasysGraph[gr.first]->SetLineWidth(2);
        datasysGraph[gr.first]->SetLineColor(numDraw+1);
        datasysGraph[gr.first]->SetMarkerColor(numDraw+1);
        numDraw++;

    }


    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 600, 600);
    if(poiList.size() > 8) c1->SetLeftMargin(0.20);;
    if(poiList.size() > 8) c1->SetBottomMargin(-0.05);;
    // TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 800, 600);
    // 
    // c1->SetBottomMargin(0.3);
    TLegend *elLeg =  new TLegend (0.6, 0.80, 0.90, 0.925);
    elLeg->SetFillColor(0);
    elLeg->SetBorderSize(0);
    elLeg->SetTextFont(42);
    elLeg->SetTextSize(0.0275);

    // draw the canvas
    axisHist->Draw();

    // Fancy title stuff
    double leftDist = 0.19;
    if(poiList.size() > 8) leftDist = 0.25;
    TString saveName = "";
    for(const auto& gr: sysGraph) gr.second->Draw("ZP");
	gStyle->SetEndErrorSize(5.0);
    for(const auto& gr: statGraph) gr.second->Draw("[]");
    for(const auto& gr: datasysGraph) gr.second->Draw("ZP");
    gStyle->SetEndErrorSize(5.0);
    for(const auto& gr: datastatGraph) gr.second->Draw("[]");

    // Write the numbers
	for(const auto& gr: datasysGraph)
	{
    	auto g = gr.second;
    	for(int i = 0; i < g->GetN()+1; i++)
    	{
    		double deltaX = 0.2;
		 	std::ostringstream outMinValXStr;
            if(i < g->GetN())
            {
                outMinValXStr << std::setprecision(3) << fixed<< g->GetX()[i];
                outMinValXStr << "^{+" << g->GetErrorXhigh(i) << "}";
                outMinValXStr << "_{-" << g->GetErrorXlow(i) << "}";
            }
            else outMinValXStr << "sys";
            // auto fitXInfo = new TLatex (0.15 + i , minYF  - (maxYF - minYF) * 0.15 , TString(outMinValXStr.str()));
            auto fitXInfo = new TLatex (labelSys , i+0.35 , TString(outMinValXStr.str()));
	        // fitXInfo->SetNDC();
	        // fitXInfo->SetTextSize(0.015);
	        fitXInfo->SetTextSize(0.025);
            if(poiList.size() > 8) fitXInfo->SetTextSize(0.0225);
	        fitXInfo->SetTextFont(42);
	        fitXInfo->Draw();
            fitXInfo->Draw();
	 	}
	}
    for(const auto& gr: datastatGraph)
    {
        auto g = gr.second;
        for(int i = 0; i < g->GetN()+1; i++)
        {
            double deltaX = 0.2;
            std::ostringstream outMinValXStr;
            if(i < g->GetN())
            {
                outMinValXStr << std::setprecision(3) << fixed<< g->GetX()[i];
                outMinValXStr << "^{+" << g->GetErrorXhigh(i) << "}";
                outMinValXStr << "_{-" << g->GetErrorXlow(i) << "}";
            }
            else outMinValXStr << "stat";
            // auto fitXInfo = new TLatex (0.15 + i , minYF  - (maxYF - minYF) * 0.15 , TString(outMinValXStr.str()));
            auto fitXInfo = new TLatex (labelStat , i+0.35 , TString(outMinValXStr.str()));
            // fitXInfo->SetNDC();
            // fitXInfo->SetTextSize(0.015);
            fitXInfo->SetTextSize(0.025);
            if(poiList.size() > 8) fitXInfo->SetTextSize(0.0225);
            fitXInfo->SetTextFont(42);
            fitXInfo->Draw();
            fitXInfo->Draw();
        }
    }


    for(auto& gr: sysGraph) elLeg->AddEntry(gr.second, labelMap[gr.first], "PE");
    for(auto& gr: datasysGraph) elLeg->AddEntry(gr.second, "data", "PE");

    elLeg->Draw();

    ATLASLabel(leftDist, 0.875, "Internal", 1);

    TString channelStr = "H #rightarrow ZZ* #rightarrow 4l";
    // channelStr = labelsList[0]["labelName"];

    TLatex* tMain = new TLatex (leftDist, 0.81, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.76, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();
	gPad->RedrawAxis();
   
    c1->SaveAs("test.eps");

	
}


TGraphAsymmErrors* getGraph(std::vector<TString> uniquePOIList, vector<ScanInfo> scanInfo, double offset)
{
	vector<double> x, y, xErrUpr, xErrLwr, yErrUpr, yErrLwr;

	double counter = 0.5;

	for(const auto& poiName: uniquePOIList)
	{
		for(const auto& poiInfo: scanInfo)
		{
			if(!poiInfo.POIName.EqualTo(poiName)) continue;
			y.push_back(counter + offset);
			yErrUpr.push_back(0);
			yErrLwr.push_back(0);

			x.push_back(poiInfo.bestFitVal);
			xErrUpr.push_back(fabs(poiInfo.uprErr));
			xErrLwr.push_back(fabs(poiInfo.lwrErr));
		}
		counter+=1;
	}

	auto gr = new TGraphAsymmErrors(x.size(), &x[0], &y[0], &xErrLwr[0], &xErrUpr[0], &yErrLwr[0], &yErrUpr[0]);
	return gr;
}


vector<ScanInfo> getPOIInfo(TString basefolderName, TString dataLabel, TString scanLabel, TString dataType, TString scanType)
{
	TString folderName = basefolderName + "/" + scanLabel + "/" + dataLabel + "/";

	vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(opts["poiList"], ",");
    std::reverse(poiList.begin(),poiList.end());

    // read all the info
    for(const auto& poiName: poiList)
    {
    	TString fileName = folderName + "/" + poiName + ".root";
        ScanInfo sInfo;
        sInfo.POIName  = poiName;
        sInfo.dataType = dataType;
        sInfo.scanType = scanType;

    	if(doesExist(fileName.Data()))
    	{
	    	auto label = getLabels(vector<TString>{fileName}).at(0);
	    	commonMetaData = label;
			
			TChain *chain = new TChain("results");
	        chain->Add(fileName);

	        // Read the NLL into a map
	        map<double,double> NLL = read1DVar("nll", chain);
	        fixOffset(NLL);
	        printFitInfo(NLL, sInfo.bestFitVal, sInfo.lwrErr, sInfo.uprErr);
		}
		else
		{
			sInfo.bestFitVal = -999;
			sInfo.lwrErr = -999;
			sInfo.uprErr = -999;
		}
        sInfoList.push_back(sInfo);
    }
    return sInfoList;
}



bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["folderList"]      = "root-files/STXS_XS_S0_v19_NN_withSys";
    opts["labelList"]       = "Asimov";
    opts["asimovLabel"]    	= "asimov";
    opts["dataLabel"]    	= "data";

    opts["statOnlyLabel"]   = "statOnly";
    opts["sysAllLabel"]    	= "allSys";

    opts["poiList"]  		= "mu_gg2H,mu_VBF,mu_VH,mu_ttH";


    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") 
        {
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    return true;
}

void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;

    new TColor(ci, 0,    0.4470,    0.7410); //56
    ci++;
    
    new TColor(ci, 0/255.,    66/255.,    125/255.); //54
    ci++;

    new TColor(ci, 254/255., 139/255., 113/255.); //55
    ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;          
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++; 
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    //new TColor(ci, 0.6350,    0.0780,    0.1840); //62
    //ci++;
    //new TColor(ci, 142.0/255 , 0.0/255 , 62.0/255);
    //ci++;
    //new TColor(ci, 96.0/255 , 78.0/255 , 0.0/255);
    //ci++;
    //new TColor(ci, 92.0/255 , 174.0/255 , 0.0/255);
    //ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}
