#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fstream>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLine.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TString.h>
#include <TString.h>
#include <TGraphAsymmErrors.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"
#include <algorithm>

// RooFit includes
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooArgSet.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/AsymptoticCalculator.h"
#include "RooMinimizer.h"
#include "RooNLLVar.h"
#include "TStopwatch.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include "RooNumIntConfig.h"
#include "RooMinuit.h"
#include "RooRealSumPdf.h"

using namespace std;

// Global vars
map<TString,TString> opts;

struct ScanInfo
{
    double bestFitVal;
    double lwrErr;
    double uprErr;

    TString POIName;
    TString scanType;
    TString dataType;
};



// Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
vector<ScanInfo> getPOIInfo(TString dataLabel, TString scanLabel, TString dataType, TString scanType, TString cPOIList);
vector<ScanInfo> dounCertDecom(vector<ScanInfo>& firstList, vector<ScanInfo>& secondList);

std::map<TString, TString> commonMetaData;

RooWorkspace* m_ws;
void fillWS(TString dataLabel, TString scanLabel, TString dataType, TString scanType, TString cPOIList);

vector<double> getBin();
TString getPOIList();

vector<double> getZZBin();
TString getZZPOIList();
vector<double> getZZIntBin();

void saveTGraph(vector<ScanInfo> scanInfo, vector<ScanInfo> ZZscanInfo, TString baseName);
TGraphAsymmErrors* getTGraph(vector<ScanInfo> scanInfo, vector<double> binList);

int main(int argc, char** argv)
{
    if (!cmdline(argc,argv,opts)) return 0;

    m_ws = NULL;
    map<TString, vector<ScanInfo>> scanInfoMap;

    auto signalPOIList = getPOIList();
    fillWS(opts["asimovLabel"], opts["sysAllLabel"], "asimov", "sysAll", signalPOIList);

    scanInfoMap["data_StatOnly"] = getPOIInfo(opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly", signalPOIList);
    scanInfoMap["data_SysAll"]   = getPOIInfo(opts["dataLabel"], opts["sysAllLabel"],   "data", "sysAll", signalPOIList);
    scanInfoMap["data_SysOnly"]  = dounCertDecom(scanInfoMap["data_SysAll"], scanInfoMap["data_StatOnly"]);



    scanInfoMap["asimov_StatOnly"] = getPOIInfo(opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly", signalPOIList);
    scanInfoMap["asimov_SysAll"]   = getPOIInfo(opts["asimovLabel"], opts["sysAllLabel"],   "asimov", "sysAll", signalPOIList);
    scanInfoMap["asimov_SysOnly"]  = dounCertDecom(scanInfoMap["asimov_SysAll"], scanInfoMap["asimov_StatOnly"]);



    auto ZZPOIList = getZZPOIList();

    scanInfoMap["ZZ_data_StatOnly"] = getPOIInfo(opts["dataLabel"], opts["statOnlyLabel"], "data", "statOnly", ZZPOIList);
    scanInfoMap["ZZ_data_SysAll"]   = getPOIInfo(opts["dataLabel"], opts["sysAllLabel"],   "data", "sysAll", ZZPOIList);
    scanInfoMap["ZZ_data_SysOnly"]  = dounCertDecom(scanInfoMap["ZZ_data_SysAll"], scanInfoMap["ZZ_data_StatOnly"]);

    scanInfoMap["ZZ_asimov_StatOnly"] = getPOIInfo(opts["asimovLabel"], opts["statOnlyLabel"], "asimov", "statOnly", ZZPOIList);
    scanInfoMap["ZZ_asimov_SysAll"]   = getPOIInfo(opts["asimovLabel"], opts["sysAllLabel"],   "asimov", "sysAll", ZZPOIList);
    scanInfoMap["ZZ_asimov_SysOnly"]  = dounCertDecom(scanInfoMap["ZZ_asimov_SysAll"], scanInfoMap["ZZ_asimov_StatOnly"]);


    // saving the TGraph
    saveTGraph(scanInfoMap["data_SysAll"],  scanInfoMap["ZZ_data_SysAll"],  "wsysdata");
    saveTGraph(scanInfoMap["data_SysOnly"], scanInfoMap["ZZ_data_SysOnly"], "wonlysysdata");

    saveTGraph(scanInfoMap["asimov_SysAll"],  scanInfoMap["ZZ_asimov_SysAll"],  "wsysExpected");
    saveTGraph(scanInfoMap["asimov_SysOnly"], scanInfoMap["ZZ_asimov_SysOnly"], "wonlysysExpected");


}
void saveTGraph(vector<ScanInfo> scanInfo, vector<ScanInfo> ZZscanInfo, TString baseName)
{

    auto signalG = getTGraph(scanInfo, getBin());
    auto zzG = getTGraph(ZZscanInfo, getZZBin());
    auto zzIntG = getTGraph(ZZscanInfo, getZZIntBin());


    TString fileName = opts["varType"] + "_" + baseName + ".root";
    TString folderName = opts["outputDir"] + "/" + opts["varType"] + "/";
    system("mkdir -vp " + folderName);

    TFile* inFile = new TFile(folderName + "/" + fileName, "recreate");
    signalG->Write(opts["varType"]);
    zzG->Write(opts["varType"] + "_ZZ");
    zzIntG->Write(opts["varType"] + "_ZZINT");
    inFile->Close();

    delete signalG;
    delete zzG;
    delete inFile;


}

TGraphAsymmErrors* getTGraph(vector<ScanInfo> scanInfo, vector<double> binList)
{
    vector<double> xNom;
    vector<double> xUpr;
    vector<double> xLwr;
    vector<double> yNom;
    vector<double> yUpr;
    vector<double> yLwr;

    for(size_t i = 0; i < scanInfo.size(); i++)
    {
        yNom.push_back(scanInfo.at(i).bestFitVal);
        yUpr.push_back(fabs(scanInfo.at(i).uprErr));
        yLwr.push_back(fabs(scanInfo.at(i).lwrErr));

        double binCen = (binList[i+1] + binList[i])/2;
        double binWidth = (binList[i+1] - binList[i])/2;

        xNom.push_back(binCen);
        xUpr.push_back(binWidth);
        xLwr.push_back(binWidth);
    }

    auto g = new TGraphAsymmErrors (xNom.size(), &xNom[0], &yNom[0], &xLwr[0], &xUpr[0], &yLwr[0], &yUpr[0]);

    return g;
}


vector<ScanInfo> getPOIInfo(TString dataLabel, TString scanLabel, TString dataType, TString scanType, TString sPoiList)
{
    TString folderName = opts["folder"] + "/" + scanLabel + "/" + dataLabel + "/";

    vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(sPoiList, ",");

    // read all the info
    for(const auto& poiName: poiList)
    {
    TString fileName = folderName + "/" + poiName + ".root";
        ScanInfo sInfo;
        sInfo.POIName  = poiName;
        sInfo.dataType = dataType;
        sInfo.scanType = scanType;

        if(doesExist(fileName.Data()))
        {
            auto label = getLabels(vector<TString>{fileName}).at(0);
            commonMetaData = label;
            
            TChain *chain = new TChain("results");
            chain->Add(fileName);

            // Read the NLL into a map
            map<double,double> NLL = read1DVar("nll", chain);
            fixOffset(NLL);
            printFitInfo(NLL, sInfo.bestFitVal, sInfo.lwrErr, sInfo.uprErr);
        }
        else
        {
            sInfo.bestFitVal = -999;
            sInfo.lwrErr = -999;
            sInfo.uprErr = -999;
        }

        cout<<folderName<<sInfo.POIName<<" best fit: "<<sInfo.bestFitVal<<" lwrErr: "<<sInfo.lwrErr<<" uprErr: "<<sInfo.uprErr<<endl;
        sInfoList.push_back(sInfo);
    }
    return sInfoList;
}

vector<ScanInfo> dounCertDecom(vector<ScanInfo>& firstList, vector<ScanInfo>& secondList)
{
    vector<ScanInfo> sInfoList;

    for(size_t i = 0; i < firstList.size(); i++)
    {
        ScanInfo sInfo = firstList[i];

        sInfo.lwrErr = sqrt(pow(firstList[i].lwrErr, 2)  - pow(secondList[i].lwrErr, 2))  *  firstList[i].lwrErr/fabs(firstList[i].lwrErr);
        sInfo.uprErr = sqrt(pow(firstList[i].uprErr, 2)  - pow(secondList[i].uprErr, 2))  *  firstList[i].uprErr/fabs(firstList[i].uprErr);

        sInfoList.push_back(sInfo);
    }
    
    return sInfoList;
}


bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["folder"]          = "root-files/STXS_XS_S0_v19_NN_withSys";
    opts["outputDir"]       = "TGraph/matrix";
    opts["asimovLabel"]     = "asimov";
    opts["dataLabel"]       = "data";

    opts["statOnlyLabel"]   = "statOnly";
    opts["sysAllLabel"]     = "allSys";

    opts["varType"]         = "pt";


    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") 
        {
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    return true;
}


void fillWS(TString dataLabel, TString scanLabel, TString dataType, TString scanType, TString sPoiList)
{
    // Grab metadata from one file
    TString folderName = opts["folder"] + "/" + scanLabel + "/" + dataLabel + "/";
    std::cout << folderName << std::endl;
    vector<ScanInfo> sInfoList;

    auto poiList = tokenizeStr(sPoiList, ",");

    // read all the info
    for(const auto& poiName: poiList)
    {
        TString fileName = folderName + "/" + poiName + ".root";
    std::cout << fileName << std::endl;
        if(doesExist(fileName.Data()))
        {
            auto label = getLabels(vector<TString>{fileName}).at(0);
            
            if(doesExist(label["fileName"].Data()))
            {
                TFile* wsFile = TFile::Open(label["fileName"]);
            std::cout << label["fileName"] << std::endl;
                m_ws = (RooWorkspace*) wsFile->Get(label["workspaceName"]);

                cout<<"Found the ws"<<endl;
                return;
            }
        }
    }
}

TString getPOIList()
{
    auto binList = getBin();

    TString poiList = "";

    for(size_t i = 0; i < binList.size()-1; i++)
    {
        TString poiName;
        poiName.Form("sigma_bin%zu_incl", i);


        if(opts["varType"].EqualTo("pt4l_y1")) poiName.Form("sigma_bin%zu_incl", i+3);
        if(opts["varType"].EqualTo("pt4l_y2")) poiName.Form("sigma_bin%zu_incl", i+6);
        if(opts["varType"].EqualTo("pt4l_y3")) poiName.Form("sigma_bin%zu_incl", i+8);


        if(opts["varType"].EqualTo("pt4l_pTLJ1")) poiName.Form("sigma_bin%zu_incl", i+2);
        if(opts["varType"].EqualTo("pt4l_pTLJ2")) poiName.Form("sigma_bin%zu_incl", i+4);

        if(opts["varType"].EqualTo("pt4l_Nj1")) poiName.Form("sigma_bin%zu_incl", i+4);
        if(opts["varType"].EqualTo("pt4l_Nj2")) poiName.Form("sigma_bin%zu_incl", i+8);
        if(opts["varType"].EqualTo("pt4l_Nj3")) poiName.Form("sigma_bin%zu_incl", i+10);


        if(i == binList.size() - 2) poiList += poiName;
        else poiList += poiName + ",";
    }

    if(opts["varType"].EqualTo("fid_sum")) poiList = "sigma_sum";
    if(opts["varType"].EqualTo("fid_comb")) poiList = "sigma";
    if(opts["varType"].EqualTo("fid_2l2l")) poiList = "sigma_2l2l";
    if(opts["varType"].EqualTo("fid_4l")) poiList = "sigma_4l";
    if(opts["varType"].EqualTo("fid_perChan")) poiList = "sigma_4mu,sigma_4e,sigma_2mu2e,sigma_2e2mu";
    if(opts["varType"].EqualTo("total")) poiList = "sigma";

    return poiList;
}

vector<double> getBin()
{
    map<TString, vector<double>> m_binMap;
    double pi = TMath::Pi();

    // Fiducial
    m_binMap["fid_perChan"]         = vector<double> {0, 1, 2, 3, 4};
    m_binMap["fid_4mu"]             = vector<double> {0,1};
    m_binMap["fid_4e"]              = vector<double> {0,1};
    m_binMap["fid_2mu2e"]           = vector<double> {0,1};
    m_binMap["fid_2e2mu"]           = vector<double> {0,1};
    m_binMap["fid_4l"]              = vector<double> {0,1};
    m_binMap["fid_2l2l"]            = vector<double> {0,1};
    m_binMap["fid_sum"]             = vector<double> {0,1};
    m_binMap["fid_comb"]            = vector<double> {0,1};
    m_binMap["total"]               = vector<double> {0,1};

    // Higgs vars
    m_binMap["pt"]                  = vector<double> {0.0, 10.0,  20.0,   30.0,   45.0, 60.0, 80.0, 120.0,  200.0,  350.0, 1000.0};
    m_binMap["m12"]                 = vector<double> {50.0, 64.0, 73.0, 85.0, 106.0};
    m_binMap["m34"]                 = vector<double> {12.0, 20.0, 24.0, 28.0, 32.0, 40.0, 55.0, 65.0};
    m_binMap["y"]                   = vector<double> {0.0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.2, 1.6, 2.0, 2.5};
    m_binMap["cts"]                 = vector<double> {0.0, 0.125, 0.250, 0.375, 0.500, 0.625, 0.750, 0.875, 1.0};
    m_binMap["ct1"]                 = vector<double> {-1.0, -0.750, -0.500, -0.250, 0.0, 0.250, 0.500, 0.750, 1.0};
    m_binMap["ct2"]                 = vector<double> {-1.0, -0.750, -0.500, -0.250, 0.0, 0.250, 0.500, 0.750, 1.0};
    m_binMap["phi"]                 = vector<double> {-pi, -6*pi/8, -4*pi/8, -2*pi/8, 0.0, 2*pi/8, 4*pi/8, 6*pi/8, pi};
    m_binMap["phi1"]                = vector<double> {-pi, -6*pi/8, -4*pi/8, -2*pi/8, 0.0, 2*pi/8, 4*pi/8, 6*pi/8, pi};


    // Jet vars
    m_binMap["njet"]                = vector<double> {0, 1, 2, 3, 4};
    m_binMap["n_bjets"]             = vector<double> {0, 1, 2, 3};
    m_binMap["ljetPt"]              = vector<double> {29.0, 30.0,  60.0,   120.0,  350.0};
    m_binMap["sljetPt"]             = vector<double> {29.0, 30.0,  60.0,   120.0,  350.0};

    m_binMap["mjj"]                 = vector<double> {-1, 0, 120.0, 450.0, 3000.0};
    m_binMap["etajj"]               = vector<double> {-1, 0, 1, 2.5, 9.0};
    m_binMap["phijj"]               = vector<double> {-1, 0.0, 0.5*pi, pi, 1.5*pi, 2*pi};


    // To come
    m_binMap["m4lj"]                = vector<double> {119, 120, 180, 220, 300, 400, 600, 2000};
    m_binMap["m4ljj"]               = vector<double> {179, 180, 320, 450, 600, 1000, 2500};
    m_binMap["pt4lj"]               = vector<double> {-1.0, 0.0, 60.0, 120.0, 350.0};
    m_binMap["pt4ljj"]              = vector<double> {-1.0, 0.0, 60.0, 122.0, 350.0};


    // 2D vars - this we will do one plot
    m_binMap["m12m34"]              = vector<double> {0, 1, 2, 3, 4, 5};

    // How we want do this
    // Per slice? // THese will need some type off offset
    m_binMap["pt4l_Nj0"]              = vector<double> {0.0,  15.0,  30.0,  120.0, 350.0};
    m_binMap["pt4l_Nj1"]              = vector<double> {0.0,  60.0,  80.0,  120.0, 350.0};
    m_binMap["pt4l_Nj2"]              = vector<double> {0.0, 120.0, 350.0};
    m_binMap["pt4l_Nj3"]              = vector<double> {0.0, 120.0, 350.0};  
    
    m_binMap["pt4l_Nj"]               = vector<double> {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};  

    m_binMap["pt4l_y0"]             = vector<double> {0.0, 45.0, 120.0, 350.0}; //|y|<0.5
    m_binMap["pt4l_y1"]             = vector<double> {0.0, 45.0, 120.0, 350.0}; //0.5<|y|<1
    m_binMap["pt4l_y2"]             = vector<double> {0.0, 45.0, 120.0, 350.0}; //1<|y|<1.5
    m_binMap["pt4l_y3"]             = vector<double> {0.0, 45.0, 120.0, 350.0}; //1.5|y|<2.5

    m_binMap["pt4l_y"]               = vector<double> {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};  


    //pt4l vs ptj1
    m_binMap["pt4l_pTLJ0"]             = vector<double> {0.0,  80.0, 350.0}; //30<ljpt<60
    m_binMap["pt4l_pTLJ1"]             = vector<double> {0.0, 120.0, 350.0}; //60<ljpt<120
    m_binMap["pt4l_pTLJ2"]             = vector<double> {0.0, 120.0, 350.0}; //120<ljpt<350

    m_binMap["pt4l_pTLJ"]              = vector<double> {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; //120<ljpt<350


    // 
    m_binMap["n_jet_v2"]            = vector<double> {0, 1, 20};
    m_binMap["n_jet_v3"]            = vector<double> {0, 1, 2, 20};
    m_binMap["n_jet_v4"]            = vector<double> {0, 1, 2, 3, 4, 20};

    // ljpt vs. sjpt
    m_binMap["ljetPt_sljetPt"]      = vector<double> {0, 1, 2, 3, 4, 5};

    // //pt4l vs pt4lj
    m_binMap["pt4l_pt4lj"]          = vector<double> {0, 1, 2, 3, 4, 5};

    // //pt4l vs m4lj
    m_binMap["pt4lj_m4lj"]           = vector<double> {0, 1, 2, 3, 4, 5};


    return m_binMap.at(opts["varType"]);
}

vector<double> getZZBin()
{
    map<TString, vector<double>> m_binMap;
    double pi = TMath::Pi();


    m_binMap["fid_4mu"]             = vector<double> {0,1};
    m_binMap["fid_4e"]              = vector<double> {0,1};
    m_binMap["fid_2mu2e"]           = vector<double> {0,1};
    m_binMap["fid_2e2mu"]           = vector<double> {0,1};
    m_binMap["fid_perChan"]         = vector<double> {0,1};
    m_binMap["fid_4l"]              = vector<double> {0,1};
    m_binMap["fid_2l2l"]            = vector<double> {0,1};
    m_binMap["fid_sum"]             = vector<double> {0,1};
    m_binMap["fid_comb"]            = vector<double> {0,1};
    m_binMap["total"]               = vector<double> {0,1};

    // Higgs vars
    m_binMap["pt"]                  = vector<double> {0, 10,  20, 30, 60, 1000};
    m_binMap["m12"]                 = vector<double> {50, 73, 85, 106};
    m_binMap["m34"]                 = vector<double> {12, 24, 32, 65};
    m_binMap["y"]                   = vector<double> {0.0, 0.15, 0.3, 0.45, 0.6, 0.75, 1.2, 2.5};
    m_binMap["cts"]                 = vector<double> {0.0, 0.25, 0.50, 0.75, 1.0};
    m_binMap["ct1"]                 = vector<double> {-1.0, -0.50, 0.0, 0.50, 1.0};
    m_binMap["ct2"]                 = vector<double> {-1.0, -0.50, 0.0, 0.50, 1.0};
    m_binMap["phi"]                 = vector<double> {-pi, -4*pi/8, 0.0,  4*pi/8, pi};
    m_binMap["phi1"]                = vector<double> {-pi, -4*pi/8, 0.0,  4*pi/8, pi};


    // Jet vars
    m_binMap["njet"]                = vector<double> {0, 1, 2, 4};
    m_binMap["n_bjets"]             = vector<double> {0, 1, 3};
    m_binMap["ljetPt"]              = vector<double> {29.0, 30.0,  60.0,   120.0,  350.0};
    m_binMap["sljetPt"]             = vector<double> {29.0, 30.0,  60.0,   350.0};

    m_binMap["mjj"]                 = vector<double> {-1, 0, 120.0, 3000.0};
    m_binMap["etajj"]               = vector<double> {-1, 0, 1, 9.0};
    m_binMap["phijj"]               = vector<double> {-1, 0.0, pi, 2*pi};


    // To come
    m_binMap["m4lj"]                = vector<double> {119, 120, 220, 2000};
    m_binMap["m4ljj"]               = vector<double> {179, 180, 450, 2500};
    m_binMap["pt4lj"]               = vector<double> {-1, 0, 350.0};
    m_binMap["pt4ljj"]              = vector<double> {-1, 0, 350.0};

    // 2D vars - this we will do one plot
    m_binMap["m12m34"]              = vector<double> {0, 2, 3, 4, 5};
    m_binMap["ljetPt_sljetPt"]      = vector<double> {0, 3, 5};
    m_binMap["ptjm4lj"]             = vector<double> {0, 3, 5};



    // How we want do this
    // Per slice? // THese will need some type off offset
    m_binMap["pt4l_Nj0"]            = vector<double> {0, 1};
    m_binMap["pt4l_Nj1"]            = vector<double> {0, 1};
    m_binMap["pt4l_Nj2"]            = vector<double> {0, 1};
    m_binMap["pt4l_Nj3"]            = vector<double> {0, 1};  
    
    m_binMap["pt4l_Nj"]             = vector<double> {0, 5, 10, 14};  

    m_binMap["pt4l_y0"]             = vector<double> {0.0, 350}; //|y|<0.5
    m_binMap["pt4l_y1"]             = vector<double> {0.0, 350}; //0.5<|y|<1
    m_binMap["pt4l_y2"]             = vector<double> {0.0, 350}; //1<|y|<1.5
    m_binMap["pt4l_y3"]             = vector<double> {0.0, 350}; //1.5|y|<2.5

    m_binMap["pt4l_y"]              = vector<double> {0, 4,  8, 12};  

    //pt4l vs ptj1
    m_binMap["pt4l_pTLJ0"]          = vector<double> {0.0, 350}; //30<ljpt<60
    m_binMap["pt4l_pTLJ1"]          = vector<double> {0.0, 350}; //60<ljpt<120
    m_binMap["pt4l_pTLJ2"]          = vector<double> {0.0, 350}; //120<ljpt<350

    m_binMap["pt4l_pTLJ"]           = vector<double> {0,  3, 6, 9}; //120<ljpt<350


    // ljpt vs. sjpt
    m_binMap["ljetPt_sljetPt"]      = vector<double> {0, 1, 3, 5};

    // //pt4l vs pt4lj
    m_binMap["pt4l_pt4lj"]          = vector<double> {0, 1, 3, 5};

    // //pt4lj vs m4lj
    m_binMap["pt4lj_m4lj"]           = vector<double> {0, 1, 5};



    return m_binMap.at(opts["varType"]);
}

vector<double> getZZIntBin()
{
    // map<TString, vector<double>> m_binMap;
    // double pi = TMath::Pi();

    
    auto signalBin = getBin();
    auto ZZBin = getZZBin();
    
    vector<double> zzIntBin;

    for(auto bin: ZZBin)
    {
        auto it = std::find(signalBin.begin(), signalBin.end(), bin);
        cout<<"Finding: "<<bin<<" index: "<<std::distance(signalBin.begin(), it)<<endl;
        zzIntBin.push_back(std::distance(signalBin.begin(), it));
    }   

    return zzIntBin;


    // m_binMap["fid_4mu"]             = vector<double> {0,1};
    // m_binMap["fid_4e"]              = vector<double> {0,1};
    // m_binMap["fid_2mu2e"]           = vector<double> {0,1};
    // m_binMap["fid_2e2mu"]           = vector<double> {0,1};
    // m_binMap["fid_perChan"]         = vector<double> {0,1};
    // m_binMap["fid_4l"]              = vector<double> {0,1};
    // m_binMap["fid_2l2l"]            = vector<double> {0,1};
    // m_binMap["fid_sum"]             = vector<double> {0,1};
    // m_binMap["fid_comb"]            = vector<double> {0,1};
    // m_binMap["total"]               = vector<double> {0,1};

    // // Higgs vars
    // m_binMap["pt"]                  = vector<double> {0  , 1   ,  2   ,   3   ,   5   , 10};
    // m_binMap["m12"]                 = vector<double> {0, 2, 3, 4};
    // m_binMap["m34"]                 = vector<double> {0, 2, 4, 7};
    // m_binMap["y"]                   = vector<double> {0, 1, 2, 3, 4, 5, 7, 9};
    // m_binMap["cts"]                 = vector<double> {0, 2, 4, 6, 9};
    // m_binMap["ct1"]                 = vector<double> {0, 2, 4, 6, 9};
    // m_binMap["ct2"]                 = vector<double> {0, 2, 4, 6, 9};
    // m_binMap["phi"]                 = vector<double> {0, 2, 4, 6, 9};
    // m_binMap["phi1"]                = vector<double> {0, 2, 4, 6, 9};


    // // Jet vars
    // m_binMap["njet"]                = vector<double> {0, 1, 2, 4};
    // m_binMap["n_bjets"]             = vector<double> {0, 1, 3};
    // m_binMap["ljetPt"]              = vector<double> {0, 1, 2, 3, 5};
    // m_binMap["sljetPt"]             = vector<double> {0, 1, 2, 4};

    // m_binMap["mjj"]                 = vector<double> {0.0, 1, 2, 4};
    // m_binMap["etajj"]               = vector<double> {0.0, 1, 2, 4};
    // m_binMap["phijj"]               = vector<double> {0.0, 1, 3, 5};


    // // To come
    // m_binMap["m4lj"]                = vector<double> {0.0, 1, 3, 5};
    // m_binMap["m4ljj"]               = vector<double> {0.0, 1, 3, 5};
    // // m_binMap["pt4lj"]               = vector<double> {0.0, 1.0, 2.0, 9.0};
    // // m_binMap["pt4ljj"]              = vector<double> {0.0, 1.0, 2.0, 9.0};


    // // 2D vars - this we will do one plot
    // m_binMap["m12m34"]              = vector<double> {0, 2, 3, 4, 5};




    // // How we want do this
    // // Per slice? // THese will need some type off offset
    // m_binMap["pt4l_Nj0"]              = vector<double> {0, 1};
    // m_binMap["pt4l_Nj1"]              = vector<double> {0, 1};
    // m_binMap["pt4l_Nj2"]              = vector<double> {0, 1};
    // m_binMap["pt4l_Nj3"]              = vector<double> {0, 1};  
    
    // m_binMap["pt4l_Nj"]               = vector<double> {0, 5, 10, 14};  

    // m_binMap["pt4l_y0"]             = vector<double> {0.0, 1.0}; //|y|<0.5
    // m_binMap["pt4l_y1"]             = vector<double> {0.0, 1.0}; //0.5<|y|<1
    // m_binMap["pt4l_y2"]             = vector<double> {0.0, 1.0}; //1<|y|<1.5
    // m_binMap["pt4l_y3"]             = vector<double> {0.0, 1.0}; //1.5|y|<2.5

    // m_binMap["pt4l_y"]               = vector<double> {0, 4,  8, 12};  

    // //pt4l vs ptj1
    // m_binMap["pt4l_pTLJ0"]             = vector<double> {0.0, 1.0}; //30<ljpt<60
    // m_binMap["pt4l_pTLJ1"]             = vector<double> {0.0, 1.0}; //60<ljpt<120
    // m_binMap["pt4l_pTLJ2"]             = vector<double> {0.0, 1.0}; //120<ljpt<350

    // m_binMap["pt4l_pTLJ"]              = vector<double> {0,  3, 6, 9}; //120<ljpt<350




    // return m_binMap.at(opts["varType"]);
}

TString getZZPOIList()
{
    auto binList = getZZBin();

    TString poiList = "";

    for(size_t i = 0; i < binList.size()-1; i++)
    {
        TString poiName;
        poiName.Form("MuqqZZ%zu", i);

        if(i == binList.size() - 2) poiList += poiName;
        else poiList += poiName + ",";
    }

    return poiList;
}
