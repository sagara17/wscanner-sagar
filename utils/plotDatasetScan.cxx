#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLeaf.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TChain.h>
#include <TMath.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TF1.h>
#include <TLine.h>
// #include "TGaxis.h"
#include <TGaxis.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <stdio.h>
#include "commonCode.h"
#include "AtlasLabels.C"
#include "AtlasStyle.C"
#include "InterpolatedGraph.h"

using namespace std;

// Global vars
map<TString,TString> opts;
map<TString,TString> metaData;


// // Functions
bool cmdline(int argc, char** argv, map<TString,TString>& opts);
TString getBaseName(TString pathName);
void setPrettyStuff();
vector<TString> getVarName(TTree* tree);
void plotVar(TH1D* hist, TString type);
TString baseName;

double lwrErrMPV    = -999;
double uprErrMPV    = -999;
double lwrErrMPVStd = -999;
double uprErrMPVStd = -999;
double MPVsym       = -999;

int main(int argc, char** argv)
{

    TGaxis::SetMaxDigits(3);


    setPrettyStuff();

    if (!cmdline(argc,argv,opts)) return 0;


    vector<TString> fileNameList = tokenizeStr(opts["fileName"],",");
    vector<TString> labelListVect = tokenizeStr(opts["label"],",");
    if(!opts["label"].EqualTo("Empty"))
    {
        if(fileNameList.size() != labelListVect.size()) 
            {
                cout << "You're label list and file list are not the same size, fix this.." << endl;
                return 1;
            }

    }

    if(fileNameList.size() > 1) baseName = opts["baseName"]; 
    else baseName = getBaseName(fileNameList.at(0));
    
    
    TString directoryName = opts["plotDir"] + "/"+baseName;
    
    system("mkdir -vp " + directoryName);

    if(fileNameList.size() == 1)
    {
        // Open the file
        TChain* basechain = new TChain("results");
        basechain->Add(fileNameList.at(0));

        // get list of variable names
        std::vector<TString> varNames = getVarName(basechain);


        // TString cutList = "(fitType == 5)&&(muSig_4mu_BDT_bin1 > 0.0001)";
        // TString cutList = "(fitType == 5)&&(mH > 120 && mH < 130)&&(fabs(uprErr) > 0.15)";
        TString cutList = "(fitType == 5)*(status == 0)";

        TTree* chain = basechain->CopyTree(cutList);

        double lwrLim = chain->GetMinimum("cenVal");
        double uprLim = chain->GetMaximum("cenVal");
        int nBins = 50;
        double epsilonVar = (uprLim - lwrLim)/nBins;


        double binWidthErr = 0.01;

        double lwrLim_lowErr = chain->GetMinimum("lwrErr");
        double uprLim_lowErr = chain->GetMaximum("lwrErr");
        int nBins_lowErr =  (uprLim_lowErr - lwrLim_lowErr)/binWidthErr;
        

        lwrLim_lowErr   = -0.15;
        uprLim_lowErr   = -0.35;
        nBins_lowErr    = 100;

        // cout<<"lwrLim_lowErr: "<<lwrLim_lowErr<<" uprLim_lowErr: "<<uprLim_lowErr<<" nBins_lowErr: "<<nBins_lowErr<<endl;

        double lwrLim_highErr = chain->GetMinimum("uprErr");
        double uprLim_highErr = chain->GetMaximum("uprErr");
        int nBins_highErr = (uprLim_highErr - lwrLim_highErr)/binWidthErr;

        lwrLim_highErr   = 0.15;
        uprLim_highErr   = 0.35;
        nBins_highErr    = 75;
        

        TH1D *cenValHist = new TH1D("cenValHist", "cenValHist", nBins + 2, lwrLim - epsilonVar, uprLim + epsilonVar);
        TH1D *uprErrHist = new TH1D("uprErrHist", "uprErrHist", nBins_highErr, lwrLim_highErr, uprLim_highErr);
        TH1D *lwrErrHist = new TH1D("lwrErrHist", "lwrErrHist", nBins_lowErr, lwrLim_lowErr, uprLim_lowErr);
        TH1D *symErrHist = new TH1D("symErrHist", "symErrHist", nBins_highErr, 0.25, 0.55);

        chain->Draw("cenVal >> cenValHist", cutList);
        chain->Draw("uprErr >> uprErrHist", cutList);
        chain->Draw("lwrErr >> lwrErrHist", cutList);

        uprErrHist->Smooth();
        lwrErrHist->Smooth();

        lwrErrMPV = lwrErrHist->GetBinCenter(lwrErrHist->GetMaximumBin());
        uprErrMPV = uprErrHist->GetBinCenter(uprErrHist->GetMaximumBin());

        // lwrErrMPV = lwrErrHist->GetMean();
        // uprErrMPV = uprErrHist->GetMean();

        double cenVal;
        double uprErr;
        double lwrErr;

        chain->SetBranchAddress("cenVal", &cenVal);         
        chain->SetBranchAddress("uprErr", &uprErr);
        chain->SetBranchAddress("lwrErr", &lwrErr);

        for(int i = 0; i < chain->GetEntries(); i++)
        {
            chain->GetEntry(i);

            //cout << lwrErr << "\t" << cenVal << "\t" << uprErr << endl;
            double symErr = fabs(lwrErr) + fabs(uprErr);
            symErr /=2.0;
            symErrHist->Fill(symErr);
        }
        symErrHist->Smooth();
        plotVar(symErrHist, "SymmetrizedErr");

        cout << "-------- Error Compatibility -------- " << endl;
        double pValErr = symErrHist->Integral(symErrHist->GetXaxis()->FindBin(opts.at("valueErr").Atof()), symErrHist->GetXaxis()->GetNbins())/symErrHist->Integral();
        cout<<"1 sided pvalue is: "<<pValErr<<" integrated from: "<<opts.at("valueErr")<<" and above"<<endl;;


        pValErr = symErrHist->Integral(1, symErrHist->GetXaxis()->FindBin(opts.at("valueErr").Atof()))/symErrHist->Integral();
        cout<<"1 sided pvalue is: "<<pValErr<<" integrated from below to "<<opts.at("valueErr")<<endl;;



        MPVsym = symErrHist->GetBinCenter(symErrHist->GetMaximumBin());

        plotVar(cenValHist, "poi");

        cout << "-------- Mean Compatibility -------- " << endl;
        double pVal = cenValHist->Integral(cenValHist->GetXaxis()->FindBin(opts.at("value").Atof()), cenValHist->GetXaxis()->GetNbins())/cenValHist->Integral();
        cout<<"1 sided pvalue is: "<<pVal<<" integrated from: "<<opts.at("value")<<" and above"<<endl;;


        pVal = cenValHist->Integral(1, cenValHist->GetXaxis()->FindBin(opts.at("value").Atof()))/cenValHist->Integral();
        cout<<"1 sided pvalue is: "<<pVal<<" integrated from below to "<<opts.at("value")<<endl;;


        //  find the pull
        TH1D *pullHist = new TH1D("pullHist", "pullHist", nBins + 2, -5, 5);
        float cenValMeta = 125;
        float pullVal    = -999;
        float diff       = -999;
        float denom      = -999;


        for(int i = 0; i < chain->GetEntries(); i++)
        {
            chain->GetEntry(i);
            if(fabs(uprErr) < 0.16 || fabs(lwrErr) < 0.16  ) continue ;

            diff  = cenVal - cenValMeta;
            if(diff < 0) denom = lwrErr;
            else         denom = uprErr;

            pullVal = diff/fabs(denom); 
            pullHist->Fill(pullVal);
        }
        plotVar(pullHist, "Pull");




        for(const auto& varName: varNames)
        {
            if(varName.EqualTo("poi")) continue;
            double lwrLim = chain->GetMinimum(varName);
            double uprLim = chain->GetMaximum(varName);
            int nBins = 50;

            if(varName.EqualTo("lwrErr") || varName.EqualTo("uprErr"))
            {
                nBins = (uprLim - lwrLim)/binWidthErr;
            }

            double epsilonVar = (uprLim - lwrLim)/nBins;

            if(varName.EqualTo("lwrErr")) 
            {
                lwrLim  = -0.35;
                uprLim  = -0.15;
                nBins = 100;
                epsilonVar = 0;
            }
            if(varName.EqualTo("uprErr")) 
            {
                uprLim  = 0.35;
                lwrLim  = 0.15;
                nBins = 100;
                epsilonVar = 0;

            }

            TH1D *cenValHist = new TH1D("cenValHist" + varName, "cenValHist" + varName, nBins + 2, lwrLim - epsilonVar, uprLim + epsilonVar);
            chain->Draw(varName + " >> cenValHist" + varName, cutList);

            plotVar(cenValHist, varName);
        }

        delete chain;

    }
        


    // overlay the symmetrized error
    else
    {
//../build/plotDatasetScan --fileName root-files/WS1D_0/statOnly/asimov/merged.root,root-files/WS1DConditional_0/statOnly/asimov/merged.root,root-files/WS1DFullConditional_0/statOnly/asimov/merged.root,root-files/WS2D_0/statOnly/asimov/merged.root,root-files/WS2DConditional_0/statOnly/asimov/merged.root,root-files/WS2DFullConditional_0/statOnly/asimov/merged.root,root-files/WSpseudoConditionall_0/statOnly/asimov/merged.root,root-files/WSpseudoConditional_v2Fixed_0/statOnly/asimov/sJob/merged.root --label 1D,1DConditional,2DConditionalFactorized,2D,2DConditional,3DConditionalFactorized,PsuedoConditionalV1,PsuedoConditionalV2



// ../build/plotDatasetScan --fileName root-files/J21_PER_All_datatset0_noNegCDFToys_V10_StatOnly_v2/statOnly/asimov/toys.root,root-files/J21_Analytic_All_datatset0_noNegCDFToys_V10/statOnly/asimov/toys.root,root-files/J21_PER_All_datatset0_noNegCDFToys_V14_SysSameToys/allSys/asimov/toys.root,root-files/J21_Analytic_All_datatset0_noNegCDFToys_V14_SysSameToys/allSys/asimov/toys.root --label PER-Stat,Analytic-Stat,PER-Sys,Analytic-Sys
// sbuild; ../build/plotDatasetScan --fileName root-files/J21_Analytic_All_datatset0_noNegCDFToys_V14_SysSameToys/allSys/asimov/toys.root,root-files/J21_PER_All_datatset0_noNegCDFToys_V14_SysSameToys/allSys/asimov/toys.root --label Analytic-Sys,PER-Sys --baseName Figure6
        
        cout << "Plotting overlay " << endl;
        TCanvas *c = new TCanvas("c", "c", 0, 0, 600, 600);

        TLegend *elLeg =  new TLegend (0.5, 0.7, 0.8, 0.925);
                // TLegend *elLeg =  new TLegend (0.53, 0.7, 0.7, 0.925);

        elLeg->SetFillColor(0);
        elLeg->SetBorderSize(0);
        elLeg->SetTextFont(42);
        elLeg->SetTextSize(0.0185*0.9);
        // elLeg->SetTextSize(0.0165);

        std::map<TString, TTree*>  overlapChains;
        std::map<TString, TH1D*>    overlapHist;
        int counter = 0;
        int ci = 1756;

        for(auto file: fileNameList) cout << file << endl;
        TH1D *dataPER = new TH1D("dataPER","dataPER",1, 0,1);
        dataPER->SetLineColor(kBlack);
        dataPER->SetLineWidth(2);
        dataPER->SetLineStyle(2);

        TH1D *dataAnal = new TH1D("dataAnal","dataAnal",1, 0,1);
        dataAnal->SetLineColor(kAzure+7);
        dataAnal->SetLineWidth(2);
        dataAnal->SetLineStyle(2);

        for(int fileIndex = 0; fileIndex < fileNameList.size(); fileIndex++)
        {
            TString cutList = "(fitType == 5)*(status == 0)";

            TChain* basechain = new TChain("results");
            basechain->Add(fileNameList.at(fileIndex));


            TTree* chain = basechain->CopyTree(cutList);


            overlapChains[fileNameList.at(fileIndex)] = chain;
            // overlapChains[fileNameList.at(fileIndex)]->Add(fileNameList.at(fileIndex));



            overlapHist[fileNameList.at(fileIndex)] = new TH1D(fileNameList.at(fileIndex), fileNameList.at(fileIndex), 60, 180, 350);

            double uprErr;
            double lwrErr;

            overlapChains[fileNameList.at(fileIndex)]->SetBranchAddress("uprErr", &uprErr);
            overlapChains[fileNameList.at(fileIndex)]->SetBranchAddress("lwrErr", &lwrErr);

            for(int i = 0; i < overlapChains[fileNameList.at(fileIndex)]->GetEntries(); i++)
            {
                overlapChains[fileNameList.at(fileIndex)]->GetEntry(i);
                double symErr = fabs(lwrErr) + fabs(uprErr);
                symErr /=2.0;
                // cout << symErr*100 << endl;
                // overlapHist[fileNameList.at(fileIndex)]->Fill(symErr*1000);
                overlapHist[fileNameList.at(fileIndex)]->Fill(uprErr*1000);

            }
            overlapHist[fileNameList.at(fileIndex)]->Smooth();

            double MPV = overlapHist[fileNameList.at(fileIndex)]->GetBinCenter(overlapHist[fileNameList.at(fileIndex)]->GetMaximumBin());
            if(labelListVect.at(fileIndex).EqualTo("Analytic-Sys")) MPV = 0.196;
            if(labelListVect.at(fileIndex).EqualTo("PER-Sys")) MPV = 0.193;


            TString labelName = labelListVect.at(fileIndex) + Form(" MPV: %.3f, RMS: %.3f", MPV, overlapHist[fileNameList.at(fileIndex)]->GetRMS()) ;
            // labelName = labelListVect.at(fileIndex) + Form(" :%.3f", overlapHist[fileNameList.at(fileIndex)]->GetMean() ) ;


            labelListVect.at(fileIndex).ReplaceAll("_", " ");
            overlapHist[fileNameList.at(fileIndex)]->Scale(1/overlapHist[fileNameList.at(fileIndex)]->Integral());
            overlapHist[fileNameList.at(fileIndex)]->SetLineWidth(2);
            overlapHist[fileNameList.at(fileIndex)]->SetLineColor(ci+counter);

            // custom
            if(labelName.Contains("Stat")) 
            {
                overlapHist[fileNameList.at(fileIndex)]->SetLineStyle(2);
            }
            if(labelName.Contains("Analytic"))   overlapHist[fileNameList.at(fileIndex)]->SetLineColor(kAzure+7);
            else if(labelName.Contains("PER")) overlapHist[fileNameList.at(fileIndex)]->SetLineColor(kBlack);


            elLeg->AddEntry(overlapHist[fileNameList.at(fileIndex)], Form("%s, Mean: %.3f, RMS: %.3f", labelListVect.at(fileIndex).Data(), overlapHist[fileNameList.at(fileIndex)]->GetMean(), overlapHist[fileNameList.at(fileIndex)]->GetRMS()), "l");

            if(counter == 0)
            {
                overlapHist[fileNameList.at(fileIndex)]->GetXaxis()->SetNdivisions(505);
                overlapHist[fileNameList.at(fileIndex)]->SetMaximum(overlapHist[fileNameList.at(fileIndex)]->GetMaximum()*1.6);

                overlapHist[fileNameList.at(fileIndex)]->Draw("hist");
            }

            else overlapHist[fileNameList.at(fileIndex)]->Draw("histsame");
            
            overlapHist[fileNameList.at(fileIndex)]->GetXaxis()->SetTitleSize(0.04*1.1);
            overlapHist[fileNameList.at(fileIndex)]->GetYaxis()->SetTitleSize(0.04*1.1);

            overlapHist[fileNameList.at(fileIndex)]->GetXaxis()->SetLabelSize(0.04*1.1);
            overlapHist[fileNameList.at(fileIndex)]->GetYaxis()->SetLabelSize(0.04*1.1);


            overlapHist[fileNameList.at(fileIndex)]->GetXaxis()->SetTitleOffset(1.3);
            overlapHist[fileNameList.at(fileIndex)]->GetYaxis()->SetTitleOffset(1.3);


            overlapHist[fileNameList.at(fileIndex)]->GetXaxis()->SetTitle("Stat Uncertainty on #it{m}_{H} [MeV]");
            overlapHist[fileNameList.at(fileIndex)]->GetYaxis()->SetTitle("Arbitrary Units");

            counter++;

            delete basechain;

        }

            // if(labelName.Contains("Analytic") ) labelName = "P(m_{4l}) Total Expected:";
            // if(labelName.Contains("PER") ) labelName = "P(m_{4l}|#sigma_{i}) Total Expected:";


        // elLeg->AddEntry(overlapHist[fileNameList.at(0)], "P(m_{4l}) Expected: 0.20 GeV", "f");
        // elLeg->AddEntry(dataAnal, "P(m_{4l}) Observed: 0.22 GeV", "l");

        // elLeg->AddEntry(overlapHist[fileNameList.at(1)], "P(m_{4l}| #sigma_{i}) Expected: 0.19 GeV", "f");
        // elLeg->AddEntry(dataPER, "P(m_{4l}| #sigma_{i}) Observed: 0.21 GeV", "l");

            // if(labelName.Contains("Analytic") ) elLeg->AddEntry(dataPER, "P(m_{4l}|#sigma_{i}): Observed: ", "l");
            // if(labelName.Contains("PER") ) elLeg->AddEntry(dataAnal, "P(m_{4l}): Observed: ", "l");

        TLine lPER;
        lPER.SetLineWidth(2);
        lPER.SetLineColor(kBlack);
        lPER.SetLineStyle(2);
        // lPER.DrawLine( 207 , 0,  207 , 0.05);

        TLine lAnal;
        lAnal.SetLineWidth(2);
        lAnal.SetLineColor(kAzure+7);
        lAnal.SetLineStyle(2);
        // lAnal.DrawLine( 216 , 0,  216 , 0.05);

        elLeg->Draw();
        double leftDist = 0.19;

        ATLASLabel(leftDist, 0.875, "Internal", 1);
        TString channelStr = "H #rightarrow ZZ* #rightarrow 4l";
        TLatex* tMain = new TLatex (leftDist, 0.81, channelStr);
        tMain->SetNDC();
        tMain->SetTextSize(0.0425);
        tMain->SetTextFont(42);
        tMain->Draw();

        TString lumiString = "13 TeV, 139 fb^{-1}";
        TLatex* lumInfo = new TLatex (leftDist, 0.76, lumiString);
        lumInfo->SetNDC();
        lumInfo->SetTextSize(0.0325);
        lumInfo->SetTextFont(42);
        lumInfo->Draw();

        TString saveName = opts["plotDir"] + "/" + baseName + "/overlayPlot_v8.eps";
        c->SaveAs(saveName);

    }



    return 0;

}


void plotVar(TH1D* hist, TString type)
{

    TCanvas* c1 = new TCanvas("c1", "c1", 0, 0, 600, 600);
    TString xLabel = "mH";
    if(type.Contains("Pull")) xLabel = "Pull";
    if(!type.Contains("poi")) xLabel = type;

    hist->GetXaxis()->SetTitle(xLabel);
    hist->SetMaximum(hist->GetMaximum()*1.7);
    hist->SetLineWidth(2);
    hist->SetLineColor(kBlack);
    hist->GetXaxis()->SetNdivisions(505);

    if(type.Contains("uprErr") || type.Contains("lwrErr") || type.Contains("SymmetrizedErr") ) hist->Smooth();

    hist->Draw("hist");

    // Fancy title stuff
    double leftDist = 0.19;

    ATLASLabel(leftDist, 0.875, "Internal", 1);
    TString channelStr = "H #rightarrow WW* #rightarrow e#nu#mu#nu";
    TLatex* tMain = new TLatex (leftDist, 0.81, channelStr);
    tMain->SetNDC();
    tMain->SetTextSize(0.0425);
    tMain->SetTextFont(42);
    tMain->Draw();

    TString lumiString = "13 TeV, 139 fb^{-1}";
    TLatex* lumInfo = new TLatex (leftDist, 0.76, lumiString);
    lumInfo->SetNDC();
    lumInfo->SetTextSize(0.0325);
    lumInfo->SetTextFont(42);
    lumInfo->Draw();

    double rightDist = 0.6;
    double topDist = 0.86;
    TString numToys = Form("Toys: %.0f", hist->GetEntries());
    TLatex* numToysInfo = new TLatex (rightDist, topDist, numToys);
    numToysInfo->SetNDC();
    numToysInfo->SetTextSize(0.0325);
    numToysInfo->SetTextFont(42);
    numToysInfo->Draw();


    if(type.Contains("poi"))
    {
        TF1* gaussFit = new TF1("gaussFit", "gaus", hist->GetXaxis()->GetXmin (), hist->GetXaxis()->GetXmax ());
        gaussFit->SetLineColor(kAzure + 7);
        gaussFit->SetLineWidth(2);
        hist->Fit("gaussFit");


        TString mean = Form("Fit Mean: %.3f #pm %.3f", gaussFit->GetParameter(1) , gaussFit->GetParError(1));
        TLatex* meanInfo = new TLatex (rightDist, topDist - 0.04, mean);
        meanInfo->SetNDC();
        meanInfo->SetTextSize(0.0325);
        meanInfo->SetTextFont(42);
        meanInfo->Draw();

        mean = Form("Fit Sigma: %.3f #pm %.3f", gaussFit->GetParameter(2) , gaussFit->GetParError(2));
        meanInfo = new TLatex (rightDist, topDist - 0.08, mean);
        meanInfo->SetNDC();
        meanInfo->SetTextSize(0.0325);
        meanInfo->SetTextFont(42);
        meanInfo->Draw();


        TString uprErrStr = Form("MPV UprErr: %.3f", uprErrMPV);
        TLatex* uprErrInfo = new TLatex (rightDist, topDist - 0.12, uprErrStr);
        uprErrInfo->SetNDC();
        uprErrInfo->SetTextSize(0.0325);
        uprErrInfo->SetTextFont(42);
        uprErrInfo->Draw();

        TString lwrErrStr = Form("MPV lwrErr: %.3f", lwrErrMPV);
        TLatex* lwrErrInfo = new TLatex (rightDist, topDist - 0.16, lwrErrStr);
        lwrErrInfo->SetNDC();
        lwrErrInfo->SetTextSize(0.0325);
        lwrErrInfo->SetTextFont(42);
        lwrErrInfo->Draw();

        TString symErrStr = Form("MPV symErr: %.3f", MPVsym);
        TLatex* symErrInfo = new TLatex (rightDist, topDist - 0.20, symErrStr);
        symErrInfo->SetNDC();
        symErrInfo->SetTextSize(0.0325);
        symErrInfo->SetTextFont(42);
        symErrInfo->Draw();

        gaussFit->Draw("same");

    }
    

    else
    {
        TString mean = Form("Mean: %.3f #pm %.3f", hist->GetMean(), hist->GetMeanError() );
        TLatex* meanInfo = new TLatex (rightDist, topDist - 0.04, mean);
        meanInfo->SetNDC();
        meanInfo->SetTextSize(0.0325);
        meanInfo->SetTextFont(42);
        meanInfo->Draw();

        TString sigma = Form("Width: %.3f #pm %.3f", hist->GetStdDev(),  hist->GetStdDevError() );
        TLatex* sigmaInfo = new TLatex (rightDist, topDist - 0.08, sigma);
        sigmaInfo->SetNDC();
        sigmaInfo->SetTextSize(0.0325);
        sigmaInfo->SetTextFont(42);
        sigmaInfo->Draw();


        if(type.Contains("uprErr") || type.Contains("lwrErr") )
        {
            TString uprErrStr = Form("MPV: %.3f", hist->GetBinCenter(hist->GetMaximumBin()));
            TLatex* uprErrInfo = new TLatex (rightDist, topDist - 0.12, uprErrStr);
            uprErrInfo->SetNDC();
            uprErrInfo->SetTextSize(0.0325);
            uprErrInfo->SetTextFont(42);
            uprErrInfo->Draw();
        }



    }


    cout << "Type: " << type << " Val: " << hist->GetMean() << " Err: " << hist->GetStdDev() <<endl;

    if(type.Contains("Pull"))
    {
        TString skew = Form("Skew: %.3f", hist->GetSkewness() );
        TLatex* skewInfo = new TLatex (rightDist, topDist - 0.12, skew);
        skewInfo->SetNDC();
        skewInfo->SetTextSize(0.0325);
        skewInfo->SetTextFont(42);
        skewInfo->Draw();

        TString kurtosis = Form("Kurtosis: %.3f", hist->GetKurtosis() );
        TLatex* kurtosisInfo = new TLatex (rightDist, topDist - 0.16, kurtosis);
        kurtosisInfo->SetNDC();
        kurtosisInfo->SetTextSize(0.0325);
        kurtosisInfo->SetTextFont(42);
        kurtosisInfo->Draw();
    }

    TString saveName = opts["plotDir"] + "/" + baseName + "/" + type + ".eps";
    c1->SaveAs(saveName);

    delete c1;


}


bool cmdline(int argc, char** argv, map<TString,TString>& opts)
{
    opts.clear();
   
    // defaults
    opts["fileName"]        = "asimovScan.root";
    opts["label"]           = "Empty";
    opts["plotDir"]         = "Plots";
    opts["baseName"]        = "FillMe";
    opts["value"]           = "-999";
    opts["valueErr"]        = "-999";

    for (int i=1;i<argc;i++) {
       
        string opt=argv[i];
       
        if (opt=="--help") {
            cout<<"--fileName       : path to ttree containing the information"<<endl;
            // cout<<"--label          : Label for the legend for the files"<<endl;
            // cout<<"--outFile        : Name for the output file"<<endl;
            // cout<<"--doXS           : do XS poi"<<endl;
            // cout<<"--doNs           : do Ns poi"<<endl;
            // cout<<"--doUpperLim     : true or false"<<endl;
            // cout<<"--plotNP     : true or false"<<endl;
            return false;
        }
       
        if(0!=opt.find("--")) {
            cout<<"ERROR: options start with '--'!"<<endl;
            cout<<"ERROR: options is: "<<opt<<endl;
            return false;
        }
        opt.erase(0,2);
        if(opts.find(opt)==opts.end()) {
            cout<<"ERROR: invalid option '"<<opt<<"'!"<<endl;
            return false;
        }
        string nxtopt=argv[i+1];
        if(0==nxtopt.find("--")||i+1>=argc) {
            cout<<"ERROR: option '"<<opt<<"' requires value!"<<endl;
            return false;
        }
       
        opts[opt]=nxtopt;
        i++;
    }
    //if(opts["fileName"].Contains("ttH")) opts["doUpperLim"]  = "true";
    return true;
}

TString getBaseName(TString pathName)
{
    TString baseName = "";
    std::vector<TString> folderName = tokenizeStr(pathName, "/");
    return folderName[1];
}


void setPrettyStuff()
{
    SetAtlasStyle();
    Int_t ci = 1754; // color index
    vector<TColor*> TColors;
    // new TColor(ci, 62/255.,    153/255.,    247/255.); //54
    // ci++;

    // new TColor(ci, 0,    0.4470,    0.7410); //56
    // ci++;
    
    // new TColor(ci, 254/255., 139/255., 113/255.); //55
    // ci++;



    new TColor(ci, 0.8500,    0.3250,    0.0980); //57
    ci++;          
    new TColor(ci, 0.4940,    0.1840,    0.5560); //58
    ci++;
    new TColor(ci, 0.9290,    0.6940,    0.1250); //59
    ci++; 
    new TColor(ci, 0.4660,    0.6740,    0.1880); //60
    ci++;
    new TColor(ci, 0.3010,    0.7450,    0.9330); //61
    ci++;
    //new TColor(ci, 0.6350,    0.0780,    0.1840); //62
    //ci++;
    //new TColor(ci, 142.0/255 , 0.0/255 , 62.0/255);
    //ci++;
    //new TColor(ci, 96.0/255 , 78.0/255 , 0.0/255);
    //ci++;
    //new TColor(ci, 92.0/255 , 174.0/255 , 0.0/255);
    //ci++;
    new TColor(ci, 1.0/255 , 237.0/255 , 171.0/255);
    ci++;
    new TColor(ci, 50.0/255 , 134.0/255 , 255.0/255);
    ci++;
    new TColor(ci, 112.0/255 , 0.0/255 , 88.0/255);
    ci++;
    new TColor(ci, 28.0/255 , 0.0/255 , 19.0/255);
    ci++;
    new TColor(ci, 255.0/255 , 102.0/255 , 165.0/255);
    ci++;
}



vector<TString> getVarName(TTree* tree)
{

    vector<TString> varList;
    TObjArray* listLf = tree->GetListOfLeaves();

    for (Int_t i = 0; i < listLf->GetEntries(); i++)
    {
        TLeaf* lf = (TLeaf*) listLf->At(i);

        TString typeName   = lf->GetTypeName();
        TString titleName  = lf->GetTitle();
        TString varName    = lf->GetName();

        if(typeName.Contains("Int_t") && !typeName.Contains("UInt_t"))
        {
            varList.push_back(varName);
        }
        else if(typeName.Contains("UInt_t"))
        {
            varList.push_back(varName);
        }
        else if(typeName.Contains("Float_t"))
        {
            varList.push_back(varName);
        }
        else if(typeName.Contains("Double_t"))
        {
            varList.push_back(varName);
        }
        else if(typeName.Contains("ULong64_t"))
        {
            varList.push_back(varName);
        }
        else
        {
            cout<<"I have been feed a type that I don't know how to deal with"<<endl;
            cout<<"Type: "<<typeName<<endl;
            exit(1);
        }
    }

    return varList;

}






