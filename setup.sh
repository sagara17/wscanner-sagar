#!/bin/bash
setupATLAS
#lsetup "root 6.22.00-x86_64-centos7-gcc8-opt"
lsetup "root 6.10.06-x86_64-slc6-gcc62-opt"
# lsetup  "root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt"
lsetup "gcc gcc620_x86_64_slc6"
#source /cvmfs/sft.cern.ch/lcg/releases/LCG_94a/cmaketools/1.6/x86_64-slc6-gcc62-opt/cmaketools-env.sh
lsetup  cmake
alias cbuild='cd ../build;make;cd ../run'
alias ccleanbuild='cd ../build && rm -rf * && cmake ../source && make && cd ../run'



