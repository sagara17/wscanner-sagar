#ifndef COMMONCODE_H
#define COMMONCODE_H

#include <stdio.h>
#include <stddef.h>
#include <map>
#include <vector>

// ROOT include
#include <TFile.h>
#include <TTree.h>
#include <TLeaf.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include "TMatrixDSym.h"

// ROOFit include
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/AsymptoticCalculator.h"
#include "RooMinimizer.h"
#include "RooNLLVar.h"
#include "TStopwatch.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include "RooNumIntConfig.h"
#include "RooMinuit.h"
// #include "RooStarMomentMorph.h"
#include "RooAddPdf.h"
#include "RooProduct.h"
#include "TColor.h"
#include "TLine.h"
#include "TLatex.h"


using namespace RooStats;
using namespace RooFit ;
using namespace std;

std::vector<TString> tokenizeStr(TString str, TString key);

template<class T>
void bookTree(TTree* currTree, map<TString, T>& outVars)
{
    for (auto it = outVars.begin(); it != outVars.end(); ++it)
    {
        TString varName = it->first;       
        if (std::is_same<T, float>::value) currTree->Branch(varName, &it->second, varName+"/F");
        if (std::is_same<T, double>::value) currTree->Branch(varName, &it->second, varName+"/D");
        if (std::is_same<T, int>::value) currTree->Branch(varName, &it->second, varName+"/I");
        if (std::is_same<T, TString>::value) currTree->Branch(varName, &it->second);
    }
}


std::map<TString, TString> readMetadata(TTree* tree);
std::vector<std::map<TString, TString>>  getLabels(std::vector<TString> fileNameList);


TString getXAxisSymbol(std::map<TString, TString> metaData, TString poiName = "");

TString getFancyXname(TString name);

TString getFancyLegname(TString name);

vector<TString> getVariableList(TTree *tree);

TString getSymbol(std::map<TString, TString> metaData, TString poiName = "");

// reads the var as a function of the poi
map<double,double> read1DVar(TString varName, TTree* tree);

// // Subtracts the minimum from the variable
void fixOffset(map<double,double>& varInfo);

TGraph* get1DGraph(map<double,double>& varInfo);

std::vector<double> printFitInfo(map<double,double>& NLLVar, double& outMinValX, double& outLeftErr, double& outRightErr, bool doBetterError = false);


struct PlottingInfo 
{
	bool hasStatSys;
	bool hasMultipleFolder;
};

PlottingInfo parsePlottingInfo(std::vector<TString> fileNameList);

bool doesExist(const std::string& name) ;

vector<TString> splitString(TString string, TString delimiter);


TString joinList(vector<TString> arr, TString delimiter);


void process_mem_usage(double& vm_usage, double& resident_set);

double plotToy(TH1D* toyHist, double nomNLLVal, TString subDirectoryName, TString histName);

#endif




