[POI] 
name:*, float:true 
name:DCB_alphaHi,float:false, centralVal:1.67458 
name:DCB_alphaLo,float:false, centralVal:1.21832 
name:DCB_etaHi,float:false, centralVal:10.3639 
name:DCB_etaLo,float:false, centralVal:3.8788 
#######################
# Systematics setup
#######################
[Sys - allSys] 
name:*, float:false

[Sys - statOnly] 
name:*, float:False

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mH, float:False, centralVal:125

[Global]
workspaceName: combined
modelConfigName:ModelConfig
datasetName: obsData
asimovName: combDataHist
fitStrategy: 0
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2
epsVal:0.1


#######################
# scanDatasets setup
#######################
[scanDatasets - scanSetup]
Dimension:1
MinosFit: True
poiName: mH
#randomizeGlobs: True
#poiInfo:mH:20:122.5:127


[extraInformation]
# Information on the workspace. To help with the plotting
workspaceType: mass
labelName: H #rightarrow ZZ* #rightarrow 4l
