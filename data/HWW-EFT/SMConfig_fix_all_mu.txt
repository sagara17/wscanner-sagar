[POI]
name:mu_wwTop3, float: false,centralVal: 1, lwrLim:1, uprLim:1
name:sigma_bin*,float:true
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu*, float:false, centralVal: 1.
name:mu_vbf*, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets_CRGGF1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfA, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfB, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfC, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1

name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1
name:mu_wwTop_bin1, float:false, centralVal: 1, lwrLim:1,uprLim:1
name:mu_wwTop_bin2, float:false, centralVal: 1, lwrLim:1,uprLim:1

#######################
# Systematics setup
#######################

[Sys - allSys1]
name:*, float:true
name:mu*, float:false, centralVal: 1.
name:mu_Zjets, float:true
name:mu_ggf_*, float:true
name:mu_wwTop*,float:true
name:mu_wwTop_bin*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false


[Sys - allSys2]
name:*, float:true
name:mu*, float:false, centralVal: 1.
name:mu_vbf_bin*, float:false
name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets, float:true
name:mu_ggfA, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfB, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfC, float:true,centralVal: 1, lwrLim:-5, uprLim:5
#name:mu_vbf_bin*, float:false
#name:mu_vbf_bin0, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin1, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin2, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin3, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin4, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin5, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin6, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf_bin7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop*, float:true
name:mu_wwTop, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin*, float:false
name:mu_wwTop_bin0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin6, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_wwTop_bin7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - allSys3]
name:*, float:true

name:  gamma_stat_CRGGF1_bdt_ggFCR1_bin_0,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF1_bdt_ggFCR1_bin_1,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF1_bdt_ggFCR1_bin_2,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF1_bdt_ggFCR1_bin_3,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF2_bdt_ggFCR2_bin_0 ,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF2_bdt_ggFCR2_bin_1,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF2_bdt_ggFCR2_bin_2 ,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF2_bdt_ggFCR2_bin_3,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_0,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_1,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_2,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_3 ,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_4,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_5,float:false ,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_6,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:  gamma_stat_CRGGF3_bdt_ggFCR3_bin_7,float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu*, float:false, centralVal: 1.
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets, float:true
name:mu_ggfA, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfB, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfC, float:true,centralVal: 1, lwrLim:-5, uprLim:5
#name:mu_ggfA, float:false,centralVal: 1
#name:mu_ggfB, float:false,centralVal: 1
#name:mu_ggfC, float:false,centralVal: 1
#name:mu_ggf_*,float:true,centralVal: 1

name:mu_wwTop_bin0, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin1, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin2, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin3, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin4, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin5, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin6, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop*, float:true
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false


[Sys - allSys_muvbf]
name:*, float:true
name:mu*, float:false, centralVal: 1.
name:mu_vbf*, float:true, centralVal: 1.
name:mu_vbf, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
#name:mu_ggf, float:false, centralVal: 1.
#name:alpha_*, float: true
#name:*top*, float:true
#name:*vbf*, float:true
#name:*ggf*, float:true
#name:*Zjets*, float:true
#name:*diboson*, float:true

[Sys - allSys] 
name:*, float:true
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - allSys_fixGamma]
name:*, float:true
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:*gamma_stat*,float:false

[Sys - allSys_muggf_noTop]
name:*, float:true
name:alpha_theo_ttbar*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1

[Sys - allSys_muggf]
name:*, float:true
#name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
#name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
#name:mu_wwTop, float:true, centralVal: 1, lwrLim:0, uprLim:5
#name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1

[Sys - allSys_fix_all_mu]
name:*, float:true
name:mu*, float:false
name:mu_wwTop3, float: false,centralVal: 1, lwrLim:1, uprLim:1
name:sigma_bin*,float:true
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu*, float:false, centralVal: 1.
name:mu_vbf*, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets_CRGGF1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfA, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfB, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfC, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1

name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1
name:mu_wwTop_bin1, float:false, centralVal: 1, lwrLim:1,uprLim:1
name:mu_wwTop_bin2, float:false, centralVal: 1, lwrLim:1,uprLim:1
[Sys - allSys_muggf_muvbf]
name:*, float:true
#name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:true, centralVal: 1.0,lwrLim:-5, uprLim:5
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma_bin*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1

[Sys - statOnly_muggf]
name:*, float:true
name:*gamma_stat*,float:false
name:alpha*,float:false
name:mu_vbf*, float:false, centralVal: 1.
#name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
#name:mu_wwTop, float:true, centralVal: 1, lwrLim:0, uprLim:5
#name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1

[Sys - statOnly_muggf_muvbf]
name:*, float:true
name:*gamma_stat*,float:false
name:alpha*,float:false
name:mu_vbf**, float:false, centralVal: 1.
name:mu_vbf, float:true,centralVal: 1.0,lwrLim:-5, uprLim:5
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:sigma_bin*, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1

[Sys - allSys_muggf_fid]
name:*, float:true
name: alpha_theo_ttbar_fsr, float: false,centralVal: 0, lwrLim:0, uprLim:0
name: alpha_theo_ttbar_matching, float: false,centralVal: 0, lwrLim:0, uprLim:0
name: alpha_theo_ztautau_generator, float: false,centralVal: 0, lwrLim:0, uprLim:0
name: alpha_theo_ttbar_isr, float: false,centralVal: 0, lwrLim:0, uprLim:0
name: alpha_theo_ttbar_pdf, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ttbar_shower, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_vbf_alphas, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_vbf_generator, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_vbf_pdf4lhc, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_vbf_scale, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_vbf_shower, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ww_alphas, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ww_pdf, float: false,centralVal: 0, lwrLim:0, uprLim:0
name: alpha_theo_ww_scale, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ztautau_alphas, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ztautau_pdf, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:alpha_theo_ztautau_scale, float: false,centralVal: 0, lwrLim:0, uprLim:0
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: false
name:mu_wwTop4, float: false

[Sys - ExpSys_fixmu_wwTop]
name:*, float:true
name:alpha_theo*,float:false
#name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: false
name:mu_wwTop4, float: false

[Sys - allSys_fixSomeNP]
name:*, float:true
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:*gamma_stat*,float:false
name: alpha_MUON_SCALE, float: false
name: alpha_theo_ttbar_fsr, float: false
name: alpha_theo_ttbar_matching, float: false
name: alpha_theo_ztautau_generator, float: false
name: alpha_JET_Flavor_Composition, float: false
name: alpha_FT_EFF_Eigen_B_0, float: false

[Sys - allSys_noMu]
name:*, float:true
#name:mu_vbf*, float:false, centralVal: 1.
name:mu*, float false
#name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - statOnly_muFloat]
name:*, float:false
name:gamma*, float:false
name:mu_ggf, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_ggf_1, float:false,centralVal: 1, lwrLim:-5, uprLim:5
#name:mu_ggf_2, float:false,centralVal: 1, lwrLim:-5, uprLim:5
#name:mu_ggf_3, float:false,centralVal: 1, lwrLim:-5, uprLim:5

name:mu_ggfA, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfB, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfC, float:true,centralVal: 1, lwrLim:-5, uprLim:5
#name:mu_ggfA, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_ggfB, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_ggfC, float:false,centralVal: 1, lwrLim:1, uprLim:1
#name:mu_ggf_*,float:true,centralVal: 1
name:mu_Zjets, float:true
name:mu_wwTop_bin0, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin1, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin2, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin3, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin4, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin5, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop*, float:true
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - statOnly_vbfFloat]
name:*, float:false
name:mu_vbf, float:true

[Sys - onlyTheory]
name:*, float:true
name:alpha*, float:false
name:*top*, float:true
name:*vbf*, float:true
name:*ggf*, float:true
name:*Zjets*, float:true
name:*diboson*, float:true

[Sys - only_Exp]
name:alpha*, float:true
name:*top*, float:false
name:*vbf*, float:false
name:*ggf*, float:false
name:*Zjets*, float:false
name:*diboson*, float:false
name:mu*, float:true

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mu*, float:false, centralVal:1

[Global]
workspaceName: w
modelConfigName: ModelConfig
#datasetName: asimovData
#asimovName: asimovData
datasetName: Data
asimovName: Data
fitStrategy: 2 
applyOffset: true
applyRooStartMomentFix: false
#loadSnapshot: true
#snapshotName: prefit 
#unconditionalSnap: prefit
#snapshotName: myPostFit
#unconditionalSnap:myPostFit
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2 
epsVal:0.01
seed:0
MinosFit: false

# Information on the workspace. To help with the plotting
workspaceType: sigma
labelName: H #rightarrow WW

