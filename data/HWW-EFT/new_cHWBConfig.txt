[POI]
name:cHWB, float:true, centralVal:0, lwrLim:-20., uprLim:20.
name:cHd, float:false
name:cHW, float:false
name:cHbox, float:false
name:cHDD, float:false
name:cll1, float:false
name:cHB, float:false
name:cHWBtil, float:false
name:cHBtil, float:false
name:cHl3, float:false
name:cHWtil, float:false
name:cHq3, float:false
name:cHq1, float:false
name:cHu, float:false

name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:sigma_bin*,float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu*, float:false, centralVal: 1.
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_0, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_3, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_4, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_5, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_6, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_vbf_bin_7, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_Zjets, float:true
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfA, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfB, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfC, float:false,centralVal: 1, lwrLim:1, uprLim:1

name:mu_ggf_1, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_2, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_3, float:false,centralVal: 1, lwrLim:1, uprLim:1

name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
name:mu_wwTop_bin0, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin1, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin2, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin3, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin4, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin5, float:true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop_bin6, float:true,centralVal: 1, lwrLim:-5, uprLim:5

#######################
# Systematics setup
#######################
[Sys - allSys]
name:*, float:true
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:1.0, lwrLim:1.0,uprLim:1.0
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - allSys_muvbf]
name:*, float:true
name:mu*, float:false, centralVal: 1.
name:mu_vbf*, float:true, centralVal: 1.
name:mu_vbf, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false

[Sys - allSys_muggf]
name:*, float:true
#name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
#name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
#name:mu_wwTop, float:true, centralVal: 1, lwrLim:0, uprLim:5
#name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1

[Sys - allSys_muggf_muvbf]
name:*, float:true
#name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:true, centralVal: 1.0,lwrLim:-5, uprLim:5
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma_bin*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1

[Sys - statOnly_muggf]
name:*, float:true
name:*gamma_stat*,float:false
name:alpha*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1
#name:mu_wwTop, float:true, centralVal: 1, lwrLim:0, uprLim:5
#name:mu_wwTop_bin0, float:false, centralVal: 1, lwrLim:1,uprLim:1

[Sys - statOnly_muggf_muvbf]
name:*, float:true
name:*gamma_stat*,float:false
name:alpha*,float:false
name:mu_vbf**, float:false, centralVal: 1.
name:mu_vbf, float:true,centralVal: 1.0,lwrLim:-5, uprLim:5
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggf_*, float:false
name:sigma, float:false
name:sigma_bin*, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false
name:mu_wwTop, float:false, centralVal: 1, lwrLim:1, uprLim:1

[Sys - allSys_muggf_fid]
name:*, float:true
#name:alpha_theo*,float:false
#name:*gamma_stat*,float:false
#name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:true, centralVal:1.0, lwrLim:-5,uprLim:5
name:mu_vbf, float:true, centralVal:2.068, lwrLim:0,uprLim:5
name:mu_ggf, float:true, centralVal: 1, lwrLim:-5, uprLim:5
name:mu_ggfA, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfB, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggfC, float:false,centralVal: 1, lwrLim:1, uprLim:1
name:mu_ggf_*, float:false
name:sigma*, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: true,centralVal: 1, lwrLim:-5, uprLim:5
name:mu_wwTop4, float: false

[Sys - allSys_fixmu_wwTop]
name:*, float:true
name:*gamma_stat*,float:false
name:mu_vbf*, float:false, centralVal: 1.
name:mu_vbf, float:false, centralVal:2.068, lwrLim:2.068,uprLim:2.068
name:mu_ggf, float:false
name:mu_ggf_*, float:false
name:sigma, float:false
name:matrix_bin*, float:false
name:OneOverC_bin*,float:false
name:mu_wwTop2, float: false
name:mu_wwTop3, float: false
name:mu_wwTop4, float: false


#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mu*, float:false, centralVal:1

[Global]
workspaceName: w
modelConfigName: ModelConfig
datasetName: asimovData
asimovName: asimovData
fitStrategy: 2
applyOffset: false
applyRooStartMomentFix: false
#loadSnapshot: true
#snapshotName: myPostFit
#unconditionalSnap: myPostFit
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2
epsVal:0.01
seed:0
MinosFit: false

# Information on the workspace. To help with the plotting
workspaceType: EFT
labelName: H #rightarrow WW

