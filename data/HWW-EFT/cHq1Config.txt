[POI]
#name:mu*, float:true
name:sigma_bin*, float:false
name:mu_vbf_bin*, float:false, centralVal:1.
name:cHq1, float:true, centralVal:0, lwrLim:-10.0, uprLim:10.0
name:cHd, float:false
name:cHWB, float:false
name:cHbox, float:false
name:cHDD, float:false
name:cll1, float:false
name:cHB, float:false
name:cHWBtil, float:false
name:cHBtil, float:false
name:cHl3, float:false
name:cHWtil, float:false
name:cHq3, float:false
name:cHW, float:false
name:cHu, float:false

[Sys - statOnly] 
name:*, float:false
name:mu*, float:true
name:mu_vbf_bin*, float:false, centralVal:1.

[Sys - allSys] 
name:*, float:true
name:alpha_*, float: true
name:*top*, float:true
name:*vbf*, float:true
name:*ggf*, float:true
name:*Zjets*, float:true
name:*diboson*, float:true

[Sys - statOnly_muFloat]
name:*, float:false
name:mu*, float:true
name:mu_vbf_bin*, float:false

[Sys - statOnly_vbfFloat]
name:*, float:false
name:mu_vbf, float:true

[Sys - onlyTheory]
name:*, float:true
name:alpha*, float:false
name:*top*, float:true
name:*vbf*, float:true
name:*ggf*, float:true
name:*Zjets*, float:true
name:*diboson*, float:true

[Sys - only_Exp]
name:alpha*, float:true
name:*top*, float:false
name:*vbf*, float:false
name:*ggf*, float:false
name:*Zjets*, float:false
name:*diboson*, float:false
name:mu*, float:true

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:cHl3, float:false, centralVal:0
name:cHWB, float:false, centralVal:0
name:cHB, float:false, centralVal:0
name:cHbox, float:false, centralVal:0
name:cHDD, float:false, centralVal:0
name:cll1, float:false, centralVal:0
name:cHWtil, float:false, centralVal:0
name:cHWBtil, float:false, centralVal:0
name:cHBtil, float:false, centralVal:0


[Global]
workspaceName: w 
modelConfigName: ModelConfig
datasetName: asimovData1
asimovName: asimovData1
fitStrategy: 2 
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2
epsVal:0.01
seed:0
MinosFit: True

# Information on the workspace. To help with the plotting
workspaceType: EFT
labelName: H #rightarrow WW


