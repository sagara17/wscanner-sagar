[POI]
name:mH*, float:true, centralVal:125, lwrLim:110, uprLim:140
name:mu*, float:true, centralVal:1, lwrLim:0, uprLim:10

#######################
# Systematics setup
#######################
[Sys - allSys] 
name:*, float:false

[Sys - statOnly] 
name:*, float:False

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mH, float:False, centralVal:125

[Global]
workspaceName: combined
modelConfigName:ModelConfig
datasetName: obsData
asimovName: combData
fitStrategy: 0
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2
epsVal:0.1


#######################
# scanDatasets setup
#######################
[scanDatasets - scanSetup]
Dimension:1
MinosFit: True
poiName: mH
#randomizeGlobs: True
#poiInfo:mH:20:122.5:127


[extraInformation]
# Information on the workspace. To help with the plotting
workspaceType: mass
labelName: H #rightarrow ZZ* #rightarrow 4l
