[POI]
name:c*, float:False, centralVal:0
name:cuH, float:True, centralVal:0, lwrLim:-100, uprLim:100
name:cHG, float:True, centralVal:0, lwrLim:-40, uprLim:40


[POIUnconditional]
name:mu*, float:true, centralVal:1, lwrLim:-10, uprLim:10
name:mu,  float:true, centralVal:1, lwrLim:-10, uprLim:10
name:r*,  float:true, centralVal:1, lwrLim:-10, uprLim:10
name:mu_ttH, lwrLim:0, uprLim:10
name:mu_VH, lwrLim:0
name:mu_qqH2qq_VHHad, lwrLim:-1
name:mu_gg2H_1J_PTH_120_200, lwrLim:-5
name:mu_gg2H_GE2J, lwrLim:-1
name:mu_VBF_qq2qq_PTH_GT200, lwrLim:-5

#######################
# Systematics setup
#######################
[Sys - allSys]
name:*, float:True

[Sys - statOnly]
name:*, float:False

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI]
name:mu_*, float:False, centralVal:0

[ModifyVar]

#######################
# Toys setup
#######################
[Toys - POISetup]
name:c*, centralVal:0
[Toys - NPSetup]
name:*, centralVal:0
[Toys - OtherSetup]
name:r*, centralVal:1
[Toys - FitSetup]
name:c*, float:False
name:r*, float:True

#######################
# Global setup
#######################
[Global]
workspaceName: combined
modelConfigName: ModelConfig_reparam
uncModelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovData
fitStrategy: 0
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
epsVal:0.1
# toys specific options
seed:0
scanToy: false



# Information on the workspace. To help with the plotting
workspaceType: H4lEFT
labelName: H #rightarrow ZZ* #rightarrow 4l
