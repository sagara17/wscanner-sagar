[POI]

name:cHGtil, float:false
name:cHWtil, float:true, centralVal:0, lwrLim:-40, uprLim:40
name:cHBtil, float:false
name:cHWtilB, float:true, centralVal:0, lwrLim:-40, uprLim:40
name:cuHtil, float:false
name:r*, float:true, centralVal:1, lwrLim:-40, uprLim:40
name:mu*, float:false

[POIUnconditional]
name:mu*, float:true, centralVal:1, lwrLim:-10, uprLim:10
name:mu,  float:true, centralVal:1, lwrLim:-10, uprLim:10
name:r*,  float:true, centralVal:1, lwrLim:-10, uprLim:10
name:mu_ttH, lwrLim:0, uprLim:10
name:mu_VH, lwrLim:0
name:mu_qqH2qq_VHHad, lwrLim:-1
name:mu_gg2H_1J_PTH_120_200, lwrLim:-5
name:mu_gg2H_GE2J, lwrLim:-1
name:mu_VBF_qq2qq_PTH_GT200, lwrLim:-5

#######################
# Systematics setup
#######################
[Sys - allSys]
name:*, float:true

[Sys - statOnly]
name:*, float:false

[Sys - modTheory]
name:*PDF*, float:false
name:*pdf*, float:false
name:*QCD*, float:false
name:*ggF*Comp*, float:false
name:*ggF_heavyFlavour*, float:false
name:*ttV_genComp*, float:false
name:*r_bbH_ggH*, float:false


#######################
# Compatibility setup
#######################
[Compatibility - singlePOI]
name:mu*, float:false, centralVal:1
name:cHGtil, float:false, centralVal:0
name:cHWtil, float:false, centralVal:0
name:cHBtil, float:false, centralVal:0
name:cHWtilB, float:false, centralVal:0
name:cuHtil, float:false, centralVal:0


[Global]
workspaceName: combined
modelConfigName: ModelConfig_reparam
uncModelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovData
fitStrategy: 0
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
epsVal:0.01
seed:0




# Information on the workspace. To help with the plotting
workspaceType: EFT
labelName: H #rightarrow ZZ* #rightarrow 4l
