[POI]

name:cHGtil, float:false
name:cHWtil, float:true, centralVal:0, lwrLim:-20, uprLim:20
name:cHBtil, float:false
name:cHWtilB, float:false
name:cuHtil, float:false
name:r*, float:true, centralVal:1, lwrLim:-20, uprLim:20

#######################
# Systematics setup
#######################
[Sys - allSys]
name:*, float:true

[Sys - statOnly]
name:*, float:false

[Sys - modTheory]
name:*PDF*, float:false
name:*pdf*, float:false
name:*QCD*, float:false
name:*ggF*Comp*, float:false
name:*ggF_heavyFlavour*, float:false
name:*ttV_genComp*, float:false
name:*r_bbH_ggH*, float:false


#######################
# Compatibility setup
#######################
[Compatibility - singlePOI]
name:mu*, float:false, centralVal:1
name:cHGtil, float:false, centralVal:0
name:cHWtil, float:false, centralVal:0
name:cHBtil, float:false, centralVal:0
name:cHWtilB, float:false, centralVal:0
name:cuHtil, float:false, centralVal:0


[Global]
workspaceName: combined
modelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovData
fitStrategy: 1
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
optimizeConst: 2
epsVal:0.01



# Information on the workspace. To help with the plotting
workspaceType: EFT
labelName: H #rightarrow ZZ* #rightarrow 4l
