[POI]
name:sigma_*, float:true, lwrLim:0, uprLim:10000000
name:kappaB, float:false, centralVal:1

#######################
# Systematics setup
#######################
[Sys - allSys] 
name:*, float:True

[Sys - statOnly] 
name:*, float:False

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mu_*, float:False, centralVal:0


[ModifyVar]
name:BR_ZZ, centralVal:1, float:False
name:A*_bin*, centralVal:1, float:False


[Global]
workspaceName: myWS
modelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovDataFullModel
fitStrategy: 1
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
epsVal:0.0001
seed:0


# Information on the workspace. To help with the plotting
workspaceType: H4lLightYukawa
labelName: H #rightarrow ZZ* #rightarrow 4l
