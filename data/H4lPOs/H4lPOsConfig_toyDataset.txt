[POI]
name:sigma*, float:true, lwrLim:-10, uprLim:10

#######################
# Systematics setup
#######################
[Sys - allSys] 
name:*, float:True

[Sys - statOnly] 
name:*, float:False

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:mu_*, float:False, centralVal:0

[ModifyVar]
name:BR_ZZ, centralVal:1, float:False
name:A*_bin*, centralVal:1, float:False
name:Muqq*, centralVal:1, float:True

#######################
# Toys setup
#######################
[Toys - POISetup]
name:kappa*, centralVal:1
[Toys - NPSetup]
name:*, centralVal:0
[Toys - OtherSetup]
name:Muqq*, centralVal:1
[Toys - FitSetup]
name:*, float:False
[Toys - scanSetup]
Dimension:2
poiInfo:kappaB:15:-4.2:8.5,kappaC:15:-12:12

#######################
# Global setup
#######################
[Global]
workspaceName: myWS
modelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovDataFullModel
fitStrategy: 1
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
epsVal:0.0001
# toys specific options
seed:0
scanToy: false

# Information on the workspace. To help with the plotting
workspaceType: H4lLightYukawa
labelName: H #rightarrow ZZ* #rightarrow 4l
