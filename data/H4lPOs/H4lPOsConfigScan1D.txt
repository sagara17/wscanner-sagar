[POI]
name:eElR*, float:true, lwrLim:-50, uprLim:50
name:eMuR*, float:false, lwrLim:-50, uprLim:50


#######################
# Systematics setup
#######################
[Sys - allSys] 
name:*, float:True

[Sys - statOnly] 
name:*, float:False

#######################
# Modify any vars before fitting
#######################
[ModifyVar]
name:BR_ZZ, centralVal:1, float:False
name:A*_bin*, centralVal:1, float:False
name:Muqq*, centralVal:1, float:True

#######################
# Compatibility setup
#######################
[Compatibility - singlePOI] 
name:eL*, float:False, centralVal:0

#######################
# Toys setup
#######################
[Toys - POISetup]
name:eMuR, centralVal:0
name:eElR, centralVal:0
[Toys - NPSetup]
name:*, centralVal:0
[Toys - OtherSetup]
name:Muqq*, centralVal:1
[Toys - FitSetup]
name:*, float:False






[Global]
workspaceName: myWS
modelConfigName: ModelConfig
datasetName: obsData
asimovName: asimovDataFullModel
fitStrategy: 1
applyOffset: true
applyRooStartMomentFix: false
loadSnapshot: false
snapshotName: noSnapshot
unconditionalSnap: noSnapshot
epsVal:0.0001


# Information on the workspace. To help with the plotting
workspaceType: H4lPOs
labelName: H #rightarrow ZZ* #rightarrow 4l
