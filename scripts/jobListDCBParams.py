
from buildList import *


binList = [
"BDT_bin1",
"BDT_bin2",
"BDT_bin3",
"BDT_bin4",
# "BDT_Inc",
]
decayList = [
"4mu",
# "4e",
# "2mu2e",
# "2e2mu",
]
massPoints = ["123", "124", "125p5", "126", "127"]


#=============================================================================
#					            MINOS Error Iter 1		    	 			 =							
#=============================================================================
minosError = []
for cBin in binList:
	for cDecay in decayList:

		inputDict = {}
		inputDict["poiList"] 		= [
		"DCB_mean_ggHVBFSig123",
		"DCB_mean_ggHVBFSig124",
		"DCB_mean_ggHVBFSig125",
		"DCB_mean_ggHVBFSig125p5",
		"DCB_mean_ggHVBFSig126",
		"DCB_mean_ggHVBFSig127",

		"DCB_sigma_ggHVBFSig123",
		"DCB_sigma_ggHVBFSig124",
		"DCB_sigma_ggHVBFSig125",
		"DCB_sigma_ggHVBFSig125p5",
		"DCB_sigma_ggHVBFSig126",
		"DCB_sigma_ggHVBFSig127",

		"DCB_alphaHi_ggHVBFSig123",
		"DCB_alphaHi_ggHVBFSig124",
		"DCB_alphaHi_ggHVBFSig125",
		"DCB_alphaHi_ggHVBFSig125p5",
		"DCB_alphaHi_ggHVBFSig126",
		"DCB_alphaHi_ggHVBFSig127",


		"DCB_alphaLo_ggHVBFSig123",
		"DCB_alphaLo_ggHVBFSig124",
		"DCB_alphaLo_ggHVBFSig125",
		"DCB_alphaLo_ggHVBFSig125p5",
		"DCB_alphaLo_ggHVBFSig126",
		"DCB_alphaLo_ggHVBFSig127",

		"DCB_etaHi_ggHVBFSig123",
		"DCB_etaHi_ggHVBFSig124",
		"DCB_etaHi_ggHVBFSig125",
		"DCB_etaHi_ggHVBFSig125p5",
		"DCB_etaHi_ggHVBFSig126",
		"DCB_etaHi_ggHVBFSig127",


		"DCB_etaLo_ggHVBFSig123",
		"DCB_etaLo_ggHVBFSig124",
		"DCB_etaLo_ggHVBFSig125",
		"DCB_etaLo_ggHVBFSig125p5",
		"DCB_etaLo_ggHVBFSig126",
		"DCB_etaLo_ggHVBFSig127",

		]

		## add the norm which has the bin and chan in its name
		# for mP in massPoints:
		# 	inputDict["poiList"].append("muSig_ggHVBFSig" + mP + "_" + cDecay + "_" + cBin)


		## other general options
		inputDict["config"]         = "../source/wscanner/data/DCBParams/v1/Iter1.txt"
		inputDict["scanType"] 		= ["allSys"]
		inputDict["dimension"] 		= "minosError"
		inputDict["doData"] 		= [False]

		workspaceName = "A10_Analytic_DCBWS_NNnewBins/workspace/A10_Analytic_DCBWS_NNnewBins_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_analytic_workspace.root"
		outputDir = "A10_Analytic_DCBWS_NNnewBins_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_allMP"

		inputDict["fileName"]       = [workspaceName]
		inputDict["outputDir"]      = [outputDir] 
		# buildList(inputDict, minosError)



#=============================================================================
#					         MINOS Error Iter 2 (1D)	    	 			 =							
#=============================================================================
for cBin in binList:
	for cDecay in decayList:
		inputDict = {}
		inputDict["poiList"] 		= [
			"DCB_mean_slope",
			"DCB_mean_intercept",
			"DCB_sigma",
			"DCB_alphaHi",
			"DCB_alphaLo",
			"DCB_etaHi",
			"DCB_etaLo",
		]

		## add the norm which has the bin and chan in its name
		# for mP in massPoints:
		# 	inputDict["poiList"].append("muSig_ggHVBFSig" + mP + "_" + cDecay + "_" + cBin)

		## other general options
		inputDict["config"]         = "../source/wscanner/data/DCBParams/v1/Iter1.txt"
		inputDict["scanType"] 		= ["allSys"]
		inputDict["dimension"] 		= "minosError"
		inputDict["doData"] 		= [False]

		if("BDT_Inc" in cBin):
			workspaceName = "A21_PERDCB_qrnn_baselineDeep_regscore/workspace/A21_PERDCB_qrnn_baselineDeep_regscore"+"_" + cDecay + "_" + cBin + "_AllP_Nominal_sim_analytic_workspace.root"
		else:
			workspaceName = "A21_PERDCB_qrnn_baselineDeep_regscore/workspace/A21_PERDCB_qrnn_baselineDeep_regscore"+"_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_sim_analytic_workspace.root"
		outputDir = "A21_PERDCB_qrnn_baselineDeep_regscore"+"_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_allMP_SimTest"

		inputDict["fileName"]       = [workspaceName]
		inputDict["outputDir"]      = [outputDir] 
		# buildList(inputDict, minosError)
 

#=============================================================================
#					   MINOS Error Iter 2 (1D Conditional)	    	 	 	 =							
#=============================================================================
baseName = "O1_PERDCB_m4lerr_reweighted"
for cBin in binList:
	for cDecay in decayList:
		inputDict = {}
		inputDict["poiList"] 		= [
			"DCB_mean_slope",
			"DCB_mean_intercept",
			"sigmaSF",
			"DCB_alphaHi",
			"DCB_alphaLo",
			"DCB_etaHi",
			"DCB_etaLo",
		]

		## add the norm which has the bin and chan in its name
		# for mP in massPoints:
		# 	inputDict["poiList"].append("muSig_ggHVBFSig" + mP + "_" + cDecay + "_" + cBin)

		## other general options
		inputDict["config"]         = "../source/wscanner/data/DCBParams/1DConditional/v1/Iter2Config.txt"
		inputDict["scanType"] 		= ["allSys"]
		inputDict["dimension"] 		= "minosError"
		inputDict["doData"] 		= [False]

		if("BDT_Inc" in cBin):
			workspaceName = baseName + "/workspace/" + baseName + "_" + cDecay + "_" + cBin + "_AllP_Nominal_sim_PER_workspace.root"
		else:
			workspaceName = baseName + "/workspace/" + baseName +"_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_sim_PER_workspace.root"
		outputDir = baseName +"_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_allMP_SimTest"

		inputDict["fileName"]       = [workspaceName]
		inputDict["outputDir"]      = [outputDir] 
		buildList(inputDict, minosError)
 		#qrnn_baselineDeep_regscore, qrnn_baseline_regscore, qrnn_justrNN_regscore, qrnn_extra_regscore, qrnn_noErr_regscore




#=============================================================================
#					            MINOS Error Iter 2p1		    	 			 =							
#=============================================================================
for cBin in binList:
	for cDecay in decayList:
		inputDict = {}
		inputDict["poiList"] 		= [
			"DCB_mean_slope",
			"DCB_mean_intercept",
			"DCB_sigma",
			# "DCB_alphaHi",
			# "DCB_alphaLo",
			# "DCB_etaHi",
			# "DCB_etaLo",
		]

		## add the norm which has the bin and chan in its name
		# for mP in massPoints:
		# 	inputDict["poiList"].append("muSig_ggHVBFSig" + mP + "_" + cDecay + "_" + cBin)

		## other general options
		inputDict["config"]         = "../source/wscanner/data/DCBParams/v1/DCBConfig_" + cDecay + "_" +  cBin + "_2p1.txt"


		inputDict["scanType"] 		= ["allSys"]
		inputDict["dimension"] 		= "minosError"
		inputDict["doData"] 		= [False]

		workspaceName = "A6_Analytic_DCBWS_BDTw_lumi/workspace/A6_Analytic_DCBWS_BDTw_lumi_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_sim_analytic_workspace.root"
		outputDir = "A6_Analytic_DCBWS_BDTw_lumi_" + cDecay + "_" + cBin.replace("BDT_", "").replace("in", "") + "_AllP_Nominal_allMP_Sim_Iter2p1"

		inputDict["fileName"]       = [workspaceName]
		inputDict["outputDir"]      = [outputDir] 
		# buildList(inputDict, minosError)




#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
inputDict["poiList"] 	    = [
"DCB_mean_slope:50:0.8:1.2",
"DCB_mean_intercept:50:-0.4:0.0",
"DCB_sigma:50:1.5:2.5",
"DCB_alphaHi:50:1.0:3",  
"DCB_alphaLo:50:0.8:3",
"DCB_etaHi:50:5:20",
"DCB_etaLo:50:-20:100",
]
inputDict["config"]         = "../source/wscanner/data/DCBParams/v1/Iter1.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["A10_Analytic_DCBWS_NNnewBins/workspace/A10_Analytic_DCBWS_NNnewBins_2mu2e_b3_AllP_Nominal_sim_analytic_workspace.root"]
inputDict["outputDir"]      = ["A10_Analytic_DCBWS_NNnewBins_2mu2e_b3_AllP_Nominal_sim_analytic_workspace_1DScan"] 
inputDict["nSplitJob"]      = 1
buildList(inputDict, scan1D)






