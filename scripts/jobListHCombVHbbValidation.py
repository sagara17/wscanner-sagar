from buildList import *

#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
inputDict["poiList"] = [
"mu:50:0.5:1.5", 
]
inputDict["config"]         = "../source/data/HComb/combination5XS.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [ True]
inputDict["fileName"]       = ["workspaces/HWW_5XS.root"]
inputDict["outputDir"]      = ["HWW_5XS"] 
inputDict["nSplitJob"]      = 5
buildList(inputDict, scan1D)


inputDict = {}
inputDict["poiList"] = [
"mu_WH:50:0.5:1.5", 
"mu_ZH:50:0.5:1.5", 
]
inputDict["config"]         = "../source/data/HComb/VHbbValidationConfig.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False, True]
inputDict["fileName"]       = ["workspaces/mergedWHZHVHbb.root"]
inputDict["outputDir"]      = ["mergedWHZHVHbb"] 
inputDict["nSplitJob"]      = 5
# buildList(inputDict, scan1D)


inputDict = {}
inputDict["poiList"] = [
"SigXsecOverSMWHx150x250PTV:50:0.2:1.5", 
"SigXsecOverSMWHxGT250PTV:50:0.5:1.5", 
"SigXsecOverSMZHx75x150PTV:50:0.25:2", 
"SigXsecOverSMZHx150x250PTV:50:0.5:1.6", 
"SigXsecOverSMZHxGT250PTV:50:0.5:1.8", 
]
inputDict["config"]         = "../source/data/HComb/VHbbValidationConfigFixed.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False, True]
inputDict["fileName"]       = ["workspaces/merged5POIVHbb.root"]
inputDict["outputDir"]      = ["merged5POIVHbb"] 
inputDict["nSplitJob"]      = 5
# buildList(inputDict, scan1D)


savePulls = []
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/VHbbValidationConfigFixed.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "savePulls"
inputDict["doData"]         = [True,False]
inputDict["fileName"]       = ["workspaces/HZZ_VHbb_HGam_HWW_5XS.root_raw.root"]
inputDict["outputDir"]      = ["HZZ_VHbb_HGam_HWW_5XS_savePulls"] 
# buildList(inputDict, savePulls)
