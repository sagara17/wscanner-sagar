from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHWBtil:50:-15.0:15.0"]
inputDict["config"]         = "../source/data/HWW-EFT/noEFTConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar_withSys/ws_SignedDPhijj_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["signedDPhijj_cHWBtil"]
buildList(inputDict, scan1D)


