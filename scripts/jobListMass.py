from buildList import *

#=============================================================================
#                                dataset Scans                               =                          
#=============================================================================
scanDatasets = []
inputDict = {}


inputDict["poiList"] 	    = ["scanDatasets:inWorkspace:1000"]
inputDict["config"]         = "../source/data/H4lMass/H4lMassConfigDatasets.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "scanDatasets"
inputDict["doData"]         = [False]
inputDict["fileName"]		= getfileList("workspaces/N25_Analytic_incBDTBin_unconstrained_All_BDT_Inc_Fixed_noNegCDFToys_datasetPart", "_workspace.root", 50)
inputDict["outputDir"]		= getfileList("N25_Analytic_incBDTBin_unconstrained_All_BDT_Inc_Fixed_noNegCDFToys_datasetPart", "_datasetScan", 50)
inputDict["nSplitJob"]      = 1
# buildList(inputDict, scanDatasets)



baseName = "WS1DConditional_m4lerr" 

for index in range(0, 25):
      inputDict = {}

      wsName = baseName + "/workspace/" + baseName + "_4mu_All_AllSigqqZZEWZZOther_PerBDTBin_noNegCDFToys_datasetPart"+str(index)+"_V1_workspace.root"
      outName = baseName + "_4mu_All_SF1_Part"+str(index)

      inputDict["poiList"]        = ["scanDatasets:"+wsName+":2000"]
      inputDict["config"]         = "../source/wscanner/data/H4lMass/H4lMassConfigDatasets.txt"
      inputDict["scanType"]       = ["statOnly"]
      inputDict["dimension"]      = "scanDatasets"
      inputDict["doData"]         = [False]
      inputDict["fileName"]       = [baseName + "/workspace/" + baseName + "_4mu_All_AllSigqqZZEWZZOther_PerBDTBin_fullMC_V1_workspace.root"]
      inputDict["outputDir"]      = [outName] 
      inputDict["nSplitJob"]      = 1
      buildList(inputDict, scanDatasets)




#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
inputDict["poiList"] 	    = ["mH:50:122:127"]
inputDict["config"]         = "../source/data/H4lMass/H4lMassConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/bkgSmooth_workspace.root"]
inputDict["outputDir"]      = ["bkgSmooth_workspace_1D"] 
inputDict["nSplitJob"]      = 1
# buildList(inputDict, scan1D)



binList = [
"BDT_bin1",
"BDT_bin2",
"BDT_bin3", 
"BDT_bin4",
"All"
]
decayList = [
"4mu",
# "4e",
# "2mu2e",
# "2e2mu",
]

baseName = "WS1DConditional_m4lerr" 
tagName = "V1"

for cBin in binList:
	for cDecay in decayList:

		inputDict = {}
		inputDict["poiList"] 		= ["mH:50:122:127"]
		inputDict["config"]         = "../source/wscanner/data/H4lMass/H4lMassConfig1DConditional.txt"
		inputDict["scanType"] 		= ["statOnly"]
		inputDict["dimension"] 		= "1D"
		inputDict["doData"] 		= [False]

		workspaceName = baseName + "/workspace/" + baseName + "_" + cDecay + "_" + cBin + "_ggHVBF_PerBDTBin_fullMC_"+ tagName + "_workspace.root"
		outputDir = baseName +"_" + cDecay + "_" + cBin + "_"+ tagName + "_ggHVBF"

		inputDict["fileName"]       = [workspaceName]
		inputDict["outputDir"]      = [outputDir] 
		buildList(inputDict, scan1D)

# WS1DConditional_qrnnRes_v2_2e2mu_BDT_bin4_ggHVBF_PerBDTBin_fullMC_V1_workspace.root

inputDict = {}
inputDict["poiList"] 		= ["mH:50:124:126"]
inputDict["config"]         = "../source/wscanner/data/H4lMass/H4lMassConfig1DConditional.txt"
inputDict["scanType"] 		= ["statOnly"]
inputDict["dimension"] 		= "1D"
inputDict["doData"] 		= [False]
inputDict["fileName"]       = ["WS1DConditional_qrnnRes_v2/workspace/WS1DConditional_qrnnRes_v2_All_All_ggHVBF_PerBDTBin_fullMC_"+ tagName + "_workspace.root"]
inputDict["outputDir"]      = ["WS1DConditional_qrnnRes_v2_All_All_"+ tagName + "_ggHVBF"] 
# buildList(inputDict, scan1D)

inputDict = {}
inputDict["poiList"] 		= ["mH:50:124:126"]
inputDict["config"]         = "../source/wscanner/data/H4lMass/H4lMassConfig1DConditionalSubSamp.txt"
inputDict["scanType"] 		= ["statOnly"]
inputDict["dimension"] 		= "1D"
inputDict["doData"] 		= [False]
inputDict["fileName"]       = ["WS1DConditional_qrnnRes_v2/workspace/WS1DConditional_qrnnRes_v2_All_All_ggHVBF_PerBDTBin_subsampledMC_V1_workspace.root"]
inputDict["outputDir"]      = ["WS1DConditional_qrnnRes_v2_All_All_V1_ggHVBF"] 
# buildList(inputDict, scan1D)


#=============================================================================
#								    Unconditional ws						 =							
#=============================================================================
# this will make an unconditional workspace
unCondWS = []
inputDict["poiList"] 		= ["'*'"]
inputDict["config"] 		= "../source/data/H4lMass/H4lMassConfig.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["doData"]         = [True]
inputDict["dimension"] 		= "saveUncondws"
inputDict["saveUncondWorkspace"] 	= True   
inputDict["fileName"] 		= ["workspaces/J21_Analytic_4mu_All_PerBDTBin_fullMC_V14_workspace.root"]
inputDict["outputDir"] 		= ["massTest_unCondWS"]
buildList(inputDict, unCondWS)















