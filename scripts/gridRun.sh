#!/bin/bash

folderName=$1
jobID=$2

jobIDStr=job${jobID}_

# make clean
# make

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
lsetup  cmake


## do the build command
#mkdir build
#cd build
#
## cmake from the source
#cmake ../source
#make 
#
## go back to the folder to run it
#cd ../

cd source
make clean
make
cd ../


# run the command
for files in $folderName/*.sh
do
    if [[ $files == *${jobIDStr}* ]]; then

        echo 'Running', ${files}
        source ${files}
    fi

done


outList=""
if [ -d "root-files" ]; then
  outList=root-files/ $outList
fi
if [ -d "uncondWs" ]; then
  outList=uncondWs/ $outList
fi
# create the output tar
tar -cvzf JobSummary.tgz.gz ${outList}
