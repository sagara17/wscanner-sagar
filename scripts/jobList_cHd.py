from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHd:50:-10.0:10.0"]
inputDict["config"]         = "../source/data/HWW-EFT/noEFTConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar_withSys/ws_jet0_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["jet0_pt_cHd"]
buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHd:50:-8.0:8.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHdConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_jet1_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["jet1_pt_cHd"]
#buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHd:50:-8.0:8.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHdConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_lep0_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["lep0_pt_cHd"]
#buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHd:40:-8.0:8.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHdConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_lep1_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["lep1_pt_cHd"]
#buildList(inputDict, scan1D)


