from buildList import *

#=============================================================================
#                                    1D Scans                                =                          
#=============================================================================
scan1D = []
inputDict = {}
POINameEFF = "eff" ## SF, eff
# taggerType = "rel20" ## QWt23, mt23
# taggerTypeList = ["rel20", "QWt23", "mt23", "t23s23", "BDT", "dNN"]
taggerTypeList = ["dNN"]
# WPList = ["60", "70", "77", "85"]
WPList = ["70"]
jetType = "calo"

for WP in WPList:
    for taggerType in taggerTypeList:
        inputDict["poiList"] = [
        # "SFPOI:100:0.5:1.5", 
        "efficiency:100:0:1", 
        ]
        inputDict["config"] 		= "../source/data/Btagging/defaultConfig.txt"
        inputDict["scanType"] 		= ["statOnly","allSys"]
        inputDict["dimension"] 		= "1D"
        inputDict["doData"] 		= [True, False]
        inputDict["fileName"] 		= [
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_1_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_2_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_3_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_4_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_All_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        ]
        inputDict["outputDir"] 		= [
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_1_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_2_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_3_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_4_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_All_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        ]
        buildList(inputDict, scan1D)

POINameEFF = "SF"     
for WP in WPList:
    for taggerType in taggerTypeList:
        inputDict["poiList"] = [
        "SFPOI:100:0.5:1.5", 
        # "efficiency:100:0.5:1", 
        ]
        inputDict["config"]         = "../source/data/Btagging/defaultConfig.txt"
        inputDict["scanType"]       = ["statOnly","allSys"]
        inputDict["dimension"]      = "1D"
        inputDict["doData"]         = [True, False]
        inputDict["fileName"]       = [
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_1_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_2_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_3_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_4_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        'v6_workspaces/category_ljetPt_'+jetType+'_rABCD16_'+WP+'_ljet_'+taggerType+'_All_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
        ]
        inputDict["outputDir"]      = [
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_1_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_2_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_3_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_4_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        "category_ljetPt_"+jetType+"_rABCD16_"+WP+"_ljet_"+taggerType+"_All_15_16_17_18_"+POINameEFF+"_combined_meas_model",
        ]
        buildList(inputDict, scan1D)


#=============================================================================
#                           Ranking - Btagging                             =                            
#=============================================================================
ranking = []
inputDict = {}
inputDict["poiList"] 		= ["(.*)"]
inputDict["config"] 		= "../source/data/Btagging/defaultConfig.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["dimension"] 		= "ranking"
inputDict["doData"] 		= [True, False]
inputDict["fileName"] 		= [
'workspaces/category_ljetPt_calo_rABCD16_77_ljet_mt23_1_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
'workspaces/category_ljetPt_calo_rABCD16_77_ljet_mt23_2_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
'workspaces/category_ljetPt_calo_rABCD16_77_ljet_mt23_3_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
'workspaces/category_ljetPt_calo_rABCD16_77_ljet_mt23_4_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
'workspaces/category_ljetPt_calo_rABCD16_77_ljet_mt23_All_15_16_17_18_'+POINameEFF+'_combined_meas_model.root',
]
inputDict["outputDir"] 		= [
"category_ljetPt_calo_rABCD16_77_ljet_mt23_1_15_16_17_18_"+POINameEFF+"_combined_meas_model",
"category_ljetPt_calo_rABCD16_77_ljet_mt23_2_15_16_17_18_"+POINameEFF+"_combined_meas_model",
"category_ljetPt_calo_rABCD16_77_ljet_mt23_3_15_16_17_18_"+POINameEFF+"_combined_meas_model",
"category_ljetPt_calo_rABCD16_77_ljet_mt23_4_15_16_17_18_"+POINameEFF+"_combined_meas_model",
"category_ljetPt_calo_rABCD16_77_ljet_mt23_All_15_16_17_18_"+POINameEFF+"_combined_meas_model",
]
buildList(inputDict, ranking)

#=============================================================================
#                       correlationHist - Btagging                           =                          
#=============================================================================
correlationHist = []

inputDict["poiList"] 		= ["'*'"]
inputDict["config"] 		= "../source/data/Btagging/defaultConfig.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["dimension"] 		= "correlationHist"
inputDict["saveCorrHist"] 	= True   
inputDict["fileName"] 		= ["workspaces/category_ljetPt_calo_rABCD16_77_ljet_rel20_1_15_16_17_18_eff.root",]
inputDict["outputDir"] 		= ["category_ljetPt_calo_rABCD16_77_ljet_rel20_1_15_16_17_18_eff_newCut_statOnly_correlation"]
buildList(inputDict, correlationHist)



