from buildList import *

#=============================================================================
#                                   2D Scans                                 =                          
#=============================================================================
scan2D = []
inputDict = {}
## XS only
inputDict["poiList"]        = ["kappaB:50:-6:8,kappaC:50:-15:20"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSOnly_reparam"]
inputDict["nSplitJob"]      = 10
buildList(inputDict, scan2D)

## with BR
inputDict["poiList"]        = ["kappaB:50:-2:2,kappaC:50:-8:8"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSandBR_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSandBR_reparam"]
inputDict["nSplitJob"]      = 10
buildList(inputDict, scan2D)


inputDict["poiList"]        = ["kappaB:50:-7.5:12.5,kappaC:50:-25:25"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_ShapeOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_ShapeOnly_reparam"]
inputDict["nSplitJob"]      = 10
buildList(inputDict, scan2D)


#=============================================================================
#                                   Run Toys                                 =                          
#=============================================================================
runToys = []
inputDict["poiList"]        = ["nToys:10000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSOnly_reparam_toys"]
inputDict["nSplitJob"]      = 50
buildList(inputDict, runToys)

inputDict["poiList"]        = ["nToys:10000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSandBR_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSandBR_reparam_toys"]
inputDict["nSplitJob"]      = 50
buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:10000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_ShapeOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_ShapeOnly_reparam_toys"]
inputDict["nSplitJob"]      = 50
buildList(inputDict, runToys)

#=============================================================================
#                                   2D Scans toys                            =                          
#=============================================================================
inputDict["poiList"]        = ["kappaB:50:-6:6,kappaC:50:-15:20,nToys:5000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSOnly_reparam_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_pt_wsys_expected_mtx_v22t2v1_XSOnly_wSys/InputsForAdaptiveSampling/kappaB_kappaC.root", "histName":"ExclusionCont"}
buildList(inputDict, scan2D)

inputDict["poiList"]        = ["kappaB:50:-6:8,kappaC:50:-15:20,nToys:5000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_XSandBR_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_XSandBR_reparam_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_pt_wsys_expected_mtx_v22t2v1_XSandBR_wSys/InputsForAdaptiveSampling/kappaB_kappaC.root", "histName":"ExclusionCont"}
buildList(inputDict, scan2D)


inputDict["poiList"]        = ["kappaB:50:-7.5:12.5,kappaC:50:-25:25,nToys:5000"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_data_mtx_v23v3_ShapeOnly_reparam.root"]
inputDict["outputDir"]      = ["ws_pt_data_mtx_v23v3_ShapeOnly_reparam_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_pt_wsys_expected_mtx_v22t2v1_ShapeOnly_wSys/InputsForAdaptiveSampling/kappaB_kappaC.root", "histName":"ExclusionCont"}
buildList(inputDict, scan2D)


# #=============================================================================
# #                                   1D Scans                                 =                          
# #=============================================================================
scan1D = []
inputDict = {}
#########
# Shape and XS
#########
inputDict["poiList"] = [
"kappaC:50:-20:20"
]
inputDict["config"]             = "../source/data/H4lLightYukawa/H4lLightYukawafixkBConfig.txt"
inputDict["scanType"]           = ["statOnly", "allSys"]
inputDict["dimension"]          = "1D"
inputDict["doData"]             = [False]
inputDict["fileName"]           = ["workspaces/ws_pt_wsys_expected_mtx_v23v0_ShapeOnly_reparam.root"]
inputDict["outputDir"]          = ["ws_pt_wsys_expected_mtx_v23v0_ShapeOnly_reparam"]

inputDict["fileName"]           += ["workspaces/ws_pt_wsys_expected_mtx_v23v0_XSOnly_reparam.root"]
inputDict["outputDir"]          += ["ws_pt_wsys_expected_mtx_v23v0_XSOnly_reparam"]

inputDict["fileName"]           += ["workspaces/ws_pt_wsys_expected_mtx_v23v0_pt4lShape_ShapeOnly_reparam.root"]
inputDict["outputDir"]          += ["ws_pt_wsys_expected_mtx_v23v0_pt4lShape_ShapeOnly_reparam"]

inputDict["fileName"]           += ["workspaces/ws_pt_wsys_expected_mtx_v23v0_pt4lXS_XSOnly_reparam.root"]
inputDict["outputDir"]          += ["ws_pt_wsys_expected_mtx_v23v0_pt4lXS_XSOnly_reparam"]

inputDict["nSplitJob"]          = 1
# buildList(inputDict, scan1D)


#########
# Shape and XS
#########
inputDict["poiList"] = [
"kappaC:50:-5:5"
]
inputDict["config"]             = "../source/data/H4lLightYukawa/H4lLightYukawafixkBConfig.txt"
inputDict["scanType"]           = ["statOnly", "allSys"]
inputDict["dimension"]          = "1D"
inputDict["doData"]             = [False]
inputDict["fileName"]           = ["workspaces/ws_pt_wsys_expected_mtx_v23v0_XSandBR_reparam.root"]
inputDict["outputDir"]          = ["ws_pt_wsys_expected_mtx_v23v0_XSandBR_reparam"]

inputDict["fileName"]           += ["workspaces/ws_pt_wsys_expected_mtx_v23v0_pt4lXS_XSandBR_reparam.root"]
inputDict["outputDir"]          += ["ws_pt_wsys_expected_mtx_v23v0_pt4lXS_XSandBR_reparam"]

inputDict["nSplitJob"]          = 1
# buildList(inputDict, scan1D)

#=============================================================================
#                                   Ranking                                  =                          
#=============================================================================
ranking = []
inputDict = {}
inputDict["poiList"]        = ["(.*)"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "ranking"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_pt_expected_mtx_v20v2_inclSM_XSOnly_wSys.root"]
inputDict["outputDir"]      = ["ws_pt_expected_mtx_v20v2_inclSM_XSOnly_wSys_ranking"]
inputDict["nSplitJob"]      = 1
# buildList(inputDict, ranking)



#=============================================================================
#                                   Correlations                                 =                          
#=============================================================================
correlationHist = []
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "correlationHist"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = True   
inputDict["fileName"]       = ["workspaces/ws_pt_wsys_data_mtx_lightFlavour_shapeOnly.root"]
inputDict["outputDir"]      = ["ws_pt_wsys_data_mtx_lightFlavour_shapeOnly_corr"]
# buildList(inputDict, correlationHist)


#=============================================================================
#                                   Run Toys                                 =                          
#=============================================================================
runToys = []
# inputDict["poiList"]        = ["nToys:100"]
inputDict["poiList"]        = ["nToys:2000:0"]
inputDict["config"]         = "../source/data/H4lLightYukawa/H4lLightYukawaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
# inputDict["fileName"]       = ["workspaces/ws_pt_expected_mtx_v22t2v1_XSOnly_wSys.root"]
# inputDict["outputDir"]      = ["ws_pt_expected_mtx_v22t2v1_XSOnly_wSys_toys"]
inputDict["fileName"]       = ["workspaces/ws_pt_expected_mtx_v22t2v1_XSandBR_wSys.root"]
inputDict["outputDir"]      = ["ws_pt_expected_mtx_v22t2v1_XSandBR_wSys_toys"]
inputDict["nSplitJob"]      = 4
buildList(inputDict, runToys)





