from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#SO
#inputDict["poiList"] = [ "cHWtil:200:-2.5:2.5"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHWtilConfig.txt"

#inputDict["poiList"] = [ "cHWBtil:200:-20:20."]
#inputDict["poiList"] = [ "cHWBtil:200:-1.5:1.5"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHWBtilConfig.txt"

#inputDict["poiList"] = [ "cHBtil:200:-30:30."]
#inputDict["poiList"] = [ "cHBtil:200:-1.25:1.25"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHBtilConfig.txt"

#inputDict["poiList"] = [ "cHW:200:-2.:2."]
inputDict["poiList"] = [ "cHW:100:-3.5:-2."]
inputDict["config"]  = "../source/data/HWW-EFT/new_cHWConfig.txt"

#inputDict["poiList"] = [ "cHWB:200:-12:12."]
#inputDict["poiList"] = [ "cHWB:100:-1.4:1.4"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHWBConfig.txt"

#inputDict["poiList"] = [ "cHB:200:-10:10."]
#inputDict["poiList"] = [ "cHB:100:-0.8:0.8"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHBConfig.txt"

#inputDict["poiList"] = [ "cHq3:100:-1.0:1.0"]
#inputDict["poiList"] = [ "cHq3:100:-0.55:0.55"]
#inputDict["poiList"] = [ "cHq3:100:-1.0:10."]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHq3Config.txt"

#inputDict["poiList"] = [ "cHq1:200:-15.0:15.0"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHq1Config.txt"

#inputDict["poiList"] = [ "cHu:200:-10.0:10.0"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHuConfig.txt"

#inputDict["poiList"] = [ "cHd:200:-25.0:25.0"]
#inputDict["poiList"] = [ "cHd:200:-4.0:4.0"]
#inputDict["config"]  = "../source/data/HWW-EFT/new_cHdConfig.txt"


#inputDict["scanType"]       = ["statOnly_muggf","stat_EFTsys"]
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]


#####cHW######
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_SignedDPhijj_parametrized_full_til.root"]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_SignedDPhijj_parametrized_full.root"]

#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_SignedDPhijj_parametrized_linearOnly_til.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_SignedDPhijj_parametrized_linearOnly.root"]

####cHq3#####
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_jet0_p_parametrized_linearOnly.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_jet0_p_parametrized_full.root"]

#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_lep0_p_parametrized_linearOnly.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_lep0_p_parametrized_full.root"]

###################################
inputDict["outputDir"]      = ["EFT_Nov15_extended_cHW"]
buildList(inputDict, scan1D)

#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/MyWS/ws_noGGF2_SignedDPhijj2_newAsimov_even_parametrized_full.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/otherObs/june23_ws/ws_noGGF2_SignedDPhijj_newAsimov_parametrized_linearOnly_addWH.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/otherObs/june23_ws/ws_noGGF2_jet0_pt_newAsimov_parametrized_fullup.root"]



