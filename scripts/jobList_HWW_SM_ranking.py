from buildList import *

#=============================================================================
#                                   Ranking                                  =                          
#=============================================================================
ranking = []
inputDict = {}
inputDict["poiList"] = ["*"]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muFloat"]
inputDict["dimension"]      = "ranking"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/ws_Mjj_newAsimov.root"]
inputDict["outputDir"]      = ["SM_mjj_ranking"]
buildList(inputDict, ranking)


#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/ws_DPhijj_newAsimov.root"]
#inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/ws_DYll_newAsimov.root"]




