from buildList import *



#=============================================================================
#                                   2D Scans                                 =                          
#=============================================================================
scan2D = []
inputDict = {}

inputDict["poiList"]        = ["CV:100:0.7:1.3,CF:150:0.45:3.75"]
inputDict["config"]         = "../source/data/H4lKappa/H4lKappa.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_S0_v22t2_V2_model_kappa.root", "workspaces/WS_combined_XS_STXS_S0_v22t2_V2_model_kappa.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_S0_v22t2_V2_model_kappa", "WS_combined_XS_STXS_S0_v22t2_V2_model_kappa"]
inputDict["nSplitJob"]      = 25
buildList(inputDict, scan2D)


#=============================================================================
#                                   Toy scanning                             =                          
#=============================================================================

inputDict["poiList"] 		= ["CV:100:0.7:1.3,CF:150:0.45:3.75,nToys:500"]
inputDict["config"]         = "../source/data/H4lKappa/H4lKappa.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_S0_v22t2_V2_model_kappa.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_S0_v22t2_V2_model_kappa_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/WS_combined_mu_STXS_S0_v22t2_V2_model_kappa/InputsForAdaptiveSampling/CV_CF.root", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)


#=============================================================================
#                                   Run Toys                                 =                          
#=============================================================================
runToys = []
inputDict["poiList"]        = ["nToys:10000"]
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_S0_v22t2_V2_model_kappa.root", "workspaces/WS_combined_XS_STXS_S0_v22t2_V2_model_kappa.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_S0_v22t2_V2_model_kappa_toy", "WS_combined_XS_STXS_S0_v22t2_V2_model_kappa_toys"]
inputDict["nSplitJob"]      = 50
buildList(inputDict, runToys)
