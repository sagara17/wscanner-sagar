from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHq1:60:-6.0:6.0"]
inputDict["config"]         = "../source/data/HWW-EFT/noEFTConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar_withSys/ws_jet0_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["jet0_pt_cHq1"]
buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHq1:40:-4.0:4.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHq1Config.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_jet1_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["jet1_pt_cHq1"]
#buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHq1:50:-4.0:4.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHq1Config.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_lep0_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["lep0_pt_cHq1"]
#buildList(inputDict, scan1D)

#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ "cHq1:50:-4.0:4.0"]
inputDict["config"]         = "../source/data/HWW-EFT/cHq1Config.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/allVar-newBin/ws_lep1_pt_morphed_newAsimov.root"]
inputDict["outputDir"]      = ["lep1_pt_cHq1"]
#buildList(inputDict, scan1D)


