# !/usr/bin/env python
import os
import math
import ROOT 
import jobListExample, jobListCouplings, jobListBtagging, jobListDiff, jobListPO, jobListLightYukawa, jobListMass, jobListKappa, jobListEFT, jobListHComb, jobListDCBParams
import jobListHCombVHbbValidation
import shlex
import subprocess
import time
import re
import tarfile
import datetime
import shutil
import os.path
import random
#import numpy as np
from collections import OrderedDict

# you will have to give your run a unique outputDir since info about dataSet, scanType, etc 

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--splitJob",       type = int, default = 1,                help = "Split the job into multiple subjobs")
parser.add_argument("--queue",          type = str, default = "longlunch",      help = "Queue to submit to.")
parser.add_argument("--dryRun",         default = False, action='store_true',   help = "Does not submit to anything") 
parser.add_argument("--runLocal",       default = False, action='store_true',   help = "Run in parallel") 
parser.add_argument("--submitCondor",   default = False, action='store_true',   help = "Submit jobs to the batch") 
parser.add_argument("--submitSlurm",    default = False, action='store_true',   help = "Submit jobs to the batch") 
parser.add_argument("--maxProcs",       type=int,   default=4,                  help="Number of parallel processed")
parser.add_argument("--scanDir",        type = str,   default="root-files",       help="Default will be root-files")
parser.add_argument("--rankPOIs",       default=False,       help="Will ranking the impact POIs have on each other if on")
parser.add_argument("--isCedarContainer",     default = False, action='store_true',   help = "submission for cedar")
parser.add_argument("--isLxplusSingularity",  default = False, action='store_true',   help = "submission for cedar")
parser.add_argument("--isCedarSingularity",  default = False, action='store_true',   help = "submission for cedar")

parser.add_argument("--runGrid",        default = False, action='store_true',   help = "If true, will submit grid jobs") 
parser.add_argument("--uniqueID",       type = str, default = "test_ID",        help = "Unique ID for the grid")

parser.add_argument("--retry",          default = False, action='store_true',   help = "If true, only retry") 

args = parser.parse_args()


scansToRun = [
    # # Example of jobList structure
    # # ------------------------
    # jobListExample.scan1D, 
    # jobListExample.scan2D,
    # jobListExample.ranking,
    # jobListExample.correlationHist,
    # jobListExample.savePulls,
    # jobListExample.scan1D_POs

    # # H4l STXS Couplings
    # # ------------------------
    # jobListCouplings.scan1D, 
    # jobListCouplings.scan2D,
    # jobListCouplings.correlationHist,
    # jobListCouplings.ranking,   
    # jobListCouplings.compatibility,   
    # jobListCouplings.scan1D_POs

    # # H4l Diff
    # # ------------------------
    # jobListDiff.scan1D, 
    # jobListDiff.scan2D, 
    # jobListDiff.ranking,
    # jobListDiff.correlationHist,
    # jobListDiff.compatibility,

    # # H4l diff Interpretations
    # # ------------------------
    # jobListPO.scan1D, 
    # jobListPO.scan2D, 
    # jobListPO.runToys, 
    # jobListPO.correlationHist, 
    # jobListLightYukawa.scan1D, 
    # jobListLightYukawa.scan2D, 
    # jobListLightYukawa.runToys,
    # jobListLightYukawa.ranking, 
    # jobListLightYukawa.correlationHist, 

    # # H4l STXS Interpretations
    # # ------------------------
    # jobListKappa.scan2D, 
    # jobListKappa.runToys,
    # jobListEFT.scan2D,
    # # ttbar Btagging
    # # ------------------------
    # jobListBtagging.scan1D, 
    # jobListBtagging.ranking,
    # jobListBtagging.correlationHist,

    # # mass
    # # ------------------------
    # jobListMass.scan1D, 
    # jobListMass.scan1DCB, 
    # jobListMass.scanDatasets,
    # jobListMass.correlationHist, 
    # jobListMass.unCondWS, 

    ## combination work
    # jobListHCombVHbbValidation.scan1D
    # jobListHCombVHbbValidation.savePulls

    # jobListHComb.savePulls,
    # jobListHComb.scan1D,
    # jobListHComb.ranking,
    # jobListHComb.minosError,
    # jobListHComb.correlationHist,
    # jobListHComb.compatibility,

    jobListDCBParams.minosError,


]  

# for rounding up the to  divisor
def round_up(num, divisor):
    if(divisor == 1): 
        return num
    return num - (num%divisor)

def getPOIRange(poiInfo):
    poiStr = poiInfo.split(":")
    poiName = poiStr[0]
    poiSteps = int(poiStr[1])
    poiLwrLim = float(poiStr[2])
    poiUprLim = float(poiStr[3])
    return poiName,poiSteps,poiLwrLim,poiUprLim

def getCommand(job, poiName, i, subJobOutDir, globalCounter = None):
    outFileName = poiName + '_sJob' + str(i) + '.root'
    if("rank" in job["dimension"]):
        outFileName = "rankJob_"+ str(globalCounter)+ '_sJob' + str(i) + '.root'

    text = "../build/runScan"
    if(args.runGrid): text = "build/runScan"

    configPath = job["config"]
    if(args.runGrid):
        # remove the ../ from config type
        if (configPath.startswith('../')):
            configPath = configPath[3:]
            # print configPath

    text += " --config %s" %(configPath)
    text += " --scanType %s" %(job["scanType"])
    text += " --dimension %s" %(job["dimension"])
    text += " --fileName %s" %(job["fileName"])
    text += " --poiScan %s" %(job["poiInfo"])
    text += " --outputDir %s/%s" %(args.scanDir,subJobOutDir)
    text += " --outFileName %s" %(outFileName)

    outputName = "%s/%s/%s" %(args.scanDir,subJobOutDir, outFileName)

    if (job["doData"] == True):
        text += ' --doData '
    if (job["saveCorrHist"] == True):
        text += ' --saveCorrHist '

    text += ' --debug '

    if (len(job["extraVarSave"]) > 0):
        text += ' --extraVarSave \'%s\'' %(",".join(job["extraVarSave"]))

    if (job["saveUncondWorkspace"] == True):
        text += ' --saveUncondWorkspace '
        outputName = "unCondWS/%s" %(job["fileName"])

    if (job["dimension"] == "runToys" or job["dimension"] == "2DToys" or job["dimension"] == "1DToys"):
        text += ' --seed %s'  %(str(random.randint(1,100000000000)))

    return text, outputName

def makeCondorExecutable(basePath):
    executableName = basePath + "/condorExe.sh"

    fileObj = open(executableName, 'w')
    fileObj.write("#!/bin/bash\n\n")
    # cd into the folder
    fileObj.write("cd %s\n" %os.getcwd())

    #setup root
    fileObj.write("source setup.sh\n")

    # run the command which is a input argument
    fileObj.write("source $1\n")

    fileObj.close()


    os.system("chmod u+x " + executableName)

    return executableName 

def makeCondorExecutableSingularity(basePath):
    executableName = basePath + "/condorExe.sh"

    fileObj = open(executableName, 'w')
    fileObj.write("#!/bin/bash\n\n")
    # cd into the folder
    fileObj.write("cd %s\n" %os.getcwd())

    # run the command which is a input argument
    # fileObj.write("source $1\n")
    fileObj.write("singularity exec -e -B /afs/cern.ch/work/b/bciungu/public/HComb/wscanner_hcomb/ ~/hcombroot.sif /bin/bash -c \"source $1\"\n")
    fileObj.close()


    os.system("chmod u+x " + executableName)

    return executableName 

def getConfigInfo(configFile):
    workspaceName = ""
    modelConfigName = ""
    with open (configFile, 'r') as infile:
        for line in infile:
            if("workspaceName:" in line):
                workspaceName = line.replace("workspaceName:","")
                workspaceName = workspaceName.replace(" ","")
                workspaceName = workspaceName.replace("\t","")
                workspaceName = workspaceName.replace("\n","")
            if("modelConfigName:" in line):
                modelConfigName = line.replace("modelConfigName:","")
                modelConfigName = modelConfigName.replace(" ","")
                modelConfigName = modelConfigName.replace("\t","")
                modelConfigName = modelConfigName.replace("\n","")
    infile.close()

    if(len(workspaceName) == 0 or len(modelConfigName) == 0):
        print("Error, please check your configFile and make sure it has a workspaceName and a modelConfigName")
        exit(1)

    return workspaceName, modelConfigName 
        
def splitNP(numNP, spliting):
    if(spliting > numNP):
        return 1
    numPerJob = math.floor(float(numNP)/spliting)
    return int(numPerJob)

def isFileOk(fileName):
    if(not os.path.isfile(fileName)): return False
    if(os.stat(fileName).st_size < 500): return False
    return True

def makeMaps(job, condorJobMap, jobsToRun, globalCounter):
    def _addSingularitySetup(fileObj):
        fileObj.write("ulimit -S -s unlimited\n")
        fileObj.write("cd /afs/cern.ch/work/b/bciungu/public/HComb/wscanner_hcomb/run\n")
        fileObj.write("ls\n")
        return

    def _addCedarSingularitySetup(fileObj):
        fileObj.write("ulimit -S -s unlimited\n")
        fileObj.write("cd /wscanner_hcomb/run\n")
        fileObj.write("ls\n")
        return

    nSplitJob = args.splitJob
    if("nSplitJob" in job): nSplitJob = job["nSplitJob"]

    if(os.path.isfile(job["fileName"])):
        jobListBaseDir, condorBaseDir, subJobOutDir, subJobOutDirBaseDir = getJobDir(job)
        if("1" in job["dimension"]):
            toyOption = job["poiInfo"].split(",")
            poiName, poiSteps, poiLwrLim, poiUprLim = getPOIRange(toyOption[0])
            subJobOutDir += poiName

            # for batch style running, split into each step
            if("batchStyle" in job):
                if(job["batchStyle"]["doBatchStyle"]):
                    nSplitJob = poiSteps

            os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

            # round up poiSteps to the nearest multiple of the number of subjob to split
            poiSteps = round_up(poiSteps, nSplitJob);
            deltaPoiStep = (poiUprLim - poiLwrLim) / poiSteps;
            deltaStep = float(poiSteps)/nSplitJob

            internalCommandList = []

            nRepeat = 1
            # if we ask the job to repeat itself multiple times
            if ("nRepeat" in job):
                nRepeat = job["nRepeat"]

            counter = 0
            # split it in sub jobs
            for i in range(0, nSplitJob):
                for iRepeat in range(0, nRepeat):
                    currLwrLim = poiLwrLim + i * deltaStep * deltaPoiStep
                    currUprLim = poiLwrLim + (i+1) * deltaStep * deltaPoiStep

                    currPoiStr = str(int(deltaStep)) + ':' + str(currLwrLim) + ':' +  str(currUprLim)

                    poiScanInfo = poiName+ ':' + currPoiStr

                    job["poiInfo"] = poiScanInfo

                    if(len(toyOption) > 1): 
                        job["poiInfo"] += "," + toyOption[1]

                    command, outName  = getCommand(job, poiName, counter, subJobOutDir)
                    if(args.retry and isFileOk(outName)): continue

                    internalCommandList.append(command)
                    counter += 1

            # repackage how many jobs are run in each list
            if("batchStyle" in job and job["batchStyle"]["doBatchStyle"]):
                nJobsBatch = 1
                if(job["batchStyle"]["doBatchStyle"]): nJobsBatch = job["batchStyle"]["nJobs"]

                print("nJobsBatch: ", nJobsBatch)

                totalJobComplexity = 0
                for job in internalCommandList:
                    complexity = getJobComplexity(job)
                    totalJobComplexity += complexity

                print("totalJobComplexity: ", totalJobComplexity)

                # Find the number of jobs to submit
                averageComplexity = math.ceil(totalJobComplexity/float(nJobsBatch))
                
                import random

                # reshape the jobs accordingly
                internalCommandList = np.array(internalCommandList)

                # shuffle randomly to make sure that each jobs gets a mix of points
                random.shuffle(internalCommandList)

                print("averageComplexity: ", averageComplexity)

                bunchedCommandlist = []
                currList = []
                currComplexity = 0
                for index in range(0, len(internalCommandList)):
                    job = internalCommandList[index]
                    currComplexity += getJobComplexity(job)

                    if(currComplexity <= averageComplexity):
                        currList.append(job)
                    else:
                        bunchedCommandlist.append(currList)
                        currList  = [job]
                        currComplexity = getJobComplexity(job)

                bunchedCommandlist.append(currList)
            else:
                bunchedCommandlist = internalCommandList

            # print("length", len(internalCommandList), len(bunchedCommandlist))


            # make the scripts
            counter = 0
            for commandList in bunchedCommandlist:
                runScriptName = "job" + str(getNextIndex(jobListBaseDir)) + "_"  + poiName + "_sJ"+str(int(counter))
                fileName = runScriptName + ".sh"
                fileName = jobListBaseDir + "/" + fileName
                print(fileName)

                fileObj  = open(fileName, 'w')
                fileObj.write("#!/bin/bash\n")

                if(args.isCedarContainer and not args.dryRun):
                    fileObj.write("source ../source/setup_lxplus.sh\n")

                if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
                if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)
                
                if type(commandList) is not list:
                    commandList = [commandList]

                for command in commandList:
                    fileObj.write(command + "\n")

                fileObj.close()


                # Create the output baseFilepath
                condorOutFileName = runScriptName
                condorOutFileName = condorBaseDir + "/" + condorOutFileName

                # push it into the map
                condorJobDef = {}
                condorJobDef["inputArgument"]   = fileName
                condorJobDef["outFile"]         = condorOutFileName

                condorJobMap.append(condorJobDef)

                # make everything an executable
                os.system("chmod u+x " + fileName)

                jobsToRun.append("%s" %(fileName) )
                counter += 1

        elif("2" in job["dimension"]):
            twoPois = job["poiInfo"].split(",")
            poiName2, poiSteps2, poiLwrLim2, poiUprLim2    = getPOIRange(twoPois[1])
            poiName,  poiSteps,  poiLwrLim,  poiUprLim     = getPOIRange(twoPois[0])
            subJobOutDir += poiName + "_" + poiName2
            os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

            nSplitJob1 = nSplitJob
            nSplitJob2 = nSplitJob

            # for batch style running, split into each step
            if("batchStyle" in job):
                if(job["batchStyle"]["doBatchStyle"]):
                    nSplitJob1 = poiSteps
                    nSplitJob2 = poiSteps2

            # round up poiSteps to the nearest multiple of the number of subjob to split
            poiSteps = round_up(poiSteps, nSplitJob1);
            deltaPoiStep = (poiUprLim - poiLwrLim) / poiSteps;
            deltaStep = float(poiSteps)/nSplitJob1

            poiSteps2 = round_up(poiSteps2, nSplitJob2);
            deltaPoiStep2 = (poiUprLim2 - poiLwrLim2) / poiSteps2;
            deltaStep2 = float(poiSteps2)/nSplitJob2

            internalCommandList = []

            nRepeat = 1
            # if we ask the job to repeat itself multiple times
            if ("nRepeat" in job):
                nRepeat = job["nRepeat"]

            counter = 0
            # split it in sub jobs
            for i in range(0, nSplitJob1):
                for j in range(0, nSplitJob2):
                    for iRepeat in range(0, nRepeat):
                        currLwrLim = poiLwrLim + i * deltaStep * deltaPoiStep
                        currUprLim = poiLwrLim + (i+1) * deltaStep * deltaPoiStep

                        currLwrLim2 = poiLwrLim2 + j * deltaStep2 * deltaPoiStep2
                        currUprLim2 = poiLwrLim2 + (j+1) * deltaStep2 * deltaPoiStep2

                        currPoiStr = str(int(deltaStep)) + ':' + str(currLwrLim) + ':' +  str(currUprLim)
                        currPoiStr2 = str(int(deltaStep2)) + ':' + str(currLwrLim2) + ':' +  str(currUprLim2)

                        poiScanInfo = poiName+ ':' + currPoiStr + "," + poiName2 + ":" + currPoiStr2

                        job["poiInfo"] = poiScanInfo

                        if(len(twoPois) > 2): 
                            job["poiInfo"] += "," + twoPois[2]

                        updateAdaptiveSample(job)

                        command, outName  = getCommand(job, poiName + "_" + poiName2, counter, subJobOutDir)
                        if(args.retry and (isFileOk(outName))): continue

                        internalCommandList.append(command)
                        counter += 1

            # repackage how many jobs are run in each list
            if("batchStyle" in job):
                nJobsBatch = 1
                if(job["batchStyle"]["doBatchStyle"]): nJobsBatch = job["batchStyle"]["nJobs"]

                totalJobComplexity = 0
                for job in internalCommandList:
                    complexity = getJobComplexity(job)
                    totalJobComplexity += complexity

                print("totalJobComplexity: ", totalJobComplexity)

                # Find the number of jobs to submit
                averageComplexity = math.ceil(totalJobComplexity/float(nJobsBatch))

                import random
                print("totalJobComplexity: ", averageComplexity)
                # reshape the jobs accordingly
                internalCommandList = internalCommandList

                # shuffle randomly to make sure that each jobs gets a mix of points
                random.shuffle(internalCommandList)


                bunchedCommandlist = []
                currList = []
                currComplexity = 0
                for index in range(0, len(internalCommandList)):
                    job = internalCommandList[index]
                    currComplexity += getJobComplexity(job)
                    #print job, getJobComplexity(job), currComplexity

                    if(currComplexity <= averageComplexity):
                        currList.append(job)
                    else:
                        bunchedCommandlist.append(currList)
                        currList  = [job]
                        currComplexity = getJobComplexity(job)
                #print len(bunchedCommandlist)
                #exit(1)
            else:
                bunchedCommandlist = internalCommandList


            # make the scripts
            counter = 0
            for commandList in bunchedCommandlist:
                runScriptName = "job" + str(getNextIndex(jobListBaseDir)) + "_"  + poiName  + poiName2 + "_sJ"+str(int(counter))
                fileName = runScriptName + ".sh"
                fileName = jobListBaseDir + "/" + fileName
                print(fileName)

                fileObj  = open(fileName, 'w')
                fileObj.write("#!/bin/bash\n")

                if(args.isCedarContainer):
                    fileObj.write("source ../source/setup_lxplus.sh\n")
                if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
                if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

                if type(commandList) is not list:
                    commandList = [commandList]

                for command in commandList:
                    fileObj.write(command + "\n")

                fileObj.close()


                # Create the output baseFilepath
                condorOutFileName = runScriptName
                condorOutFileName = condorBaseDir + "/" + condorOutFileName

                # push it into the map
                condorJobDef = {}
                condorJobDef["inputArgument"]   = fileName
                condorJobDef["outFile"]         = condorOutFileName

                condorJobMap.append(condorJobDef)

                # make everything an executable
                os.system("chmod u+x " + fileName)

                jobsToRun.append("%s" %(fileName) )
                counter += 1

        elif("runToys" in job["dimension"] or "scanDatasets" in job["dimension"]):
            toyFile = ""
            jobKey = job['poiInfo'].split(":")[0]
            nToys = job['poiInfo'].split(":")[1]
            if(not RepresentsNum(nToys)):
                toyFile = job['poiInfo'].split(":")[1]
                nToys = job['poiInfo'].split(":")[2]
            toyPerJob = float(nToys)/nSplitJob
            
            # split it in sub jobs
            for i in range(0, nSplitJob):           
                os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

                if(len(toyFile) == 0):
                    job["poiInfo"] = jobKey + ":" + str(int(toyPerJob))
                else:
                    job["poiInfo"] = jobKey + ":" + toyFile + ":" + str(int(toyPerJob)) + ":" + str(int(i*toyPerJob))

                command, outName  = getCommand(job, "toys", i, subJobOutDir)

                if(args.retry and isFileOk(outName)): continue

                runScriptName = "job" + str(getNextIndex(jobListBaseDir)) + "_" + jobKey + "_sJ"+str(int(i))
                fileName = runScriptName + ".sh"
                fileName = jobListBaseDir + "/" + fileName
                print(fileName)

                fileObj  = open(fileName, 'w')
                fileObj.write("#!/bin/bash\n")

                if(args.isCedarContainer and not args.dryRun):
                    fileObj.write("source ../source/setup_lxplus.sh\n")
                if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
                if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

                fileObj.write(command)
                fileObj.close()

                # Create the output baseFilepath
                condorOutFileName = runScriptName
                condorOutFileName = condorBaseDir + "/" + condorOutFileName

                # push it into the map
                condorJobDef = {}
                condorJobDef["inputArgument"]   = fileName
                condorJobDef["outFile"]         = condorOutFileName

                condorJobMap.append(condorJobDef)

                # make everything an executable
                os.system("chmod u+x " + fileName)

                jobsToRun.append("%s" %(fileName) )
            
        elif("rank" in job["dimension"]):
            workspaceName, modelConfigName = getConfigInfo(job["config"])
            wsfile = ROOT.TFile(job["fileName"])
            ws = wsfile.Get(workspaceName)
            mc = ws.obj(modelConfigName)        
            nitr = mc.GetNuisanceParameters().createIterator()
            var = nitr.Next()

            NPNames = []

            while var:
                NPNames.append(var.GetName())
                var = nitr.Next()

            # Append the POI to this list as well if --rankPOIs is added
            if(args.rankPOIs):
                nitr = mc.GetParametersOfInterest().createIterator()
                var = nitr.Next()
                while var:
                    print(var)
                    NPNames.append(var.GetName())
                    var = nitr.Next()

            print("NP or POIs to rank:", NPNames)

            regexList = []
            if(job["poiInfo"] == "*"):
                regexList = NPNames

            else:
                for NP in NPNames:
                    isMatch = re.search(job["poiInfo"], NP)
                    if(isMatch):
                        regexList.append(NP)
                        # print NP

            # print regexList

            numPerJob = splitNP(len(regexList), nSplitJob)
            remaining = len(regexList)
            exitFlag = False
            usedNPName = []
            poiScanList = []

            for i in range(0, len(regexList)):
                poiScanInfo = ""
                tmppoiScanList = []
                startIndex = i * numPerJob
                for j in range(startIndex, startIndex + int(numPerJob)):
                    # poiScanInfo += NPNames[j]
                    usedNPName.append(regexList[j])
                    tmppoiScanList.append(regexList[j])
                poiScanInfo = ",".join(tmppoiScanList)

                poiScanList.append(poiScanInfo)

                remaining -= numPerJob
                if(remaining < numPerJob):
                    exitFlag = True     
                if(exitFlag):
                    break

            if(usedNPName != regexList):
                notNPName = list(set(regexList).difference(set(usedNPName)))
                poiScanList.append(",".join(notNPName))
            
            for i in range(0, len(poiScanList)):
                job["poiInfo"] = poiScanList[i]

                command, outName  = getCommand(job, "rank", i, subJobOutDir, globalCounter)
                if(args.retry and (isFileOk(outName))): continue

                runScriptName = "job" + str(getNextIndex(jobListBaseDir)) + "_sJ"+str(int(globalCounter))
                fileName = runScriptName + ".sh"
                fileName = jobListBaseDir + "/" + fileName
                print(fileName)
                
                fileObj  = open(fileName, 'w')
                fileObj.write("#!/bin/bash\n")
                if(args.isCedarContainer and not args.dryRun):
                    fileObj.write("source ../source/setup_lxplus.sh\n")
                if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
                if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

                fileObj.write(command)
                fileObj.close()

                # Create the output baseFilepath
                condorOutFileName = runScriptName
                condorOutFileName = condorBaseDir + "/" + condorOutFileName

                # push it into the map
                condorJobDef = {}
                condorJobDef["inputArgument"]   = fileName
                condorJobDef["outFile"]         = condorOutFileName

                condorJobMap.append(condorJobDef)

                # make everything an executable
                os.system("chmod u+x " + fileName)

                jobsToRun.append("%s" %(fileName) )

        elif("compatibility" in job["dimension"]):  
            if("compatibility" in job["dimension"]): subJobOutDir += "/compatibility" 
            os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

            command, outName  = getCommand(job, job['scanType'], 0, subJobOutDir)
            if(args.retry and (isFileOk(outName))): return

            runScriptName = "job" +str(getNextIndex(jobListBaseDir)) + "_" + job['scanType'] + "_sJ0"
            fileName = runScriptName + ".sh"
            fileName = jobListBaseDir + "/" + fileName
            print(fileName)

            fileObj  = open(fileName, 'w')
            fileObj.write("#!/bin/bash\n")
            if(args.isCedarContainer and not args.dryRun):
                fileObj.write("source ../source/setup_lxplus.sh\n")
            if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
            if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

            fileObj.write(command)
            fileObj.close()

            # Create the output baseFilepath
            condorOutFileName = runScriptName
            condorOutFileName = condorBaseDir + "/" + condorOutFileName

            # push it into the map
            condorJobDef = {}
            condorJobDef["inputArgument"]   = fileName
            condorJobDef["outFile"]         = condorOutFileName

            condorJobMap.append(condorJobDef)

            # make everything an executable
            os.system("chmod u+x " + fileName)

            jobsToRun.append("%s" %(fileName) )

        elif("minosError" in job["dimension"]):  
            poiName = job["poiInfo"].replace(",","").replace("'","")

            subJobOutDir += "/minosError/" + poiName 
            os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

            command, outName  = getCommand(job, poiName, 0, subJobOutDir)
            if(args.retry and (isFileOk(outName))): return

            runScriptName = "job" +str(getNextIndex(jobListBaseDir)) + "_" + job['scanType'] + "_sJ0"
            fileName = runScriptName + ".sh"
            fileName = jobListBaseDir + "/" + fileName
            print(fileName)

            fileObj  = open(fileName, 'w')
            fileObj.write("#!/bin/bash\n")
            if(args.isCedarContainer and not args.dryRun):
                fileObj.write("source ../source/setup_lxplus.sh\n")
            if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
            if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

            fileObj.write(command)
            fileObj.close()

            # Create the output baseFilepath
            condorOutFileName = runScriptName
            condorOutFileName = condorBaseDir + "/" + condorOutFileName

            # push it into the map
            condorJobDef = {}
            condorJobDef["inputArgument"]   = fileName
            condorJobDef["outFile"]         = condorOutFileName

            condorJobMap.append(condorJobDef)

            # make everything an executable
            os.system("chmod u+x " + fileName)

            jobsToRun.append("%s" %(fileName) )

        elif("correlationHist" in job["dimension"] or "saveUncondws" in job["dimension"] or "savePulls" in job["dimension"]):
            if("correlationHist" in job["dimension"]):  subJobOutDir += "/correlationHist"
            if("saveUncondws" in job["dimension"]):     subJobOutDir += "/condWS" 
            if("savePulls" in job["dimension"]):     subJobOutDir += "" 
            # get the first POI in the workspace. For this, the POI doesn't really matter
            workspaceName, modelConfigName = getConfigInfo(job["config"])
            wsfile = ROOT.TFile(job["fileName"])
            ws = wsfile.Get(workspaceName)
            mc = ws.obj(modelConfigName)        
            poi = mc.GetParametersOfInterest().first()

            poiName = poi.GetName()
            wsfile.Close()
            poiSteps = 0
            poiLwrLim = 0
            poiUprLim = 0.1    
            os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

            currPoiStr = str(int(poiSteps)) + ':' + str(poiLwrLim) + ':' +  str(poiUprLim)

            poiScanInfo = poiName+ ':' + currPoiStr
            job["poiInfo"] = poiScanInfo

            command, outName = getCommand(job, poiName, 0, subJobOutDir)
            if(args.retry and (isFileOk(outName))): return

            runScriptName = "job" +str(getNextIndex(jobListBaseDir)) + "_" + job['scanType'] + "_sJ0"
            fileName = runScriptName + ".sh"
            fileName = jobListBaseDir + "/" + fileName
            print(fileName)

            fileObj  = open(fileName, 'w')
            fileObj.write("#!/bin/bash\n")
            if(args.isCedarContainer and not args.dryRun):
                fileObj.write("source ../source/setup_lxplus.sh\n")
            if(args.isLxplusSingularity): _addSingularitySetup(fileObj)
            if(args.isCedarSingularity): _addCedarSingularitySetup(fileObj)

            fileObj.write(command)
            fileObj.write("\n")
            fileObj.close()

            # Create the output baseFilepath
            condorOutFileName = runScriptName
            condorOutFileName = condorBaseDir + "/" + condorOutFileName

            # push it into the map
            condorJobDef = {}
            condorJobDef["inputArgument"]   = fileName
            condorJobDef["outFile"]         = condorOutFileName

            condorJobMap.append(condorJobDef)

            # make everything an executable
            os.system("chmod u+x " + fileName)

            jobsToRun.append("%s" %(fileName) )
    else:
        pass
    

# update based on adaptive job sample    
def updateAdaptiveSample(job):
    if("adaptiveSampling" in job):
        if(not job["adaptiveSampling"]["doAdaptiveSampling"]): return 
    else:
        return

    if(job["dimension"] == "2DToys"):
        # get the histogram
        apdativeFile = TFile.Open(job["adaptiveSampling"]["inputFile"])
        apdativeHist = apdativeFile.Get(job["adaptiveSampling"]["histName"])

        twoPois = job["poiInfo"].split(",")
        poiName1, poiSteps1, poiLwrLim1, poiUprLim1    = getPOIRange(twoPois[0])
        poiName2, poiSteps2, poiLwrLim2, poiUprLim2    = getPOIRange(twoPois[1])
        toyInfo = twoPois[2].split(":")

        exclusionVal = []

        import itertools
        for val1 in [poiLwrLim1, poiUprLim1]:
            for val2 in [poiLwrLim2, poiUprLim2]:

                xBin = apdativeHist.GetXaxis().FindBin(val1)
                yBin = apdativeHist.GetYaxis().FindBin(val2)

                if(xBin > apdativeHist.GetXaxis().GetNbins() or xBin < 1):
                    exclusionVal.append(0.75)
                    continue
                if(yBin > apdativeHist.GetYaxis().GetNbins()):
                    exclusionVal.append(0.75)
                    continue

                #print xBin, yBin, val1, val2, apdativeHist.Interpolate (val1, val2)
                exclusionVal.append(apdativeHist.Interpolate (val1, val2))

        # print [poiLwrLim1, poiUprLim1], [poiLwrLim2, poiUprLim2], exclusionVal

        minExclu = min(exclusionVal)
        maxExclu = max(exclusionVal)

        if(minExclu > 0.50 and maxExclu < 0.999): return job

        # reduce the number of toys for these
        toyInfo[1] = "10"
        twoPois[2] = ":".join(toyInfo)

        job["poiInfo"] = ",".join(twoPois)

        return job

    elif(job["dimension"] == "1DToys"):
        # get the histogram
        apdativeFile = TFile.Open(job["adaptiveSampling"]["inputFile"])
        apdativeHist = apdativeFile.Get(job["adaptiveSampling"]["histName"])

        twoPois = job["poiInfo"].split(",")
        poiName1, poiSteps1, poiLwrLim1, poiUprLim1    = getPOIRange(twoPois[0])
        toyInfo = twoPois[1].split(":")

        exclusionVal = []

        import itertools
        for val1 in [poiLwrLim1, poiUprLim1]:
            xBin = apdativeHist.GetXaxis().FindBin(val1)
            if(xBin > apdativeHist.GetXaxis().GetNbins()):
                exclusionVal.append(1)

            exclusionVal.append(apdativeHist.Interpolate (val1))

        # print [poiLwrLim1, poiUprLim1], [poiLwrLim2, poiUprLim2], exclusionVal

        minExclu = min(exclusionVal)
        maxExclu = max(exclusionVal)

        if(minExclu > 0.40 and maxExclu < 0.99999): return job

        # reduce the number of toys for these
        toyInfo[1] = "10"
        twoPois[1] = ":".join(toyInfo)

        job["poiInfo"] = ",".join(twoPois)

        return job

    else: return job

def getJobComplexity(currCommand):
    if("2DToys" in currCommand):
        commandSplit = currCommand.split(" ")

        index = -1
        for i in range(0, len(commandSplit)):
            if("poiScan" in commandSplit[i]): index = i + 1

        twoPois = commandSplit[index].split(",")
        poiName1, poiSteps1, poiLwrLim1, poiUprLim1    = getPOIRange(twoPois[0])
        poiName2, poiSteps2, poiLwrLim2, poiUprLim2    = getPOIRange(twoPois[1])
        toyInfo = twoPois[2].split(":")

        return float(poiSteps1) * float(poiSteps2) * float(toyInfo[1])
    elif("1DToys" in currCommand):
        commandSplit = currCommand.split(" ")

        index = -1
        for i in range(0, len(commandSplit)):
            if("poiScan" in commandSplit[i]): index = i + 1

        twoPois = commandSplit[index].split(",")
        poiName1, poiSteps1, poiLwrLim1, poiUprLim1    = getPOIRange(twoPois[0])
        toyInfo = twoPois[1].split(":")

        return float(poiSteps1) * float(toyInfo[1])

    else: return 1

def runLocal(jobsToRun):
    # actually start the combine process
    processes = set()
    procList = []
    max_processes = args.maxProcs


    i = 0;
    for command in jobsToRun:
        subFolder = command.split("/")[-2]
        subProcFileName = command.split("/")[-1].replace(".sh",".txt")
        os.system("mkdir -vp tmpDir/%s" % subFolder)
        
        # print command
        i = i + 1
        if(i%5 == 0):
            precentDone = float(i)/len(jobsToRun) * 100
            print('Running command i: ',i, ' percentage done: ' , precentDone)

        # get the arguments
        shellCommand = "source %s " %command
        arguments = shlex.split(shellCommand)
        tmpLogFile = "tmpDir/" + subFolder +'/tmpLogFile_running_'+ str(i) + '_' + subProcFileName
        # print tmpLogFile

        w = open(tmpLogFile,'w')

        # start the process
        p = subprocess.Popen(shellCommand, stdout=w, stderr=w, shell=True)


        # store for backup and for the queue
        processes.add(p)

        # check if the current number of processes exceed the maximum number
        if len(processes) >= max_processes:
            #print 'holding since max process exceeded limit'            
            while True:
                processes.difference_update([p for p in processes if p.poll() is not None])
                if len(processes) < max_processes:
                    break
                # sleep for a bit to save polling time
                time.sleep(10)


    print('Polling for the last few jobs')
    # poll till all the processes are done
    while True:
        processes.difference_update([p for p in processes if p.poll() is not None])
        if len(processes) == 0:
            break
        pass
        # sleep for a bit to save polling time
        time.sleep(5)
    pass
    
    print('Waiting for output files to get written to disk')
    # to wait for the last few files to be written
    time.sleep(5)
    
def runGrid(jobsToRun):

    # write the script that will run the code on the grid
    tarFileName = "gridSubmit"+datetime.datetime.now().strftime('%Y%b%d_%I%M')+".tgz.gz"
    # lets make the tar ball first
    inTarFile = tarfile.open(tarFileName, "w:gz")

    EXCLUDE_FILES = ['.git', '.pyc']

    # for excluding fucntion
    def filter_function(tarinfo):
        for fileName in EXCLUDE_FILES:
            if(fileName in tarinfo.name):
                return None
        return tarinfo


    inTarFile.add("workspaces", filter=filter_function)
    inTarFile.add("jobList", filter=filter_function)
    inTarFile.add("../source", arcname="source", filter=filter_function)

    inTarFile.close()



    # get the list of folders to submit the jobs
    folderNameMap = OrderedDict()
    for jobToRun in jobsToRun:
        jobInfo = jobToRun.split("/")
        folderName = "_".join(jobInfo[1:-1])
        folderNameMap[folderName] = "/".join(jobInfo[:-1])
    print(folderNameMap)

    jobNum = 0
    # submit job on this
    for folderKey in folderNameMap:
        prunCommand = composePrunCommand(folderKey, folderNameMap[folderKey], tarFileName, jobNum)
        jobNum += 1 
        # Print
        print(prunCommand)

        # submit
        status,output = commands.getstatusoutput(prunCommand)
        print(output)

    return

def composePrunCommand(folderKey, folderPath, tarFileName, jobNum):
    # extract the info
    
    command = 'prun --exec "source source/scripts/gridRun.sh %s' %folderPath
    command += ' %RNDM:0"'

    # Outputs
    command += ' --outputs JobSummary.tgz.gz' 

    # setup
    #command += ' --rootVer=6.10.06 --cmtConfig=x86_64-slc6-gcc62-opt'
    command += ' --rootVer=6.08.06-HiggsComb --cmtConfig=x86_64-slc6-gcc49-opt'

    
    command += ' --tmpDir /tmp'
    command += ' --inTarBall %s' %tarFileName

    # input dataset
    command += ' --nJobs %s' %(str(getNjobs(folderPath)))
    command += ' --expertOnly_skipScout'
    command += ' --mergeOutput'

    # output DS name
    # For higgs production role
    if('sabidi' in os.environ["USER"]):
        command += ' --outDS group.phys-higgs.sabidi.'
    else:
        command += ' --outDS user.%s.' %os.environ["USER"]

    command += '%s.' %(folderKey.replace("jobList/","")[0:55])
    command += '%i.' %(jobNum)

    command += args.uniqueID 

    # for higgs production role
    if('sabidi' in os.environ["USER"]):    
        command += ' --official --voms=atlas:/atlas/phys-higgs/Role=production'

    return command

def getNjobs(dirPath):
    count = 0
    for fileName in os.listdir(dirPath):
        if ('.sh' in fileName and 'job' in fileName):
            count += 1
            pass
        pass
    return count

def getDataSetName(dataSet):
    if(dataSet == True):
        return "data"
    else:
        return "asimov"
       
def cleanFolders(job):
    if(args.dryRun):
        return

    jobListBaseDir, condorBaseDir, subJobOutDir, subJobOutDirBaseDir = getJobDir(job)

    for folderName in [jobListBaseDir, condorBaseDir, args.scanDir+ "/" + subJobOutDir, args.scanDir+ "/"  + subJobOutDirBaseDir]:
        if folderName in cleanFolders.doneList: continue
        if(args.retry and args.scanDir in folderName): continue

        shutil.rmtree(folderName, ignore_errors=True)
        os.system("mkdir -vp %s" % folderName)
        cleanFolders.doneList.append(folderName)

cleanFolders.doneList = []

def getJobDir(job):
    # print job
    # output folder
    subJobOutDirBaseDir =   job["outputDir"] + "/" + job["scanType"] + "/" + getDataSetName(job["doData"]) 
    os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDirBaseDir))

    subJobOutDir =  subJobOutDirBaseDir+ "/sJob/" 
    # create the folder
    os.system("mkdir -vp %s/%s" % (args.scanDir,subJobOutDir))

    # construct the job list ID
    jobListBaseDir = "jobList/%s" %(job["outputDir"] + "/" + job["scanType"] + "/" + getDataSetName(job["doData"]) )
    os.system("mkdir -vp %s" % jobListBaseDir)

    condorBaseDir = "condor/%s" %(job["outputDir"] + "/" + job["scanType"] + "/" + getDataSetName(job["doData"]))
    os.system("mkdir -vp %s" % condorBaseDir)



    return jobListBaseDir, condorBaseDir, subJobOutDir, subJobOutDirBaseDir

def getSlurmExec(cJob):

    # subProcFileName = cJob.split("/")[-1].replace(".sh","")
    outFileName = cJob.replace("jobList/", "")
    outFileName = outFileName.replace("/","_")
    outFileName = outFileName.replace(".sh","")
    executableName = "slurm/" + outFileName 

    fileObj = open(executableName + ".sh", 'w')
    OutFileName = "stdout_" + outFileName + ".txt"

    CWD = os.getcwd()

    text = """#!/bin/bash    
#SBATCH 
#SBATCH --time=4:00:00
#SBATCH --mem=4000M
"""
    text += """
#SBATCH --job-name %s """ %(outFileName)

    if(os.environ["USER"] == "ciungubi"):
        text += """
#SBATCH --account=def-sinervo""" 
    else:
        text += """
#SBATCH --account=def-psavard""" 
    
    text += """
#SBATCH --output ./slurm/outputs/%s""" %outFileName + ".txt"


    text += """
cd ~/
cd %s
module load nixpkgs/16.09 gcc/5.4.0 root/6.14.04 boost cmake
source %s
""" %(CWD,cJob)
    # text += cmdToRun
    text += """
pwd""" 
    text += """
ls""" 
    fileObj.write(text)


    os.system("chmod u+x " + executableName + ".sh" )
    execName  = executableName + ".sh"
    return execName

def getSlurmSingularityExec(cJob):

    # subProcFileName = cJob.split("/")[-1].replace(".sh","")
    outFileName = cJob.replace("jobList/", "")
    outFileName = outFileName.replace("/","_")
    outFileName = outFileName.replace(".sh","")
    executableName = "slurm/" + outFileName 

    fileObj = open(executableName + ".sh", 'w')
    OutFileName = "stdout_" + outFileName + ".txt"

    CWD = os.getcwd()

    text = """#!/bin/bash    
#SBATCH 
#SBATCH --time=30:00:00
#SBATCH --mem=8000M
"""
    text += """
#SBATCH --job-name %s """ %(outFileName)

    if(os.environ["USER"] == "ciungubi"):
        text += """
#SBATCH --account=def-sinervo""" 
    else:
        text += """
#SBATCH --account=def-psavard""" 
    
    text += """
#SBATCH --output ./slurm/outputs/%s""" %outFileName + ".txt"


    text += """
cd ~/
cd %s
singularity exec -e -B /home/ciungubi/projects/def-sinervo/ciungubi/wscanner_hcomb/build:/wscanner_hcomb/build -B /home/ciungubi/projects/def-sinervo/ciungubi/wscanner_hcomb/run:/wscanner_hcomb/run  -B /home/ciungubi/projects/def-sinervo/ciungubi/wscanner_hcomb/source:/wscanner_hcomb/source  ~/hcombroot.sif /bin/bash -c \"source %s\"
""" %(CWD,cJob)
    # text += cmdToRun
    text += """
pwd""" 
    text += """
ls""" 
    fileObj.write(text)


    os.system("chmod u+x " + executableName + ".sh" )
    execName  = executableName + ".sh"
    return execName


def getNextIndex(filePath):
    if(filePath not in getNextIndex.indexMap):
        getNextIndex.indexMap[filePath] = 0
    getNextIndex.indexMap[filePath] += 1
    return getNextIndex.indexMap[filePath]

getNextIndex.indexMap = {}

def main():
    jobToRunForGrid = []
    for scan in scansToRun:
        for job in scan:
            if(not os.path.isfile(job["fileName"])): continue

            # makes jobList and prepares for condor submission
            cleanFolders(job)

    for scan in scansToRun:
        condorJobMap    = []
        jobsToRun       = []
        globalCounter = 0
        for job in scan:
            if(not os.path.isfile(job["fileName"])): 
                print("Workspace not found in the folder provided..", job["fileName"])
                continue
            # makes jobList and prepares for condor submission
            makeMaps(job, condorJobMap, jobsToRun, globalCounter)  
            globalCounter +=1
        if(args.runLocal):
            if(args.dryRun):
              continue
            runLocal(jobsToRun)

        elif(args.submitCondor):
            condorSubDir = job["outputDir"] + "/" + job["scanType"] + "/" + getDataSetName(job["doData"])
            condorBaseDir = "condor/" + condorSubDir

            # The bigger submit 
            exeName = makeCondorExecutable(condorBaseDir)

            ## lets make the submission script
            subScriptName = condorBaseDir + "/condorSteer.sub"
            fileObj = open(subScriptName, 'w')


            # overall information
            fileObj.write("executable = %s\n" %exeName)
            fileObj.write("+JobFlavour = \"%s\"\n" %args.queue)
            fileObj.write("getenv = True\n\n")

            # each jobs
            for condorJob in condorJobMap:
                fileObj.write("arguments = %s\n"     %condorJob['inputArgument'])
                fileObj.write("output    = %s.out\n" %condorJob['outFile'])
                fileObj.write("error     = %s.err\n" %condorJob['outFile'])
                fileObj.write("log       = %s.log\n" %condorJob['outFile'])
                fileObj.write("queue\n\n")

            fileObj.close()
            
            if(args.dryRun):
              continue

            command = "condor_submit -batch-name " + condorBaseDir + " " + subScriptName
            print(command)
            os.system(command)

        elif(args.submitSlurm):
            ## submit on slurm  
            os.system("mkdir -vp slurm/outputs")
            slurmDir = "slurm/"
            for cJob in jobsToRun:
                exeName = getSlurmExec(cJob)

                if(args.dryRun):
                  continue
                else:
                    command = "sbatch " + exeName
                    print(command)
                    os.system(command)

        elif(args.isCedarSingularity):
            ## submit on slurm  
            os.system("mkdir -vp slurm/outputs")
            slurmDir = "slurm/"
            for cJob in jobsToRun:
                exeName = getSlurmSingularityExec(cJob)

                if(args.dryRun):
                    command = "sbatch " + exeName
                    print(command)
                    continue
                else:
                    command = "sbatch " + exeName
                    print(command)
                    os.system(command)

        elif(args.isCedarContainer): 
            for cJob in jobsToRun:
                # print cJob

                batch_script_file = "{}_batch.sh".format(cJob.rstrip(".sh"))
                create_script = "batchScript \"source {}\" -O {}".format(cJob, batch_script_file)
                os.system(create_script)
                os.system("chmod -R 775 " + batch_script_file)
                command = "sbatch --time=0:30:00 --mem=8000M {}".format(batch_script_file)

                if(args.dryRun):
                  continue
                else:
                    print(command)
                    os.system(command)

        elif(args.isLxplusSingularity):
            condorSubDir = job["outputDir"] + "/" + job["scanType"] + "/" + getDataSetName(job["doData"])
            condorBaseDir = "condor/" + condorSubDir

            # The bigger submit 
            exeName = makeCondorExecutableSingularity(condorBaseDir)

            ## lets make the submission script
            subScriptName = condorBaseDir + "/condorSteer.sub"
            fileObj = open(subScriptName, 'w')


            # overall information
            fileObj.write("executable = %s\n" %exeName)
            fileObj.write("+JobFlavour = \"%s\"\n" %args.queue)
            fileObj.write("getenv = True\n\n")

            # each jobs
            for condorJob in condorJobMap:
                fileObj.write("arguments = %s\n"     %condorJob['inputArgument'])
                fileObj.write("output    = %s.out\n" %condorJob['outFile'])
                fileObj.write("error     = %s.err\n" %condorJob['outFile'])
                fileObj.write("log       = %s.log\n" %condorJob['outFile'])
                fileObj.write("queue\n\n")

            fileObj.close()
            
            if(args.dryRun):
              continue

            command = "condor_submit -batch-name " + condorBaseDir + " " + subScriptName
            print(command)
            os.system(command)
            
        elif(args.runGrid):
            jobToRunForGrid += jobsToRun
        pass


    # if submitting to the grid, actually submit the job at the very end
    if(args.runGrid):
        runGrid(jobToRunForGrid)
            

def RepresentsNum(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False

# run the main function
if __name__ == "__main__":
    main()








