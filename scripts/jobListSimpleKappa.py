from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"cos2A:20:0.:1.",
"K_V:20:0.:1.",
"K_Fs:20:-5.:5.",
"K_Fp:20:-5.:5."
]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]#, "allSys", "modTheory"]
inputDict["dimension"]      = "1D"
# inputDict["doData"]         = [False, True]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
inputDict["nSplitJob"]      = 2
buildList(inputDict, scan1D)

#=============================================================================
#                                   2D Scans                                 =                          
#=============================================================================
scan2D = []
#1
inputDict["poiList"] = ["cos2A:100:0.:1.,K_V:100:0.:1."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#2
inputDict["poiList"] = ["cos2A:100:0.:1.,K_Fs:100:-5.:5."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#3
inputDict["poiList"] = ["cos2A:100:0.:1.,K_Fp:100:-5.:5."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#4
inputDict["poiList"] = ["K_V:100:0.:1.,K_Fs:100:-5.:5."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#5
inputDict["poiList"] = ["K_V:100:0.:1.,K_Fp:100:-5.:5."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#6
inputDict["poiList"] = ["K_Fs:100:-5.:5.,K_Fp:100:-5.:5."]
inputDict["config"]         = "../source/data/H4lSimpleKappa/H4lSimpleKappaConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
#inputDict["doData"]         = [True,False]
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd.root"]
inputDict["outputDir"]      = ["WS_combined_noSys_STXS_S_EFT_r_All_NN_v20_model_simplifiedKappa_CPevenCPodd"]
#inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)




# #=============================================================================
# #                                   Ranking                                  =                          
# #=============================================================================
# ranking = []
# inputDict = {}
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/muConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False]
# #inputDict["fileName"]       = ["workspaces/WS_combined_XS_STXS_S_S0_r_v19_NN_withSys_model.root", "workspaces/WS_combined_XS_STXS_S_S0_r_LHCP2018_withSys_model.root"] + ["workspaces/WS_combined_XS_STXS_S_RS1_r_LHCP2018_withSys_model.root"] + ["workspaces/WS_combined_XS_STXS_S_VBF_pTH_r_v19_NN_withSys_model.root"]
# #inputDict["outputDir"]      = ["WS_combined_XS_STXS_S_S0_r_v19_NN_withSys_model_ranking", "WS_combined_XS_STXS_S_S0_r_LHCP2018_withSys_model_ranking"] + ["WS_combined_XS_STXS_S_RS1_r_LHCP2018_withSys_model_ranking"] + ["WS_combined_XS_STXS_S_VBF_pTH_r_v19_NN_withSys_model_ranking"]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_S_S0_r_v19_NN_withSys_model.root", "workspaces/WS_combined_mu_STXS_S_S0_r_LHCP2018_withSys_model.root"] + ["workspaces/WS_combined_mu_STXS_S_RS1_r_LHCP2018_withSys_model.root"] + ["workspaces/WS_combined_mu_STXS_S_VBF_pTH_r_v19_NN_withSys_model.root"]
# inputDict["outputDir"]      = ["WS_combined_mu_STXS_S_S0_r_v19_NN_withSys_model_ranking", "WS_combined_mu_STXS_S_S0_r_LHCP2018_withSys_model_ranking"] + ["WS_combined_mu_STXS_S_RS1_r_LHCP2018_withSys_model_ranking"] + ["WS_combined_mu_STXS_S_VBF_pTH_r_v19_NN_withSys_model_ranking"]

# buildList(inputDict, ranking)


# #=============================================================================
# #                                   Compatibility                            =                          
# #=============================================================================
# compatibility = []
# inputDict = {}
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/H4lCoup/muConfig.txt"
# inputDict["scanType"]       = ["singlePOI"]
# inputDict["dimension"]      = "compatibility"
# inputDict["doData"]         = [True,False]
# inputDict["fileName"]       = ["workspaces/STXS_mu_v18_S0.root"]
# inputDict["outputDir"]      = ["STXS_mu_v18_S0_compatibility"]
# buildList(inputDict, compatibility)


# #=============================================================================
# #                                   correlationHist                          =                          
# #=============================================================================
# correlationHist = []
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/H4lCoup/muConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "correlationHist"
# inputDict["saveCorrHist"]   = True   
# inputDict["fileName"]       = ["workspaces/STXS_mu_v18_S0.root"]
# inputDict["outputDir"]      = ["STXS_mu_v18_S0_corrHist"]
# buildList(inputDict, correlationHist)


# #=============================================================================
# #                                   Unconditional ws                         =                          
# #=============================================================================
# # this will make an unconditional workspace
# unCondWS = []
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/H4lCoup/muConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "saveUncondws"
# inputDict["saveUncondWorkspace"]    = True   
# inputDict["fileName"]       = ["workspaces/STXS_mu_v18_S0.root"]
# inputDict["outputDir"]      = ["STXS_mu_v18_S0_unCondWS"]
# buildList(inputDict, unCondWS)


