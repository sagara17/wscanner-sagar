from buildList import *

#=============================================================================
#                                   1D Scans                                 =
#=============================================================================
scan1D = []
inputDict = {}

# #CP-even

inputDict["poiList"] = [
"cuH:40:-40:60"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_DataBSMasimov_CP_even2.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_DataBSMasimov_2"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)


inputDict["poiList"] = [
"cuH:40:-40:60"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)



inputDict["poiList"] = [
"cHG:30:-0.01:0.01"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHGConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_DataBSMasimov_CP_even2.root"]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_DataBSMasimov_2"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHG:30:-0.01:0.01"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHGConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)





inputDict["poiList"] = [
"cHW:40:-4:4"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_DataBSMasimov_CP_even2.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_DataBSMasimov_2"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)


inputDict["poiList"] = [
"cHW:40:-4:4"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHW:40:-4:4"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)


inputDict["poiList"] = [
"cHB:40:-1:1"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_DataBSMasimov_CP_even2.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_DataBSMasimov_2"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHB:40:-1:1"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)



inputDict["poiList"] = [
"cHWB:40:-1.5:1.5"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_DataBSMasimov_CP_even2.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_DataBSMasimov_2"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHWB:40:-1.5:1.5"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)


#CP-odd WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root
inputDict["poiList"] = [
"cuHtil:40:-60:60"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cuHtil:40:-60:60"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHGtil:60:-0.05:0.05"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHGtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 20
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHGtil:60:-0.05:0.05"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHGtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 20
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHWtil:40:-4:4"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHWtil:40:-4:4"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHBtil:40:-1:1"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHBtil:40:-1:1"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBtilConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHWtilB:40:-1.5:1.5"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)

inputDict["poiList"] = [
"cHWtilB:40:-1.5:1.5"
]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilBConfig.txt"
inputDict["scanType"]       = ["statOnly","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan1D)
# #=============================================================================
# #                                   2D Scans                                 =
# #=============================================================================
scan2D = []
inputDict = {}

inputDict["poiList"] = ["cHW:60:-5:5,cHB:60:-5:5"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWcHBConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_1"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)


inputDict["poiList"] = ["cHW:60:-7:7,cHWB:60:-7:7"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWcHWBConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_2"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)
#

#
inputDict["poiList"] = ["cHB:60:-20:20,cHWB:60:-20:20"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBcHWBConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_3"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)
#
#
#
inputDict["poiList"] = ["cHW:60:-6:5,cHG:60:-0.05:0.05"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWcHGConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_4"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#
inputDict["poiList"] = ["cHB:60:-20:20,cHG:60:-0.03:0.1"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBcHGConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_5"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)
#
inputDict["poiList"] = ["cHWB:60:-20:20,cHG:60:-0.03:0.09"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWBcHGConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_6"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)


inputDict["poiList"] = ["cuH:60:-40:60,cHG:60:-0.02:0.04"]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHcHGConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_7"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)


#EFT_CPodd
inputDict["poiList"] = ["cHWtil:60:-7:7,cHBtil:60:-7:7"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilcHBtilConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_8"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

inputDict["poiList"] = ["cHWtil:60:-7:7,cHWtilB:60:-7:7"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilcHWtilBConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_9"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#
inputDict["poiList"] = ["cHBtil:60:-18:18,cHWtilB:60:-18:18"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBtilcHWtilBConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_10"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

inputDict["poiList"] = ["cHWtil:60:-8:8,cHGtil:60:-0.3:0.3"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilcHGtilConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_11"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)


inputDict["poiList"] = ["cHBtil:60:-30:30,cHGtil:60:-0.3:0.3"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHBtilcHGtilConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_12"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)

#
inputDict["poiList"] = ["cHWtilB:60:-30:30,cHGtil:60:-0.3:0.3"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWtilBcHGtilConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_13"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)


inputDict["poiList"] = ["cuHtil:60:-60:60,cHGtil:60:-0.2:0.2"]
inputDict["config"]         = "../source/data/EFTInterpretation/cuHtilcHGtilConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True, False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_CPodd.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_14"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, scan2D)



#=============================================================================
#                                   Run Toys                                 =
#=============================================================================


inputDict["poiList"]        = ["cHW:60:-5:5,cHB:60:-5:5,nToys:10000"]
inputDict["config"]         = "../source/data/EFTInterpretation/cHWcHBConfig.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_toyScan"]
inputDict["nSplitJob"]      = 20
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 4
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750}
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D/InputsForAdaptiveSampling/cHW_cHB.root", "histName":"ExclusionCont"}
buildList(inputDict, scan2D)


# inputDict["poiList"] = [
# "cHW:60:-5:5,cHB:60:-5:5"]
# inputDict["config"]         = "../source/data/EFTInterpretation/cHWcHBConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "2D"
# inputDict["doData"]         = [True]
# inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
# inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_2D_Nllnorm_1"]
# inputDict["nSplitJob"]      = 20
# buildList(inputDict, scan2D)




inputDict = {}
runToys = []
inputDict["poiList"]        = ["nToys:40000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHG_cHW.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven_toys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_CPeven_sys"]
inputDict["nSplitJob"]      = 600
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHG_cHB.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cHG_cHB"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHG_cHWB.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cHG_cHWB"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHW_cHB.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cHW_cHB"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHW_cHWB.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cHW_cHWB"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cHB_cHWB.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cHB_cHWB"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)

# inputDict = {}
# runToys = []
inputDict["poiList"]        = ["nToys:6000"]
inputDict["config"]         = "../source/data/EFTInterpretation/H4lEFT_cuH_cHG.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [True]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys.root"]
inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_EFTtoy_sys_cuH_cHG"]
inputDict["nSplitJob"]      = 400
# buildList(inputDict, runToys)
#
#
# #=============================================================================
# #                                   Ranking                                  =
# #=============================================================================
# ranking = []
# inputDict = {}
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/WS_combined_sigma_Mu__STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_sigma_Mu__STXS_S0_v20FixV6_model_ranking"]
# inputDict["nSplitJob"]      = 10
# buildList(inputDict, ranking)
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/XSConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/WS_combined_sigmaXS_STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_sigmaXS_STXS_S0_v20FixV6_model_ranking"]
# inputDict["nSplitJob"]      = 10
# buildList(inputDict, ranking)
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/XSConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_S0_v20FixV6_model_ranking"]
# inputDict["nSplitJob"]      = 10
# buildList(inputDict, ranking)
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_S0_v20FixV6_model_ranking"]
# inputDict["nSplitJob"]      = 10
# # buildList(inputDict, ranking)
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/XSConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [True]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_RS1p1_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_RS1p1_v20FixV6_model_ranking"]
# inputDict["nSplitJob"]      = 10
# buildList(inputDict, ranking)
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_RS1p1_v20FixV4_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_RS1p1_v20FixV4_model_ranking"]
# inputDict["nSplitJob"]      = 10
# # buildList(inputDict, ranking)
#
#
# inputDict["poiList"]        = ["(.*)"]
# inputDict["config"]         = "../source/data/H4lCoup/XSConfig_40fb.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "ranking"
# inputDict["doData"]         = [False, True]
# inputDict["fileName"]       = ["workspaces/STXS_Mu_v17_S0.root"]
# inputDict["outputDir"]      = ["STXS_Mu_v17_S0_ranking"]
# inputDict["nSplitJob"]      = 10
# # buildList(inputDict, ranking)
#
#
# #=============================================================================
# #								    Compatibility							 =
# #=============================================================================
# compatibility = []
# inputDict = {}
# inputDict["poiList"] 		= ["'*'"]
# inputDict["config"] 		= "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"] 		= ["singlePOI"]
# inputDict["dimension"] 		= "compatibility"
# inputDict["doData"] 		= [True]
# inputDict["fileName"] 		= ["workspaces/WS_combined_Mu_STXS_RS1p1_v20FixV4_model.root"]
# inputDict["outputDir"] 		= ["WS_combined_Mu_STXS_RS1p1_v20FixV4_model_compatibility"]
# buildList(inputDict, compatibility)
#
# #=============================================================================
# #								    correlationHist							 =
# #=============================================================================

# correlationHist = []
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/EFTInterpretation/cZZConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_cZZ.root"]
# inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_cZZ_corr"]
# buildList(inputDict, correlationHist)


# correlationHist = []
# inputDict["poiList"] 	    = ["cZZ:60:-4:2"]
# inputDict["config"] 	    = "../source/data/EFTInterpretation/cZZConfig.txt"
# inputDict["scanType"] 	    = ["statOnly"]
# inputDict["dimension"] 	    = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["doData"] 	    = [False]
# inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_cZZ.root"]
# inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3_cZZ_corr"]
# inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)

# inputDict["poiList"] = [
# "cZZ:60:-4:2"
# ]
# inputDict["config"]         = "../source/data/EFTInterpretation/cZZConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "1D"
# inputDict["doData"]         = [True]
# inputDict["fileName"]       = ["workspaces/WS_combined_WS_combined_mu_STXS_RS1p1qqH_v23_V3_model_cZZ.root"]
# inputDict["outputDir"]      = ["WS_combined_mu_STXS_RS1p1qqH_v23_V3"]
# inputDict["nSplitJob"]      = 1


# correlationHist = []
# inputDict["poiList"] 	    = ["'*'"]
# inputDict["config"] 	    = "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"] 	    = ["allSys"]
# inputDict["dimension"] 	    = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["doData"] 	    = [True, False]
# inputDict["fileName"]       = [ "workspaces/WS_combined_sigma_Mu__STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_sigma_Mu__STXS_S0_v20FixV6_model_corr"]
# inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)
#
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/H4lCoup/XSConfig.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["doData"]         = [True, False]
# inputDict["fileName"]       = ["workspaces/WS_combined_sigmaXS_STXS_S0_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_sigmaXS_STXS_S0_v20FixV6_model_corr"]
# inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)
#
# inputDict["poiList"] 	    = ["'*'"]
# inputDict["config"] 	    = "../source/data/H4lCoup/XSConfig.txt"
# inputDict["scanType"] 	    = ["allSys"]
# inputDict["dimension"] 	    = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["doData"] 	    = [True, False]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_S0_v20FixV6_model.root", "workspaces/WS_combined_Mu_STXS_RS1p1_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_S0_v20FixV6_model_corr","WS_combined_Mu_STXS_RS1p1_v20FixV6_model_corr"]
# inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)
#
#
#
# inputDict["poiList"]        = ["'*'"]
# inputDict["config"]         = "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"]       = ["allSys"]
# inputDict["dimension"]      = "correlationHist"
# inputDict["saveCorrHist"]   = True
# inputDict["doData"]         = [True, False]
# inputDict["fileName"]       = ["workspaces/WS_combined_Mu_STXS_S0_v20FixV6_model.root", "workspaces/WS_combined_Mu_STXS_RS1p1_v20FixV6_model.root"]
# inputDict["outputDir"]      = ["WS_combined_Mu_STXS_S0_v20FixV6_model_corr","WS_combined_Mu_STXS_RS1p1_v20FixV6_model_corr"]
# inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)
#
# #=============================================================================
# #								    Unconditional ws						 =
# #=============================================================================
# # this will make an unconditional workspace
# unCondWS = []
# inputDict["poiList"] 		= ["'*'"]
# inputDict["config"] 		= "../source/data/H4lCoup/_Mu_Config.txt"
# inputDict["scanType"] 		= ["allSys"]
# inputDict["doData"]         = [True]
# inputDict["dimension"] 		= "saveUncondws"
# inputDict["saveUncondWorkspace"] 	= True
# inputDict["fileName"] 		= ["workspaces/STXS_Mu_v18_S0.root"]
# inputDict["outputDir"] 		= ["STXS_Mu_v18_S0_unCondWS"]
# buildList(inputDict, unCondWS)
