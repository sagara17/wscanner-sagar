from buildList import *

#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
inputDict["poiList"] = [
"r_ggF:20:0.8:1.2", 
"r_VBF:20:0.8:1.5", 
"r_WH:20:0.8:1.6", 
"r_ZH:20:0.6:1.4", 
"r_ttH:20:0.7:1.5", 
]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_dataPruned_S0.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_data_fullFit"] 
inputDict["nSplitJob"]      = 10
buildList(inputDict, scan1D)

inputDict = {}
inputDict["poiList"] = [
"r_ggF:20:0.8:1.2", 
"r_VBF:20:0.6:1.4", 
"r_WH:20:0.6:1.4", 
"r_ZH:20:0.6:1.4", 
"r_ttH:20:0.6:1.4", 
]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_asimovPruned_S0.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_asimov_fullFit"] 
inputDict["nSplitJob"]      = 10
buildList(inputDict, scan1D)


#=============================================================================
#						    savePulls           							 =							
#=============================================================================
savePulls = []
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_uncond.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "savePulls"
inputDict["doData"]         = [True]
inputDict["saveUncondWorkspace"] = True
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_2_savePulls"] 
#buildList(inputDict, savePulls)

inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combination4XS_uncond.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "savePulls"
inputDict["doData"]         = [True]
inputDict["saveUncondWorkspace"] = True
inputDict["fileName"]       = ["workspaces/WS_Comb_4XS_V3_1.root"]
inputDict["outputDir"]      = ["WS_Comb_4XS_V3_2_savePulls"] 
#buildList(inputDict, savePulls)

inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combinationProdBr_uncond.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "savePulls"
inputDict["doData"]         = [True]
inputDict["saveUncondWorkspace"] = True
inputDict["fileName"]       = ["workspaces/WS_Comb_ProdBr_V3_1.root"]
inputDict["outputDir"]      = ["WS_Comb_ProdBr_V3_2_savePulls"] 
buildList(inputDict, savePulls)


inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5ratio_uncond.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "savePulls"
inputDict["doData"]         = [True]
inputDict["saveUncondWorkspace"] = True
inputDict["fileName"]       = ["workspaces/WS_Comb_ratio5XS_V3_1.root"]
inputDict["outputDir"]      = ["WS_Comb_ratio5XS_V3_2_savePulls"] 
buildList(inputDict, savePulls)

#=============================================================================
#								    MINOS Error 							 =							
#=============================================================================
minosError = []
inputDict = {}
inputDict["poiList"] 		= ["'r_ggF'","'r_VBF'","'r_WH'","'r_ZH'","'r_ttH'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_dataPruned.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["dimension"] 		= "minosError"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_minosError_allSys_data"] 
buildList(inputDict, minosError)

inputDict["poiList"] 		= ["'r_ggF,r_VBF,r_WH,r_ZH,r_ttH'"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_minosError_statOnly_data"] 
inputDict["scanType"] 		= ["statOnly"]
buildList(inputDict, minosError)


inputDict = {}
inputDict["poiList"] 		= ["'r_ggF'","'r_VBF'","'r_WH'","'r_ZH'","'r_ttH'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_asimovPruned.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["dimension"] 		= "minosError"
inputDict["doData"] 		= [False]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_minosError_allSys_asimov"] 
buildList(inputDict, minosError)

inputDict["poiList"] 		= ["'r_ggF,r_VBF,r_WH,r_ZH,r_ttH'"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_minosError_statOnly_asimov"] 
inputDict["scanType"] 		= ["statOnly"]
buildList(inputDict, minosError)



inputDict = {}
inputDict["poiList"] 		= ["'r_gg_H_ZZ'","'vbf_o_ggf'","'wh_o_ggf'","'zh_o_ggf'","'tth_o_ggf'","'r_BR_yy_o_ZZ'","'r_BR_bb_o_ZZ'","'r_BR_WW_o_ZZ'","'r_BR_tautau_o_ZZ'"]
inputDict["config"]         = "../source/data/HComb/combination5ratio_consPrun_Strag0.txt"
inputDict["scanType"] 		= ["allSys"]
inputDict["dimension"] 		= "minosError"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-ratio5XS_V2.root"]
inputDict["outputDir"]      = ["WS-Comb-ratio5XS_V2_minosError"] 
# buildList(inputDict, minosError)

inputDict["poiList"] 		= ["'r_gg_H_ZZ,vbf_o_ggf,wh_o_ggf,zh_o_ggf,tth_o_ggf,r_BR_yy_o_ZZ,r_BR_bb_o_ZZ,r_BR_WW_o_ZZ,r_BR_tautau_o_ZZ'"]
inputDict["scanType"] 		= ["statOnly"]
# buildList(inputDict, minosError)

inputDict = {}
inputDict["poiList"] 		= ["'r_ggF_yy'","'r_VBF_yy'","'r_VH_yy'","'r_ttH_yy'","'r_ggF_ZZ'","'r_VBF_ZZ'","'r_VH_ZZ'","'r_ggF_WW'","'r_VBF_WW'","'r_ggF_tautau'","'r_VBF_tautau'","'r_ttH_tautau'","'r_VBF_bb'","'r_VH_bb'","'r_ttH_bb'","'r_ttH_VV'"]
inputDict["config"]         = "../source/data/HComb/combination5x5XS_consPrun_Strag0.txt"
inputDict["scanType"] 		= ["allSys",]
inputDict["dimension"] 		= "minosError"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-5x5XS_V2.root"]
inputDict["outputDir"]      = ["WS-Comb-5x5XS_V2_minosError"] 
# buildList(inputDict, minosError)

inputDict["poiList"] 		= ["'r_ggF_yy,r_VBF_yy,r_VH_yy,r_ttH_yy,r_ggF_ZZ,r_VBF_ZZ,r_VH_ZZ,r_ggF_WW,r_VBF_WW,r_ggF_tautau,r_VBF_tautau,r_ttH_tautau,r_VBF_bb,r_VH_bb,r_ttH_bb,r_ttH_VV'"]
inputDict["scanType"] 		= ["statOnly"]
# buildList(inputDict, minosError)

#=============================================================================
#								    correlationHist							 =							
#=============================================================================
correlationHist = []
inputDict = {}
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_dataPruned.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"] 	    = "correlationHist"
inputDict["saveCorrHist"]   = True 
inputDict["doData"] 	    = [True]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_corrHesse_data"] 
inputDict["nSplitJob"]      = 1
buildList(inputDict, correlationHist)

inputDict = {}
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/v3/combination5XS_asimovPruned.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"] 	    = "correlationHist"
inputDict["saveCorrHist"]   = True 
inputDict["doData"] 	    = [False]
inputDict["fileName"]       = ["workspaces/WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov.root"]
inputDict["outputDir"]      = ["WS_Comb_5XS_V3_1_prune_cnd1_muFloat_wAsimov_corrHesse_asimov"] 
inputDict["nSplitJob"]      = 1
buildList(inputDict, correlationHist)


inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5ratio_Strag1.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"] 	    = "correlationHist"
inputDict["saveCorrHist"]   = True 
inputDict["doData"] 	    = [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-ratio5XS_V3.root"]
inputDict["outputDir"]      = ["WS-Comb-ratio5XS_V3_corrHesse"] 
inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)

inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5x5XS_Strag1.txt"
inputDict["dimension"] 	    = "correlationHist"
inputDict["saveCorrHist"]   = True 
inputDict["doData"] 	    = [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-5x5XS_V3.root"]
inputDict["outputDir"]      = ["WS-Comb-5x5XS_V3_corrHesse"] 
inputDict["nSplitJob"]      = 1
# buildList(inputDict, correlationHist)


inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5x5XS_Strag1.txt"
inputDict["dimension"] 	    = "correlationHist"
inputDict["saveCorrHist"]   = True 
inputDict["doData"] 	    = [True]
inputDict["fileName"]       = ["workspaces/HWW_5XS.root"]
inputDict["outputDir"]      = ["HWW_5XS_corr"] 
inputDict["nSplitJob"]      = 1
#buildList(inputDict, correlationHist)

#=============================================================================
#								    Compatibility							 =							
#=============================================================================
compatibility = []

######################## data
inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag1.txt"
inputDict["scanType"] 		= ["singlePOI"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_compatibility"] 
# buildList(inputDict, compatibility)

inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag0.txt"
inputDict["scanType"] 		= ["VBFsig"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_VBFsig"] 
buildList(inputDict, compatibility)

inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag0.txt"
inputDict["scanType"] 		= ["WHsig"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_WHsig"] 
# buildList(inputDict, compatibility)

inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag0.txt"
inputDict["scanType"] 		= ["ZHsig"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_ZHsig"] 
# buildList(inputDict, compatibility)

inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag0.txt"
inputDict["scanType"] 		= ["VHsig"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_VHsig"] 
# buildList(inputDict, compatibility)

inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5XS_Strag0.txt"
inputDict["scanType"] 		= ["ttHsig"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/Total_5XS_V2.root"]
inputDict["outputDir"]      = ["Total_5XS_V2_ttHsig"] 
buildList(inputDict, compatibility)

######################## ProdBR
inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5x5XS_Strag1.txt"
inputDict["scanType"] 		= ["singlePOI"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-5x5XS_V2.root"]
inputDict["outputDir"]      = ["WS-Comb-5x5XS_V2_compatibility"] 
# buildList(inputDict, compatibility)

######################## Ratio
inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HComb/combination5ratio_Strag1.txt"
inputDict["scanType"] 		= ["singlePOI"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [True]
inputDict["fileName"]       = ["workspaces/WS-Comb-ratio5XS_V2.root"]
inputDict["outputDir"]      = ["WS-Comb-ratio5XS_V2_compatibility"] 
# buildList(inputDict, compatibility)
