from buildList import *


#=============================================================================
#                                   1D Scans                                 =                          
#=============================================================================
scan1D = []
inputDict = {}
#########
# m4l per channel 
#########
inputDict["poiList"] = [
"sigma_bin1_4mu:100:0:0.3",
"sigma_bin1_4e:100:0:0.3",
"sigma_bin1_2mu2e:100:0:0.3",
"sigma_bin1_2e2mu:100:0:0.3",
"sigma_bin2_4mu:100:0:0.4",
"sigma_bin2_4e:100:0:0.4",
"sigma_bin2_2mu2e:100:0:0.4",
"sigma_bin2_2e2mu:100:0:0.4",
"sigma_bin3_4mu:100:0:0.7",
"sigma_bin3_4e:100:0:0.7",
"sigma_bin3_2mu2e:100:0:0.7",
"sigma_bin3_2e2mu:100:0:0.7",
"sigma_bin4_4mu:100:0:0.7",
"sigma_bin4_4e:100:0:0.7",
"sigma_bin4_2mu2e:100:0:0.7",
"sigma_bin4_2e2mu:100:0:0.7",
"MuqqZZ0:25:0.5:1.5",
"MuqqZZ1:25:0.5:1.5",
"MuqqZZ2:25:0.5:1.5",
"MuqqZZ3:25:0.5:1.5",
]
inputDict["config"]         = "../source/data/H4lDiff/H4lDiffConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_expected_mtx_v20v2_4ch09SM.root"]
inputDict["outputDir"]      = ["ws_m12m34_expected_mtx_v20v2_4ch09SM"]
inputDict["nSplitJob"]          = 5
# buildList(inputDict, scan1D)

inputDict["poiList"] =      ["eElR:25:-0.09:0.09"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly", "allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False, True]
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_data_mtx_sLChan_v23v0_fV_axial.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_data_mtx_sLChan_v23v0_fV_axial"]
inputDict["nSplitJob"]      = 5
buildList(inputDict, scan1D)



inputDict["poiList"] =      ["eL:25:-0.55:0.2,nToys:1000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "1DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_expected_mtx_incl_v22t_flavourUniversal_contact.root"]
inputDict["outputDir"]      = ["ws_m12m34_expected_mtx_incl_v22t_flavourUniversal_contact_1DToys"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 1
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 25} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_m12m34_expected_mtx_incl_v22t_flavourUniversal_EFT/InputsForAdaptiveSampling/eL_kZZ.root", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)

# buildList(inputDict, scan1D)

#=============================================================================
#                                   2D Scans                                 =                          
#=============================================================================
scan2D = []
inputDict = {}

inputDict["poiList"]        = ["eL:50:-0.475:0.475,kZZ:50:-1.5:1.5"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_v23v3_fU_EFT_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_v23v3_fU_EFT_fixed"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan2D)

inputDict["poiList"] 		= ["eL:50:-0.55:0.25,eR:50:-0.25:0.6"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_v23v3_fU_contact_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_v23v3_fU_contact_fixed"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan2D)

inputDict["poiList"]        = ["eMuR:50:-0.09:0.09,eElR:50:-0.09:0.09"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan2D)

inputDict["poiList"]        = ["eMu:50:-0.25:0.25,eEl:50:-0.25:0.25"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2D"
inputDict["doData"]         = [True]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, scan2D)

#=============================================================================
#                                   Run Toys                                 =                          
#=============================================================================
runToys = []
inputDict["poiList"]        = ["nToys:10000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed_toys"]
inputDict["nSplitJob"]      = 20
buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:10000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed_toys"]
inputDict["fileName"]      += ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed.root"]
inputDict["outputDir"]     += ["ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed_toys"]
inputDict["fileName"]      += ["workspaces/ws_m12m34_data_mtx_v23v3_fU_EFT_fixed.root"]
inputDict["outputDir"]     += ["ws_m12m34_data_mtx_v23v3_fU_EFT_fixed_toys"]
inputDict["fileName"]      += ["workspaces/ws_m12m34_data_mtx_v23v3_fU_contact_fixed.root"]
inputDict["outputDir"]     += ["ws_m12m34_data_mtx_v23v3_fU_contact_fixed_toys"]
inputDict["nSplitJob"]      = 20
# buildList(inputDict, runToys)



inputDict["poiList"]        = ["nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_mtx_v23v0_fU_contact.root", "workspaces/ws_m12m34_wsys_expected_mtx_v23v0_m12m34Flat_fU_contact.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_mtx_v23v0_fU_contact_toys", "ws_m12m34_wsys_expected_mtx_v23v0_m12m34Flat_fU_contact_toys"]
inputDict["nSplitJob"]      = 50
# buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_axial.root", "workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_axial.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_axial_toys", "ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_axial_toys"]
inputDict["nSplitJob"]      = 50
# buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_vector.root", "workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_vector.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_vector_toys", "ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_vector_toys"]
inputDict["nSplitJob"]      = 50
# buildList(inputDict, runToys)



inputDict["poiList"]        = ["nToys:500"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig_rand.txt"
# inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed_rand"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig_rand.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_mtx_v23v0_fU_contact.root", "workspaces/ws_m12m34_wsys_expected_mtx_v23v0_m12m34Flat_fU_contact.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_mtx_v23v0_fU_contact_toys_rand", "ws_m12m34_wsys_expected_mtx_v23v0_m12m34Flat_fU_contact_toys_rand"]
inputDict["nSplitJob"]      = 50
# buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig_rand.txt"
inputDict["scanType"]       = ["allSys", "statOnly"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_axial.root", "workspaces/ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_axial.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_mtx_sLChan_v23v0_fV_axial_toys_rand", "ws_m12m34_wsys_expected_mtx_sLChan_v23v0_m12m34Flat_fV_axial_toys_rand"]
inputDict["nSplitJob"]      = 50
# buildList(inputDict, runToys)


inputDict["poiList"]        = ["nToys:100"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig_rand.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed_SM_toys_rand"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, runToys)

inputDict["poiList"]        = ["nToys:100"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig_randBSM.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "runToys"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = False
inputDict["scanToy"]        = False
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_wsys_data_mtx_v23v3_m12m34Flat_fU_EFT_fixed_BSM_Toys_rand"]
inputDict["nSplitJob"]      = 10
# buildList(inputDict, runToys)


#=============================================================================
#                                   Toy scanning                             =                          
#=============================================================================
inputDict["poiList"]        = ["eL:50:-0.475:0.475,kZZ:50:-1.5:1.5,nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_v23v3_fU_EFT_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_v23v3_fU_EFT_fixed_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_m12m34_prun_expected_mtx_v22t2v1_fU_EFT/InputsForAdaptiveSampling/eL_kZZ.roott", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)

inputDict["poiList"] 		= ["eL:50:-0.55:0.25,eR:50:-0.25:0.6,nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_v23v3_fU_contact_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_v23v3_fU_contact_fixed_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_m12m34_prun_expected_mtx_v22t2v1_fU_contact/InputsForAdaptiveSampling/eL_eR.root", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)

inputDict["poiList"]        = ["eMuR:50:-0.09:0.09,eElR:50:-0.09:0.09,nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_axial_fixed_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_m12m34_prun_expected_mtx_subleadChan_v22t2v1_fV_axial/InputsForAdaptiveSampling/eMuR_eElR.root", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)


inputDict["poiList"]        = ["eMu:50:-0.25:0.25,eEl:50:-0.25:0.25,nToys:5000"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["statOnly"]
inputDict["dimension"]      = "2DToys"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["workspaces/ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed.root"]
inputDict["outputDir"]      = ["ws_m12m34_data_mtx_sLChan_v23v3_fV_vector_fixed_toyScan"]
inputDict["nSplitJob"]      = 5
# just repeat it multiple times to all for more stats
inputDict["nRepeat"]        = 2
# This option splits the job in a into 1 point per actual run Command
# but then it bunches up the commands into nJobs each with ~ equal number of complexity
# This is to allow the memory to be cleared after each point
# a nessecary evil cuz roofit leaks memory
# inputDict["batchStyle"]     = {"doBatchStyle": True, "nJobs": 750} 
# the scanning code adapts the number of toy per point based on what the exclusion is for that job
# inputDict["adaptiveSampling"] = {"doAdaptiveSampling":True, "inputFile":"Plots/ws_m12m34_prun_expected_mtx_subleadChan_v22t2v1_fV_vector/InputsForAdaptiveSampling/eMu_eEl.root", "histName":"ExclusionCont"}
# buildList(inputDict, scan2D)


#=============================================================================
#                                   Correlations                                 =                          
#=============================================================================
correlationHist = []
inputDict["poiList"]        = ["'*'"]
inputDict["config"]         = "../source/data/H4lPOs/H4lPOsConfig.txt"
inputDict["scanType"]       = ["allSys"]
inputDict["dimension"]      = "correlationHist"
inputDict["doData"]         = [False]
inputDict["saveCorrHist"]   = True                           
inputDict["fileName"]       = ["workspaces/ws_m12m34_wsys_expected_cf_v23v0_m12m34Flat_fU_contact.root"]                          
inputDict["outputDir"]      = ["ws_m12m34_wsys_expected_cf_v23v0_m12m34Flat_fU_contact_corr"]                                                                      
buildList(inputDict, correlationHist)

