from buildList import *


#=============================================================================
#                                   1D Scans                                 =
#=============================================================================

#signed DPhijj
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:1.0",
"sigma_bin1:40:0:1.0",
"sigma_bin2:40:0:1.0",
"sigma_bin3:40:0:1.0",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
#inputDict["scanType"]       = ["statOnly_muFloat","allSys"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_SignedDPhijj_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_dphijj"]
#buildList(inputDict, scan1D)

#jet0_pt
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:60:0.1:1.4",
"sigma_bin1:40:0:1.0",
"sigma_bin2:40:0:0.9",
"sigma_bin3:40:0:0.6",
"sigma_bin4:40:0:0.5",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_jet0_pt_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_jet0_pt"]
#buildList(inputDict, scan1D)

#jet1_pt
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:60:0:1.5",
"sigma_bin1:60:0:1.2",
"sigma_bin2:40:0:1.1",
"sigma_bin3:40:0:0.5",
"sigma_bin4:20:0:0.3",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_jet1_pt_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_jet1_pt"]
#buildList(inputDict, scan1D)

#lep0_pt
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:1.2",
"sigma_bin1:40:0:0.8",
"sigma_bin2:40:0:0.8",
"sigma_bin3:40:0:0.6",
"sigma_bin4:40:0:0.8",
"sigma_bin5:40:0:0.8"
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_lep0_pt_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_lep0_pt"]
#buildList(inputDict, scan1D)

#lep1_pt
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:1.2",
"sigma_bin1:40:0:1.0",
"sigma_bin2:40:0:1.0",
"sigma_bin3:40:0:0.6",
"sigma_bin4:40:0:0.6",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_lep1_pt_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_lep1_pt"]
#buildList(inputDict, scan1D)

#costhetastar
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:0.8",
"sigma_bin1:40:0:0.8",
"sigma_bin2:40:0:0.8",
"sigma_bin3:40:0:1.0",
"sigma_bin4:40:0:1.2",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_costhetastar_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_cosTheta"]
#buildList(inputDict, scan1D)

#DPhill
#scan1D = []
#inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:1.0",
"sigma_bin1:40:0:1.0",
"sigma_bin2:40:0:1.0",
"sigma_bin3:40:0:0.6",
"sigma_bin4:40:0:0.9",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_DPhill_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_DPhill"]
#buildList(inputDict, scan1D)

#pt_H
#scan1D = []
#inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0:1.2",
"sigma_bin1:40:0:1.2",
"sigma_bin2:40:0:1.0",
"sigma_bin3:40:0:0.8",
"sigma_bin4:40:0:0.5",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_pt_H_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_pt_H"]
#buildList(inputDict, scan1D)

#Mll
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0.0:1.2",
"sigma_bin1:40:0.0:1.2",
"sigma_bin2:40:0.0:1.2",
"sigma_bin3:40:0.0:1.0",
"sigma_bin4:40:0.0:0.6"
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_Mll_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_Mll"]
#buildList(inputDict, scan1D)

#Mjj
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [ 
"sigma_bin0:40:0.0:1.2",
"sigma_bin1:40:0.0:1.0",
"sigma_bin2:40:0.0:1.2",
"sigma_bin3:40:0.0:0.7",
"sigma_bin4:40:0.0:0.6",
"sigma_bin5:40:0.0:0.5",
"mu_Zjets:40:0.7:1.1",
"mu_Zjets_CRGGF1:40:0.5:1.2",
"mu_wwTop_bin0:40:0.7:1.1",
"mu_wwTop_bin1:40:0.7:1.1",
"mu_wwTop_bin2:40:0.4:1.0",
"mu_ggf:50:0.3:1.7",
"mu_wwTop3:50:0.85:1.05"
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_Mjj_Data_postfit.root"]
inputDict["outputDir"]      = ["NLL_Unblinded_Mjj"]
buildList(inputDict, scan1D)

#DYjj
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0.0:0.9",
"sigma_bin1:40:0.0:0.7",
"sigma_bin2:40:0.0:1.0",
"sigma_bin3:40:0.0:0.8",
"sigma_bin4:40:0.0:0.8",
"sigma_bin5:40:0.0:0.8",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_DYjj_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_DYjj"]
#buildList(inputDict, scan1D)

#Ptll
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:40:0.0:1.2",
"sigma_bin1:40:0.0:1.0",
"sigma_bin2:40:0.0:1.0",
"sigma_bin3:40:0.0:0.9",
"sigma_bin4:40:0.0:0.6",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_Ptll_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_Ptll"]
#buildList(inputDict, scan1D)

#DYll
#scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:60:0.4:1.5",
"sigma_bin1:40:0.0:0.8",
"sigma_bin2:40:0.0:0.6",
"sigma_bin3:40:0.0:0.6",
"sigma_bin4:40:0.0:0.7",
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov16/ws_noGGF2_DYll_newAsimov.root"]
inputDict["outputDir"]      = ["NLLScan_Nov5_DYll"]
#buildList(inputDict, scan1D)

#scan1D = []
#inputDict = {}
#SO
inputDict["poiList"] = [
"mu_vbf:40:0.:2.0"
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["statOnly_muggf_muvbf","allSys_muggf_muvbf"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/sagar_theory_fix/ws_noGGF2_Mjj_muvbf_newAsimov.root"]
inputDict["outputDir"]      = ["scan_inclusive_Mjj"]
#buildList(inputDict, scan1D)

#inc
scan1D = []
inputDict = {}
#SO
inputDict["poiList"] = [
"sigma_bin0:50:-0.1:3.5",
#"mu_Zjets:40:0.5:1.5",
#"mu_Zjets_CRGGF1:40:0.3:1.7",
#"mu_wwTop:40:0.75:1.25",
#"mu_ggf:40:0.2:1.8",
#"mu_wwTop3:50:0.9:1.1"
]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig_inc.txt"
inputDict["scanType"]       = ["statOnly_muggf","allSys_muggf_fid"]
inputDict["dimension"]      = "1D"
inputDict["doData"]         = [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Feb15/ws_noGGF2_inc.root"]
inputDict["outputDir"]      = ["nllScan_asimov_inc_editSmooth"]
buildList(inputDict, scan1D)

#=============================================================================
#                                   Ranking                                  =
#=============================================================================

ranking = []
inputDict = {}
#inputDict["extraVarSave"] = ["mu_Zjets","mu_wwTop_bin0","mu_wwTop_bin1","mu_wwTop_bin2","mu_ggf","mu_wwTop3","mu_wwTop","mu_Zjets_CRGGF1","mu_vbf"]
inputDict["extraVarSave"] = ["isigma_bin0","isigma_bin1","isigma_bin2","isigma_bin3","isigma_bin4","mu_Zjets","mu_wwTop_bin0","mu_wwTop_bin1","mu_wwTop_bin2","mu_ggf","mu_wwTop3","mu_Zjets_CRGGF1"]
inputDict["poiList"] = ["*"]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig.txt"
inputDict["scanType"]       = ["allSys_muggf"]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/gaetano_mergeBin/merged_ws_noGGF2_Mjj_Data.root"]
inputDict["outputDir"]      = ["mergeBinWS_Mjj"]
inputDict["dimension"]      = "ranking"
inputDict["doData"]         = [True]
buildList(inputDict, ranking)


#inputDict["poiList"] = ["mu_diff"]
#inputDict["poiList"] = ["mu_inc"]
#inputDict["config"]         = "../source/data/HWW-EFT/SMConfig_diff_muvbf.txt"
#inputDict["config"]         = "../source/data/HWW-EFT/SMConfig_inc.txt"
#inputDict["config"]         = "../source/data/HWW-EFT/SMConfig_fix_all_mu.txt"
#inputDict["scanType"]       = ["allSys_fix_all_mu"]
#inputDict["scanType"]       = ["allSys_muggf_fid"]
#inputDict["scanType"]       = ["allSys_muggf_muvbf"]
#inputDict["scanType"]       = ["allSys_muggf_fid"]
#inputDict["scanType"]       = ["allSys_muggf_muvbf"]

#=============================================================================
#								    Compatibility							 =		
#=============================================================================
compatibility = []

######################## data
inputDict = {}
inputDict["poiList"] 		= ["'*'"]
inputDict["config"]         = "../source/data/HWW-EFT/SMConfig_inc.txt"
inputDict["scanType"]       = ["singlePOI"]
inputDict["dimension"] 		= "compatibility"
inputDict["doData"] 		= [False]
inputDict["fileName"]       = ["/atlasgpfs01/usatlas/data/jennyz/Rivet/vbf-hww-eftfit/analysisbase/Nov5_SRVBFnosmooth/ws_noGGF2_inc_Data.root"]
inputDict["outputDir"]      = ["Compati_asimov_inc"]
buildList(inputDict, compatibility)
