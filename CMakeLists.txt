
cmake_minimum_required(VERSION 3.9)
#cmake_policy(SET CMP0022 OLD)
project(WScanner)

set(CMAKE_BUILD_TYPE Debug)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS RIO EG Minuit RooFit RooFitCore RooStats HistFactory Core RIO Net Hist Graf Graf3d Gpad Tree Rint Postscript Matrix Physics MathCore Thread MultiProc TMVA)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

# compile options
add_compile_options(-std=c++14 -fPIC -Wall -Wextra -Wno-unused-parameter -O2)


include_directories(${CMAKE_CURRENT_SOURCE_DIR}/WScanner)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/Root/atlasstyle)

#---Create a shared library with generated dictionary
file(GLOB sources "Root/*.cxx")

add_library(projectLib SHARED ${sources})
target_link_libraries(projectLib ${ROOT_LIBRARIES})

#---Create  a main program using the library
add_executable(runScan utils/runScan.cxx)
target_link_libraries(runScan projectLib)

add_executable(plot1D utils/plot1D.cxx) 
target_link_libraries(plot1D projectLib)

add_executable(plot2D utils/plot2D.cxx) 
target_link_libraries(plot2D projectLib)

add_executable(plot2DToys utils/plot2DToys.cxx) 
target_link_libraries(plot2DToys projectLib)

add_executable(plotDatasetScan utils/plotDatasetScan.cxx) 
target_link_libraries(plotDatasetScan projectLib)

add_executable(plotRanking utils/plotRanking.cxx) 
target_link_libraries(plotRanking projectLib)

add_executable(plotToys utils/plotToys.cxx) 
target_link_libraries(plotToys projectLib)

#lsetup "gcc gcc620_x86_64_slc6"
#cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILE=gcc ../source/

add_executable(plotCorrHist utils/plotCorrHist.cxx) 
target_link_libraries(plotCorrHist projectLib)


add_executable(plotCorrHistDiff utils/plotCorrHistDiff.cxx) 
target_link_libraries(plotCorrHistDiff projectLib)

add_executable(createTable utils/createTable.cxx) 
target_link_libraries(createTable projectLib)

add_executable(plotCompare utils/plotCompare.cxx) 
target_link_libraries(plotCompare projectLib)

add_executable(plotCoupling utils/plotCoupling.cxx) 
target_link_libraries(plotCoupling projectLib)

add_executable(makeTGraph utils/makeTGraph.cxx) 
target_link_libraries(makeTGraph projectLib)

add_executable(plotPulls utils/plotPulls.cxx) 
target_link_libraries(plotPulls projectLib)